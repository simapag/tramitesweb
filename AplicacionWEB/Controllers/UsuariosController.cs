﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class UsuariosController : Controller
    {
        // GET: Usuarios

          /// <summary>
          ///  Respositorio  para acceder a la tabla  Usuario
          /// </summary>
        private IUsuarios_Repositorio ObjUsuariosRepositorio;

        /// <summary>
        ///  constructor del controlador
        /// </summary>
        /// <param name="p_ObjUsuariosRepositorio">Repositorio de Usuario</param>
        public UsuariosController(IUsuarios_Repositorio p_ObjUsuariosRepositorio) {
            this.ObjUsuariosRepositorio = p_ObjUsuariosRepositorio;
        }


        /// <summary>
        ///  Muestra la vista de Usuarios
        /// </summary>
        /// <returns>
        /// Redirige la a vista correspondiente
        /// </returns>
        public ViewResult Listar_Usuarios()
        {
            return View();

        }

        /// <summary>
        /// Pone el estatus inactivo a un elemento de la tabla usuario
        /// </summary>
        /// <param name="p_Usuario">Entidad Usuario</param>
        /// <returns>
        ///  nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Inactivar_Usuario(Usuarios p_Usuario)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {

                Usuarios usuarioBuscar = (from u in ObjUsuariosRepositorio.Usuarios
                                          where p_Usuario.Usuarios_ID == u.Usuarios_ID
                                          select u).FirstOrDefault();

                usuarioBuscar.Usuario_Modifico = Cls_Sesiones.NOMBRE_COMPLETO;
                usuarioBuscar.Fecha_Modifico = DateTime.Now;
                usuarioBuscar.Estatus = "INACTIVO";

                ObjUsuariosRepositorio.Guardar_Usuarios(usuarioBuscar);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;


        }






        /// <summary>
        /// Consulta para que devuelve una lista de usuarios
        /// </summary>
        /// <param name="offset">ese un parámetro del tabla bootstrap en la página donde inicia</param>
        /// <param name="limit">ese un parámetro del tabla bootstrap total de renglones a tomar</param>
        /// <param name="search">ese un parámetro del tabla bootstrap la cajita de búsqueda</param>
        /// <returns>
        ///  regresa la lista de usuario en json
        /// </returns>
        public JsonResult Obtener_Usuarios(int offset, int limit, string search = null) {

            var usuarioRepositorio = ObjUsuariosRepositorio.Usuarios;
            IEnumerable<Usuarios> usuariosFiltro;

            if (!string.IsNullOrEmpty(search))
            {
                usuariosFiltro = ObjUsuariosRepositorio.Usuarios.Where(x => x.Nombre.Contains(search)
                || x.Apellido_Materno.Contains(search) || x.Apellido_Paterno.Contains(search)
                || x.Correo_Electronico.Contains(search));
            }
            else {
                usuariosFiltro = usuarioRepositorio;
            }

            var empleadosDesplegar = (usuariosFiltro.Skip(offset)
                                                     .Take(limit)
                                                     .OrderBy(u=> u.Nombre).ThenBy(u=> u.Apellido_Paterno)).ToList();

            var resultado = from u in empleadosDesplegar
                            select new { id = u.Usuarios_ID, nombre = u.Nombre, apellido_paterno = u.Apellido_Paterno, apellido_materno = u.Apellido_Materno, correo= u.Correo_Electronico, direccion = u.Direccion, telefono_celular = u.Telefono_Celular, telefono_oficina  = u.Telefono_Oficina, estatus = u.Estatus};

            return Json(new
            {
                total = usuariosFiltro.Count(),
                rows = resultado

            }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        ///  Edito un usuario
        /// </summary>
        /// <param name="p_Usuario">Entidad usuario</param>
        /// <returns>
        ///  nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Editar_Usuario(Usuarios p_Usuario)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {

                Usuarios usuarioBuscar = (from u in ObjUsuariosRepositorio.Usuarios
                                      where u.Correo_Electronico == p_Usuario.Correo_Electronico && p_Usuario.Usuarios_ID != u.Usuarios_ID
                                      select u).FirstOrDefault();

                if (usuarioBuscar != null)
                {
                    objMensajeServidor.Mensaje = "El correo electrónico esta repetido";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                p_Usuario.Usuario_Modifico = Cls_Sesiones.NOMBRE_COMPLETO;
                p_Usuario.Fecha_Modifico = DateTime.Now;

                ObjUsuariosRepositorio.Guardar_Usuarios(p_Usuario);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;


        }


        /// <summary>
        /// Elimina un usuario de la base de datos
        /// </summary>
        /// <param name="p_Usuario">Entidad usuario</param>
        /// <returns>
        ///  nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Eliminar_Usuario(Usuarios p_Usuario) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;


            try
            {

                objMensajeServidor.Mensaje = ObjUsuariosRepositorio.Eliminar_Usuario(p_Usuario.Usuarios_ID);
            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }

        /// <summary>
        ///  Agrega un usuario a la base datos
        /// </summary>
        /// <param name="p_Usuario">Entidad Usuario</param>
        /// <returns>
        ///  nos indica si fue exitoso o no el procesos
        /// </returns>

        [HttpPost]
        public string Nuevo_Usuario(Usuarios p_Usuario)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {

                Usuarios usuarioBuscar = (from u in ObjUsuariosRepositorio.Usuarios where u.Correo_Electronico == p_Usuario.Correo_Electronico select u).FirstOrDefault();

                if (usuarioBuscar != null)
                {
                    objMensajeServidor.Mensaje = "El correo electrónico esta repetido";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                p_Usuario.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                p_Usuario.Fecha_Creo = DateTime.Now;

                ObjUsuariosRepositorio.Guardar_Usuarios(p_Usuario);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }




    }
}