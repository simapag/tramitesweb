﻿using AplicacionWEB.Models;
using AplicacionWEB.Models.VerificarSession;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class ReporteSolicitudesController : Controller
    {
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        ITramites_Repo Tramites_Repo;

        public ReporteSolicitudesController(ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
            ITramites_Repo tramites_Repo)
        {
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            Tramites_Repo = tramites_Repo;
        }


        public ActionResult Listado_Solicitudes(string sortOrder, string currentFechaInicio, string FechaInicio, string currentFechaFin, string FechaFin, int? currentTramite_ID, int? tramite_ID, int? page)
        {
            IQueryable<TramitesSolicitudView> Lista_Solicitudes;
            int pageSize = 10;
            int pageNumber = 0;

            try
            {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.FechaSortParam = String.IsNullOrEmpty(sortOrder) ? "fecha_asc" : "";
                ViewBag.NombreTramiteSortParam = sortOrder == "Nombre_Tramite" ? "nombre_tramite_desc" : "Nombre_Tramite";


                if (FechaInicio != null)
                {
                    page = 1;
                }
                else
                {
                    FechaInicio = currentFechaInicio;
                }


                if (FechaFin != null)
                {
                    page = 1;
                }
                else
                {
                    FechaFin = currentFechaFin;
                }

                if (tramite_ID.HasValue)
                {
                    page = 1;
                }
                else
                {

                    tramite_ID = currentTramite_ID;
                }

                ViewBag.CurrentTramite_ID = tramite_ID;

                ViewBag.ComboTramites = Tramites_Repo.Obtener_Tramites_Combo_Especial();

                ViewBag.CurrentFechaInicio = FechaInicio;
                ViewBag.CurrentFechaFin = FechaFin;

                pageNumber = (page ?? 1);

                Lista_Solicitudes = TramitesSolicitudes_Repo.Obtener_Reporte_Solicitudes(sortOrder, FechaInicio, FechaFin, tramite_ID);
            }
            catch(Exception ex)
            {
                Lista_Solicitudes = Enumerable.Empty<TramitesSolicitudView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }

            return View(Lista_Solicitudes.ToPagedList(pageNumber, pageSize));
        }

    }
}