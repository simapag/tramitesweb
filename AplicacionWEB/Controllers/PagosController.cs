﻿using AplicacionWEB.Models.VerificarSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using Newtonsoft.Json;
using AplicacionWEB.Models.Enums;
using System.Data.SqlClient;
using System.Configuration;
using AplicacionWEB.Infraestructura.Servicios;
using System.Data;

namespace AplicacionWEB.Controllers
{
   
    public class PagosController : Controller
    {

        IUsuarios_Repositorio Usuarios_Repositorio;
        IAtenderTramite_Repo AtenderTramite_Repo;
        IPagos_Repo Pagos_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        IPagosEnLinea_Repo PagosEnLinea_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        ICorreo_Repo Correo_Repo;

        public PagosController(IAtenderTramite_Repo atenderTramite_Repo, IPagos_Repo pagos_Repo, 
            ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
            IPagosEnLinea_Repo pagosEnLinea_Repo,
            IMensajesSolicitudes_Repo mensajesSolicitudes_Repo,
            IUsuarios_Repositorio usuarios_Repositorio,
            ICorreo_Repo correo_Repo) {
            AtenderTramite_Repo = atenderTramite_Repo;
            Pagos_Repo = pagos_Repo;
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            PagosEnLinea_Repo = pagosEnLinea_Repo;
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            Usuarios_Repositorio = usuarios_Repositorio;
            Correo_Repo = correo_Repo;
        }

        // GET: Pagos
        [SessionExpireAttribute]
        [RoleAuthorize(Roles.Usuario_Externo)]
        public ActionResult Realizar_Pago(int Tramite_Solicitados_ID , int? Pago_Solicitudes_ID)
        {
            List<Pagos_Solicitudes_View> Lista_Pagos;
            List<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalle = new List<Pagos_Solicitudes_Detalle_View>();
            Form_Pago_Multipagos form_Pago_Multipagos = new Form_Pago_Multipagos();
            TramitesSolicitudView tramitesSolicitudView = new TramitesSolicitudView();
            Pagos_Solicitudes_View pagos_Solicitudes_View = new Pagos_Solicitudes_View();

            PagosView pagosView = new PagosView();

            ViewBag.Tramite_Solicitados_ID = Tramite_Solicitados_ID;
            

            Lista_Pagos = Pagos_Repo.Obtener_Pagos(Tramite_Solicitados_ID);
          

            if (Pago_Solicitudes_ID.HasValue)
            {
                ViewBag.Pago_Solicitudes_ID = Pago_Solicitudes_ID.Value;
                Lista_Pagos_Detalle = Pagos_Repo.Obtener_Pagos_Detalle(Pago_Solicitudes_ID.Value);
                tramitesSolicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);
                pagos_Solicitudes_View = Pagos_Repo.Obtener_Un_Pago(Pago_Solicitudes_ID.Value);
                form_Pago_Multipagos = Pagos_Repo.Obtener_Parametro_Pago(tramitesSolicitudView, pagos_Solicitudes_View);
                form_Pago_Multipagos.mp_urlsuccess = ConfigurationManager.AppSettings["url_exitoso"];
                form_Pago_Multipagos.mp_urlfailure = ConfigurationManager.AppSettings["url_error"];
                form_Pago_Multipagos.mp_account = ConfigurationManager.AppSettings["mp_count"];
            }
            else {
                pagosView.Lista_Pagos_Detalle = new List<Pagos_Solicitudes_Detalle_View>();
                ViewBag.Pago_Solicitudes_ID = 0;
            }

            pagosView.Lista_Pagos = Lista_Pagos;
            pagosView.Lista_Pagos_Detalle = Lista_Pagos_Detalle;
            pagosView.Form_Pago_Multipagos = form_Pago_Multipagos;
            return View(pagosView);
        }

        [HttpPost]
        public ActionResult PagoExitoso(Form_Pago_Multipagos form_Pago_Multipagos) {
            Pagos_Solicitudes_View pagos_Solicitudes_View;
            TramitesSolicitudView tramitesSolicitudView;
            UsuarioView usuarioView;
            string cadenaProteger = "";
            int Pago_Solicitudes_ID;
            int Tramite_Solicitados_ID = 0;
            string No_Diverso = "";
            int.TryParse(form_Pago_Multipagos.mp_order, out Pago_Solicitudes_ID);
            PagosEnLineaView pagosEnLineaView = new PagosEnLineaView();
            MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();
            try
            {
             

                if (PagosEnLinea_Repo.Existe_EL_Pago(Pago_Solicitudes_ID))
                {
                    form_Pago_Multipagos.mensaje = "Error: El Pago seleccionado ya ha sido pagado";
                    return View(form_Pago_Multipagos);
                }

                pagos_Solicitudes_View = Pagos_Repo.Obtener_Un_Pago( Convert.ToInt32(form_Pago_Multipagos.mp_order));
                Tramite_Solicitados_ID = pagos_Solicitudes_View.Tramite_Solicitados_ID;
                

                No_Diverso = pagos_Solicitudes_View.No_Diverso;
                cadenaProteger = Pagos_Repo.Obtener_Hash(form_Pago_Multipagos);

                pagosEnLineaView.Banco_Emisor = form_Pago_Multipagos.mp_bankname;
                pagosEnLineaView.Correo_Electronico = form_Pago_Multipagos.mp_email;
                pagosEnLineaView.Folio_Pago = form_Pago_Multipagos.mp_trx_historyid;
                pagosEnLineaView.Importe = form_Pago_Multipagos.mp_amount;
                pagosEnLineaView.Nombre_CuentaHabiente = form_Pago_Multipagos.mp_cardholdername;
                pagosEnLineaView.Numero_Aprobacion_Bancaria = form_Pago_Multipagos.mp_authorization;
                pagosEnLineaView.Pago_Solicitudes_ID = Pago_Solicitudes_ID;
                pagosEnLineaView.No_Diverso = No_Diverso;
                pagosEnLineaView.Pago_Solicitudes_ID = Pago_Solicitudes_ID;
                pagosEnLineaView.Referencia = form_Pago_Multipagos.mp_pan;
                pagosEnLineaView.Telefono = form_Pago_Multipagos.mp_phone;
                pagosEnLineaView.Tipo_Pago = form_Pago_Multipagos.mp_paymentMethod;
                pagosEnLineaView.Tipo_Tarjeta = form_Pago_Multipagos.mp_cardType;
                pagosEnLineaView.Cadena_Hash_Generada = cadenaProteger.ToLower();
                pagosEnLineaView.Cadena_Hash_Regresada = form_Pago_Multipagos.mp_signature.ToLower();

                tramitesSolicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);
                mensajesSolicitudesView.Entidad = "USUARIO";
                mensajesSolicitudesView.Estatus_Solicitud = tramitesSolicitudView.Estatus_Revision;
                mensajesSolicitudesView.Nombre_Tramite = tramitesSolicitudView.Nombre_Tramite;
                mensajesSolicitudesView.Mensaje = "He pagado la referencia : " + form_Pago_Multipagos.mp_reference;
                mensajesSolicitudesView.Tramite_Solicitados_ID = tramitesSolicitudView.Tramite_Solicitados_ID;
                mensajesSolicitudesView.Usuarios_ID = tramitesSolicitudView.Usuarios_ID;
                mensajesSolicitudesView.Usuario_Creo = tramitesSolicitudView.Usuario;
                mensajesSolicitudesView.Visto = false;

                usuarioView = Usuarios_Repositorio.Buscar_Usuario(tramitesSolicitudView.Usuarios_Asignado_ID);
                form_Pago_Multipagos.nombre_tramite = tramitesSolicitudView.Nombre_Tramite;



                if (cadenaProteger != form_Pago_Multipagos.mp_signature) {
                    // significa que algo esta mal
                    form_Pago_Multipagos.mensaje = "Error: número de autorización desconocido";
                    return View(form_Pago_Multipagos);

                }

                Correo_Repo.Enviar_Correo_Registro_Pago(usuarioView, form_Pago_Multipagos, tramitesSolicitudView);



            }
            catch(Exception ex)
            {
                form_Pago_Multipagos.mensaje = ex.Message; 
                Cls_Util.Logger(ex.Message + " PagosController línea 161");
                Correo_Repo.Enviar_Correo_Error(ex.Message + " PagosController línea 161");
                return View(form_Pago_Multipagos);
            }


            using (EFDbContext contexto = new EFDbContext())
            {
           
                using (DbContextTransaction transaction = contexto.Database.BeginTransaction())
                {
                    try
                    {
              
                            Pagos_Repo.Actualizar_Estatus_Pagos(contexto, Pago_Solicitudes_ID);
                            int cantidadPagosPendientes = Pagos_Repo.Obtener_Pagos_Pendientes(contexto, Tramite_Solicitados_ID);

                            if(cantidadPagosPendientes == 0)
                            { 
                                // cambia el estatus de la solicitud , si esa solicitud no tiene pagos pendientes
                                TramitesSolicitudes_Repo.Cambiar_Estatus(contexto, "PAGADO", Tramite_Solicitados_ID);
                                mensajesSolicitudesView.Estatus_Solicitud = "PAGADO";
                            }

                        PagosEnLinea_Repo.Registrar_Pago(contexto, pagosEnLineaView);
                        MensajesSolicitudes_Repo.Guardar_Mensaje(contexto, mensajesSolicitudesView);  

                        transaction.Commit();
                    }
                    catch(Exception ex)
                    {
                        transaction.Rollback();
                        form_Pago_Multipagos.mensaje = ex.Message;
                        Cls_Util.Logger(ex.Message + " PagosController línea 194");
                        Correo_Repo.Enviar_Correo_Error(ex.Message + " PagosController línea 194");
                        return View(form_Pago_Multipagos);
                    }

                }
            }


            try
            {

                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContextSIMAPAG"].ToString()))
                {
                    conexion.Open();

                    using (SqlCommand comando = conexion.CreateCommand())
                    {

                        comando.CommandText = "Sp_Tra_Pagar_Diverso";
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add(new SqlParameter("@No_Diverso", No_Diverso));
                        comando.Parameters.Add(new SqlParameter("@Referencia", form_Pago_Multipagos.mp_pan));
                        comando.Parameters.Add(new SqlParameter("@Tipo_Tarjeta", form_Pago_Multipagos.mp_paymentMethod));
                        comando.Parameters.Add(new SqlParameter("@Banco_Emisor", "BANCOMER"));
                        comando.Parameters.Add(new SqlParameter("@Numero_Aprobacion", form_Pago_Multipagos.mp_authorization));
                        comando.ExecuteNonQuery();


                    }

                }

            }
            catch (Exception ex) {
                form_Pago_Multipagos.mensaje = ex.Message;
                Cls_Util.Logger(ex.Message + " PagosController línea 229");
                Correo_Repo.Enviar_Correo_Error(ex.Message + " PagosController línea 229");
                return View(form_Pago_Multipagos);
            }

           

           form_Pago_Multipagos.mensaje = "bien";
            return View(form_Pago_Multipagos);
        }

        [HttpPost]
        public ActionResult PagoError(Form_Pago_Multipagos form_Pago_Multipagos) {

            return View(form_Pago_Multipagos);
        }
    }
}