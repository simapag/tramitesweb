﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models;
using AplicacionWEB.Models.VerificarSession;
using ModelDomain.Interfaces;
using PagedList;
using ModelDomain.ViewModel;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class ReportePagoReciboController : Controller
    {
        IPagosEnLinea_Repo PagosEnLinea_Repo;


        public ReportePagoReciboController(IPagosEnLinea_Repo pagosEnLinea_Repo)
        {
            PagosEnLinea_Repo = pagosEnLinea_Repo;
        }

        // GET: ReportePagoRecibo
        public ActionResult Listado_Pagos(string sortOrder, string currentRPU, string RPU, string currentFolioBancomer , string FolioBancomer, string currentFechaInicio, string FechaInicio, string currentFechaFin, string FechaFin, int? page)
        {
            IQueryable<PagosEnLineaReciboView> Lista_Pagos;
            int pageSize = 10;
            int pageNumber = 0;

            try
            {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.FechaSortParam = String.IsNullOrEmpty(sortOrder) ? "fecha_asc" : "";

                if (RPU != null)
                {
                    page = 1;
                }
                else {
                    RPU = currentRPU;
                }


                if (FolioBancomer != null)
                {
                    page = 1;
                }
                else {
                    FolioBancomer = currentFolioBancomer;
                }

                if(FechaInicio != null)
                {
                    page = 1;
                }
                else
                {
                    FechaInicio = currentFechaInicio;
                }


                if (FechaFin != null)
                {
                    page = 1;
                }
                else
                {
                    FechaFin = currentFechaFin;
                }

                ViewBag.CurrentFechaInicio = FechaInicio;
                ViewBag.CurrentFechaFin = FechaFin;
                ViewBag.CurrentRPU = RPU;
                ViewBag.CurrentFolioBancomer = FolioBancomer;

                pageNumber = (page ?? 1);

                Lista_Pagos = PagosEnLinea_Repo.Obtener_Pagos_En_Linea_Recibo(sortOrder, FechaInicio, FechaFin, RPU, FolioBancomer);
            }
            catch(Exception ex)
            {
                Lista_Pagos = Enumerable.Empty<PagosEnLineaReciboView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }

            return View(Lista_Pagos.ToPagedList(pageNumber, pageSize));
        }
    }
}