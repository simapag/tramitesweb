﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.Models.Enums;
using AplicacionWEB.Infraestructura.Servicios;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class MensajesController : Controller
    {
        ITramitesSolicitudes_Repo Tramite_Solicitudes_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        IRoles_Repositorio Roles_Repositorio;
        IUsuarios_Repositorio Usuarios_Repositorio;
        ICorreo_Repo Correo_Repo;

        public MensajesController(IMensajesSolicitudes_Repo mensajesSolicitudes_Repo, 
            IRoles_Repositorio roles_Repositorio,
            ITramitesSolicitudes_Repo solicitudes_Repo,
            IUsuarios_Repositorio usuarios_Repositorio,
            Correo_Repo correo_Repo) {
            this.MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            this.Tramite_Solicitudes_Repo = solicitudes_Repo;
            Roles_Repositorio = roles_Repositorio;
            Usuarios_Repositorio = usuarios_Repositorio;
            Correo_Repo = correo_Repo;
        }

        // GET: Mensajes
        public ActionResult Visualizar(int Tramite_Solicitados_ID , string returnUrl , string message)
        {
            List<MensajesSolicitudesView> Lista_Mensaje = new List<MensajesSolicitudesView>();

            try
            {
                TempData["tramite_solicitado_id"] = Tramite_Solicitados_ID;
                ViewBag.MensajeCliente = message;
                ViewBag.Rol = Cls_Sesiones.ROL;
                ViewBag.ID = Tramite_Solicitados_ID;
                ViewBag.ReturnUrl = returnUrl;
                Lista_Mensaje = MensajesSolicitudes_Repo.Obtener_Mensajes(Tramite_Solicitados_ID);
            }
            catch (Exception ex) {

            }

            return View(Lista_Mensaje);
        }

        public ActionResult Crear(int ID, string Comentario, string returnUrl) {

            string status = "";
            try
            {
                UsuarioView usuarioView = new UsuarioView();
                MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();
                string respuesta = "USUARIO";

                if (Cls_Sesiones.ROL_ID.HasValue)
                {
                    if (Roles_Repositorio.Es_Rol_Ejecutivo(Cls_Sesiones.ROL_ID.Value) == "SI")
                    {
                        respuesta = "SIMAPAG";
                    }
                }

                TramitesSolicitudView tramitesSolicitudView = Tramite_Solicitudes_Repo.Obtener_Una_Solicitud(ID);

                mensajesSolicitudesView.Estatus_Solicitud = tramitesSolicitudView.Estatus_Revision;
                mensajesSolicitudesView.Nombre_Tramite = tramitesSolicitudView.Nombre_Tramite;
                mensajesSolicitudesView.Entidad = respuesta;
                mensajesSolicitudesView.Mensaje = Comentario;
                mensajesSolicitudesView.Tramite_Solicitados_ID = ID;
                mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                mensajesSolicitudesView.Visto = false;
                status = "Proceso Exitoso";
                MensajesSolicitudes_Repo.Guardar_Mensaje(mensajesSolicitudesView);


                if (respuesta == "USUARIO")
                {

                    if (tramitesSolicitudView.Usuarios_Asignado_ID > 0)
                    {
                        // usuario asignado a este persona se le va mandar el correo
                        usuarioView = Usuarios_Repositorio.Buscar_Usuario(tramitesSolicitudView.Usuarios_Asignado_ID);
                        Correo_Repo.Enviar_Correo_Mensaje(usuarioView, Cls_Sesiones.NOMBRE_COMPLETO, tramitesSolicitudView, Comentario);

                   }
                 }
                else
                {
                    // usuario del trámite  a esta persona se le va manda el correo
                    usuarioView = Usuarios_Repositorio.Buscar_Usuario(tramitesSolicitudView.Usuarios_ID);
                    Correo_Repo.Enviar_Correo_Mensaje(usuarioView, Cls_Sesiones.NOMBRE_COMPLETO, tramitesSolicitudView, Comentario);
                }
            }
            catch (Exception ex) {
                status = ex.Message;
            }


            return RedirectToAction("Visualizar", new { Tramite_Solicitados_ID = ID , message = status,  returnUrl });
        }


        public PartialViewResult Obtener_Mensajes()
        {
            int cantidadMensajes=0;

            if (Cls_Sesiones.ROL.ToLower() == "ejecutivo") {
                cantidadMensajes = MensajesSolicitudes_Repo.Obtener_Numero_Mensaje_Ejecutivo(Cls_Sesiones.USUARIO_ID);
            }
            else 
            {
                cantidadMensajes = MensajesSolicitudes_Repo.Obtener_Numero_Mensajes(Cls_Sesiones.USUARIO_ID);
            }



            return PartialView(cantidadMensajes);
        }
    }
}