﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelDomain.Contexto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using System.Web.Security;
using System.Web.Script.Serialization;
using ModelDomain.ViewModel;
using AplicacionWEB.Infraestructura;
using Ninject;
using AplicacionWEB.Infraestructura.Servicios;
using System.Text.RegularExpressions;

namespace AplicacionWEB.Controllers
{
    public class LoginController : Controller
    {

        /// <summary>
        /// Respositorio  para acceder a la tabla  Usuario
        /// </summary>
        private IUsuarios_Repositorio ObjUsuariosRepositorio;
        IParametroCorreo_Repo ParametroCorreo_Repo;
        IAsociarRPU_Repo AsociarRPU_Repo;
        /// <summary>
        ///  constructor del controlador
        /// </summary>
        /// <param name="p_ObjUsuariosRepositorio">Repositorio de Usuario</param>
        public LoginController(IUsuarios_Repositorio p_ObjUsuariosRepositorio, IParametroCorreo_Repo parametroCorreo_Repo,
            IAsociarRPU_Repo asociarRPU_Repo) {
            this.ObjUsuariosRepositorio = p_ObjUsuariosRepositorio;
            this.ParametroCorreo_Repo = parametroCorreo_Repo;
            AsociarRPU_Repo = asociarRPU_Repo;
        }


        // GET: Login


            [Authorize]
            /// <summary>
            ///  Nos lleva a la página inicio
            /// </summary>
            /// <returns>Nos lleva a la pagina de inicio</returns>
        public ViewResult  Inicio() {

            return View("Pagina_Inicio");
        }



        private bool sonNumeros(string cadena)
        {
            Regex objPattern = new Regex("^[0-9]+$");
            return objPattern.IsMatch(cadena);
        }

        /// <summary>
        ///  Mustra la vista de autenticacion
        /// </summary>
        /// <returns>
        /// redirige a la vista correspondiente
        /// </returns>

        [HttpGet]
        public ViewResult Verificar_Usuario(string Mensaje) {
            ViewBag.Mensaje = Mensaje;
            return View();
        }


        [HttpGet]
        public ActionResult Pagar_Recibo(string RPU) {
            ViewBag.Mensaje = "";
            string Mensaje="";

            if (!string.IsNullOrEmpty(RPU) && sonNumeros(RPU))
            {
                List<DatosCuentaView> Lista_Datos_Cuenta = AsociarRPU_Repo.Obtener_Datos_Cuenta(RPU);

                if (Lista_Datos_Cuenta.Count > 0)
                {
                    return RedirectToAction("RealizarPagoAnonimo", "PagoRecibo", new { RPU });
                }
                else
                {
                    Mensaje = "RPU no valido o la cuenta no esta activa";
                  return  RedirectToAction("Verificar_Usuario","Login", new { Mensaje });
                }
            }

            Mensaje = "Escribe un rpu valido";
            return RedirectToAction("Verificar_Usuario","Login", new { Mensaje });
        }


        /// <summary>
        /// Verifica si el usuario y contraseña son corretos
        /// </summary>
        /// <param name="p_ObjAutenticacion">Model que contiene dos propiedades password y contraseña</param>
        /// <returns>
        ///   nos dirige a la misma pagina si no pasa la verificación y si pasa nos dirige a la pagina de inicio
        /// </returns>

        [HttpPost]
        public ActionResult Verificar_Usuario(Cls_Autenticacion p_ObjAutenticacion) {
            

            if (ModelState.IsValid)
            {
                try
                {
                   
                    Usuarios usuario = ObjUsuariosRepositorio.Usuarios.Where(x => x.Correo_Electronico == p_ObjAutenticacion.CorreoElectronico && x.Password == p_ObjAutenticacion.Password).FirstOrDefault();


                    if (usuario == null)
                    {
                        ModelState.AddModelError("error", "Corrreo o Contraseña incorrecta");
                        return View();
                    }
                    else {

                        Cls_Sesiones.NOMBRE_COMPLETO = usuario.Nombre + " " + usuario.Apellido_Paterno + " " + usuario.Apellido_Materno;
                        Cls_Sesiones.ROL_ID = usuario.Rol_ID;
                        Cls_Sesiones.USUARIO_ID = usuario.Usuarios_ID;
                        if (usuario.Roles != null)
                            Cls_Sesiones.ROL = usuario.Roles.Nombre;
                        else
                            Cls_Sesiones.ROL = "";

                        FormsAuthentication.SetAuthCookie(p_ObjAutenticacion.CorreoElectronico, false);
                        return RedirectToAction("Principal", "Inicio");

                    }

                }
                catch (Exception ex) {

                    return View();
                }



            }
            else {
                return View();
            }


        }


        /// <summary>
        ///  Para salir del sistema
        /// </summary>
        /// <returns>Redirige a la pagina de login</returns>
        public ActionResult Salir() {

            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Verificar_Usuario","Login");
        }


      
        public string Recuperar_Password(string Email)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;

            try
            {
                Correo_Repo correo_Repo = new Correo_Repo(ParametroCorreo_Repo);


                UsuarioView usuarioView = ObjUsuariosRepositorio.Buscar_Usuario_Por_Correo(Email);

                if(usuarioView == null)
                {
                    objMensajeServidor.Mensaje = "Correo Electrónico no encontrado";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                objMensajeServidor.Mensaje = correo_Repo.Enviar_Correo(usuarioView);


            }
            catch(Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }

        [HttpPost]
        public string Registrar_Usuario(RegistroUsuarioViewModel registroUsuarioViewModel) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {
       
                if(ObjUsuariosRepositorio.Esta_Registrado_Rol_Usuario_Externo() == "NO")
                {
                    objMensajeServidor.Mensaje = "Se requiere registrar un rol para los usuarios externos";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }

                if (ObjUsuariosRepositorio.Esta_Repetido_Correo(registroUsuarioViewModel.Correo_Electronico) == "SI") {
                    objMensajeServidor.Mensaje = "El correo que intenta registrar ya esta registrado";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;

                }

                ObjUsuariosRepositorio.Registrar_Usuario_Externo(registroUsuarioViewModel);
                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }


        /// <summary>
        ///  Cambia el password
        /// </summary>
        /// <param name="p_Modelo_Cambio_Password">Modelo cambio de password</param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>

        [HttpPost]
        public string Cambiar_Password(Cls_Cambio_Password_Modelo p_Modelo_Cambio_Password) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {
                //verificamos si las credenciales son correctas
                Usuarios usuariosBuscar = (from u in ObjUsuariosRepositorio.Usuarios
                                           where u.Correo_Electronico == p_Modelo_Cambio_Password.Correo && u.Password == p_Modelo_Cambio_Password.PasswordActual
                                           select u).FirstOrDefault();

                if (usuariosBuscar == null) {
                    objMensajeServidor.Mensaje = "Credenciales Incorrectas";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                var buscarContraseña = (from u in ObjUsuariosRepositorio.Usuarios
                                        where u.Password == p_Modelo_Cambio_Password.Password
                                        select u).FirstOrDefault();

                if (buscarContraseña != null)
                {
                    objMensajeServidor.Mensaje = "La constraseña escrita ya existe, proporciona otra";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }

                usuariosBuscar.Password = p_Modelo_Cambio_Password.Password;
                ObjUsuariosRepositorio.Guardar_Usuarios(usuariosBuscar);



                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }

    }
}