﻿using AplicacionWEB.Models.VerificarSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using System.Configuration;
using Newtonsoft.Json;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class ContratacionAguaController : Controller
    {

        ITramitesProceso_Repo TramitesProceso_Repo;
        IAsociarRPU_Repo AsociarRPU_Repo;
        ITramites_Documento_Repo Tramites_Documento_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        IRoles_Repositorio Roles_Repositorio;
        ITramites_Repo Tramite_Repositorio;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;

        public ContratacionAguaController(ITramitesProceso_Repo tramitesProceso_Repo,
             IAsociarRPU_Repo asociarRPU_Repo,
             ITramites_Documento_Repo tramites_Documento_Repo,
             ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
             IRoles_Repositorio roles_Repositorio,
             ITramites_Repo tramites_Repo,
             IMensajesSolicitudes_Repo mensajesSolicitudes_Repo) {

            this.TramitesProceso_Repo = tramitesProceso_Repo;
            this.AsociarRPU_Repo = asociarRPU_Repo;
            this.Tramites_Documento_Repo = tramites_Documento_Repo;
            this.TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            this.Roles_Repositorio = roles_Repositorio;
            this.Tramite_Repositorio = tramites_Repo;
            this.MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
        }

        // GET: ContratacionAgua
        [HttpGet]
        public ActionResult Crear(int Tramite_ID)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            List<ComboView> Lista_Documentos = Tramites_Documento_Repo.Obtener_Documentos_Por_Tramite(Tramite_ID);
            TramitesSolicitudView solicitudView = new TramitesSolicitudView();
            TramitesView tramitesView = TramitesProceso_Repo.Obtener_Un_Tramite(Tramite_ID);
            solicitudView.Tramite_ID = Tramite_ID;
            solicitudView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
            solicitudView.RPU_Requerido = false;
            solicitudView.Estatus = "PENDIENTE";
            solicitudView.Estatus_Revision = "PENDIENTE";
            solicitudView.Seleccionado = true;
            solicitudView.Nombre_Tramite = tramitesView.Nombre_Tramite;
            solicitudView.Costo_Tramite = Tramite_Repositorio.Obtener_Costos_Tramite(Tramite_ID);
            ViewBag.Lista_Documentos = serializer.Serialize(Lista_Documentos);


            return View("FormContratacionAgua", solicitudView);
        }
        [HttpGet]
        public ActionResult Editar(int Tramite_Solicitados_ID, string returnUrl) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            TempData["tramite_solicitado_id"] = Tramite_Solicitados_ID;
            TramitesSolicitudView solicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);
            solicitudView.RPU_Requerido = false;
            solicitudView.Seleccionado = true;
            if (Cls_Sesiones.ROL_ID.HasValue)
            {
                if (Roles_Repositorio.Es_Rol_Ejecutivo(Cls_Sesiones.ROL_ID.Value) == "SI") {
                    solicitudView.Seleccionado = false;
                }
            }

            List<ComboView> Lista_Documentos = Tramites_Documento_Repo.Obtener_Documentos_Por_Tramite(solicitudView.Tramite_ID);
            ViewBag.Lista_Documentos = serializer.Serialize(Lista_Documentos);
            ViewBag.ReturnUrl = returnUrl;
            return View("FormContratacionAgua", solicitudView);
        }


        [HttpPost]
        public string Editar(string strSolicitudTramites, string strListaDocumentos)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
            EFDbContext Contexto = new EFDbContext();
            string rutaPrimeraParte = HttpContext.Server.MapPath("~/Documentos/");
            string urlArchivo = ConfigurationManager.AppSettings["url_documento"];



            TramitesSolicitudView tramitesSolicitudView;
            List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos;
            MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();

            using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
            {
                try
                {
                    strSolicitudTramites = strSolicitudTramites.Replace(@"\", "");
                    tramitesSolicitudView = JsonConvert.DeserializeObject<TramitesSolicitudView>(strSolicitudTramites);
                    strListaDocumentos = strListaDocumentos.Replace(@"\", "");
                    listaDocumentos = JsonConvert.DeserializeObject<List<Tra_Tramites_Solicitados_DocumentosView>>(strListaDocumentos);
                   
                    TramitesSolicitudes_Repo.Guardar_Archivos(listaDocumentos, rutaPrimeraParte, urlArchivo);
                    tramitesSolicitudView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                    TramitesSolicitudes_Repo.Editar_Solicitud_Tramite(Contexto, tramitesSolicitudView, listaDocumentos);

                    mensajesSolicitudesView.Estatus_Solicitud = tramitesSolicitudView.Estatus_Revision;
                    mensajesSolicitudesView.Nombre_Tramite = TramitesSolicitudes_Repo.Obtener_Nombre_Tramite(Contexto, tramitesSolicitudView.Tramite_Solicitados_ID);
                    mensajesSolicitudesView.Entidad = "USUARIO";
                    mensajesSolicitudesView.Mensaje = "EDICIÓN DE LA SOLICTUD DE TRÁMITE";
                    mensajesSolicitudesView.Tramite_Solicitados_ID = tramitesSolicitudView.Tramite_Solicitados_ID;
                    mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                    mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                    mensajesSolicitudesView.Visto = false;
                    MensajesSolicitudes_Repo.Guardar_Mensaje(Contexto, mensajesSolicitudesView);


                    transaction.Commit();
                    objMensajeServidor.Mensaje = "bien";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    objMensajeServidor.Mensaje = ex.Message;
                }
                finally
                {
                    Contexto.Dispose();
                }

            }


            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }


        [HttpPost]
        public string Guardar(string strSolicitudTramites, string strListaDocumentos)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
            EFDbContext Contexto = new EFDbContext();
            string rutaPrimeraParte = HttpContext.Server.MapPath("~/Documentos/");
            string urlArchivo = ConfigurationManager.AppSettings["url_documento"];
            int Tramite_Solicitados_ID = 0;

            TramitesSolicitudView tramitesSolicitudView;
            List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos;
  

            using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
            {

                try
                {
                    tramitesSolicitudView = JsonConvert.DeserializeObject<TramitesSolicitudView>(strSolicitudTramites);
                    listaDocumentos = JsonConvert.DeserializeObject<List<Tra_Tramites_Solicitados_DocumentosView>>(strListaDocumentos);


                    TramitesSolicitudes_Repo.Guardar_Archivos(listaDocumentos, rutaPrimeraParte, urlArchivo);
                    tramitesSolicitudView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                    Tramite_Solicitados_ID = TramitesSolicitudes_Repo.Guardar_Solicitud_Tramite(Contexto, tramitesSolicitudView, listaDocumentos);
               
                    transaction.Commit();
                    objMensajeServidor.Mensaje = "bien";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    objMensajeServidor.Mensaje = ex.Message;

                }
                finally
                {
                    Contexto.Dispose();
                }
            }
            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }
    }
}