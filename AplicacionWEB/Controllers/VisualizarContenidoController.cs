﻿using AplicacionWEB.Models;
using AplicacionWEB.Models.Enums;
using AplicacionWEB.Models.VerificarSession;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]

    public class VisualizarContenidoController : Controller
    {
        // GET: VisualizarContenido
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Visulizar_Archivo_PDF(string ruta)
        {

            try
            {
                FileStream fss = new FileStream(ruta, FileMode.Open);
                byte[] bytes = new byte[fss.Length];
                fss.Read(bytes, 0, Convert.ToInt32(fss.Length));
                fss.Close();


                Response.ClearContent();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Length", bytes.Length.ToString());
                Response.Flush();
                Response.BinaryWrite(bytes);
                Response.End();



            }
            catch (Exception ex)
            {

            }


            return null;
        }
    }
}