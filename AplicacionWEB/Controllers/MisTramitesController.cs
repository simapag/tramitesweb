﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;
using PagedList;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    // [Authorize(Roles = "Administrador,Usuario_Externo")]
    //[MyAuthorization(MyKeys = "admin,sergio")]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class MisTramitesController : Controller
    {
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;


        public MisTramitesController(ITramitesSolicitudes_Repo tramitesSolicitudes_Repo) {
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;

        }

        // GET: MisTramites
        public ActionResult Listar_Mis_Tramites(string sortOrder, string currentFilter, string searchString,int? page)
        {
            IQueryable<TramitesSolicitudView> Lista_Solicitudes;
            int pageSize = 5;
            int pageNumber = 0;

            try
            {

                ViewBag.CurrentSort = sortOrder;
                ViewBag.NombreTramiteSortParm = String.IsNullOrEmpty(sortOrder) ? "nombre_tramite_desc" : "";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;

                pageNumber = (page ?? 1);
                Lista_Solicitudes = TramitesSolicitudes_Repo.Obtener_Solicitudes_Por_Usuario(sortOrder, searchString, Cls_Sesiones.USUARIO_ID);

            }catch(Exception ex)
            {
                Lista_Solicitudes = Enumerable.Empty<TramitesSolicitudView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }

            return View(Lista_Solicitudes.ToPagedList(pageNumber, pageSize));
        }
    }
}