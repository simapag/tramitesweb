﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using System.Data.Entity;
using System.Data.SqlClient;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    //[Authorize]
    /// <summary>
    /// Controlador Menu
    /// </summary>
    /// 
    [SessionExpireAttribute]
    [RoleAuthorize( AplicacionWEB.Models.Enums.Roles.Usuario_Externo)]
    public class MenuController : Controller
    {

        private IMenus_Repositorio ObjMenuRepositorio;

        public MenuController(IMenus_Repositorio p_ObjMenuRepositorio) {
            this.ObjMenuRepositorio = p_ObjMenuRepositorio;
        }

        // GET: Menu


        /// <summary>
        ///  Muestra la Lista de Menu
        /// </summary>
        /// <returns>
        /// Redirige la a vista correspondiente
        /// </returns>
        public ViewResult Listar_Menu() {
            return View();

        }

        /// <summary>
        /// Elimina un menu de la base de datos
        /// </summary>
        /// <param name="p_Menu">Entidad Menu</param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Eliminar_Menu(Menu p_Menu) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {
                objMensajeServidor.Mensaje = ObjMenuRepositorio.Eliminar_Menu(p_Menu.Menu_ID);
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;
        }

        /// <summary>
        /// Cambia el estatus a inactivo a un elemento del menú
        /// </summary>
        /// <param name="p_Menu"></param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Inactivar_Menu(Menu p_Menu)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;

            try
            {
                Menu menuBuscar = (from m in ObjMenuRepositorio.Menus
                                   where m.Menu_ID == p_Menu.Menu_ID 
                                   select m).FirstOrDefault();




                menuBuscar.Fecha_Modifico = DateTime.Now;
                menuBuscar.Usuario_Modifico = Cls_Sesiones.NOMBRE_COMPLETO;
                menuBuscar.Estatus = "INACTIVO";
                ObjMenuRepositorio.Guarda_Menu(menuBuscar);

                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }
            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }





        /// <summary>
        /// Edita un menu en la base de datos
        /// </summary>
        /// <param name="p_Menu">Entidad Menu</param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Editar_Menu(Menu p_Menu) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;

            try
            {
                Menu menuBuscar = (from m in ObjMenuRepositorio.Menus
                                    where m.Menu_ID != p_Menu.Menu_ID && m.Menu_Descripcion == p_Menu.Menu_Descripcion
                                    select m).FirstOrDefault();

                if (menuBuscar != null) {
                    objMensajeServidor.Mensaje = "El nombre del menu esta repetido";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                p_Menu.Fecha_Modifico = DateTime.Now;
                p_Menu.Usuario_Modifico = Cls_Sesiones.NOMBRE_COMPLETO;
                ObjMenuRepositorio.Guarda_Menu(p_Menu);

                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }
            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }



        /// <summary>
        /// Da de alta un nuevo elemento
        /// </summary>
        /// <param name="p_Menu">Entidad Menu</param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Nuevo_Menu(Menu p_Menu) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;

            try
            {

                Menu menuBuscar = (from m in ObjMenuRepositorio.Menus
                                    where m.Menu_Descripcion == p_Menu.Menu_Descripcion
                                    select m).FirstOrDefault();

                if (menuBuscar != null) {
                    objMensajeServidor.Mensaje = "El nombre del menú esta repetido";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                p_Menu.Fecha_Creo = DateTime.Now;
                p_Menu.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                ObjMenuRepositorio.Guarda_Menu(p_Menu);
                
                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;

            }


            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;
        }

        /// <summary>
        /// Obtener_Menus_Papas
        /// </summary>
        /// <returns>
        /// regresa la lista de menus que son raiz en json
        /// </returns>
        public string Obtener_Menus_Papas() {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;
            try
            {
                var menusPapa = (from m in ObjMenuRepositorio.Menus
                                  where m.Parent_ID == null
                                  orderby m.Menu_Descripcion descending
                                  select new { name = m.Menu_Descripcion, id = m.Menu_ID }).ToList();


                objMensajeServidor.Dato1 = serializer.Serialize(menusPapa);
                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }
            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }


        /// <summary>
        /// consulta  que devuelve un listado de menus
        /// </summary>
        /// <param name="offset">ese un parámetro del tabla bootstrap en la página donde inicia</param>
        /// <param name="limit">ese un parámetro del tabla bootstrap total de renglones a tomar</param>
        /// <param name="search">ese un parámetro del tabla bootstrap la cajita de búsqueda</param>
        /// <returns>  regresa la lista de menus en json
        /// </returns>
        public JsonResult Obtener_Menus(int offset, int limit, string search = null)
        {

            var menuRepositorio = ObjMenuRepositorio.Menus;
            List<Menu> menusFiltro;

            if (!string.IsNullOrEmpty(search))
            {
                menusFiltro = ObjMenuRepositorio.Menus.Include(s => s.Child_Menu).Where(x => x.Menu_Descripcion.Contains(search)).ToList();
            }else
            {
                menusFiltro = menuRepositorio.Include(x => x.Child_Menu).ToList();

            }


            List<Menu> menuDesplegar = (menusFiltro.Skip(offset).Take(limit).OrderBy(m=> m.Orden).ThenBy(x=> x.Menu_Descripcion)).ToList();

            var Resultado = from m in menuDesplegar
                            select new { id = m.Menu_ID, id_parent= m.Menu_Parent == null ? 0: m.Menu_Parent.Menu_ID ,sub_menu = m.Menu_Descripcion, menu_padre = m.Menu_Parent == null ? "Raiz": m.Menu_Parent.Menu_Descripcion, accion= m.Accion, controlador= m.Controlador, orden=m.Orden,estatus= m.Estatus };


            return Json(new
            {
                total = menusFiltro.Count(),
                rows = Resultado

            }, JsonRequestBehavior.AllowGet);



        }
    }
}