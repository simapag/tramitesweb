﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using System.Data.Entity;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    //[Authorize]
    /// <summary>
    /// 
    /// </summary>
    /// 
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class AccesosController : Controller
    {

        private IAcceso_Respositorio ObjAccesoRepositorio;
        private IRoles_Repositorio ObjRolesRepositorio;
        private IMenus_Repositorio ObjMenuRepositorio;

        public AccesosController(IAcceso_Respositorio p_ObjAccesoRepositorio, IRoles_Repositorio p_ObjRolesRepositorio, IMenus_Repositorio p_ObjMenuRepositorio) {
            this.ObjAccesoRepositorio = p_ObjAccesoRepositorio;
            this.ObjRolesRepositorio = p_ObjRolesRepositorio;
            this.ObjMenuRepositorio = p_ObjMenuRepositorio;
        }

        // GET: Accesos
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Muestra la vista acceso y pone en memoria 
        /// datos de los rolos y los menu para desplegarlos en la vista
        /// </summary>
        /// <returns>
        ///  redirige a la vista correspondiente
        /// </returns>
        public ViewResult Listar_Acceso() {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            string strRoles = "";
            string strMenu = "";

            try
            {

                var objRoles = (from r in ObjRolesRepositorio.Roles
                                where r.Estatus == "ACTIVO"
                                orderby r.Nombre select new { name = r.Nombre, id = r.Rol_ID }).ToList();

                List<Menu> Menu = ObjMenuRepositorio.Menus.OrderBy(x => x.Menu_Parent.Menu_Descripcion).ThenBy(x => x.Menu_Descripcion).Include(m => m.Menu_Parent).Where(x => x.Parent_ID != null && x.Estatus == "ACTIVO").ToList();

                var menuDesplegar = from m in Menu
                                     select new { id = m.Menu_ID, sub_menu = m.Menu_Descripcion, menu_padre = m.Menu_Parent.Menu_Descripcion, estatus = m.Estatus };


                strRoles = serializer.Serialize(objRoles); 
                strMenu = serializer.Serialize(menuDesplegar);

                ViewBag.Roles = strRoles;
                ViewBag.Menu = strMenu;

            }
            catch (Exception ex)
            {

            }


            return View();           
        }

        /// <summary>
        /// Obtiene los menus asignados a un determinado rol
        /// </summary>
        /// <param name="p_Rol_ID"> el id del rol </param>
        /// <returns>
        ///  respuesta en cadena
        /// </returns>
        public string Obtener_Menu_A_Partir_Rol(int p_Rol_ID) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;
            string strMenus;

            try
            {

                var menus = from a in ObjAccesoRepositorio.Accesos
                                   where a.Rol_ID == p_Rol_ID
                                   select new { id = a.Menu_ID };

                strMenus = serializer.Serialize(menus);

                objMensajeServidor.Mensaje = "bien";
                objMensajeServidor.Dato1 = strMenus;

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }

        /// <summary>
        /// Guarda en la base datos en la configuración de que menús se le asignaron a un dertminado rol
        /// </summary>
        /// <param name="p_Lista_Acceso">
        ///   Colección de datos de opciones de menú seleccionadas
        /// </param>
        /// <returns>
        ///  respuesta si fue exitoso el metódo que se ejecuto
        /// </returns>
        [HttpPost]
        public string Guardar_Configuracion(List<Acceso> p_Lista_Acceso) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;


            try
            {

                int rolID = 0;

                if (p_Lista_Acceso.Count > 0)
                {
                    rolID = p_Lista_Acceso[0].Rol_ID;

                }

                List<Acceso> accesosAnteriores = (from a in ObjAccesoRepositorio.Accesos where a.Rol_ID == rolID select a).ToList();
                ObjAccesoRepositorio.Guardar_Accesos(p_Lista_Acceso, accesosAnteriores, "SERGIO");

               objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }


    }
}