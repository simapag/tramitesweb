﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{

    //[Authorize]
    [SessionExpireAttribute]
    [RoleAuthorize()]
    /// <summary>
    /// Controlador de Roles
    /// </summary>
    public class RolesController : Controller
    {
        // GET: Roles

          /// <summary>
          /// Repositorio para acceder a la tabla de Roles
          /// </summary>
        private IRoles_Repositorio ObjRolesRepositorio; 
      
        /// <summary>
        ///  constructor del controlador
        /// </summary>
        /// <param name="p_ObjRolesRepositorio">Repositorio de Roles</param>
        public RolesController(IRoles_Repositorio p_ObjRolesRepositorio) {
            this.ObjRolesRepositorio = p_ObjRolesRepositorio;
        }


        /// <summary>
        /// Muestra la vista de los roles
        /// </summary>
        /// <returns>
        ///  Redirige la a vista correspondiente
        /// </returns>
        public ViewResult Listar_Roles()
        {
            return View();
        }

        /// <summary>
        /// Consulta para que devuelve una lista de roles
        /// </summary>
        /// <param name="offset">ese un parámetro del tabla bootstrap en la página donde inicia</param>
        /// <param name="limit">ese un parámetro del tabla bootstrap total de renglones a tomar</param>
        /// <param name="search">ese un parámetro del tabla bootstrap la cajita de búsqueda</param>
        /// <returns>
        ///  regresa la lista de roles en json
        /// </returns>
        public JsonResult Obtener_Roles(int offset, int limit, string search=null)
        {

            var rolerepositorio = ObjRolesRepositorio.Roles;
            IEnumerable<Roles> rolesfiltro;


            if (!string.IsNullOrEmpty(search))
            {
                rolesfiltro = ObjRolesRepositorio.Roles.Where(x => x.Nombre.Contains(search));
            }
            else {
                rolesfiltro = rolerepositorio;
            }
        
            var rolesdesplegar = rolesfiltro
                                  .OrderBy(r=> r.Nombre)
                                  .Skip(offset)
                                  .Take(limit);

            var resultado = from r in rolesdesplegar
                            select new { id = r.Rol_ID, nombre= r.Nombre, descripcion = r.Descripcion, estatus =r.Estatus };
                

            return Json(new
            {
                total = rolesfiltro.Count(),
                rows = resultado

            }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Elimina un rol de la base datos
        /// </summary>
        /// <param name="p_Rol"> Entidad Rol</param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Eliminar_Rol(Roles p_Rol) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {
                objMensajeServidor.Mensaje = ObjRolesRepositorio.Eliminar_Rol(p_Rol.Rol_ID);
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;
        }

        /// <summary>
        /// Cambia el estatus de un elemento a inactivo
        /// </summary>
        /// <param name="p_Rol"></param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Inactivar_Rol(Roles p_Rol)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {

                Roles rolesBuscar = (from r in ObjRolesRepositorio.Roles
                                     where r.Rol_ID == p_Rol.Rol_ID
                                     select r).FirstOrDefault();



                rolesBuscar.Estatus = "INACTIVO";
                rolesBuscar.Usuario_Modifico = Cls_Sesiones.NOMBRE_COMPLETO;
                rolesBuscar.Fecha_Modifico = DateTime.Now;

                ObjRolesRepositorio.Guardar_Roles(rolesBuscar);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;


        }


        /// <summary>
        /// Edita Un Rol
        /// </summary>
        /// <param name="p_Rol">
        ///  Entidad Rol
        /// </param>
        /// <returns>
        /// nos indica si fue exitoso o no el procesos
        /// </returns>

        [HttpPost]
        public string Editar_Rol(Roles p_Rol) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {

                Roles rolesBuscar = (from r in ObjRolesRepositorio.Roles where r.Nombre ==
                                      p_Rol.Nombre && p_Rol.Rol_ID != r.Rol_ID select r).FirstOrDefault();

                if (rolesBuscar != null)
                {
                    objMensajeServidor.Mensaje = "El nombre del Rol esta repetido";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }

                
                p_Rol.Usuario_Modifico = Cls_Sesiones.NOMBRE_COMPLETO;
                p_Rol.Fecha_Modifico = DateTime.Now;

                ObjRolesRepositorio.Guardar_Roles(p_Rol);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;


        }


        /// <summary>
        /// Agrega un nuevo rol a la base datos
        /// </summary>
        /// <param name="p_Rol">
        ///  Entidad Rol
        /// </param>
        /// <returns>
        ///  nos indica si fue exitoso o no el procesos
        /// </returns>
        [HttpPost]
        public string Nuevo_Rol(Roles p_Rol) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();

            string strDatos;

            try
            {

                Roles rolesBuscar = (from r in ObjRolesRepositorio.Roles where r.Nombre == p_Rol.Nombre select r).FirstOrDefault();
                
                if(rolesBuscar != null)
                {
                    objMensajeServidor.Mensaje = "El nombre del Rol esta repetido";
                    strDatos= serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                p_Rol.Usuario_Creo = "s";
                p_Rol.Fecha_Creo = DateTime.Now;

                ObjRolesRepositorio.Guardar_Roles(p_Rol);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;

        }


    }
}