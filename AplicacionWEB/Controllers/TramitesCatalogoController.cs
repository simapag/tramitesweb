﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    //[Authorize]
    [RoleAuthorize()]
    public class TramitesCatalogoController : Controller
    {

        ITramites_Repo Tramites_Repo;
        ITramites_Documento_Repo Tramites_Documento_Repo;
        IDocuemento_Repo Documento_Repo;

        public TramitesCatalogoController(ITramites_Repo tramites_Repo, ITramites_Documento_Repo tramites_Documento_Repo,
            IDocuemento_Repo docuemento_Repo) {
            Tramites_Repo = tramites_Repo;
            Tramites_Documento_Repo = tramites_Documento_Repo;
            Documento_Repo = docuemento_Repo;
        }

        // GET: TramitesCatalogo
        public ActionResult Catalogo_Tramites()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer(); 

            try
            {
                List<ComboView> listaDocumentos = Documento_Repo.Obtener_Documentos_Combo();
                ViewBag.Lista_Documentos = serializer.Serialize(listaDocumentos);
            }
            catch (Exception ex) {

            }

            return View();
        }


        public string Obtener_Tramites_Documento(int offset, int limit, int Tramite_ID) {

            Cls_Paginado<Tramites_Documento_View> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = Tramites_Documento_Repo.Obtener_Tramites_Documentos(offset, limit, Tramite_ID);
                str_datos = serializer.Serialize(Paginado);
            }
            catch (Exception ex) {

            }

            return str_datos;
        }

        public string Obtener_Tramites(int offset, int limit, string search = null) {

            Cls_Paginado<TramitesView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {

                Paginado = Tramites_Repo.Obtener_Tramites(offset, limit, search);
                str_datos = serializer.Serialize(Paginado);

            }
            catch (Exception ex) {

            }


            return str_datos;
        }



        [HttpPost]
        public string Guardar_Tramite_Documento(Tramites_Documento_View tramites_Documento) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {

                if(Tramites_Documento_Repo.Esta_Repetido(tramites_Documento.Tramite_ID, tramites_Documento.Documento_ID) == "SI")
                {
                    objMensajeServidor.Mensaje = "El documento ya esta agregado al trámite";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }


                Tramites_Documento_Repo.Guardar_Tramites_Documento(tramites_Documento);
                tramites_Documento.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;

            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }

        [HttpPost]
        public string Guardar_Tramite(TramitesView tramitesView) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {

                if (Tramites_Repo.Esta_Repetido_Tramite(tramitesView.Nombre_Tramite, tramitesView.Tramite_ID) == "SI") {

                    objMensajeServidor.Mensaje = "El nombre tramite esta repetido";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

                tramitesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                Tramites_Repo.Guardar_Tramite(tramitesView);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }


        [HttpPost]
        public string Eliminar_Tramite_Documento(Tramites_Documento_View tramites_Documento_View) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {
                objMensajeServidor.Mensaje = Tramites_Documento_Repo.Eliminar_Tramite_Documento(tramites_Documento_View.Tramite_Documento_ID);
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }

        [HttpPost]
        public string Eliminar_Tramite(TramitesView tramitesView) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";


            try
            {
                objMensajeServidor.Mensaje = Tramites_Repo.Eliminar_Tramite(tramitesView);
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }
    }

            
}