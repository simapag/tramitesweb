﻿using AplicacionWEB.Models.VerificarSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using Newtonsoft.Json;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    //[Authorize]
    [RoleAuthorize()]
    public class AtenderTramiteController : Controller
    {

        IAtenderTramite_Repo AtenderTramite_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;

        public AtenderTramiteController(IAtenderTramite_Repo atenderTramite_Repo,
            IMensajesSolicitudes_Repo mensajesSolicitudes_Repo, ITramitesSolicitudes_Repo tramitesSolicitudes_Repo) {
            AtenderTramite_Repo = atenderTramite_Repo;
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
        }

        // GET: AtenderTramite
        public ActionResult Tramites_Pendientes()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                List<ComboView> Lista_Tramites = AtenderTramite_Repo.Obtener_Tramites_Asignados(Cls_Sesiones.USUARIO_ID);
                ViewBag.Lista_Tramites = serializer.Serialize(Lista_Tramites);
            }
            catch (Exception ex) {

            }

           

            return View();
        }


        public string Obtener_Solicitudes_Pendientes(int offset, int limit, int tramite_id, string search = null , string estatus = null ) {

            Cls_Paginado<TramitesSolicitudView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = AtenderTramite_Repo.Obtener_Solicitudes(offset, limit, tramite_id, search, estatus, Cls_Sesiones.USUARIO_ID);
                str_datos = serializer.Serialize(Paginado);
            }
            catch (Exception ex) {
            }

            return str_datos;
        }

        public string Obtener_Pagos_Asignados(int offset, int limit, int tramite_solicitados_id, string search)
        {
            Cls_Paginado<Pagos_Solicitudes_View> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                Paginado = AtenderTramite_Repo.Obtener_Pagos_Registrados(offset, limit, tramite_solicitados_id, search);
                str_datos = serializer.Serialize(Paginado);

            }
            catch (Exception ex) {

            }
           
         
            return str_datos;

        }

        public string Obtener_Pago_Asignado_Detalle(int offset, int limit,int pago_solicitudes_id)
        {
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Paginado<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalle;

            try {
                Lista_Pago_Detalle = AtenderTramite_Repo.Obtener_Detalla_Pago(offset,limit,pago_solicitudes_id);
                str_datos = serializer.Serialize(Lista_Pago_Detalle);

            }
            catch(Exception ex)
            {


            }

            return str_datos;

        }

        public string Obtener_Pago_Busqueda_Detalle(int offset, int limit, string no_diverso)
        {
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalles = new List<Pagos_Solicitudes_Detalle_View>();
            Cls_Paginado<Pagos_Solicitudes_Detalle_View> Lista_Paginado;
            try
            {
                Lista_Pago_Detalles = AtenderTramite_Repo.Obtener_Datos_Codigo_Barras_Detalle(no_diverso);
                Lista_Paginado = new Cls_Paginado<Pagos_Solicitudes_Detalle_View>(Lista_Pago_Detalles, Lista_Pago_Detalles.Count());
                str_datos = serializer.Serialize(Lista_Paginado);

            }
            catch(Exception ex)
            {

            }

            return str_datos;
        }

        public string Buscar_Codigo_Barras(int offset, int limit,string codigo_barras) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Paginado<Pagos_Solicitudes_View> Listado_Busqueda;
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
        
            try
            {
                List<Pagos_Solicitudes_View> Lista_Pagos = AtenderTramite_Repo.Obtener_Datos_Codigo_Barras(codigo_barras);
                Listado_Busqueda = new Cls_Paginado<Pagos_Solicitudes_View>(Lista_Pagos, Lista_Pagos.Count());
                str_datos = serializer.Serialize(Listado_Busqueda);
                //if (Lista_Pagos.Count == 0) {
                //    objMensajeServidor.Mensaje = "No se Encontro informacion";
                //    str_datos = serializer.Serialize(objMensajeServidor);
                //    return str_datos;
                //}

                //Pagos_Solicitudes_View pagos_Solicitudes_View = Lista_Pagos[0];

                //List<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalles = new List<Pagos_Solicitudes_Detalle_View>();
                // Lista_Pago_Detalles = AtenderTramite_Repo.Obtener_Datos_Codigo_Barras_Detalle(pagos_Solicitudes_View.No_Diverso);


                //objMensajeServidor.Mensaje = "bien";
               // objMensajeServidor.Dato1 = serializer.Serialize(Lista_Pagos);
               // objMensajeServidor.Dato2 = serializer.Serialize(Lista_Pago_Detalles);

            }
            catch (Exception ex) {
               // objMensajeServidor.Mensaje = ex.Message;
            };
           // str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }



       


        [HttpPost]
        public string GuadarPago(int Tramite_Solicitados_ID, string strListaPagoEncabezado, string strListaPagoDetalle)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };


            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
            TramitesSolicitudView tramitesSolicitudView = new TramitesSolicitudView();
            Pagos_Solicitudes_View Lista_Pagos = new Pagos_Solicitudes_View();

            try
            {
                Lista_Pagos = JsonConvert.DeserializeObject<Pagos_Solicitudes_View>(strListaPagoEncabezado, settings);
                tramitesSolicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);

                if (AtenderTramite_Repo.Esta_Repetido_Codigo_Barras(Lista_Pagos.Codigo_Barras) == "SI") {

                    objMensajeServidor.Mensaje = "Código de barras Repetido";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

            }catch(Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;

            }

            using (EFDbContext Contexto = new EFDbContext())
            {
                using (DbContextTransaction transaction = Contexto.Database.BeginTransaction()) {

                    try
                    {
                        
                        List<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalle = JsonConvert.DeserializeObject<List<Pagos_Solicitudes_Detalle_View>>(strListaPagoDetalle);


                        Pagos_Solicitudes_View pagos_Solicitudes_View = Lista_Pagos;

                        pagos_Solicitudes_View.Tramite_Solicitados_ID = Tramite_Solicitados_ID;
                        pagos_Solicitudes_View.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                        pagos_Solicitudes_View.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;

                        AtenderTramite_Repo.Agregar_Pagos(Contexto, Tramite_Solicitados_ID, pagos_Solicitudes_View, Lista_Pago_Detalle);
                        TramitesSolicitudes_Repo.Cambiar_Estatus(Contexto, "PENDIENTE PAGO", Tramite_Solicitados_ID);

                        MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();
                        mensajesSolicitudesView.Tramite_Solicitados_ID = Tramite_Solicitados_ID;
                        mensajesSolicitudesView.Nombre_Tramite = tramitesSolicitudView.Nombre_Tramite;
                        mensajesSolicitudesView.Estatus_Solicitud = "PENDIENTE PAGO";
                        mensajesSolicitudesView.Mensaje = "SE LE HA AGREGADO UN CONCEPTO DE PAGO";
                        mensajesSolicitudesView.Entidad = "SIMAPAG";
                        mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                        mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                        MensajesSolicitudes_Repo.Guardar_Mensaje(Contexto, mensajesSolicitudesView);

                        objMensajeServidor.Mensaje = "bien";
                        transaction.Commit();

                    }
                    catch (Exception ex) {
                        transaction.Rollback();
                        objMensajeServidor.Mensaje = ex.Message;
                    }

                }

            }


                str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }


        public string Cancelar_Pagos(int Pago_Solicitudes_ID) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
            Pagos_Solicitudes_View pagos_Solicitudes_View = new Pagos_Solicitudes_View();
            try
            {
                 pagos_Solicitudes_View = AtenderTramite_Repo.Obtener_Un_Pago(Pago_Solicitudes_ID);

                if(pagos_Solicitudes_View.Estatus.ToLower() != "pendiente")
                {
                    objMensajeServidor.Mensaje = "Solo se pueden cancelar pagos pendientes";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

               // AtenderTramite_Repo.Cambiar_Estatus_Pago("CANCELADO", Cls_Sesiones.NOMBRE_COMPLETO, Pago_Solicitudes_ID);
               // objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }


            using (EFDbContext Contexto = new EFDbContext())
            {

                using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
                {

                    try
                    {
                        TramitesSolicitudes_Repo.Cambiar_Estatus(Contexto, "EN PROCESO", pagos_Solicitudes_View.Tramite_Solicitados_ID);
                        AtenderTramite_Repo.Cambiar_Estatus_Pago(Contexto, "CANCELADO", Cls_Sesiones.NOMBRE_COMPLETO, Pago_Solicitudes_ID);

                        objMensajeServidor.Mensaje = "bien";
                        transaction.Commit();
                    }
                    catch (Exception ex) {
                        transaction.Rollback();
                        objMensajeServidor.Mensaje = ex.Message;
                    }

                }

            }
                str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }

        [HttpPost]
        public string GuardarMensaje(MensajesSolicitudesView mensajesSolicitudesView) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            using (EFDbContext Contexto = new EFDbContext())
            {
                using (DbContextTransaction transaction = Contexto.Database.BeginTransaction()) {

                    try
                    {

                        TramitesSolicitudes_Repo.Cambiar_Estatus(Contexto, mensajesSolicitudesView.Estatus_Solicitud, mensajesSolicitudesView.Tramite_Solicitados_ID);
                        mensajesSolicitudesView.Entidad = "SIMAPAG";
                        mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                        mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                        MensajesSolicitudes_Repo.Guardar_Mensaje(Contexto, mensajesSolicitudesView);
                        transaction.Commit();
                        objMensajeServidor.Mensaje = "bien";
                    }
                    catch (Exception ex) {
                        transaction.Rollback();
                        objMensajeServidor.Mensaje = ex.Message;
                    }

                }

            }


                str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        } 
    }
}