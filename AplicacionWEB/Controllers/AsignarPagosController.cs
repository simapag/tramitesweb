﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using Newtonsoft.Json;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.Infraestructura.Servicios;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class AsignarPagosController : Controller
    {
        IAtenderTramite_Repo AtenderTramite_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        IPagos_Repo Pagos_Repo;
        IUsuarios_Repositorio Usuarios_Repositorio;
        ICorreo_Repo Correo_Repo;

        public AsignarPagosController(IAtenderTramite_Repo atenderTramite_Repo,
            ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
            IMensajesSolicitudes_Repo mensajesSolicitudes_Repo,
            IPagos_Repo pagos_Repo,
            IUsuarios_Repositorio usuarios_Repositorio,
             Correo_Repo correo_Repo)
        {
            AtenderTramite_Repo = atenderTramite_Repo;
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            Pagos_Repo = pagos_Repo;
            Usuarios_Repositorio = usuarios_Repositorio;
            Correo_Repo = correo_Repo;
        }

        /// <summary>
        /// Es para visualizar los pagos asignados a un trámite
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="mensaje"></param>
        /// <param name="Tramite_Solicitados_ID"></param>
        /// <param name="Pago_Solicitudes_ID"></param>
        /// <returns></returns>
        public ActionResult Pagos_Asignados(string returnUrl, string mensaje, int? Tramite_Solicitados_ID, int? Pago_Solicitudes_ID)
        {
            PagosViewAsignacion pagosViewAsignacion = new PagosViewAsignacion();
            List<Pagos_Solicitudes_View> Lista_Pagos = new List<Pagos_Solicitudes_View>();
            List<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalles = new List<Pagos_Solicitudes_Detalle_View>();
            try
            {
                pagosViewAsignacion.returnUrl = returnUrl;
                pagosViewAsignacion.mensaje = mensaje;
                TempData["tramite_solicitado_id"] = Tramite_Solicitados_ID;
                if (Tramite_Solicitados_ID.HasValue)
                {
                    pagosViewAsignacion.Tramite_Solicitados_ID = Tramite_Solicitados_ID.Value;
                    Lista_Pagos = AtenderTramite_Repo.Obtener_Pagos_Registrados(Tramite_Solicitados_ID.Value);
                }
             

                if (Pago_Solicitudes_ID.HasValue)
                {
                    pagosViewAsignacion.Pago_Solicitudes_ID = Pago_Solicitudes_ID.Value;
                    Lista_Pago_Detalles = AtenderTramite_Repo.Obtener_Pagos_Registrados_Detalle(Pago_Solicitudes_ID.Value);
                }


                pagosViewAsignacion.Lista_Pagos = Lista_Pagos;
                pagosViewAsignacion.Lista_Pagos_Detalle = Lista_Pago_Detalles;
            }
            catch (Exception)
            {

            }


            return View(pagosViewAsignacion);
        }

        /// <summary>
        /// Para la página de agregar Pagos listado y búsqueda
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="No_Diverso"></param>
        /// <param name="queryString"></param>
        /// <param name="mensaje"></param>
        /// <param name="Tramite_Solicitados_ID"></param>
        /// <param name="Tramite_ID"></param>
        /// <returns></returns>
        public ActionResult Agregar(  string returnUrl, string No_Diverso, string queryString , string mensaje , int? Tramite_Solicitados_ID, int? Tramite_ID)
        {
            PagosViewAsignacion pagosViewAsignacion = new PagosViewAsignacion();
            List<Pagos_Solicitudes_View> Lista_Pagos = new List<Pagos_Solicitudes_View>();
            List<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalles = new List<Pagos_Solicitudes_Detalle_View>();
            try
            {
                TempData["tramite_solicitado_id"] = Tramite_Solicitados_ID;
                pagosViewAsignacion.returnUrl = returnUrl;
                pagosViewAsignacion.No_Diverso = No_Diverso;
                pagosViewAsignacion.queryString = queryString;
                pagosViewAsignacion.mensaje = mensaje;
                if (Tramite_Solicitados_ID.HasValue)
                    pagosViewAsignacion.Tramite_Solicitados_ID = Tramite_Solicitados_ID.Value;
                else
                    pagosViewAsignacion.Tramite_Solicitados_ID = 0;

                if (Tramite_ID.HasValue)
                    pagosViewAsignacion.Tramite_ID = Tramite_ID.Value;
                else
                    pagosViewAsignacion.Tramite_ID = 0;
                

                if (!String.IsNullOrEmpty(queryString))
                {
                   Lista_Pagos  = AtenderTramite_Repo.Obtener_Datos_Codigo_Barras(queryString);
                }


                if (!String.IsNullOrEmpty(No_Diverso))
                {
                    Lista_Pago_Detalles = AtenderTramite_Repo.Obtener_Datos_Codigo_Barras_Detalle(No_Diverso);
                }

                    pagosViewAsignacion.Lista_Pagos = Lista_Pagos;
                pagosViewAsignacion.Lista_Pagos_Detalle = Lista_Pago_Detalles;

                }
            catch (Exception ex) {

            }

            return View(pagosViewAsignacion);
        }



        [HttpPost]
        public ActionResult Cancelar_Pago(string returnUrl, int Tramite_Solicitados_ID, int  Pago_Solicitudes_ID)
        {
            string mensaje = "";
            Pagos_Solicitudes_View pagos_Solicitudes_View = new Pagos_Solicitudes_View();

            try
            {

                pagos_Solicitudes_View = AtenderTramite_Repo.Obtener_Un_Pago(Pago_Solicitudes_ID);

                if(pagos_Solicitudes_View.Estatus.ToLower() != "pendiente")
                {
                    mensaje =   "Solo se pueden cancelar pagos pendientes";
                    return RedirectToAction("Pagos_Asignados", new { returnUrl,  Tramite_Solicitados_ID, Pago_Solicitudes_ID , mensaje });
                }

            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                return RedirectToAction("Pagos_Asignados", new { returnUrl, Tramite_Solicitados_ID, Pago_Solicitudes_ID, mensaje });
            }


            using (EFDbContext Contexto = new EFDbContext())
            {
                using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
                {
                    try
                    {
                        int cantidadPagosPendientes = Pagos_Repo.Obtner_Pagos_Pagados(Contexto, Tramite_Solicitados_ID);

                        if(cantidadPagosPendientes == 0)
                        {
                            TramitesSolicitudes_Repo.Cambiar_Estatus(Contexto, "EN PROCESO", pagos_Solicitudes_View.Tramite_Solicitados_ID);
                        }

                        
                        AtenderTramite_Repo.Cambiar_Estatus_Pago(Contexto, "CANCELADO", Cls_Sesiones.NOMBRE_COMPLETO, Pago_Solicitudes_ID);
                        transaction.Commit();
                    }
                    catch(Exception ex)
                    {
                        mensaje = ex.Message;
                        RedirectToAction("Pagos_Asignados", new { returnUrl, Tramite_Solicitados_ID, Pago_Solicitudes_ID, mensaje });
                        transaction.Rollback();
                    }
                }
            }



                mensaje = "Proceso Correcto";

            return RedirectToAction("Pagos_Asignados", new { returnUrl, Tramite_Solicitados_ID, mensaje });
        }


        [HttpPost]
        public ActionResult Guardar_Pago(string returnUrl, string queryString, string No_Diverso, int Tramite_Solicitados_ID, int Tramite_ID)
        {
            TramitesSolicitudView tramitesSolicitudView = new TramitesSolicitudView();
            List<Pagos_Solicitudes_View> Lista_Pagos = new List<Pagos_Solicitudes_View>();
            List<Pagos_Solicitudes_Detalle_View> Lista_Pago_Detalle = new List<Pagos_Solicitudes_Detalle_View>();
            string mensaje = "";
            UsuarioView usuarioView = new UsuarioView();
            try {


                Lista_Pagos = AtenderTramite_Repo.Obtener_Un_Diverso(No_Diverso);
                Lista_Pago_Detalle = AtenderTramite_Repo.Obtener_Datos_Codigo_Barras_Detalle(No_Diverso);
                tramitesSolicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);
                usuarioView = Usuarios_Repositorio.Buscar_Usuario(tramitesSolicitudView.Usuarios_ID);

                if (AtenderTramite_Repo.Esta_Repetido_Codigo_Barras(Lista_Pagos[0].Codigo_Barras) == "SI")
                {

                    mensaje = "Código de barras Repetido";
                  return   RedirectToAction("Agregar", new { returnUrl, No_Diverso, queryString, Tramite_Solicitados_ID, Tramite_ID , mensaje   });
                }

            }
            catch(Exception ex)
            {

                mensaje = ex.Message;

               return  RedirectToAction("Agregar", new { returnUrl, No_Diverso, queryString, Tramite_Solicitados_ID, Tramite_ID, mensaje });
            }


            using (EFDbContext Contexto = new EFDbContext())
            {
                using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
                {
                    try
                    {

                        Pagos_Solicitudes_View pagos_Solicitudes_View = Lista_Pagos[0];

                        pagos_Solicitudes_View.Tramite_Solicitados_ID = Tramite_Solicitados_ID;
                        pagos_Solicitudes_View.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                        pagos_Solicitudes_View.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;

                        AtenderTramite_Repo.Agregar_Pagos(Contexto, Tramite_Solicitados_ID, pagos_Solicitudes_View, Lista_Pago_Detalle);
                        TramitesSolicitudes_Repo.Cambiar_Estatus(Contexto, "PENDIENTE PAGO", Tramite_Solicitados_ID);

                        MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();
                        mensajesSolicitudesView.Tramite_Solicitados_ID = Tramite_Solicitados_ID;
                        mensajesSolicitudesView.Nombre_Tramite = tramitesSolicitudView.Nombre_Tramite;
                        mensajesSolicitudesView.Estatus_Solicitud = "PENDIENTE PAGO";
                        mensajesSolicitudesView.Mensaje = "SE LE HA AGREGADO UN CONCEPTO DE PAGO";
                        mensajesSolicitudesView.Entidad = "SIMAPAG";
                        mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                        mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                        MensajesSolicitudes_Repo.Guardar_Mensaje(Contexto, mensajesSolicitudesView);
                        transaction.Commit();

                    }
                    catch(Exception ex)
                    {
                        mensaje = ex.Message;
                        transaction.Rollback();
                        return  RedirectToAction("Agregar", new { returnUrl, No_Diverso ,queryString, Tramite_Solicitados_ID, Tramite_ID , mensaje });
                    }
                }
            }

            string comentario = "SE LE HA AGREGADO UN CONCEPTO DE PAGO <br> con  <b>importe:</b> " + Lista_Pagos[0].Total;
            Correo_Repo.Enviar_Correo_Mensaje(usuarioView, Cls_Sesiones.NOMBRE_COMPLETO, tramitesSolicitudView, comentario);

            mensaje = "Proceso Correcto";

           return RedirectToAction("Agregar", new { returnUrl, queryString, Tramite_Solicitados_ID, Tramite_ID, mensaje });
        }


    }
}