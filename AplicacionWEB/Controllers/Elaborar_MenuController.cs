﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AplicacionWEB.Models;
using ModelDomain.Contexto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.Util;
using System.Data.Entity;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    //[Authorize]
    [SessionExpireAttribute]
    [RoleAuthorize(AplicacionWEB.Models.Enums.Roles.Usuario_Externo)]
    public class Elaborar_MenuController : Controller
    {
        // GET: Elaborar_Menu

        private IMenus_Repositorio ObjMenuRepositorio;
        private IRoles_Repositorio ObjRolesRepositorio;
        private IAcceso_Respositorio ObjAccesoRespositorio;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="p_ObjMenuRepositorio"></param>
        /// <param name="p_ObjRolesRepositorio"></param>
        /// <param name="p_ObjAccesoRespositorio"></param>
        public Elaborar_MenuController(IMenus_Repositorio p_ObjMenuRepositorio, IRoles_Repositorio p_ObjRolesRepositorio, IAcceso_Respositorio p_ObjAccesoRespositorio)
        {
            this.ObjMenuRepositorio = p_ObjMenuRepositorio;
            this.ObjAccesoRespositorio = p_ObjAccesoRespositorio;
            this.ObjRolesRepositorio = p_ObjRolesRepositorio;
        }

        /// <summary>
        ///  Contruye el menú a partir del rol del usuario
        /// </summary>
        /// <returns>
        ///  se redirige a una vista donde se construye las opciones del menu
        /// </returns>
        public PartialViewResult Construir_Menu()
        {
            List<Cls_Modelo_Menu> objNavegacionPapa = new List<Cls_Modelo_Menu>();
            Cls_Modelo_Menu objElementoPrincipal;
            Cls_Modelo_Menu objElementoSecundario;

            List<Cls_Modelo_Menu> objNavegacionHija;

            int usuarioID = Cls_Sesiones.USUARIO_ID;
            int? rolID = Cls_Sesiones.ROL_ID;

            // Obtenemos los ID de los nodo padres de acuerdo al rol
            int?[] idNodoPadre = ObjAccesoRespositorio.Accesos.Include(x => x.Roles).Include(x => x.Menu).
                Where(x => x.Roles.Rol_ID == rolID && x.Roles.Estatus=="ACTIVO" && x.Menu.Menu_Parent.Estatus == "ACTIVO").
                Select(x => x.Menu.Parent_ID).Distinct().ToArray();

            // Obtenemos los ID de los nodo padres de acuerdo al rol -- antigua consulta no se recomienda porque las tablas tienen sus campos de navegacion
            //int?[] idNodoPadre = (from r in ObjRolesRepositorio.Roles
            //                      join a in ObjRolesRepositorio.Accesos
            //                      on r.Rol_ID equals a.Rol_ID
            //                      join m in ObjRolesRepositorio.Menu
            //                      on a.Menu_ID equals m.Menu_ID
            //                      where r.Estatus == "ACTIVO" && r.Rol_ID == rolID  && m.Menu_Parent.Estatus == "ACTIVO"
            //                      select m.Parent_ID).Distinct().ToArray();

            // obtenemos los datos de los nodos padres
            List<Menu> listaMenuPadre = (from menu in ObjMenuRepositorio.Menus
                                         where idNodoPadre.Contains(menu.Menu_ID) && menu.Parent_ID == null
                                         orderby menu.Menu_Descripcion
                                         select menu).ToList();

            // recorre la lista de menus papa
            foreach (Menu MenuPadre in listaMenuPadre)
            {

                objElementoPrincipal = new Cls_Modelo_Menu();
                objElementoPrincipal.Nombre = MenuPadre.Menu_Descripcion;
                objElementoPrincipal.Accion = "";
                objElementoPrincipal.Controlador = "";




                List<Menu> listaMenuHijo = ObjAccesoRespositorio.Accesos.Include(x => x.Roles).Include(x => x.Menu).
                     Where(x => x.Rol_ID == rolID && x.Menu.Parent_ID == MenuPadre.Menu_ID && x.Menu.Estatus=="ACTIVO").OrderBy(x=> x.Menu.Orden).Select(x => x.Menu).ToList();

                //obtenemos los datos de los nodos hijos -- antigua consulta no se recomienda porque las tablas tienen sus campos de navegacion
                //List<Menu> listaMenuHijo = (from menu_hijo in ObjMenuRepositorio.Menus
                //                            join acceso in ObjMenuRepositorio.Acceso
                //                            on menu_hijo.Menu_ID equals acceso.Menu_ID
                //                            orderby menu_hijo.Orden
                //                            where menu_hijo.Parent_ID == MenuPadre.Menu_ID && acceso.Rol_ID == rolID && menu_hijo.Estatus== "ACTIVO"
                //                            select menu_hijo).ToList();

                objNavegacionHija = new List<Cls_Modelo_Menu>();

                foreach (Menu MenuHijo in listaMenuHijo)
                {
                    objElementoSecundario = new Cls_Modelo_Menu();
                    objElementoSecundario.Nombre = MenuHijo.Menu_Descripcion;
                    objElementoSecundario.Accion = MenuHijo.Accion;
                    objElementoSecundario.Controlador = MenuHijo.Controlador;
                    objNavegacionHija.Add(objElementoSecundario);
                }


                objElementoPrincipal.Elementos = objNavegacionHija;
                objNavegacionPapa.Add(objElementoPrincipal);

            }

            return PartialView(objNavegacionPapa);

        }
    }
}