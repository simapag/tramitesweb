﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;
using PagedList;

namespace AplicacionWEB.Controllers
{

    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class AsignarTramiteController : Controller
    {
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        ITramites_Repo Tramites_Repo;
        IUsuarios_Repositorio Usuario_Repo;


        public AsignarTramiteController(ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
            ITramites_Repo tramites_Repo, IUsuarios_Repositorio usuarios_Repositorio) {
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            Tramites_Repo = tramites_Repo;
            Usuario_Repo = usuarios_Repositorio;

        }

        // GET: AsignarTramite
        public ActionResult ListarTramites(string sortOrder, int? currentFilter, int? Tramite_ID, int? page)
        {
         

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NombreTramiteSortParm = String.IsNullOrEmpty(sortOrder) ? "nombre_tramite_desc" : "";

            if (Tramite_ID.HasValue)
            {
                page = 1;
            }
            else {

                Tramite_ID = currentFilter;
            }


            ViewBag.CurrentFilter = Tramite_ID;
            ViewBag.ComboTramites = new SelectList(Tramites_Repo.Obtener_Tramites_Combo(), "id", "name", Tramite_ID);
           


         var dbSolicitudTramite = TramitesSolicitudes_Repo.Tra_Tramites_Solicitados.Where(x=> x.Estatus== "PENDIENTE");

            if (Tramite_ID.HasValue)
            {
                dbSolicitudTramite = dbSolicitudTramite.Where(x => x.Tramite_ID == Tramite_ID.Value);
            }

            switch (sortOrder)
            {
                case "nombre_tramite_desc":
                    dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.Tra_Tramites.Nombre_Tramite);
                    break;
                default:
                    dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.Tra_Tramites.Nombre_Tramite);
                    break;
            }


            int pageSize = 5;
            int pageNumber = (page ?? 1);



            var listaSolicitud = dbSolicitudTramite.Select(x => new TramitesSolicitudView
            {
                Controlador = x.Tra_Tramites.Controlador,
                Costo_Tramite = x.Costo_Tramite,
                Estatus = x.Estatus,
                Estatus_Revision = x.Estatus_Revision,
                Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                Observaciones = x.Observaciones,
                RPU = x.RPU == null ? "" : x.RPU,
                Tramite_ID = x.Tramite_ID,
                Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                Usuarios_ID = x.Usuarios_ID,
                Fecha_Solicitud = x.Fecha_Creo,
                Referencia_Pago = x.Referencia_Pago == null ? "" : x.Referencia_Pago.Trim()
            });

            return View(listaSolicitud.ToPagedList(pageNumber, pageSize));
        }


        [HttpPost]
        public ActionResult ProcesarTramitesSeleccionados(int[] SolicitudesTramiteLista) {


            List<TramitesSolicitudView> Lista_Solicitudes = TramitesSolicitudes_Repo.Obtener_Solicitudes_Seleccionadas(SolicitudesTramiteLista);
            List<UsuarioView> Lista_Ejecutivos = Usuario_Repo.Obtener_Usuarios_Ejecutivos();

            AsignarSolicitudesViewModel asignarSolicitudesViewModel = new AsignarSolicitudesViewModel();
            asignarSolicitudesViewModel.Lista_Solicitudes = Lista_Solicitudes;
            asignarSolicitudesViewModel.Lista_Ejecutivos = Lista_Ejecutivos;

            return View(asignarSolicitudesViewModel);
        }

        public ActionResult Guardar_Tramite(int[] SolicitudesTramiteLista, int UsuarioSelected) {

            string respuesta = TramitesSolicitudes_Repo.Guardar_Asignacion(SolicitudesTramiteLista, UsuarioSelected, Cls_Sesiones.NOMBRE_COMPLETO);

            if (respuesta == "bien")
            {
                ViewBag.Mensaje = "Proceso Correcto";
               return RedirectToAction("ListarTramites");
            }
            else {
                ViewBag.Mensaje = "Ocurrio un Error :" + respuesta;
                return RedirectToAction("ProcesarTramitesSeleccionados", SolicitudesTramiteLista);
            }
        }
    }
}