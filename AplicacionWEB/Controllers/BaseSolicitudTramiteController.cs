﻿using AplicacionWEB.Models.VerificarSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using System.IO;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    /// <summary>
    /// crea una vista parcial , que contiene el nombre del documento y crea un tabla donde se descargan los documentos
    /// </summary>
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class BaseSolicitudTramiteController : Controller
    {
        ITramitesProceso_Repo TramitesProceso_Repo;
        private string nombre_archivo_extension;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;


        public BaseSolicitudTramiteController(ITramitesProceso_Repo tramitesProceso_Repo, ITramitesSolicitudes_Repo tramitesSolicitudes_Repo) {
            this.TramitesProceso_Repo = tramitesProceso_Repo;
            this.TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
        }

        public PartialViewResult CrearVistaDocumentoTitulo(int Tramite_ID , string returnUrl)
        {
            TramitesView tramitesView = TramitesProceso_Repo.Obtener_Un_Tramite(Tramite_ID);
            ViewBag.ReturnUrl = returnUrl;
            return PartialView(tramitesView);
        }

        public PartialViewResult CrearTituloTramite(int Tramite_Solicitados_ID, string returnUrl)
        {
            TramitesSolicitudView tramitesSolicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);
            ViewBag.ReturnUrl = returnUrl;
            return PartialView(tramitesSolicitudView);
        }


        
        public string Obtener_Documentos_En_Una_Solicitud(int offset, int limit, int Tramite_Solicitados_ID) {

            Cls_Paginado<Tra_Tramites_Solicitados_DocumentosView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = TramitesSolicitudes_Repo.Obtener_Documento_Solicitudes(offset, limit, Tramite_Solicitados_ID);
                str_datos = serializer.Serialize(Paginado);

            }
            catch (Exception ex) {
                
            }

            return str_datos;
        }


   
        public string Subir_Archivo_Temporal()
        {
            String Respuesta = "";
            string Nombre_Archivo = "";
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            JavaScriptSerializer configuracionJson = new JavaScriptSerializer();

            try
            {
                Nombre_Archivo = Obtener_Ruta_ParaGuardar(Path.GetFileName(Request.Files[0].FileName));
                Request.Files[0].SaveAs(Nombre_Archivo);
                objMensajeServidor.Mensaje = "bien";
                objMensajeServidor.Dato1 = Path.GetFileName(nombre_archivo_extension);

            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            Respuesta = configuracionJson.Serialize(objMensajeServidor);
            return Respuesta;

        }


        public string Obtener_Ruta_ParaGuardar(string sFileName)
        {
            string respuesta = "";

            if (!Path.IsPathRooted(sFileName))
            {
                sFileName = Path.Combine(Path.GetTempPath(), sFileName);
            }
            String sDateTime = DateTime.Now.ToString("yyyyMMdd\\_HHmmss");
            String s = Path.GetFileNameWithoutExtension(sFileName) + "_" + sDateTime + Path.GetExtension(sFileName);
            sFileName = Path.Combine(Path.GetDirectoryName(sFileName), s);
            nombre_archivo_extension = sFileName;
            Directory.CreateDirectory(Path.GetDirectoryName(sFileName));
            respuesta = sFileName;

            return respuesta;

        }
    }

    
}