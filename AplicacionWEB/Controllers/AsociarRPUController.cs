﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class AsociarRPUController : Controller
    {
        IAsociarRPU_Repo AsociarRPU_Repo;

        public AsociarRPUController(IAsociarRPU_Repo asociarRPU_Repo)
        {
            AsociarRPU_Repo = asociarRPU_Repo;

        }


        public ActionResult Agregar_RPU()
        {
            ViewBag.Usuario_ID = Cls_Sesiones.USUARIO_ID;
            return View();
        }

        
        

        // GET: AsociarRPU
        public ActionResult Listar_RPU()
        {
            ViewBag.Usuario_ID = Cls_Sesiones.USUARIO_ID;

            return View();
        }

        [HttpGet]
        public string Obtener_Datos_Cuenta(string RPU) {

            string str_datos = "";
            string str_informacion;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            try
            {
                List<DatosCuentaView> Lista_Datos_Cuenta = AsociarRPU_Repo.Obtener_Datos_Cuenta(RPU);
                str_informacion = serializer.Serialize(Lista_Datos_Cuenta);

                if (Lista_Datos_Cuenta.Count == 0) {
                    objMensajeServidor.Mensaje = "No se encontro información con el rpu escrito";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

                objMensajeServidor.Mensaje = "bien";
                objMensajeServidor.Dato1 = str_informacion;
              

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }

        public string Obtener_RPU_Asociados(int offset, int limit, int Usuarios_ID) {

            Cls_Paginado<RPUAsociadosView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = AsociarRPU_Repo.Obtener_RPU_Asociados(offset, limit, Usuarios_ID);
                str_datos = serializer.Serialize(Paginado);
            }
            catch (Exception ex) {

            }

            return str_datos;
        }

        [HttpPost]
        public string Registar_RPU_Asociado(RPUAsociadosView rPUAsociadosView)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {

                if(AsociarRPU_Repo.Esta_Repetido_RPU(rPUAsociadosView.RPU,rPUAsociadosView.Usuarios_ID) == "SI")
                {
                    objMensajeServidor.Mensaje = "El RPU ya esta agregado";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

                rPUAsociadosView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                AsociarRPU_Repo.Guardar_RPU_Asociados(rPUAsociadosView);

                List<ComboView> Lista_RPU_Asociados = AsociarRPU_Repo.Obtener_RPU_Asociados_Combo(Cls_Sesiones.USUARIO_ID);

                objMensajeServidor.Mensaje = "bien";
                objMensajeServidor.Dato1 = serializer.Serialize(Lista_RPU_Asociados);

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }

        [HttpPost]
        public string Eliminar_RPU_Asociado(RPUAsociadosView rPUAsociadosView ) {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {
                objMensajeServidor.Mensaje = AsociarRPU_Repo.Eliminar_RPU_Asociados(rPUAsociadosView.RPU_Asociados_ID);

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }


    }
}