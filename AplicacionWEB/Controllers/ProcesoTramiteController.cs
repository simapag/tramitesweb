﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    /// <summary>
    /// crea una lista de trámites que el usuario pueda seleccionar
    /// </summary>
    [SessionExpireAttribute]
    //[Authorize]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class ProcesoTramiteController : Controller
    {
        ITramitesProceso_Repo TramitesProceso_Repo;

        public ProcesoTramiteController(ITramitesProceso_Repo tramitesProceso_Repo) {
            TramitesProceso_Repo = tramitesProceso_Repo;
        }



        // GET: ProcesoTramite
        public ActionResult Listar_Tramites()
        {
            List<TramitesView> Lista_Tramites = new List<TramitesView>() ;
            try
            {
                Lista_Tramites = TramitesProceso_Repo.Obtener_Tramites();

            }
            catch (Exception ex) {

            }
            return View(Lista_Tramites);
        }

    }
}