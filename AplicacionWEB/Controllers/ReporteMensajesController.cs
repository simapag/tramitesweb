﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.Models;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;

namespace AplicacionWEB.Controllers
{

    [SessionExpireAttribute]
    [RoleAuthorize()]

    public class ReporteMensajesController : Controller
    {
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;

        public ReporteMensajesController(IMensajesSolicitudes_Repo mensajesSolicitudes_Repo)
        {
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
        }


        public ActionResult Mis_Mensajes(string sortOrder, string currentFechaInicio, string FechaInicio, string currentFechaFin, string FechaFin, int? page)
        {
            IQueryable<ReporteMensajesView> listaMensaje;
            int pageSize = 10;
            int pageNumber = 0;

            try
            {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.FechaSortParam = String.IsNullOrEmpty(sortOrder) ? "fecha_asc" : "";


                if (FechaInicio != null)
                {
                    page = 1;
                }
                else
                {
                    FechaInicio = currentFechaInicio;
                }


                if (FechaFin != null)
                {
                    page = 1;
                }
                else
                {
                    FechaFin = currentFechaFin;
                }

                ViewBag.CurrentFechaInicio = FechaInicio;
                ViewBag.CurrentFechaFin = FechaFin;


                pageNumber = (page ?? 1);

                listaMensaje = MensajesSolicitudes_Repo.Obtener_Reporte_Mensaje(sortOrder, FechaInicio, FechaFin, Cls_Sesiones.USUARIO_ID);


            }
            catch(Exception ex)
            {
                listaMensaje = Enumerable.Empty<ReporteMensajesView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }

            return View(listaMensaje.ToPagedList(pageNumber, pageSize));

        }

    }
}