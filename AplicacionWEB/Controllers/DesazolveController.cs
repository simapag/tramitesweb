﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;
using ModelDomain.Contexto;
using System.Data.Entity;
using System.Configuration;
using Newtonsoft.Json;
using AplicacionWEB.Models.Enums;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]
    public class DesazolveController : Controller
    {
        ITramitesProceso_Repo TramitesProceso_Repo;
        ITramites_Documento_Repo Tramites_Documento_Repo;
        IAsociarRPU_Repo AsociarRPU_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        IRoles_Repositorio Roles_Repositorio;
        ITramites_Repo Tramite_Repositorio;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;

        public DesazolveController(ITramitesProceso_Repo tramitesProceso_Repo,
              IAsociarRPU_Repo asociarRPU_Repo,
              ITramites_Documento_Repo tramites_Documento_Repo,
              ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
              IRoles_Repositorio roles_Repositorio,
              ITramites_Repo tramites_Repo,
              IMensajesSolicitudes_Repo mensajesSolicitudes_Repo)
        {
            this.TramitesProceso_Repo = tramitesProceso_Repo;
            this.AsociarRPU_Repo = asociarRPU_Repo;
            this.Tramites_Documento_Repo = tramites_Documento_Repo;
            this.TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            this.Roles_Repositorio = roles_Repositorio;
            this.Tramite_Repositorio = tramites_Repo;
            this.MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
        }

        // GET: Desazolve
        [HttpGet]
        public ActionResult Crear(int Tramite_ID)
        {
            TramitesSolicitudView solicitudView = new TramitesSolicitudView();

            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                TramitesView tramitesView = TramitesProceso_Repo.Obtener_Un_Tramite(Tramite_ID);
                List<ComboView> Lista_Documentos = Tramites_Documento_Repo.Obtener_Documentos_Por_Tramite(Tramite_ID);
                solicitudView.Tramite_ID = Tramite_ID;
                solicitudView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                solicitudView.RPU_Requerido = true;
                solicitudView.Estatus = "PENDIENTE";
                solicitudView.Estatus_Revision = "PENDIENTE";
                solicitudView.Seleccionado = true;
                solicitudView.Costo_Tramite = tramitesView.Costo_Tramite;
                solicitudView.Nombre_Tramite = tramitesView.Nombre_Tramite;
                List<ComboView> Lista_RPU_Asociados = AsociarRPU_Repo.Obtener_RPU_Asociados_Combo(Cls_Sesiones.USUARIO_ID);
                ViewBag.Lista_RPU_Asociados = serializer.Serialize(Lista_RPU_Asociados);
                ViewBag.Lista_Documentos = serializer.Serialize(Lista_Documentos);

                FormServicioOperativoView formServicioOperativoView = new FormServicioOperativoView();
                ViewBag.Formulario_Operativo = serializer.Serialize(formServicioOperativoView);

            }
            catch(Exception ex)
            {

            }

            return View("FormDesazolve",solicitudView);
        }
        [HttpGet]
        public ActionResult Editar(int Tramite_Solicitados_ID, string returnUrl)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            TramitesSolicitudView solicitudView = new TramitesSolicitudView();

            FormServicioOperativoView formServicioOperativoView;

            try
            {
                TempData["tramite_solicitado_id"] = Tramite_Solicitados_ID;
                solicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(Tramite_Solicitados_ID);
                solicitudView.RPU_Requerido = true;
                solicitudView.Seleccionado = true;

                if (Cls_Sesiones.ROL.ToLower() == "ejecutivo")
                {
                    solicitudView.Seleccionado = false;
                }

                List<ComboView> Lista_Documentos = Tramites_Documento_Repo.Obtener_Documentos_Por_Tramite(solicitudView.Tramite_ID);
                List<ComboView> Lista_RPU_Asociados = AsociarRPU_Repo.Obtener_RPU_Asociados_Combo(Cls_Sesiones.USUARIO_ID);
                ViewBag.Lista_RPU_Asociados = serializer.Serialize(Lista_RPU_Asociados);
                ViewBag.Lista_Documentos = serializer.Serialize(Lista_Documentos);

                formServicioOperativoView = new FormServicioOperativoView();
                formServicioOperativoView = TramitesProceso_Repo.Obtener_Formulario_Servicio_Operativo(solicitudView.Tramite_Solicitados_ID);
                ViewBag.Formulario_Operativo = serializer.Serialize(formServicioOperativoView);
                ViewBag.ReturnUrl = returnUrl;
            }
            catch (Exception ex)
            {


            }

            return View("FormDesazolve", solicitudView);
        }



        [HttpPost]
        public string Editar(string strSolicitudTramites, string strListaDocumentos, string strFormulario)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
            EFDbContext Contexto = new EFDbContext();
            string rutaPrimeraParte = HttpContext.Server.MapPath("~/Documentos/");
            string urlArchivo = ConfigurationManager.AppSettings["url_documento"];
            MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();

            TramitesSolicitudView tramitesSolicitudView;
            List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos;
            FormServicioOperativoView formServicioOperativoView;

            using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
            {

                try
                {
                    strSolicitudTramites = strSolicitudTramites.Replace(@"\", "");
                    tramitesSolicitudView = JsonConvert.DeserializeObject<TramitesSolicitudView>(strSolicitudTramites);
                    strListaDocumentos = strListaDocumentos.Replace(@"\", "");
                    listaDocumentos = JsonConvert.DeserializeObject<List<Tra_Tramites_Solicitados_DocumentosView>>(strListaDocumentos);
                    formServicioOperativoView = JsonConvert.DeserializeObject<FormServicioOperativoView>(strFormulario);

                    TramitesSolicitudes_Repo.Guardar_Archivos(listaDocumentos, rutaPrimeraParte, urlArchivo);
                    tramitesSolicitudView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                    TramitesSolicitudes_Repo.Editar_Solicitud_Tramite(Contexto, tramitesSolicitudView, listaDocumentos);

                    TramitesProceso_Repo.Editar_Datos_Servicio_Operativo(Contexto, formServicioOperativoView);

                    mensajesSolicitudesView.Estatus_Solicitud = tramitesSolicitudView.Estatus_Revision;
                    mensajesSolicitudesView.Nombre_Tramite = TramitesSolicitudes_Repo.Obtener_Nombre_Tramite(Contexto, tramitesSolicitudView.Tramite_Solicitados_ID);
                    mensajesSolicitudesView.Entidad = "USUARIO";
                    mensajesSolicitudesView.Mensaje = "EDICIÓN DE LA SOLICTUD DE TRÁMITE";
                    mensajesSolicitudesView.Tramite_Solicitados_ID = tramitesSolicitudView.Tramite_Solicitados_ID;
                    mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                    mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                    mensajesSolicitudesView.Visto = false;

                    MensajesSolicitudes_Repo.Guardar_Mensaje(Contexto, mensajesSolicitudesView);

                    transaction.Commit();
                    objMensajeServidor.Mensaje = "bien";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    objMensajeServidor.Mensaje = ex.Message;
                }
                finally
                {
                    Contexto.Dispose();
                }
            }

            str_datos = serializer.Serialize(objMensajeServidor);

            return str_datos;

        }

        [HttpPost]
        public string Guardar(string strSolicitudTramites, string strListaDocumentos, string strFormulario)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";
            EFDbContext Contexto = new EFDbContext();
            string rutaPrimeraParte = HttpContext.Server.MapPath("~/Documentos/");
            string urlArchivo = ConfigurationManager.AppSettings["url_documento"];
            int Tramite_Solicitados_ID = 0;

            TramitesSolicitudView tramitesSolicitudView;
            List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos;
            FormServicioOperativoView formServicioOperativoView;

            using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
            {

                try
                {
                    tramitesSolicitudView = JsonConvert.DeserializeObject<TramitesSolicitudView>(strSolicitudTramites);
                    listaDocumentos = JsonConvert.DeserializeObject<List<Tra_Tramites_Solicitados_DocumentosView>>(strListaDocumentos);
                    formServicioOperativoView = JsonConvert.DeserializeObject<FormServicioOperativoView>(strFormulario);

                    TramitesSolicitudes_Repo.Guardar_Archivos(listaDocumentos, rutaPrimeraParte, urlArchivo);
                    tramitesSolicitudView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                    Tramite_Solicitados_ID = TramitesSolicitudes_Repo.Guardar_Solicitud_Tramite(Contexto, tramitesSolicitudView, listaDocumentos);
                    formServicioOperativoView.Tramite_Solicitados_ID = Tramite_Solicitados_ID;

                    TramitesProceso_Repo.Guardar_Datos_Servicio_Operativo(Contexto, formServicioOperativoView);

                    transaction.Commit();
                    objMensajeServidor.Mensaje = "bien";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    objMensajeServidor.Mensaje = ex.Message;
                }
                finally
                {
                    Contexto.Dispose();
                }
            }

            str_datos = serializer.Serialize(objMensajeServidor);

            return str_datos;

        }


        public string Obtener_Informacion_RPU(string RPU)
        {
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            try
            {
                List<DatosCuentaView> datosCuentas = AsociarRPU_Repo.Obtener_Datos_Cuenta(RPU);

                if (datosCuentas.Count > 0)
                {
                    objMensajeServidor.Mensaje = "bien";
                    string domicilio = datosCuentas[0].Colonia.Trim() + " " + datosCuentas[0].Calle.Trim() + " No.Ext. " + datosCuentas[0].Numero_Exterior.Trim();

                    if (datosCuentas[0].Numero_Interior.Trim().Length > 0)
                    {

                        domicilio += "No.Int. " + datosCuentas[0].Numero_Interior.Trim();
                    }

                    objMensajeServidor.Dato1 = datosCuentas[0].Nombre_Usuario.Trim();
                    objMensajeServidor.Dato2 = domicilio.Trim();
                    objMensajeServidor.Dato3 = datosCuentas[0].No_Cuenta.Trim();
                }
                else
                {
                    objMensajeServidor.Mensaje = "No encontro información";
                }


            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }
            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }



    

    }
}