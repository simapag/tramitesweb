﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;


namespace AplicacionWEB.Controllers
{
    public class CostosController : Controller
    {
        ICostos_Repo Costos_Repo;


        public CostosController(ICostos_Repo costos_Repo)
        {
            this.Costos_Repo = costos_Repo;
        }

        // GET: Costos
        public ActionResult Index()
        {
            return View();
        }



        public string Obtener_Costo_Desazolve(int offset, int limit)
        {
            Cls_Paginado<CostosView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = Costos_Repo.Obtener_Costo_Desazolve(offset, limit);
                str_datos = serializer.Serialize(Paginado);

            }
            catch(Exception ex)
            {

            }

            return str_datos;
        }

        public string Obtener_Costo_Factibilidad(int offset, int limit)
        {
            Cls_Paginado<CostosView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = Costos_Repo.Obtener_Costo_Factibilidad(offset, limit);
                str_datos = serializer.Serialize(Paginado);
            }
            catch (Exception ex) {

            }

            return str_datos;
        }
    }
}