﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using Newtonsoft.Json;
using AplicacionWEB.Models.VerificarSession;
using PagedList;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    //[Authorize]
    [RoleAuthorize()]
    public class TramitesPendientesController : Controller
    {

        IAtenderTramite_Repo AtenderTramite_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        ITramites_Repo Tramites_Repo;

        public TramitesPendientesController(IAtenderTramite_Repo atenderTramite_Repo,
          IMensajesSolicitudes_Repo mensajesSolicitudes_Repo, ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
             ITramites_Repo tramites_Repo)
        {
            AtenderTramite_Repo = atenderTramite_Repo;
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            Tramites_Repo = tramites_Repo;
        }


        public ActionResult Listado(string sortOrder, string currentFilter, string searchString, string currentFechaInicio, string FechaInicio, string currentFechaFin, string FechaFin, string currentEstatus, string estatus ,int? currentTramite_ID, int? tramite_ID, int? page)
        {
            IQueryable<TramitesSolicitudView> Lista_Tramites;
            int pageSize = 10;
            int pageNumber = 0;

            try
            {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.FechaSortParam = String.IsNullOrEmpty(sortOrder) ? "fecha_asc" : "";
                ViewBag.FolioSortParam = sortOrder == "Folio" ? "folio_desc" : "Folio";
                ViewBag.RPUSortParam = sortOrder == "RPU" ? "rpu_desc" : "RPU";
                ViewBag.EstatusSortParam = sortOrder == "Estatus" ? "estatus_desc" : "Estatus";
                ViewBag.NombreTramiteSortParam = sortOrder == "Nombre_Tramite" ? "nombre_tramite_desc" : "Nombre_Tramite";

                var dbSolicitudTramite = TramitesSolicitudes_Repo.Tra_Tramites_Solicitados.Where(x=> x.Usuarios_Asignado_ID == Cls_Sesiones.USUARIO_ID);


                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }


                if (FechaInicio != null)
                {
                    page = 1;
                }
                else
                {
                    FechaInicio = currentFechaInicio;
                }


                if (FechaFin != null)
                {
                    page = 1;
                }
                else
                {
                    FechaFin = currentFechaFin;
                }


                if(estatus != null)
                {
                    page = 1;
                }
                else
                {
                    estatus = currentEstatus;
                }


                if (tramite_ID.HasValue)
                {
                    page = 1;
                }
                else
                {

                    tramite_ID = currentTramite_ID;
                }

                ViewBag.CurrentTramite_ID = tramite_ID;
                ViewBag.ComboTramites = Tramites_Repo.Obtener_Tramites_Combo_Especial();

                ViewBag.ComboEstatus = TramitesSolicitudes_Repo.Obtener_Estatus_Solicitud();
                ViewBag.CurrentEstatus = estatus;


                ViewBag.CurrentFechaInicio = FechaInicio;
                ViewBag.CurrentFechaFin = FechaFin;
                ViewBag.CurrentFilter = searchString;
            
             


                if (!String.IsNullOrEmpty(searchString))
                {

                    dbSolicitudTramite = dbSolicitudTramite.Where(x =>
                            x.Tramite_Solicitados_ID.ToString().Contains(searchString)
                            || x.RPU.Contains(searchString)
                            || x.Usuarios.Nombre.ToLower().Contains(searchString.ToLower())
                            || x.Usuarios.Apellido_Paterno.ToLower().Contains(searchString)
                            || x.Usuarios.Apellido_Materno.ToLower().Contains(searchString)
                    );
                }


                if (tramite_ID.HasValue)
                {
                    dbSolicitudTramite = dbSolicitudTramite.Where(x => x.Tramite_ID == tramite_ID.Value);
                }

                if (!String.IsNullOrEmpty(estatus))
                {

                    dbSolicitudTramite = dbSolicitudTramite.Where(x =>
                            x.Estatus_Revision == estatus
                    );
                }


                if (!String.IsNullOrEmpty(FechaInicio))
                {
                    DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                    dbSolicitudTramite = dbSolicitudTramite.Where(x => x.Fecha_Creo >= dateTimeIncio);
                }

                if (!String.IsNullOrEmpty(FechaFin))
                {
                    DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                    DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                    dbSolicitudTramite = dbSolicitudTramite.Where(x => x.Fecha_Creo <= dateTimeFinMas);
                }


                switch (sortOrder)
                {
                    case "Folio":
                        dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.Tramite_Solicitados_ID);
                        break;
                    case "RPU":
                        dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.RPU);
                        break;
                    case "Estatus":
                        dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.Estatus);
                        break;
                    case "Nombre_Tramite":
                        dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.Tra_Tramites.Nombre_Tramite);
                        break;
                    case "nombre_tramite_desc":
                        dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.Tra_Tramites.Nombre_Tramite);
                        break;
                    case "folio_desc":
                        dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.Tramite_Solicitados_ID);
                        break;
                    case "rpu_desc":
                        dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.RPU);
                        break;
                    case "estatus_desc":
                        dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.Estatus);
                        break;
                    case "fecha_asc":
                        dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.Fecha_Creo);
                        break;
                    default:
                        dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.Fecha_Creo);
                        break;
                }


                pageNumber = (page ?? 1);
                Lista_Tramites = dbSolicitudTramite.Select(x => new TramitesSolicitudView
                {
                    Controlador = x.Tra_Tramites.Controlador,
                    Costo_Tramite = x.Costo_Tramite,
                    Estatus = x.Estatus,
                    Estatus_Revision = x.Estatus_Revision,
                    Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                    Observaciones = x.Observaciones,
                    RPU = x.RPU == null ? "" : x.RPU,
                    Tramite_ID = x.Tramite_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Usuarios_ID = x.Usuarios_ID,
                    Fecha_Solicitud = x.Fecha_Creo,
                    Referencia_Pago = x.Referencia_Pago == null ? "" : x.Referencia_Pago.Trim(),
                    Usuario = x.Usuarios.Nombre + " " + x.Usuarios.Apellido_Paterno + " " + (x.Usuarios.Apellido_Materno == null ? "" : x.Usuarios.Apellido_Materno),
                    Telefono = x.Usuarios.Telefono_Celular == null ? "" : x.Usuarios.Telefono_Celular,
                    NumeroMensajes = x.Tra_Mensajes_Solicitudes.Count,
                    NumeroPago = x.Tra_Pagos_Solicitudes.Where(y => y.Estatus != "CANCELADO").Count()
                });



            }
            catch (Exception ex) {
                Lista_Tramites = Enumerable.Empty<TramitesSolicitudView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }


            return View(Lista_Tramites.ToPagedList(pageNumber,pageSize));
        }
    }
}