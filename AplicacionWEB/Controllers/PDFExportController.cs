﻿using AplicacionWEB.Models;
using AplicacionWEB.Models.Enums;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.ReportePDF;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionWEB.Controllers
{

    
    public class PDFExportController : Controller
    {

        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        IAsociarRPU_Repo AsociarRPU_Repo;
        IPagoLinea PagoLinea_Repo;


        public PDFExportController(ITramitesSolicitudes_Repo tramitesSolicitudes_Repo,
            IAsociarRPU_Repo asociarRPU_Repo, IPagoLinea pagoLinea)
        {
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            AsociarRPU_Repo = asociarRPU_Repo;
            PagoLinea_Repo = pagoLinea;
        }

        public ActionResult Generar_Ticket(string RPU) {
            List<ConsultaSaldoView> Lista_Saldo = new List<ConsultaSaldoView>();
            List<DatosCuentaView> Lista_Datos_Cuenta;
            DatosCuentaView datosCuentaView;
            DatosPagoView datosPagoView;
            string No_Factura_Recibo = "";
            string Predio_ID;
            decimal Total_Pagar=0;
            try
            {

              
              Lista_Datos_Cuenta = AsociarRPU_Repo.Obtener_Datos_Cuenta(RPU);

                if (Lista_Datos_Cuenta.Count > 0)
                {
                    datosCuentaView = Lista_Datos_Cuenta[0];
                    No_Factura_Recibo = PagoLinea_Repo.Obtener_NoFactura_Actual(datosCuentaView.RPU);
                    Predio_ID = PagoLinea_Repo.Obtener_Predio_ID(datosCuentaView.RPU);
                    Total_Pagar = PagoLinea_Repo.Obtener_Total_Pagar(Predio_ID);
                    Total_Pagar = Cls_Util.redondearTotalPagar(Total_Pagar, true);
                    datosPagoView = PagoLinea_Repo.Obtener_Datos_Pagos(No_Factura_Recibo);
                }
                else
                {
                    Total_Pagar = 0;
                    No_Factura_Recibo = "";
                    datosCuentaView = new DatosCuentaView();
                    datosPagoView = new DatosPagoView();
                }

                string nombre_archivo = "ticket.pdf";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                Ticket ticket = new Ticket(ruta_almacenamiento, datosCuentaView, datosPagoView, Total_Pagar, No_Factura_Recibo);
                ticket.IniciarReporte();
                FileStream fss = new FileStream(ruta_almacenamiento, FileMode.Open);
                byte[] bytes = new byte[fss.Length];
                fss.Read(bytes, 0, Convert.ToInt32(fss.Length));
                fss.Close();
                System.IO.File.Delete(ruta_almacenamiento);

                Response.ClearContent();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Length", bytes.Length.ToString());
                Response.Flush();
                Response.BinaryWrite(bytes);
                Response.End();

            }
            catch(Exception ex)
            {


            }

            return null;

        }

        [SessionExpireAttribute]
        [RoleAuthorize(Roles.Usuario_Externo)]
        public ActionResult Generar_Mis_Tramites(string sortOrder, string searchString)
        {
            try
            {
                string nombre_archivo = "reporte_mis_tramites.pdf";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                List<TramitesSolicitudView> Lista_Solicitudes = TramitesSolicitudes_Repo.Obtener_Solicitudes_Por_Usuario(sortOrder, searchString, Cls_Sesiones.USUARIO_ID).ToList();

                Reporte_Mis_Tramites reporte_Mis_Tramites = new Reporte_Mis_Tramites(ruta_almacenamiento, Lista_Solicitudes);
                reporte_Mis_Tramites.IniciarReporte();

                FileStream fss = new FileStream(ruta_almacenamiento, FileMode.Open);
                byte[] bytes = new byte[fss.Length];
                fss.Read(bytes, 0, Convert.ToInt32(fss.Length));
                fss.Close();
                System.IO.File.Delete(ruta_almacenamiento);

                Response.ClearContent();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Length", bytes.Length.ToString());
                Response.Flush();
                Response.BinaryWrite(bytes);
                Response.End();


            }
            catch(Exception ex)
            {

            }

            return null;
        }

        private string obtenerRutaParaGuardar(string sFileName)
        {
            string respuesta = "";

            if (!Path.IsPathRooted(sFileName))
            {
                sFileName = Path.Combine(Path.GetTempPath(), sFileName);
            }

            if (System.IO.File.Exists(sFileName))
            {
                String sDateTime = DateTime.Now.ToString("yyyyMMdd\\_HHmmss");
                String s = Path.GetFileNameWithoutExtension(sFileName) + "_" + sDateTime + Path.GetExtension(sFileName);
                sFileName = Path.Combine(Path.GetDirectoryName(sFileName), s);
            }
            else
            {
                Directory.CreateDirectory(Path.GetDirectoryName(sFileName));
            }
            respuesta = sFileName;
            return respuesta;

        }

    }



}