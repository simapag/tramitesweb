﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Web.Script.Serialization;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    //[Authorize]
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class Asignar_RolController : Controller
    {
        // GET: Asignar_Rol

        IUsuarios_Repositorio ObjUsuarioRepositorio;
        IRoles_Repositorio ObjRolesRepositorio;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_ObjUsuarioRepositorio"></param>
        /// <param name="p_ObjRolesRepositorio"></param>
        public Asignar_RolController(IUsuarios_Repositorio p_ObjUsuarioRepositorio, IRoles_Repositorio p_ObjRolesRepositorio) {
            this.ObjUsuarioRepositorio = p_ObjUsuarioRepositorio;
            this.ObjRolesRepositorio = p_ObjRolesRepositorio;

        }

        /// <summary>
        /// Consulta para que devuelve una lista de usuarios
        /// </summary>
        /// <param name="offset">ese un parámetro del tabla bootstrap en la página donde inicia</param>
        /// <param name="limit">ese un parámetro del tabla bootstrap total de renglones a tomar</param>
        /// <param name="search">ese un parámetro del tabla bootstrap la cajita de búsqueda</param>
        /// <returns>
        ///  regresa la lista de usuario en json
        /// </returns>
        public JsonResult Obtener_Usuarios(int offset, int limit, string search = null)
        {

            var usuarioRepositorio = ObjUsuarioRepositorio.Usuarios;
            IEnumerable<Usuarios> usuariosFiltro;

            if (!string.IsNullOrEmpty(search))
            {
                usuariosFiltro = ObjUsuarioRepositorio.Usuarios.Where(x => x.Nombre.Contains(search)
                || x.Apellido_Materno.Contains(search) || x.Apellido_Paterno.Contains(search)
                || x.Correo_Electronico.Contains(search));
            }
            else
            {
                usuariosFiltro = usuarioRepositorio;
            }

            var empleadosDesplegar = (usuariosFiltro.Skip(offset)
                                                     .Take(limit)
                                                     .OrderBy(u => u.Nombre).ThenBy(u => u.Apellido_Paterno)).ToList();

            var resultado = from u in empleadosDesplegar
                            select new { id = u.Usuarios_ID, nombre = u.Nombre, apellido_paterno = u.Apellido_Paterno, apellido_materno = u.Apellido_Materno, correo = u.Correo_Electronico, direccion = u.Direccion, telefono_celular = u.Telefono_Celular, telefono_oficina = u.Telefono_Oficina, estatus = u.Estatus, id_rol= u.Roles == null ? 0: u.Roles.Rol_ID , password = u.Password == null ? "": u.Password };

            return Json(new
            {
                total = usuariosFiltro.Count(),
                rows = resultado

            }, JsonRequestBehavior.AllowGet);

        }





        /// <summary>
        ///  Muestra la vista Asignación de rol
        /// </summary>
        /// <returns>
        ///  Redirige la a vista correspondiente
        /// </returns>
        public ViewResult Listar_Asignacion_Rol() {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var objRoles = from r in ObjRolesRepositorio.Roles
                           where r.Estatus == "ACTIVO"
                           orderby r.Nombre ascending
                           select new { id = r.Rol_ID, name = r.Nombre };


            var objUsuario = (from u in ObjUsuarioRepositorio.Usuarios
                              where u.Usuarios_ID == Cls_Sesiones.USUARIO_ID
                              select new { id = u.Usuarios_ID, nombre = u.Nombre, apellido_paterno = u.Apellido_Paterno, apellido_materno = u.Apellido_Materno, correo = u.Correo_Electronico, direccion = u.Direccion, telefono_celular = u.Telefono_Celular, telefono_oficina = u.Telefono_Oficina, estatus = u.Estatus, id_rol = u.Roles == null ? 0 : u.Roles.Rol_ID, password = u.Password == null ? "" : u.Password });

            ViewBag.Roles = serializer.Serialize(objRoles);
            ViewBag.Usuario = serializer.Serialize(objUsuario);

            return View();
        }


        /// <summary>
        ///  Guarda el password y la asignación del rol a un usuario
        /// </summary>
        ///  <param name="p_Modelo_Asignacion_Roles">Modelo Asignacion Roles</param>
        /// <returns>
        ///    nos indica si fue exitoso o no el procesos
        /// </returns>

        [HttpPost]
        public string Guardar_Asignacion(Cls_Asignar_Rol_Modelo p_Modelo_Asignacion_Roles) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string strDatos;

            try
            {

                var buscarContraseña = (from u in ObjUsuarioRepositorio.Usuarios
                                        where u.Password == p_Modelo_Asignacion_Roles.Password
                                        select u).FirstOrDefault();

                if (buscarContraseña != null) {
                    objMensajeServidor.Mensaje = "La constraseña escrita ya existe, proporciona otra";
                    strDatos = serializer.Serialize(objMensajeServidor);
                    return strDatos;
                }


                Usuarios usuario = (from u in ObjUsuarioRepositorio.Usuarios
                                    where u.Usuarios_ID == p_Modelo_Asignacion_Roles.Usuario_ID
                                    select u).FirstOrDefault();

                usuario.Rol_ID = Convert.ToInt16(p_Modelo_Asignacion_Roles.SeleccionRol.id);
                usuario.Password = p_Modelo_Asignacion_Roles.Password;

                ObjUsuarioRepositorio.Guardar_Usuarios(usuario);
                objMensajeServidor.Mensaje = "bien";

            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }


            strDatos = serializer.Serialize(objMensajeServidor);
            return strDatos;


        }


    }
}