﻿using AplicacionWEB.Models.VerificarSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using System.Data.Entity;
using AplicacionWEB.Models.Enums;
using AplicacionWEB.Models;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize(Roles.Usuario_Externo)]

    public class InicioController : Controller
    {
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;

        public InicioController(IMensajesSolicitudes_Repo mensajesSolicitudes_Repo) {
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
        }

        // GET: Inicio
        public ActionResult Principal()
        {
            List<MensajesSolicitudesView> Lista_Mensajes = new List<MensajesSolicitudesView>();

            ViewBag.Rol_Usuario = Cls_Sesiones.ROL.ToLower();

            if (Cls_Sesiones.ROL.ToLower() == "ejecutivo") {
                Lista_Mensajes = MensajesSolicitudes_Repo.Obtener_Mensaje_Ejecutivo(Cls_Sesiones.USUARIO_ID);
                MensajesSolicitudes_Repo.Actualizar_Mensaje_Ejecutivo(Cls_Sesiones.USUARIO_ID);
            }
            else
            {
                Lista_Mensajes = MensajesSolicitudes_Repo.Obtener_Mensajes_Usuario(Cls_Sesiones.USUARIO_ID);
                MensajesSolicitudes_Repo.Actualizar_Mensaje_Usuario(Cls_Sesiones.USUARIO_ID);
            }

          

            return View(Lista_Mensajes);
        }
    }
}