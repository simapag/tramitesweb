﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;
using AplicacionWEB.Models.Enums;
using AplicacionWEB.Infraestructura.Servicios;
using System.Text.RegularExpressions;
using ModelDomain.Contexto;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace AplicacionWEB.Controllers
{

    public class PagoReciboController : Controller
    {
        IAsociarRPU_Repo AsociarRPU_Repo;
        // consultas para visualizar informacion del recibo se conecta a la base del siac
        IPagoLinea PagoLinea_Repo;
        // esta son consultas a las tabla donde se almacena las respuesta de los bancos
        IPagosEnLinea_Repo PagosEnLinea_Repo;
        // consultas que hacen para hacer los pagos de los trámites
        IPagos_Repo Pagos_Repo;
        // repositorio donde se registrar los pagos que no se alacanzan a registrar;
        IPagos_Linea_No_Registrados_Repo Pagos_No_Aplicados_Repo;
        ICorreo_Repo Correo_Repo;

        public PagoReciboController(IAsociarRPU_Repo asociarRPU_Repo, IPagoLinea pagoLinea, IPagosEnLinea_Repo pagosEnLinea_Repo,
            IPagos_Repo pagos_Repo, ICorreo_Repo correo_Repo, IPagos_Linea_No_Registrados_Repo pagos_Linea_No_Registrados_Repo)
        {
            AsociarRPU_Repo = asociarRPU_Repo;
            PagoLinea_Repo = pagoLinea;
            PagosEnLinea_Repo = pagosEnLinea_Repo;
            Pagos_Repo = pagos_Repo;
            Correo_Repo = correo_Repo;
            Pagos_No_Aplicados_Repo = pagos_Linea_No_Registrados_Repo;
        }

        private bool sonNumeros(string cadena)
        {
            Regex objPattern = new Regex("^[0-9]+$");
            return objPattern.IsMatch(cadena);
        }


        private string Obtener_Mes(int mes)
        {
            string resultado = "";

            switch (mes)
            {
               case 1:
                     resultado= "ENERO";
                   break;
                case 2:
                    resultado = "FEBRERO";
                    break;
                case 3:
                    resultado = "MARZO";
                    break;
                case 4:
                    resultado = "ABRIL";
                    break;
                case 5:
                    resultado = "MAYO";
                    break;
                case 6:
                    resultado = "JUNIO";
                    break;
                case 7:
                    resultado = "JULIO";
                    break;
                case 8:
                    resultado = "AGOSTO";
                    break;
                case 9:
                    resultado = "SEPTIEMBRE";
                    break;
                case 10:
                    resultado = "OCTUBRE";
                    break;
                case 11:
                    resultado = "NOVIEMBRE";
                    break;
                case 12:
                    resultado = "DICIEMBRE";
                    break;
                default:
                    resultado = "";
                    break;
        
            }


            return resultado;
        }

        // GET: PagoRecibo
        [SessionExpireAttribute]
        [RoleAuthorize(Roles.Usuario_Externo)]
        public ActionResult Listado()
        {
            List<RPUAsociadosView> Lista; 
            try
            {
                Lista = AsociarRPU_Repo.Obtener_RPU_Asociados(Cls_Sesiones.USUARIO_ID);

            }
            catch(Exception ex)
            {
                Lista = new List<RPUAsociadosView>();
            }


            return View(Lista);
        }


        [SessionExpireAttribute]
        [RoleAuthorize(Roles.Usuario_Externo)]
        public ActionResult RealizarPago(string RPU)
        {
            PagosLineaView pagosLineaView;
            DatosPagoView datosPagoView;
            string No_Factura_Recibo;
            string Predio_ID;

            decimal Total_Pagar;
            try
            {

                if (!string.IsNullOrEmpty(RPU) && sonNumeros(RPU))
                {
                    pagosLineaView = new PagosLineaView();           
                    List<DatosCuentaView> Lista_Datos_Cuenta = AsociarRPU_Repo.Obtener_Datos_Cuenta(RPU);
                    List<App_ConfiguracionView> Lista_Configuracion = PagoLinea_Repo.Obtener_Configuracion();

                    if(Lista_Configuracion.Count > 0)
                    {
                        pagosLineaView.ConfiguracionView = Lista_Configuracion[0];
                    }

                    if(Lista_Datos_Cuenta.Count > 0)
                    {

                        pagosLineaView.Datos_Cuenta = Lista_Datos_Cuenta[0];
                        No_Factura_Recibo = PagoLinea_Repo.Obtener_NoFactura_Actual(pagosLineaView.Datos_Cuenta.RPU);
                        Predio_ID = PagoLinea_Repo.Obtener_Predio_ID(pagosLineaView.Datos_Cuenta.RPU);
                        // este es total a pagar sin la suma de diversos, convenios, sanciones
                        Total_Pagar = PagoLinea_Repo.Obtener_Total_Pagar(Predio_ID);
                        Total_Pagar = Cls_Util.redondearTotalPagar(Total_Pagar, true);
                        datosPagoView = PagoLinea_Repo.Obtener_Datos_Pagos(No_Factura_Recibo);
                        pagosLineaView.Total = Total_Pagar;
                        pagosLineaView.Datos_Pago = datosPagoView;
                        pagosLineaView.Form_Multipagos = PagoLinea_Repo.Obtener_Parametros_Pago(Total_Pagar, No_Factura_Recibo, Lista_Datos_Cuenta[0].RPU, Cls_Sesiones.NOMBRE_COMPLETO);

                    }
                }
                else
                {
                    pagosLineaView = new PagosLineaView();
                }

            }
            catch(Exception ex)
            {
                pagosLineaView = new PagosLineaView();
            }


            return View(pagosLineaView);
        }


        public ActionResult RealizarPagoAnonimo(string RPU)
        {
            PagosLineaView pagosLineaView;
            DatosPagoView datosPagoView;
            string No_Factura_Recibo;
            string Predio_ID;

            decimal Total_Pagar =0;

            try
            {
                if (!string.IsNullOrEmpty(RPU) && sonNumeros(RPU))
                {
                    pagosLineaView = new PagosLineaView();
                    List<DatosCuentaView> Lista_Datos_Cuenta = AsociarRPU_Repo.Obtener_Datos_Cuenta(RPU);
                    List<App_ConfiguracionView> Lista_Configuracion = PagoLinea_Repo.Obtener_Configuracion();

                    if (Lista_Configuracion.Count > 0)
                    {
                        pagosLineaView.ConfiguracionView = Lista_Configuracion[0];
                    }

                    if (Lista_Datos_Cuenta.Count > 0)
                    {

                        pagosLineaView.Datos_Cuenta = Lista_Datos_Cuenta[0];
                        No_Factura_Recibo = PagoLinea_Repo.Obtener_NoFactura_Actual(pagosLineaView.Datos_Cuenta.RPU);
                        Predio_ID = PagoLinea_Repo.Obtener_Predio_ID(pagosLineaView.Datos_Cuenta.RPU);
                        // este es total a pagar sin la suma de diversos, convenios, sanciones

                        if (pagosLineaView.Datos_Cuenta.Descuento_Empleado == "NO" & pagosLineaView.Datos_Cuenta.Tiene_Beneficio_JPA == "NO")
                            Total_Pagar = PagoLinea_Repo.Obtener_Total_Pagar(Predio_ID);
                        else if (pagosLineaView.Datos_Cuenta.Descuento_Empleado == "SI")
                            Total_Pagar = PagoLinea_Repo.Obtener_Total_Pagar_Empleado(pagosLineaView.Datos_Cuenta.RPU);
                        else if (pagosLineaView.Datos_Cuenta.Tiene_Beneficio_JPA == "SI")
                            Total_Pagar = PagoLinea_Repo.Obtener_total_Pagar_Tiene_Beneficio(pagosLineaView.Datos_Cuenta.RPU);

                        Total_Pagar = Cls_Util.redondearTotalPagar(Total_Pagar, true);
                        datosPagoView = PagoLinea_Repo.Obtener_Datos_Pagos(No_Factura_Recibo);

                        if (pagosLineaView.Datos_Cuenta.Descuento_Empleado == "SI")
                            datosPagoView.TotalDescuentoEmpleado = PagoLinea_Repo.Obtener_Total_Descuento_Empleado(pagosLineaView.Datos_Cuenta.RPU);
                        if (pagosLineaView.Datos_Cuenta.Tiene_Beneficio_JPA == "SI")
                            datosPagoView.TotalDescuentoEmpleado = PagoLinea_Repo.Obtener_Total_Descuento_Beneficio(pagosLineaView.Datos_Cuenta.RPU);

                        datosPagoView.TotalConvenio = PagoLinea_Repo.Obtener_Saldo_Convenio_Total(Lista_Datos_Cuenta[0].RPU);
                        pagosLineaView.Total = Total_Pagar;
                        pagosLineaView.Datos_Pago = datosPagoView;

                        pagosLineaView.Form_Multipagos = PagoLinea_Repo.Obtener_Parametros_Pago(Total_Pagar, No_Factura_Recibo, Lista_Datos_Cuenta[0].RPU, Cls_Sesiones.NOMBRE_COMPLETO);

                    }
                }
                else
                {
                    pagosLineaView = new PagosLineaView();
                }


            }
            catch (Exception)
            {
                pagosLineaView = new PagosLineaView();
            }

            return View(pagosLineaView);
        }


        [HttpPost]
        public ActionResult PagoExitoso(Form_Pago_Multipagos form_Pago_Multipagos)
        {
            string cadenaProteger = "";
            string No_Factura_Recibo = "";
            string RPU = "";
            PagosEnLineaReciboView pagosEnLineaReciboView = new PagosEnLineaReciboView();

            try {
                No_Factura_Recibo = form_Pago_Multipagos.mp_order;
                RPU = form_Pago_Multipagos.mp_reference;


                if (PagosEnLinea_Repo.Existe_El_Pago_Recibo(No_Factura_Recibo))
                {
                    form_Pago_Multipagos.mensaje = "Error: El Recibo seleccionado ya ha sido pagado";
                    return View(form_Pago_Multipagos);
                }

                cadenaProteger = Pagos_Repo.Obtener_Hash(form_Pago_Multipagos);

                pagosEnLineaReciboView.Importe = form_Pago_Multipagos.mp_amount;
                pagosEnLineaReciboView.Folio_Pago = form_Pago_Multipagos.mp_trx_historyid;
                pagosEnLineaReciboView.Correo_Electronico = form_Pago_Multipagos.mp_email;
                pagosEnLineaReciboView.Banco_Emisor = form_Pago_Multipagos.mp_bankname;
                pagosEnLineaReciboView.Cadena_Hash_Generada = cadenaProteger.ToLower();
                pagosEnLineaReciboView.Cadena_Hash_Regresada = form_Pago_Multipagos.mp_signature.ToLower();
                pagosEnLineaReciboView.No_Factura_Recibo = No_Factura_Recibo;
                pagosEnLineaReciboView.RPU = RPU;
                pagosEnLineaReciboView.Telefono = form_Pago_Multipagos.mp_phone;
                pagosEnLineaReciboView.Tipo_Pago = form_Pago_Multipagos.mp_paymentMethod;
                pagosEnLineaReciboView.Tipo_Tarjeta = form_Pago_Multipagos.mp_cardType;
                pagosEnLineaReciboView.Numero_Apobacion_Bancaria = form_Pago_Multipagos.mp_authorization;
                pagosEnLineaReciboView.Referencia_Bancaria = form_Pago_Multipagos.mp_pan;
                pagosEnLineaReciboView.Nombre_Cuenta_Habiente = form_Pago_Multipagos.mp_cardholdername;
                pagosEnLineaReciboView.Node_Banco = form_Pago_Multipagos.mp_node;

                if (cadenaProteger != form_Pago_Multipagos.mp_signature)
                {
                    // significa que algo esta mal
                    form_Pago_Multipagos.mensaje = "Error: número de autorización desconocido";
                    return View(form_Pago_Multipagos);

                }

                PagosEnLinea_Repo.Registrar_Pago_Recibo(pagosEnLineaReciboView);
            }
            catch (Exception ex)
            {
                form_Pago_Multipagos.mensaje = ex.Message;
                Cls_Util.Logger(ex.Message + " PagosRecibosController línea 219");
                Correo_Repo.Enviar_Correo_Error(ex.Message + " PagosRecibosController línea 219");
                return View(form_Pago_Multipagos);
            }

            try
            {

                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContextSIMAPAG"].ToString()))
                {
                    conexion.Open();
                    using (SqlCommand comando = conexion.CreateCommand())
                    {
                        comando.CommandText = "Sp_Tra_Pagar_Facturacion";
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add(new SqlParameter("@No_Factura", No_Factura_Recibo));
                        comando.Parameters.Add(new SqlParameter("@Rpu", RPU));
                        comando.Parameters.Add(new SqlParameter("@Folio_Pago", form_Pago_Multipagos.mp_trx_historyid));
                        comando.Parameters.Add(new SqlParameter("@Referencia", form_Pago_Multipagos.mp_pan));
                        comando.Parameters.Add(new SqlParameter("@Tipo_Pago", form_Pago_Multipagos.mp_paymentMethod));
                        comando.Parameters.Add(new SqlParameter("@Tipo_Tarjeta", form_Pago_Multipagos.mp_cardType));
                        comando.Parameters.Add(new SqlParameter("@Monto", form_Pago_Multipagos.mp_amount));
                        comando.ExecuteNonQuery();
                    }
                }

            }catch(Exception ex)
            {
                form_Pago_Multipagos.mensaje = ex.Message;
                Cls_Util.Logger(ex.Message + " PagosReciboController línea 304 folio banco" + form_Pago_Multipagos.mp_trx_historyid + " RPU: " + RPU);
                Correo_Repo.Enviar_Correo_Error(ex.Message + " PagosReciboController línea 304 folio banco " + form_Pago_Multipagos.mp_trx_historyid + " RPU:" + RPU);
                Pagos_No_Aplicados_Repo.Registrar_Pagos_No_Aplicados(pagosEnLineaReciboView);
                return View(form_Pago_Multipagos);
            }

                form_Pago_Multipagos.mensaje = "bien";
            return View(form_Pago_Multipagos);
        }


        [HttpPost]
        public ActionResult PagoError(Form_Pago_Multipagos form_Pago_Multipagos)
        {
            return View(form_Pago_Multipagos);

        }

    }
}