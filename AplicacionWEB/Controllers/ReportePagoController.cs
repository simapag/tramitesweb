﻿using AplicacionWEB.Models;
using AplicacionWEB.Models.VerificarSession;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using AplicacionWEB.Models.Util;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]

    public class ReportePagoController : Controller
    {
        IPagosEnLinea_Repo PagosEnLinea_Repo;

        public ReportePagoController(IPagosEnLinea_Repo pagosEnLinea_Repo)
        {
            PagosEnLinea_Repo = pagosEnLinea_Repo;
        }

        // GET: ReportePago
        public ActionResult Listado_Pagos(string sortOrder , string currentFilter, string searchString, string currentFechaInicio, string FechaInicio, string currentFechaFin, string FechaFin, string currentFolioPagoInterno, string FolioPagoInterno,int? page)
        {
            IQueryable<PagosEnLineaView> listaPagosEnLinea;
            int pageSize = 10;
            int pageNumber = 0;

            try
            {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.FechaSortParam= String.IsNullOrEmpty(sortOrder) ? "fecha_asc" : "";


                var dbPagosEnLinea = PagosEnLinea_Repo.Tra_Pago_En_Linea;


                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }


                if (FechaInicio != null)
                {
                    page = 1;
                }
                else
                {
                    FechaInicio = currentFechaInicio;
                }


                if (FechaFin != null)
                {
                    page = 1;
                }
                else
                {
                    FechaFin = currentFechaFin;
                }


                if (FolioPagoInterno != null) {
                    page = 1;
                }else
                {
                    FolioPagoInterno = currentFolioPagoInterno;
                }

                ViewBag.CurrentFechaInicio = FechaInicio;
                ViewBag.CurrentFechaFin = FechaFin;
                ViewBag.CurrentFilter = searchString;
                ViewBag.currentFolioPagoInterno = FolioPagoInterno;


                if (!String.IsNullOrEmpty(searchString)) {

                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.RPU.Contains(searchString)
                    || x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.Usuario_Creo.ToLower().Contains(searchString.ToLower())
                    || x.Tra_Pagos_Solicitudes.Codigo_Barras.Contains(searchString)
                    );
                }


                if (!String.IsNullOrEmpty(FolioPagoInterno))
                {
                    int folio = 0;
                    int.TryParse(FolioPagoInterno, out folio);

                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Pago_Solicitudes_ID == folio);

                }

                    if (!String.IsNullOrEmpty(FechaInicio))
                {
                    DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Fecha >= dateTimeIncio);
                }

                if (!String.IsNullOrEmpty(FechaFin)) {
                    DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                    DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Fecha <= dateTimeFinMas);
                }


                    switch (sortOrder) {

                    case "fecha_asc":
                        dbPagosEnLinea = dbPagosEnLinea.OrderBy(x => x.Fecha);
                        break;
                    default:
                        dbPagosEnLinea = dbPagosEnLinea.OrderByDescending(x => x.Fecha);
                        break;
                           
                }

                pageNumber = (page ?? 1);

                listaPagosEnLinea = dbPagosEnLinea.Select(x => new PagosEnLineaView
                {
                    Banco_Emisor = x.Banco_Emisor,
                    Correo_Electronico = x.Correo_Electronico,
                    Fecha_Pago = x.Fecha,
                    Folio_Pago = x.Folio_Pago,
                    Folio_Solicitud = x.Tra_Pagos_Solicitudes.Tramite_Solicitados_ID,
                    Nombre_CuentaHabiente = x.Nombre_CuentaHabiente,
                    Importe = x.Importe,
                    Nombre_Tramite = x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.Tra_Tramites.Nombre_Tramite,
                    No_Diverso = x.No_Diverso,
                    Numero_Aprobacion_Bancaria = x.Numero_Aprobacion_Bancaria,
                    Referencia = x.Referencia,
                    RPU = x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.RPU,
                    Telefono = x.Telefono,
                    Tipo_Pago = x.Tipo_Pago,
                    Tipo_Tarjeta = x.Tipo_Tarjeta,
                    Codigo_Barras = x.Tra_Pagos_Solicitudes.Codigo_Barras,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Usuario = x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.Usuario_Creo

                });

            }
            catch (Exception ex) {
                listaPagosEnLinea = Enumerable.Empty<PagosEnLineaView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }



            return View(listaPagosEnLinea.ToPagedList(pageNumber,pageSize));
        }
    }
}