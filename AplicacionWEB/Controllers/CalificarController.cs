﻿using AplicacionWEB.Infraestructura.Servicios;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using AplicacionWEB.Models.VerificarSession;
using ModelDomain.Contexto;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    //[Authorize]
    [RoleAuthorize()]
    public class CalificarController : Controller
    {
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;
        IPagos_Repo Pagos_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        IUsuarios_Repositorio Usuarios_Repositorio;
        ICorreo_Repo Correo_Repo;

        public CalificarController(ITramitesSolicitudes_Repo tramitesSolicitudes_Repo, IPagos_Repo pagos_Repo,
             IMensajesSolicitudes_Repo mensajesSolicitudes_Repo,
            IUsuarios_Repositorio usuarios_Repositorio,
             Correo_Repo correo_Repo)
        {
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
            Pagos_Repo = pagos_Repo;
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            Usuarios_Repositorio = usuarios_Repositorio;
            Correo_Repo = correo_Repo;
        }


        public ActionResult Registrar(string returnUrl, string Estatus , int? Tramite_Solicitados_ID)
        {
            FormCalificarView formCalificarView = new FormCalificarView();
            try
            {

                formCalificarView.returnUrl = returnUrl;
                TempData["tramite_solicitado_id"] = Tramite_Solicitados_ID;
                ViewBag.Estatus = new SelectList(TramitesSolicitudes_Repo.Obtener_Estatus_Solicitud(), "name", "name", Estatus);


                if (Tramite_Solicitados_ID.HasValue)
                    formCalificarView.Tramite_Solicitados_ID = Tramite_Solicitados_ID.Value;



            }
            catch (Exception ex)
            {

            }

            return View(formCalificarView);
        }

        [HttpPost]
        public ActionResult Registrar(FormCalificarView formCalificarView)
        {
            ViewBag.Estatus = new SelectList(TramitesSolicitudes_Repo.Obtener_Estatus_Solicitud(), "name", "name", formCalificarView.Estatus);
            TramitesSolicitudView tramitesSolicitudView = new TramitesSolicitudView();
            UsuarioView usuarioView = new UsuarioView();
            if (ModelState.IsValid)
            {
               

                try
                {
                    tramitesSolicitudView = TramitesSolicitudes_Repo.Obtener_Una_Solicitud(formCalificarView.Tramite_Solicitados_ID);
                    usuarioView = Usuarios_Repositorio.Buscar_Usuario(tramitesSolicitudView.Usuarios_ID);

                    int cantidadPagosPendientes = Pagos_Repo.Obtener_Pagos_Cantidad("PENDIENTE", formCalificarView.Tramite_Solicitados_ID);
                    int cantidadPagosPagados = Pagos_Repo.Obtener_Pagos_Cantidad("PAGADO", formCalificarView.Tramite_Solicitados_ID); 

                    if(formCalificarView.Estatus == "CANCELADO")
                    {

                        if (cantidadPagosPendientes > 0 || cantidadPagosPagados > 0)
                        {
                            ModelState.AddModelError("error", "Tiene Pagos Pendientes o Pagado, No se puede  cambiar a Cancelar");
                            return View(formCalificarView);
                        }

                    }

                    if (formCalificarView.Estatus == "TERMINADO")
                    {
                        if(cantidadPagosPendientes > 0)
                        {
                            ModelState.AddModelError("error", "Tiene Pagos Pendientes, No se puede  cambiar a Terminado");
                            return View(formCalificarView);
                        }
                    }


                    if(formCalificarView.Estatus == "PAGADO")
                    {
                        if (cantidadPagosPendientes > 0)
                        {
                            ModelState.AddModelError("error", "Tiene Pagos Pendientes, No se puede  cambiar a Pagado");
                            return View(formCalificarView);
                        }
                    }

                    if (formCalificarView.Estatus == "PENDIENTE PAGO")
                    {
                        if (cantidadPagosPendientes == 0 )
                        {
                            ModelState.AddModelError("error", "No Tiene Pagos Pendientes");
                            return View(formCalificarView);
                        }
                    }

                    if (formCalificarView.Estatus == "PENDIENTE")
                    {
                        if (cantidadPagosPendientes > 0 || cantidadPagosPagados > 0)
                        {
                            ModelState.AddModelError("error", "Tiene Pagos Asignados, No se puede cambiar Pendiente");
                            return View(formCalificarView);
                        }
                    }


                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("error", ex.Message);
                }

                using (EFDbContext Contexto = new EFDbContext())
                {
                    using (DbContextTransaction transaction = Contexto.Database.BeginTransaction())
                    {

                        try
                        {
                            MensajesSolicitudesView mensajesSolicitudesView = new MensajesSolicitudesView();
                            mensajesSolicitudesView.Estatus_Solicitud = formCalificarView.Estatus;
                            mensajesSolicitudesView.Tramite_Solicitados_ID = formCalificarView.Tramite_Solicitados_ID;
                            TramitesSolicitudes_Repo.Cambiar_Estatus(Contexto, mensajesSolicitudesView.Estatus_Solicitud, mensajesSolicitudesView.Tramite_Solicitados_ID);
                            mensajesSolicitudesView.Entidad = "SIMAPAG";
                            mensajesSolicitudesView.Mensaje = formCalificarView.Mensaje;
                            mensajesSolicitudesView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                            mensajesSolicitudesView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                            MensajesSolicitudes_Repo.Guardar_Mensaje(Contexto, mensajesSolicitudesView);
                            transaction.Commit();

                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            ModelState.AddModelError("error", ex.Message);
                            return View(formCalificarView);

                        }

                    }

                }

                tramitesSolicitudView.Estatus_Revision = formCalificarView.Estatus;
                Correo_Repo.Enviar_Correo_Mensaje(usuarioView, Cls_Sesiones.NOMBRE_COMPLETO, tramitesSolicitudView, formCalificarView.Mensaje);


                formCalificarView.Resultado = "Proceso Correcto";
                ModelState.Clear();
                formCalificarView.Mensaje = string.Empty;
            }



            return View(formCalificarView);
        }

    }
}