﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AplicacionWEB.Models;
using AplicacionWEB.Models.Util;
using ModelDomain.Dto;
using ModelDomain.ViewModel;
using ModelDomain.Interfaces;
using AplicacionWEB.Models.VerificarSession;

namespace AplicacionWEB.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class DocumentosCatalogoController : Controller
    {

        IDocuemento_Repo Documento_Repo;

        public DocumentosCatalogoController(IDocuemento_Repo docuemento_Repo) {
            Documento_Repo = docuemento_Repo;
        }

        // GET: DocumentosCatalogo
        public ActionResult Catalogo_Documento()
        {
            return View();
        }

        public string Obtener_Documentos(int offset, int limit, string search = null) {

            Cls_Paginado<DocumentosView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                Paginado = Documento_Repo.Obtener_Documentos(offset, limit, search);
                str_datos = serializer.Serialize(Paginado);
            }
            catch (Exception ex) {

            }

            return str_datos;
        }

        [HttpPost]
        public string Eliminar_Documento(DocumentosView documentosView) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {
                objMensajeServidor.Mensaje = Documento_Repo.Eliminar_Documento(documentosView);
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }

        [HttpPost]
        public string Guardar_Documento(DocumentosView documentosView) {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {

                if (Documento_Repo.Esta_Repetido_Documento(documentosView.Nombre_Documento, documentosView.Documento_ID) == "SI") {

                    objMensajeServidor.Mensaje = "El nombre del documento esta repetido";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

                documentosView.Usuario_Creo = Cls_Sesiones.NOMBRE_COMPLETO;
                Documento_Repo.Registrar_Documento(documentosView);
                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex) {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }

    }
}