﻿using AplicacionWEB.Models.Util;
using AplicacionWEB.ReportesExcel;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionWEB.Controllers
{
    public class ExcelExportController : Controller
    {
        IPagosEnLinea_Repo PagosEnLinea_Repo;
        IMensajesSolicitudes_Repo MensajesSolicitudes_Repo;
        ITramitesSolicitudes_Repo TramitesSolicitudes_Repo;

        public ExcelExportController(IPagosEnLinea_Repo pagosEnLinea_Repo, IMensajesSolicitudes_Repo mensajesSolicitudes_Repo,
            ITramitesSolicitudes_Repo tramitesSolicitudes_Repo)
        {
            PagosEnLinea_Repo = pagosEnLinea_Repo;
            MensajesSolicitudes_Repo = mensajesSolicitudes_Repo;
            TramitesSolicitudes_Repo = tramitesSolicitudes_Repo;
        }

        // GET: ExcelExport
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Elaborar_Reporte_Solicitudes(string sortOrder, string FechaInicio, string FechaFin, int? tramite_ID)
        {
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "PlantillasExcel\\ReporteSolicitud.xlsx";
            try
            {

                if (sortOrder == null)
                {
                    sortOrder = "";
                }

                string nombre_archivo = "reporte_solicitudes_tramites.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);
                List<TramitesSolicitudView> Lista_Tramites = TramitesSolicitudes_Repo.Obtener_Reporte_Solicitudes(sortOrder, FechaInicio, FechaFin, tramite_ID).ToList();
                Reporte_Solicitudes reporte_Solicitudes = new Reporte_Solicitudes(ruta_plantilla, ruta_almacenamiento, Lista_Tramites);

                reporte_Solicitudes.Elaborar_Reporte();
                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();

            }
            catch(Exception ex)
            {
                return RedirectToAction("Listado_Solicitudes", "ReporteSolicitudes");
            }

            return null;

        }

        public ActionResult Elaborar_Reporte_Mensajes(string sortOrder,string FechaInicio, string FechaFin)
        {
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "PlantillasExcel\\ReporteMensajes.xlsx";

            try
            {

                if(sortOrder == null)
                {
                    sortOrder = "";
                }
                string nombre_archivo = "reporte_pagos_de_mensajes.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                List<ReporteMensajesView> Lista_Reportes_Mensaje = MensajesSolicitudes_Repo.Obtener_Reporte_Mensaje(sortOrder, FechaInicio, FechaFin, Cls_Sesiones.USUARIO_ID).ToList();
                Reporte_Mensajes reporte_Mensajes = new Reporte_Mensajes(ruta_plantilla, ruta_almacenamiento, Lista_Reportes_Mensaje, Cls_Sesiones.NOMBRE_COMPLETO);
                reporte_Mensajes.Elaborar_Reporte();
                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
            }
            catch(Exception ex)
            {
                return RedirectToAction("Mis_Mensajes", "ReporteMensajes");
            }

            return null;
        }


        public ActionResult Elaborar_Reporte_Pago_En_Liena_Recibo(string sortOrder, string RPU , string FolioBancomer, string FechaInicio, string FechaFin)
        {
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "PlantillasExcel\\PagosEnLineaRecibos.xlsx";
            try
            {
                string nombre_archivo = "reporte_pagos_de_en_linea_recibo.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                if (sortOrder == null)
                {
                    sortOrder = "";
                }

                List<PagosEnLineaReciboView> Lista_Pagos = PagosEnLinea_Repo.Obtener_Pagos_En_Linea_Recibo(sortOrder, FechaInicio, FechaFin, RPU, FolioBancomer).ToList();
                Reporte_Pagos_En_Linea_Recibos reporte_Pagos_En_Linea_Recibos = new Reporte_Pagos_En_Linea_Recibos(ruta_plantilla, ruta_almacenamiento, Lista_Pagos);
                reporte_Pagos_En_Linea_Recibos.Elaborar_Reporte();

                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();


            }
            catch(Exception ex)
            {
                return RedirectToAction("Listado_Pagos", " ReportePagoRecibo");
            }

            return null;
        }

        public ActionResult Elaborar_Reporte_Pagos_En_Linea(string searchString, string FechaInicio, string FechaFin, string FolioPagoInterno)
        {
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "PlantillasExcel\\PagosEnLinea.xlsx";

            try {

                string nombre_archivo = "reporte_pagos_en_linea.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                List<PagosEnLineaView> listaPagosEnLinea;

                var dbPagosEnLinea = PagosEnLinea_Repo.Tra_Pago_En_Linea;

                if (!String.IsNullOrEmpty(searchString))
                {

                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.RPU.Contains(searchString)
                    || x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.Usuario_Creo.ToLower().Contains(searchString.ToLower())
                    || x.Tra_Pagos_Solicitudes.Codigo_Barras.Contains(searchString)
                    );
                }


                if (!String.IsNullOrEmpty(FolioPagoInterno))
                {
                    int folio = 0;
                    int.TryParse(FolioPagoInterno, out folio);

                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Pago_Solicitudes_ID == folio);

                }

                if (!String.IsNullOrEmpty(FechaInicio))
                {
                    DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Fecha >= dateTimeIncio);
                }

                if (!String.IsNullOrEmpty(FechaFin))
                {
                    DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                    DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                    dbPagosEnLinea = dbPagosEnLinea.Where(x => x.Fecha <= dateTimeFinMas);
                }


                dbPagosEnLinea = dbPagosEnLinea.OrderByDescending(x => x.Fecha);

                listaPagosEnLinea = dbPagosEnLinea.Select(x => new PagosEnLineaView
                {
                    Banco_Emisor = x.Banco_Emisor,
                    Correo_Electronico = x.Correo_Electronico,
                    Fecha_Pago = x.Fecha,
                    Folio_Pago = x.Folio_Pago,
                    Folio_Solicitud = x.Tra_Pagos_Solicitudes.Tramite_Solicitados_ID,
                    Nombre_CuentaHabiente = x.Nombre_CuentaHabiente,
                    Importe = x.Importe,
                    Nombre_Tramite = x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.Tra_Tramites.Nombre_Tramite,
                    No_Diverso = x.No_Diverso,
                    Numero_Aprobacion_Bancaria = x.Numero_Aprobacion_Bancaria,
                    Referencia = x.Referencia,
                    RPU = x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.RPU,
                    Telefono = x.Telefono,
                    Tipo_Pago = x.Tipo_Pago,
                    Tipo_Tarjeta = x.Tipo_Tarjeta,
                    Codigo_Barras = x.Tra_Pagos_Solicitudes.Codigo_Barras,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Usuario = x.Tra_Pagos_Solicitudes.Tra_Tramites_Solicitados.Usuario_Creo

                }).ToList();

                Reporte_Pagos_En_Linea reporte_Pagos_En_Linea = new Reporte_Pagos_En_Linea(ruta_plantilla, ruta_almacenamiento, listaPagosEnLinea);
                reporte_Pagos_En_Linea.Elaborar_Reporte();

                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Listado_Pagos", "ReportePago");
            }


            return null;
        }

        private string obtenerRutaParaGuardar(string sFileName)
        {
            string respuesta = "";

            if (!Path.IsPathRooted(sFileName))
            {
                sFileName = Path.Combine(Path.GetTempPath(), sFileName);
            }

            if (System.IO.File.Exists(sFileName))
            {
                String sDateTime = DateTime.Now.ToString("yyyyMMdd\\_HHmmss");
                String s = Path.GetFileNameWithoutExtension(sFileName) + "_" + sDateTime + Path.GetExtension(sFileName);
                sFileName = Path.Combine(Path.GetDirectoryName(sFileName), s);
            }
            else
            {
                Directory.CreateDirectory(Path.GetDirectoryName(sFileName));
            }
            respuesta = sFileName;
            return respuesta;

        }
    }
}