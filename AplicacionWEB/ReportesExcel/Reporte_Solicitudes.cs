﻿using ModelDomain.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AplicacionWEB.ReportesExcel
{
    public class Reporte_Solicitudes
    {
        private FileInfo template;
        private string rutaNuevaArchivo;
        private int renglon = 10;
        private int aux_renglon;
        private List<TramitesSolicitudView> Lista_Tramite_Solicitudes;

        public Reporte_Solicitudes(string ruta_plantilla, string ruta_nuevo_archivo, List<TramitesSolicitudView> listaTramites)
        {
            template = new FileInfo(ruta_plantilla);
            rutaNuevaArchivo = ruta_nuevo_archivo;
            Lista_Tramite_Solicitudes = listaTramites;
        }


        public void Elaborar_Reporte()
        {

            using (ExcelPackage p = new ExcelPackage(template, true)) {

                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;


                foreach(TramitesSolicitudView lista in Lista_Tramite_Solicitudes)
                {
                    ws.Cells[renglon, 2].Value = String.Format("{0:dd/MM/yyyy HH:mm}", lista.Fecha_Solicitud);
                    ws.Cells[renglon, 3].Value = lista.Tramite_Solicitados_ID;
                    ws.Cells[renglon, 4].Value = lista.Nombre_Tramite;
                    ws.Cells[renglon, 5].Value = lista.Estatus;
                    ws.Cells[renglon, 6].Value = lista.Estatus_Revision;
                    ws.Cells[renglon, 7].Value = lista.RPU;
                    ws.Cells[renglon, 8].Value = lista.Usuario;
                    ws.Cells[renglon, 9].Value = lista.Telefono;
                    ws.Cells[renglon, 10].Value = lista.Correo_Electronico;
                    ws.Cells[renglon, 11].Value = lista.Usuario;
                    ws.Cells[renglon, 12].Value = lista.Costo_Tramite;
                    ws.Cells[renglon, 13].Value = lista.Observaciones;
                    renglon++;
                }

                ws.Cells[aux_renglon, 2, renglon, 13].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                Byte[] bin = p.GetAsByteArray();
                string file = rutaNuevaArchivo;
                File.WriteAllBytes(file, bin);

            }

        }

    }
}