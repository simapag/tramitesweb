﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Linq;
using System.IO;
using ModelDomain.ViewModel;

namespace AplicacionWEB.ReportesExcel
{
    public class Reporte_Pagos_En_Linea_Recibos
    {

        FileInfo template;
        string rutaNuevaArchivo;
        List<PagosEnLineaReciboView> Lista_Pagos;
        int renglon = 10;
        int aux_renglon;



        public Reporte_Pagos_En_Linea_Recibos(string ruta_plantilla, string ruta_nuevo_archivo, List<PagosEnLineaReciboView> lineaReciboViews)
        {

            template = new FileInfo(ruta_plantilla);
            rutaNuevaArchivo = ruta_nuevo_archivo;
            Lista_Pagos = lineaReciboViews;
        }

        public void Elaborar_Reporte()
        {
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                foreach(PagosEnLineaReciboView item in Lista_Pagos)
                {
                    ws.Cells[renglon, 2].Value = String.Format("{0:dd/MM/yyyy HH:mm}", item.Fecha);
                    ws.Cells[renglon, 3].Value = item.RPU;
                    ws.Cells[renglon, 4].Value = item.Folio_Pago;
                    ws.Cells[renglon, 5].Value = item.Numero_Apobacion_Bancaria;
                    ws.Cells[renglon, 6].Value = item.Referencia_Bancaria;
                    ws.Cells[renglon, 7].Value = item.Importe;
                    ws.Cells[renglon, 8].Value = item.Correo_Electronico;
                    ws.Cells[renglon, 9].Value = item.Nombre_Cuenta_Habiente;
                    renglon++;
                }

                ws.Cells[aux_renglon, 2, renglon, 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                Byte[] bin = p.GetAsByteArray();
                string file = rutaNuevaArchivo;
                File.WriteAllBytes(file, bin);
            }
        }

    }
}