﻿using ModelDomain.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AplicacionWEB.ReportesExcel
{
    public class Reporte_Mensajes
    {

      private FileInfo template;
      private  string rutaNuevaArchivo;
      private  int renglon = 10;
      private List<ReporteMensajesView> Lista_Mensajes;
      private  int aux_renglon;
        private string usuarioDirigido;   

      public Reporte_Mensajes(string ruta_plantilla, string ruta_nuevo_archivo, 
          List<ReporteMensajesView> listaMensajes,
          string usuario)
        {
            template = new FileInfo(ruta_plantilla);
            rutaNuevaArchivo = ruta_nuevo_archivo;
            Lista_Mensajes = listaMensajes;
            usuarioDirigido = usuario;
        }


        public void Elaborar_Reporte()
        {
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                ws.Cells[1, 5].Value = usuarioDirigido;

                foreach (ReporteMensajesView lista in Lista_Mensajes)
                {
                    ws.Cells[renglon, 2].Value = String.Format("{0:dd/MM/yyyy HH:mm}", lista.Fecha_Creo);
                    ws.Cells[renglon, 3].Value = lista.Tramite_Solicitados_ID;
                    ws.Cells[renglon, 4].Value = lista.Nombre_Tramite;
                    ws.Cells[renglon, 5].Value = lista.RPU;
                    ws.Cells[renglon, 6].Value = lista.Estatus_Solicitud;
                    ws.Cells[renglon, 7].Value = lista.Usuario_Creo.ToUpper();
                    ws.Cells[renglon, 8].Value = lista.Mensaje;

                    renglon++;

                }
                ws.Cells[aux_renglon, 2, renglon, 8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                Byte[] bin = p.GetAsByteArray();
                string file = rutaNuevaArchivo;
                File.WriteAllBytes(file, bin);
            }
        }
    }
}