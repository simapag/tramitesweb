﻿using ModelDomain.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AplicacionWEB.ReportesExcel
{
    public class Reporte_Pagos_En_Linea
    {
        FileInfo template;
        string rutaNuevaArchivo;
        int renglon = 10;
        List<PagosEnLineaView> Lista_Pagos_En_Linea;
        int aux_renglon;

        public Reporte_Pagos_En_Linea(string ruta_plantilla, string ruta_nuevo_archivo, List<PagosEnLineaView> pagosEnLineaViews)
        {
            template = new FileInfo(ruta_plantilla);
            rutaNuevaArchivo = ruta_nuevo_archivo;
            Lista_Pagos_En_Linea = pagosEnLineaViews;
        }

        public void Elaborar_Reporte() {


            using (ExcelPackage p = new ExcelPackage(template, true)) {

                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                foreach(PagosEnLineaView lista in Lista_Pagos_En_Linea)
                {

                    ws.Cells[renglon, 2].Value = String.Format("{0:dd/MM/yyyy HH:mm}", lista.Fecha_Pago);
                    ws.Cells[renglon, 3].Value = lista.Pago_Solicitudes_ID;
                    ws.Cells[renglon, 4].Value = lista.Folio_Solicitud;
                    ws.Cells[renglon, 5].Value = lista.RPU;
                    ws.Cells[renglon, 6].Value = lista.Nombre_Tramite;
                    ws.Cells[renglon, 7].Value = lista.Codigo_Barras;
                    ws.Cells[renglon, 8].Value = lista.Correo_Electronico;
                    ws.Cells[renglon, 9].Value = lista.Usuario;
                    ws.Cells[renglon, 10].Value = lista.Importe;
                

                    renglon++;
                }

                ws.Cells[aux_renglon, 2, renglon, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                ws.Cells[aux_renglon, 2, renglon, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                Byte[] bin = p.GetAsByteArray();
                string file = rutaNuevaArchivo;
                File.WriteAllBytes(file, bin);

            }

        }

    }
}