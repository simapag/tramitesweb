﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ModelDomain.Interfaces;
using ModelDomain.Contexto;
using ModelDomain.ViewModel;
using System.Net.Mail;
using System.Configuration;

namespace AplicacionWEB.Infraestructura.Servicios
{
    public class Correo_Repo :ICorreo_Repo
    {
        IParametroCorreo_Repo ParametroCorreo_Repo;
        ParametrosCorreoView parametrosCorreo;

        public Correo_Repo(IParametroCorreo_Repo parametroCorreo) {
            ParametroCorreo_Repo = parametroCorreo;
            parametrosCorreo = ParametroCorreo_Repo.ObtenerParametros();
        }

     

        public string Enviar_Correo(UsuarioView usuarioView)
        {
            string respuesta = "bien";
            string mensaje = "";

            try
            {
                string url_apliacativo = ConfigurationManager.AppSettings["url_aplicativo"];

                mensaje = "<p> <h2> Estimado Usuario:</h2> </p>";
                mensaje += " <hr>";
                mensaje += "<b> " + usuarioView.Nombre + ' ' + usuarioView.Apellido_Paterno + ' ' + usuarioView.Apellido_Materno +  "</b>";
                mensaje += "<p> Se ha recuperado su constraseña : <span style='color: red; '>  " + usuarioView.Password + " </span> </p>";
                mensaje += "<br>";
                mensaje += "<br>";
                mensaje += "puede acceder ala plataforma de trámites en línea en el siguiente enlace :";
                mensaje += " <a href='" + url_apliacativo + "'> Trámites en línea </a>";

                Procesar_Correo(usuarioView.Correo_Electronico, "Recuperar Contraseña", mensaje);

            }
            catch(Exception ex)
            {
                respuesta = ex.Message;
            }



            return respuesta;

        }

        public string Enviar_Correo_Error(string mensaje_error)
        {
            string respuesta = "bien";
            string mensaje = "";
            string correos;
            try
            {
                correos = ConfigurationManager.AppSettings["correo_para_mandar_errores"];

                mensaje = "<h1> Ocurrio un error: </h1>";
                mensaje += " <hr>";
                mensaje += " <p> <b>  " +  mensaje_error + " </b> </p>";


                Procesar_Correo(correos, "Error", mensaje_error);

            }
            catch (Exception ex) {
                respuesta = ex.Message;
            }
           

            return respuesta;
        }

        public string Enviar_Correo_Mensaje(UsuarioView usuarioDestino, string usuarioEmisor, TramitesSolicitudView tramitesSolicitudView,string Comentario)
        {
            string respuesta = "bien";
            string mensaje = "";

            try
            {
                mensaje = "<p><h2> El Usuario:  </h2></p>";
                mensaje += " <hr>";
                mensaje += "<b> <span style='color: red; '> " + usuarioEmisor.ToUpper() + " </span> </b>";
                mensaje += "<br>";
                mensaje += "<p> Le ha escrito el siguiente Mensaje: </p>";
                mensaje += "<br>";
                mensaje += "<b> " + Comentario + "  </b>";
                mensaje += "<br>";
                mensaje += "<p> En Referencia al siguiente trámite: </p>";
                mensaje += "<dl>";
                mensaje += "<dt> <b> No. Solicitud: </b></dt>";
                mensaje += " <dd>" + tramitesSolicitudView.Tramite_Solicitados_ID + "</dd>";
                mensaje += "<dt> <b> Trámite: </b></dt>";
                mensaje += " <dd>" + tramitesSolicitudView.Nombre_Tramite + "</dd>";
                mensaje += "<dt> <b> RPU: </b> </dt>";
                mensaje += " <dd>" + tramitesSolicitudView.RPU + "</dd>";
                mensaje += "<dt> <b> Estatus: </b> </dt>";
                mensaje += " <dd>" + tramitesSolicitudView.Estatus_Revision + "</dd>";
                mensaje += "</dl>";



                Procesar_Correo(usuarioDestino.Correo_Electronico, "Mensaje Informartivo", mensaje);
            }
            catch(Exception ex)
            {
                respuesta = ex.Message;
            }

            return respuesta;
        }

        public string Enviar_Correo_Registro_Pago(UsuarioView usuarioView, Form_Pago_Multipagos form_Pago_Multipagos, TramitesSolicitudView tramitesSolicitudView)
        {
            string respuesta = "bien";
            string mensaje = "";

            try
            {

                mensaje = "<p><h2> El Usuario:  </h2></p>";
                mensaje += " <hr>";
                mensaje += "<b> <span style='color: red; '> " + form_Pago_Multipagos.mp_customername + " </span> </b>";
                mensaje += "<br>";
                mensaje += "<p> Ha realizado un pago con la siguientes características: </p>";
                mensaje += "<dl>";
                mensaje += "<dt> <b> Referencia: </b></dt>";
                mensaje += " <dd>" + form_Pago_Multipagos.mp_reference + "</dd>";
                mensaje += "<dt> <b> Importe: </b></dt>";
                mensaje += " <dd>" + form_Pago_Multipagos.mp_amount + "</dd>";
                mensaje += "<dt> <b> Fecha: </b> </dt>";
                mensaje += " <dd>" + form_Pago_Multipagos.mp_date + "</dd>";
                mensaje += "<dt> <b> Trámite: </b> </dt>";
                mensaje += " <dd>" + tramitesSolicitudView.Nombre_Tramite + "</dd>";
                mensaje += "<dt> <b> Folio Trámite Solicitud: </b></dt>";
                mensaje += " <dd>" + tramitesSolicitudView.Tramite_Solicitados_ID.ToString() + "</dd>";
                mensaje += "</dl>";

                Procesar_Correo(usuarioView.Correo_Electronico, "Pago Realizado", mensaje);
            }
            catch (Exception ex) {
                respuesta = ex.Message;
            }

            return respuesta;
        }

     


        public void Procesar_Correo(string pCorreoDestinatario, string pAsunto, string pMensaje)
        {
            int puerto =0;
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(pCorreoDestinatario);
            mailMessage.From = new MailAddress(parametrosCorreo.Email, "Trámites SIMAPAG", System.Text.Encoding.UTF8);
            mailMessage.Subject = pAsunto;

            mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            mailMessage.Body = pMensaje;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.High;
            Int32.TryParse(parametrosCorreo.Puerto, out puerto);

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Credentials = new System.Net.NetworkCredential(parametrosCorreo.Email, parametrosCorreo.Password);
            smtpClient.Port = puerto;
            smtpClient.Host = parametrosCorreo.Host_Email;
            smtpClient.EnableSsl = true;
            smtpClient.Send(mailMessage);

            mailMessage.Dispose();
        }

    }
}