﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;

namespace AplicacionWEB.Infraestructura.Servicios
{
   public interface ICorreo_Repo
    {
        string Enviar_Correo(UsuarioView usuarioView);
        string Enviar_Correo_Registro_Pago(UsuarioView usuarioView, Form_Pago_Multipagos form_Pago_Multipagos , TramitesSolicitudView tramitesSolicitudView);
        string Enviar_Correo_Error(string mensaje_error);

        string Enviar_Correo_Mensaje(UsuarioView usuarioDestino, string usuarioEmisor, TramitesSolicitudView tramitesSolicitudView ,string Comentario);
    }
}
