﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;

using ModelDomain.Contexto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using AplicacionWEB.Infraestructura.Servicios;

namespace AplicacionWEB.Infraestructura
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {

            return controllerType == null
                   ? null
                   : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<IRoles_Repositorio>().To<EFRoles_Repositorio>();
            ninjectKernel.Bind<IMenus_Repositorio>().To<EFMenus_Repositorio>();
            ninjectKernel.Bind<IAcceso_Respositorio>().To<EFAcceso_Repositorio>();
            ninjectKernel.Bind<IUsuarios_Repositorio>().To<EFUsuarios>();
            ninjectKernel.Bind<ITramites_Repo>().To<EFTramites_Repo>();
            ninjectKernel.Bind<IDocuemento_Repo>().To<EFDocumento_Repo>();
            ninjectKernel.Bind<ITramites_Documento_Repo>().To<EFTramites_Documento_Repo>();
            ninjectKernel.Bind<ITramitesProceso_Repo>().To<EFTramitesProceso_Repo>();
            ninjectKernel.Bind<IAsociarRPU_Repo>().To<EFRPU_Asociados_Repo>();
            ninjectKernel.Bind<ITramitesSolicitudes_Repo>().To<EFTramitesSolicitudes_Repo>();
            ninjectKernel.Bind<IAtenderTramite_Repo>().To<EFAtenderTramite_Repo>();
            ninjectKernel.Bind<IMensajesSolicitudes_Repo>().To<EFMensajeSolictudes_Repo>();
            ninjectKernel.Bind<IPagos_Repo>().To<EFPagos_Repo>();
            ninjectKernel.Bind<ICostos_Repo>().To<EFCosto_Repo>();
            ninjectKernel.Bind<IParametroCorreo_Repo>().To<EFParametrosCorreo_Repo>();
            ninjectKernel.Bind<IComprobanteCfedi_Repo>().To<EFComprobanteCfedi_Repo>();
            ninjectKernel.Bind<IPagosEnLinea_Repo>().To<EFPagoEnLinea_Repo>();
            ninjectKernel.Bind<ICorreo_Repo>().To<Correo_Repo>();
            ninjectKernel.Bind<IPagoLinea>().To<EFPagoLinea>();
            ninjectKernel.Bind<IPagos_Linea_No_Registrados_Repo>().To<EFPagos_Linea_No_Registrados_Repo>();
        }


    }
}