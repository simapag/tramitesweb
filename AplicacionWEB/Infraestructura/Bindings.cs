﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using Ninject;
using ModelDomain.Contexto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using AplicacionWEB.Infraestructura.Servicios;

namespace AplicacionWEB.Infraestructura
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
                     Bind<ICorreo_Repo>().To<Correo_Repo>();
        }
    }
}