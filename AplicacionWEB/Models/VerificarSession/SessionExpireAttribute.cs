﻿using AplicacionWEB.Models.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionWEB.Models.VerificarSession
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            HttpContext context = HttpContext.Current;

            // check session here

            if (Cls_Sesiones.NOMBRE_COMPLETO.Length == 0) {
                filterContext.Result = new RedirectResult("~/Login/Verificar_Usuario");
                return;
            }

            base.OnActionExecuted(filterContext);
        }

    }
}