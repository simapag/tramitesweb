﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AplicacionWEB.Models.Util;


namespace AplicacionWEB.Models
{
    public class Cls_Asignar_Rol_Modelo
    {

        public string Correo { get; set; }
        public string Password { get; set; }
        public Cls_Elementos_Combo SeleccionRol { get; set; }
        public int Usuario_ID { get; set; }
    }
}