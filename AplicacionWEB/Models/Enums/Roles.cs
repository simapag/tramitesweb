﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AplicacionWEB.Models.Enums
{
    public enum Roles
    {
        Administrador,
        Usuario_Externo,
        Ejecutivo,
        Supervisor
    }
}