﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AplicacionWEB.Models.Util
{/// <summary>
/// Esta clase se usa para el manejo de las lista desplegables en ko
/// </summary>
    public class Cls_Elementos_Combo
    {
        public string name { get; set; }
        public string id { get; set; }
    }
}