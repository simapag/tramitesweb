﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AplicacionWEB.Models.Util
{
    /// <summary>
    /// Clase para el manejo de sanciones
    /// </summary>
    public class Cls_Sesiones
    {


        public static string ROL
        {

            set { HttpContext.Current.Session["NombreRol"] = value; }
            get
            {
                if (HttpContext.Current.Session["NombreRol"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["NombreRol"].ToString();
            }

        }

        /// <summary>
        /// Almacena el nombre del usuario
        /// </summary>
        public static string NOMBRE_COMPLETO
        {
            set { HttpContext.Current.Session["NombreCompleto"] = value; }
            get
            {
                if (HttpContext.Current.Session["NombreCompleto"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["NombreCompleto"].ToString();
            }
        }

        /// <summary>
        /// Almacena el rol id
        /// </summary>
        public static Nullable<int> ROL_ID
        {
            set { HttpContext.Current.Session["RolID"] = value; }
            get
            {

                if (HttpContext.Current.Session["RolID"] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session["RolID"]);

            }
        }

        /// <summary>
        /// Almacena usuario id
        /// </summary>
        public static int USUARIO_ID
        {
            set
            {
                HttpContext.Current.Session["UsuarioID"] = value;
            }
            get
            {
                if (HttpContext.Current.Session["UsuarioID"] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session["UsuarioID"]);
            }

        }



    }
}