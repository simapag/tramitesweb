﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AplicacionWEB.Models.Util
{
    public class Cls_Modelo_Menu
    {
        public string Nombre { get; set; }
        public string Accion { get; set; }
        public string Controlador { get; set; }
        public List<Cls_Modelo_Menu> Elementos { get; set; }
    }
}