﻿using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionWEB.Models
{
    public class FormMultipagoBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            Form_Pago_Multipagos form_Pago_Multipagos = new Form_Pago_Multipagos();

            string key;
            string value;
            string origenPeticion = "";
            string str_cantidad = "";
            double cantidad = 0;

            //for (int i = 0; i < request.Headers.Count; i++)
            //{
            //    key = request.Headers.GetKey(i);
            //    value = request.Headers.Get(i);

            //    if (key == "Origen" || key == "Origin")
            //    {
            //        origenPeticion = value;
            //    }

            //}
            form_Pago_Multipagos.mp_node = obtenerValor("mp_node", request);
            form_Pago_Multipagos.mp_concept = obtenerValor("mp_concept", request); 
            form_Pago_Multipagos.mp_order = obtenerValor("mp_order",request);
            form_Pago_Multipagos.mp_reference = obtenerValor("mp_reference", request); 
            form_Pago_Multipagos.mp_customername = obtenerValor("mp_customername", request);
            form_Pago_Multipagos.mp_signature = obtenerValor("mp_signature", request);
     
            form_Pago_Multipagos.mp_paymentMethod = obtenerValor("mp_paymentMethod", request);
            form_Pago_Multipagos.mp_date = obtenerValor("mp_date", request); 
            form_Pago_Multipagos.mp_pan = obtenerValor("mp_pan", request);    
            form_Pago_Multipagos.mp_authorization = obtenerValor("mp_authorization", request); 
            form_Pago_Multipagos.mp_responsemsg = obtenerValor("mp_responsemsg", request);
        
            form_Pago_Multipagos.mp_response = obtenerValor("mp_response", request);
            form_Pago_Multipagos.mp_trx_historyid = obtenerValor("mp_trx_historyid", request);
            form_Pago_Multipagos.mp_cardholdername = obtenerValor("mp_cardholdername", request);
            form_Pago_Multipagos.mp_email = obtenerValor("mp_email", request);
            form_Pago_Multipagos.mp_phone = obtenerValor("mp_phone", request);
            form_Pago_Multipagos.mp_cardType = obtenerValor("mp_cardType", request);
            form_Pago_Multipagos.mp_bankname = obtenerValor("mp_bankname", request); 
            form_Pago_Multipagos.origen = "";
            str_cantidad = obtenerValor("mp_amount", request);
            form_Pago_Multipagos.cantidad_cadena = str_cantidad;

            double.TryParse(str_cantidad, out cantidad);
            form_Pago_Multipagos.mp_amount = Convert.ToDecimal(cantidad);

            return form_Pago_Multipagos;

        }


        public string obtenerValor(string nombreParametro, HttpRequestBase request)
        {
            string respuesta = "";

            if (request[nombreParametro] != null)
                respuesta = request[nombreParametro].ToString();

            return respuesta;
        }
    }
}