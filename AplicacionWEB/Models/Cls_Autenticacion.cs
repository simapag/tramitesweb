﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace AplicacionWEB.Models
{
    public class Cls_Autenticacion
    {
        [Required(ErrorMessage = "Se requiere un correo")]
        [RegularExpression(".+\\@.+\\..+",
      ErrorMessage = "Se requiere un correo electrónico valido")]
        public string CorreoElectronico { get; set; }

        [Required(ErrorMessage = "Se requiere una Contraseña")]
        public string Password { get; set; }
    }
}