﻿var Correccion_Domicilio_Model = function () {

    var _Tramite_Solicitados_ID = ko.observable(''),
        _Formulario_Correccion_Domicilio_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _Domicilio_Anterior = ko.observable(''),
        _Domicilio_Nuevo = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' });

    return {
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Formulario_Correccion_Domicilio_ID: _Formulario_Correccion_Domicilio_ID,
        RPU: _RPU,
        Domicilio_Anterior: _Domicilio_Anterior,
        Domicilio_Nuevo: _Domicilio_Nuevo
    }

}