﻿
var Actualizar_Titular_Contrato_Model = function () {

    var _Formulario_Actualizar_Contrato_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _Nombre_Actual = ko.observable('').extend({ required: true }),
        _Domicilio_Actual = ko.observable(''),
        _Nombre_Nuevo = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 100 });

    return {

        Formulario_Actualizar_Contrato_ID: _Formulario_Actualizar_Contrato_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        RPU: _RPU,
        Nombre_Actual: _Nombre_Actual,
        Domicilio_Actual: _Domicilio_Actual,
        Nombre_Nuevo:_Nombre_Nuevo
    }

}