﻿var Modelo_Tramite = function () {

    var _Tramite_ID = ko.observable(''),
        _Nombre_Tramite = ko.observable('')
            .extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ maxLength: 199 }),
        _Costo_Tramite = ko.observable('')
            .extend({ min: 0 })
            .extend({ max: 10000 })
            .extend({ required: true })
            .extend({ number: true })
            .extend({ maxLength: 6 }),
        _Estatus = ko.observable('').extend({ required: true })
    _Controlador = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
    _Nivel_Banco = ko.observable('')
       .extend({ digit: true })
        .extend({ maxLength: 2 });

    return {
        Tramite_ID: _Tramite_ID,
        Nombre_Tramite: _Nombre_Tramite,
        Costo_Tramite: _Costo_Tramite,
        Estatus: _Estatus,
        Controlador: _Controlador,
        Nivel_Banco: _Nivel_Banco
    }

};


var Model_Tramite_Documento = function () {

    var _Tramite_Documento_ID = ko.observable(''),
        _Tramite_ID = ko.observable('0'),
        _Nombre_Tramite = ko.observable('').extend({ required: true }),
        _Documento_ID = ko.observable(''),
        _Documento_Selected = ko.observable('').extend({ required: true });

    return {
        Tramite_Documento_ID: _Tramite_Documento_ID,
        Tramite_ID: _Tramite_ID,
        Nombre_Tramite: _Nombre_Tramite,
        Documento_ID: _Documento_ID,
        Documento_Selected : _Documento_Selected
    }

}