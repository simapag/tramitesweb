﻿
var ObjTramiteDocumentoViewModel;


$(function () {
    ObjTramiteDocumentoViewModel = new tramiteDocumentoViewModel();
    ObjTramiteDocumentoViewModel.ModeloTramiteDocumento.errors = ko.validation.group(ObjTramiteDocumentoViewModel.ModeloTramiteDocumento);
    ko.applyBindings(ObjTramiteDocumentoViewModel, $("#Interfaz_Tramite_Documentos")[0]);
    ObjTramiteDocumentoViewModel.Cargar_Informacion();
});


function tramiteDocumentoViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";



    self.ModeloTramiteDocumento = Model_Tramite_Documento();
    self.ArregloDocumentos = ko.observableArray([]);
    self.bloquearControl = ko.observable(false);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);

    //Ventanas *******************************************

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };


    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz_Tramites_Documento');
        ventanaInterfaz.modal();
    };

    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz_Tramites_Documento');
        ventanaInterfaz.modal('hide');
    };


    // Busquedas *************************************************
    self.Cargar_Informacion = function () {
        crearTablaTramiteDocumento();
        self.ArregloDocumentos($.parseJSON(Datos_Lista_Documentos));
    };

    self.Buscar_Datos = function () {
        TablaTramiteDocumento.bootstrapTable('refresh');
    };


    self.Limpiar_Cajas = function () {

        self.ModeloTramiteDocumento.Tramite_Documento_ID('0');
        self.ModeloTramiteDocumento.Tramite_ID('');
        self.ModeloTramiteDocumento.Nombre_Tramite('');
        self.ModeloTramiteDocumento.Documento_ID('');
        self.ModeloTramiteDocumento.Documento_Selected(self.ArregloDocumentos.find('name', { name: 'Selecciona' }));
        self.ModeloTramiteDocumento.errors.showAllMessages(false);

    };

    self.Asignar_Valores = function (p_Row) {
        self.ModeloTramiteDocumento.Tramite_Documento_ID(p_Row.Tramite_Documento_ID);
        self.ModeloTramiteDocumento.Tramite_ID(p_Row.Tramite_ID);
        self.ModeloTramiteDocumento.Nombre_Tramite(p_Row.Nombre_Tramite);
        self.ModeloTramiteDocumento.Documento_ID(p_Row.Documento_ID);
        self.ModeloTramiteDocumento.Documento_Selected(self.ArregloDocumentos.find('id', { id: p_Row.Documento_ID }));
        self.ModeloTramiteDocumento.errors.showAllMessages(false);
    }



    self.Nuevo_Elemento = function () {
        self.Limpiar_Cajas();
        self.Abrir_Ventana_Interfaz();

    };

    self.Borrar_Elemento = function (p_Row) {

        self.ValidarDatos(false);
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar_Tramite_Documento");

        self.Procesar_Operacion();
    };

    self.Aceptar_Operacion = function () {
        self.TipoOperacion('Guardar_Tramite_Documento');
            self.ValidarDatos(true);
            self.Procesar_Operacion();
    };


    // Peticiones Ajax ***********************************************

    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (self.ModeloTramiteDocumento.errors().length > 0) {
                self.ModeloTramiteDocumento.errors.showAllMessages();
                $.toast({
                    heading: 'Catálogo de Trámites',
                    text: 'Se requiere un valores',
                    position: 'top-right',
                    stack: false
                });
                return;
            }
        }

        self.ModeloTramiteDocumento.Documento_ID(self.ModeloTramiteDocumento.Documento_Selected().id);
        strDatos = ko.toJSON(self.ModeloTramiteDocumento);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Interfaz();
                    $.toast({
                        heading: 'Catálogo de Trámites',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                    
                }
                else {
                    $.toast({
                        heading: 'Catálogo de Trámites',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    }); 
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Trámite y Documentos");

            }

        });

    };



}