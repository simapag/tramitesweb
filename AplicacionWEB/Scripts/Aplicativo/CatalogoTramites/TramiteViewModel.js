﻿var ObjTramiteViewModel;

$(function () {
    ObjTramiteViewModel = new tramiteViewModel();
    ObjTramiteViewModel.ModeloTramite.errors = ko.validation.group(ObjTramiteViewModel.ModeloTramite);
    ko.applyBindings(ObjTramiteViewModel, $("#interfaz_catalogo_tramites")[0]);
    
});


function tramiteViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloTramite = Modelo_Tramite();
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);

    //Ventanas *******************************************

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    };

    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    };

      // Busquedas *************************************************
    self.Buscar_Datos = function () {
        TablaTramite.bootstrapTable('refresh');
    };

 

    self.Limpiar_Cajas = function () {

        self.ModeloTramite.Tramite_ID('0');
        self.ModeloTramite.Nombre_Tramite('');
        self.ModeloTramite.Costo_Tramite('');
        self.ModeloTramite.Estatus('');
        self.ModeloTramite.Controlador('');
        self.ModeloTramite.Nivel_Banco('');
        self.ModeloTramite.errors.showAllMessages(false);

    };

    self.Asignar_Valores = function (p_Row) {
        self.ModeloTramite.Tramite_ID(p_Row.Tramite_ID);
        self.ModeloTramite.Nombre_Tramite(p_Row.Nombre_Tramite);
        self.ModeloTramite.Costo_Tramite(p_Row.Costo_Tramite);
        self.ModeloTramite.Estatus(p_Row.Estatus);
        self.ModeloTramite.Controlador(p_Row.Controlador);
        self.ModeloTramite.Nivel_Banco(p_Row.Nivel_Banco);
        self.ModeloTramite.errors.showAllMessages(false);
    }

    // Operaciones ***********************************************

    self.Nuevo_Elemento = function () {
        self.Abrir_Ventana_Interfaz();
        self.Limpiar_Cajas();
    };

    self.Editar_Elemento = function (p_Row) {
        self.Limpiar_Cajas();
        self.Asignar_Valores(p_Row);
        self.Abrir_Ventana_Interfaz();
    };

    self.Borrar_Elemento = function (p_Row) {
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar_Tramite");
        self.ValidarDatos(false);
        self.Procesar_Operacion();
    };

    self.Aceptar_Operacion = function () { 
        self.TipoOperacion('Guardar_Tramite');
        self.ValidarDatos(true);
        self.Procesar_Operacion();
    };


    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (self.ModeloTramite.errors().length > 0) {
                self.ModeloTramite.errors.showAllMessages();
                $.toast({
                    heading: 'Catálogo de Trámites',
                    text: 'Se requiere un valores',
                    position: 'top-right',
                    stack: false
                });
                return;
            }
        }

   

        strDatos = ko.toJSON(self.ModeloTramite);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Interfaz();
                    $.toast({
                        heading: 'Catálogo de Trámites',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                }
                else {
                    $.toast({
                        heading: 'Catálogo de Trámites',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });  
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Catálogo de Trámites");

            }

        });

    };

}