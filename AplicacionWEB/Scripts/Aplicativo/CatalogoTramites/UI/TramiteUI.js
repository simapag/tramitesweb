﻿
var TablaTramite = $("#Tabla_Tramite");
var TablaTramiteDocumento = $("#Tabla_Tramites_Documento");
var RenglonSeleccionado;
var RenglonSeleccionadoTramiteDocumento;

$(function () {
    Inicializar_Tabla();

    TablaTramite.on('click-row.bs.table', function (e, row, $element) {
        $('.success').removeClass('success');
        $($element).addClass('success');
        ObjTramiteDocumentoViewModel.ModeloTramiteDocumento.Tramite_ID(row.Tramite_ID);
        TablaTramiteDocumento.bootstrapTable('refresh');
    });

    TablaTramite.on('search.bs.table', function (e, row, $element) {
        TablaTramiteDocumento.bootstrapTable('removeAll');
    });

    TablaTramite.on('page-change.bs.table', function (e, row, $element) {
        TablaTramiteDocumento.bootstrapTable('removeAll');
    });



});


function Inicializar_Tabla() {
    TablaTramite.bootstrapTable('destroy');
    TablaTramite.bootstrapTable({ url: 'Obtener_Tramites' });

}



function crearTablaTramiteDocumento() {

    TablaTramiteDocumento.bootstrapTable('destroy');
    TablaTramiteDocumento.bootstrapTable({
        url: 'Obtener_Tramites_Documento',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                Tramite_ID: ObjTramiteDocumentoViewModel.ModeloTramiteDocumento.Tramite_ID()
            };
        }
    });
}

function Botones_Operacion_Formato_Eliminar_Tramite_Documento(value, row, index) {
    return [
        '<a class="eliminar_documento" href="javascript:void(0)" title="Eliminar Documento">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}



function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Eliminar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}

function Botones_Operacion_Formato_Editar(value, row, index) {
    return [
        '<a class="editar" href="javascript:void(0)" title="Editar">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>'
    ].join('');
}

function Botones_Operacion_Agregar_Documento(value, row, index) {
    return [
        '<a class="agregar_documento" href="javascript:void(0)" title="Agregar Documento">',
        '<i class="glyphicon glyphicon-plus-sign"></i>',
        '</a>'
    ].join('');
}


window.Botones_Operacion_Evento = {

    'click .eliminar': function (e, value, row, index) {

        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.Nombre_Tramite + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionado = row;


    },
    'click .editar': function (e, value, row, index) {
        ObjTramiteViewModel.Editar_Elemento(row);
    },
    'click .agregar_documento': function (e, value, row, index) {
        ObjTramiteDocumentoViewModel.Nuevo_Elemento();
        ObjTramiteDocumentoViewModel.ModeloTramiteDocumento.Nombre_Tramite(row.Nombre_Tramite);
        ObjTramiteDocumentoViewModel.ModeloTramiteDocumento.Tramite_ID(row.Tramite_ID);
    },
    'click .eliminar_documento': function (e, value, row, index) {
        var ventanaEliminar = $('#VentanaEliminarTramiteDocumento');
        $('#SpanDatoTramite').html('<b> ' + row.Nombre_Documento + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionadoTramiteDocumento = row;
    }


};

function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjTramiteViewModel.Borrar_Elemento(RenglonSeleccionado);
}


function Eliminar_Registro_Tramite_Documento() {
    var ventanaEliminar = $('#VentanaEliminarTramiteDocumento');
    ventanaEliminar.modal('hide');
    ObjTramiteDocumentoViewModel.Borrar_Elemento(RenglonSeleccionadoTramiteDocumento);
}