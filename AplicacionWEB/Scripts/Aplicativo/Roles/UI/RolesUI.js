﻿
var TablaRoles = $('#tabla_roles');
var RenglonSeleccionado;

$(function () {

    Inicializar_Tabla();
  
});


   /// <summary>
        ///    Inicializar parametros de la tabla
     /// </summary>
function Inicializar_Tabla() {  
    TablaRoles.bootstrapTable('destroy');
    TablaRoles.bootstrapTable({ url: 'Obtener_Roles' });

}


   /// <summary>
        ///   pone los iconos de operación Eliminar
     /// </summary>
function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Eliminar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
         ].join('');
}


   /// <summary>
        ///   pone los iconos de operación Editar
   /// </summary>
function Botones_Operacion_Formato_Editar(value, row, index) {
    return [
        '<a class="editar" href="javascript:void(0)" title="Editar">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>'
    ].join('');
}



 /// <summary>
        ///   escuchador de icones de operación
     /// </summary>
window.Botones_Operacion_Evento = {

    'click .eliminar': function (e, value, row, index) {

        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.nombre + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionado = row; 

         
    },
    'click .editar': function (e, value, row, index) {
        ObjRolesViewModel.Editar_Elemento(row);
    }


};

/// <summary>
///   Ejecuta Proceso de Eliminacion
/// </summary>
function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjRolesViewModel.Borrar_Elemento(RenglonSeleccionado);
}


/// <summary>
///   Ejecuta Proceso de Inactivación
/// </summary>
function Inactivar_Elemento() {
    ObjRolesViewModel.Inactivar_Elemento();

}