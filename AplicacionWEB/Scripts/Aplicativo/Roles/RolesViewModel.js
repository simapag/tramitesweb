﻿
var ObjRolesViewModel;


$(function () {

    ObjRolesViewModel = new Roles_ViewModel();
    ObjRolesViewModel.ModeloRoles.errors = ko.validation.group(ObjRolesViewModel.ModeloRoles);

    ko.applyBindings(ObjRolesViewModel, $("#Interface_Datos")[0]);
    ko.applyBindings(ObjRolesViewModel, $("#Interface_Botones")[0]);

});

   /// <summary>
        ///    El view model para roles
   /// </summary>
function Roles_ViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";


    self.ModeloRoles = Modelo_Roles();
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);

    //-----------------------------------------------
    
   /// <summary>
        ///     abre una ventana indicando que se esta haciendo una petición ajax
     /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }

     /// <summary>
        ///     cierra una ventana indicando que se esta haciendo una petición ajax
     /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }


    /// <summary>
        ///     abre una ventana para registrar información en la base de datos
     /// </summary>

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    }

     /// <summary>
        ///     cierra una ventana para registrar información en la base de datos
     /// </summary>
    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    }


    /// <summary>
    ///     abre una ventana para inactivar elemento
    /// </summary>
    self.Abrir_Ventana_Inactivar = function () {
        var ventanaInterfaz = $('#VentanaInactivar')
        ventanaInterfaz.modal();
    }

    /// <summary>
    ///     cierra una ventana para inactivar elemento
    /// </summary>
    self.Cerrar_Ventana_Inactivar = function () {
        var ventanaInterfaz = $('#VentanaInactivar')
        ventanaInterfaz.modal('hide');
    }




    //----------------------------------------------
     /// <summary>
        ///    hace una busqueda para traer información a la tabla
     /// </summary>
    self.Buscar_Datos = function () {
        TablaRoles.bootstrapTable('refresh');
    }


     /// <summary>
        ///    Limpia los datos del modelo
     /// </summary>
    self.Limpiar_Cajas = function () {
        
        self.ModeloRoles.Rol_ID('0');
        self.ModeloRoles.Nombre('');
        self.ModeloRoles.Descripcion('');
        self.ModeloRoles.Estatus('ACTIVO');
    }
 
     /// <summary>
        ///    Asigna datos al modelo
     /// </summary>
    self.Asignar_Valores = function (p_Row) {

        self.ModeloRoles.Rol_ID(p_Row.id);
        self.ModeloRoles.Nombre(p_Row.nombre);
        self.ModeloRoles.Descripcion(p_Row.descripcion);
        self.ModeloRoles.Estatus(p_Row.estatus);
    }


     /// <summary>
        ///    Operación para un Nuevo Elemento
     /// </summary>
    self.Nuevo_Elemento = function () {
        self.Abrir_Ventana_Interfaz();
        self.Limpiar_Cajas();
    }


     /// <summary>
        ///   Operación para editar un elemento
    /// </summary>
    self.Editar_Elemento = function (p_Row) {
        self.Limpiar_Cajas();
        self.Asignar_Valores(p_Row);
        self.Abrir_Ventana_Interfaz();
    }

     /// <summary>
        ///  Operación para eliminar un elemento
    /// </summary>
    self.Borrar_Elemento = function (p_Row) {
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar_Rol");
        self.ValidarDatos(false);
        self.Procesar_Operacion();
    }


    
    /// <summary>
        ///  funcionalidad del botón de aceptar
    /// </summary>
    self.Aceptar_Operacion = function () {

        if (self.ModeloRoles.Rol_ID() === "0") {
            self.TipoOperacion('Nuevo_Rol')
        } else
            self.TipoOperacion('Editar_Rol')

        self.ValidarDatos(true);
        self.Procesar_Operacion();
    }

    //-----------------------------------------------


      /// <summary>
        ///   Ejecuta las operaciones de nuevo y editar
    /// </summary>
    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (ObjRolesViewModel.ModeloRoles.errors().length > 0) {
                ObjRolesViewModel.ModeloRoles.errors.showAllMessages();
                $.alert("Se requiren unos valores", "Roles");
                return;
            }
        }


        strDatos = ko.toJSON(self.ModeloRoles);
        self.Abrir_Ventana_Espera();


        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                        self.Buscar_Datos();
                        self.Cerrar_Ventana_Interfaz();
                        $.alert("Proceso Terminado", { type:'success'});
                }
                else {

                    if (self.ValidarDatos() === false) {

                        if (response.Mensaje === 'El elemento  esta referenciado en otra tabla') {
                            self.Abrir_Ventana_Inactivar();
                            $('#SpanDatoInactivar').html(self.ModeloRoles.Nombre());
                        }

                    } else {
                        $.alert(" " + response.Mensaje, "Roles");
                    }
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");
                
            }

        });


    }

   /// <summary>
        ///   Inactiva el elemento si el usuario así lo desea
    /// </summary>
    self.Inactivar_Elemento = function () {

        var strDatos;
        strDatos = ko.toJSON(self.ModeloRoles);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Inactivar_Rol',
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Inactivar();
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {
                        $.alert(" " + response.Mensaje, "Roles");
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");
            }

        });

    }




}



