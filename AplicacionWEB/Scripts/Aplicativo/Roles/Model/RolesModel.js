﻿

   /// <summary>
        ///    Modelo de datos para roles
     /// </summary>
var Modelo_Roles = function () {


    var _Rol_ID = ko.observable(''),
        _Nombre = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ maxLength: 50 }),
        _Descripcion = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ maxLength: 50 }),
        _Estatus = ko.observable('').extend({ required: true });

    return {
        Rol_ID :_Rol_ID,
        Nombre : _Nombre,
        Descripcion : _Descripcion,
        Estatus : _Estatus
    }

}