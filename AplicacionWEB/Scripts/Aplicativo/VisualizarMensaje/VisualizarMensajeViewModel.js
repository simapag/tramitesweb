﻿var objMensajeViewModel;

$(function () {

    objMensajeViewModel = new mensajesViewModel();
    objMensajeViewModel.errors = ko.validation.group(objMensajeViewModel);
    ko.applyBindings(objMensajeViewModel, $("#interfaz_formulario")[0]);
});


function mensajesViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.Comentario = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
        .extend({ maxLength: 500 })
        .extend({ required: true });



    self.estaValidado = function () {
        var respuesta = true;


        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            respuesta = false;
        }

        return respuesta;

    }
}