﻿

var RPU_Asociado_Modelo = function () {

    var _RPU_Asociados_ID = ko.observable('0'),
        _Usuarios_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _Predio_ID = ko.observable('');

    return {
        RPU_Asociados_ID: _RPU_Asociados_ID,
        Usuarios_ID: _Usuarios_ID,
        RPU: _RPU,
        Predio_ID: _Predio_ID
    }

}

var Datos_Cuenta_Modelo = function () {


    var _RPU = ko.observable('').extend({ required: true }),
        _Usuarios_ID = ko.observable(''),
        _Nombre_Usuario = ko.observable('').extend({ required: true }),
        _Predio_ID = ko.observable(''),
        _Colonia = ko.observable(''),
        _Calle = ko.observable(''),
        _Numero_Exterior = ko.observable(''),
        _Numero_Interior = ko.observable('');

    return {
        RPU: _RPU,
        Usuarios_ID:_Usuarios_ID,
        Nombre_Usuario: _Nombre_Usuario,
        Predio_ID: _Predio_ID,
        Colonia: _Colonia,
        Calle: _Calle,
        Numero_Exterior: _Numero_Exterior,
        Numero_Interior: _Numero_Interior
    }
}


var Busqueda_Modelo = function () {
    var _RPU = ko.observable('').extend({ required: true })
        .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' });

    return {
        RPU:_RPU
    }
}