﻿

var TablaRPUAsociados = $("#Tabla_RPU_Asociados");
var RenglonSeleccionado;
$(function () {


});


function createTableRPUAsociados() {

    TablaRPUAsociados.bootstrapTable('destroy');
    TablaRPUAsociados.bootstrapTable({
        url: 'Obtener_RPU_Asociados',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                Usuarios_ID: ObjAsociarRPUViewModel.ModeloDatosCuenta.Usuarios_ID()
            };
        }
    });

}

function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Eliminar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}



window.Botones_Operacion_Evento = {
    'click .eliminar': function (e, value, row, index) {
        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.RPU + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionado = row;
    }
};


function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjAsociarRPUViewModel.eliminarElemento(RenglonSeleccionado);
}