﻿
var ObjAsociarRPUViewModel;

$(function () {

    ObjAsociarRPUViewModel = new asociarRPUViewModel();
    ObjAsociarRPUViewModel.ModeloBusqueda.errors = ko.validation.group(ObjAsociarRPUViewModel.ModeloBusqueda);
    ObjAsociarRPUViewModel.ModeloDatosCuenta.errors = ko.validation.group(ObjAsociarRPUViewModel.ModeloDatosCuenta);
    ko.applyBindings(ObjAsociarRPUViewModel, $("#Interfaz_Asociar_RPU")[0]);
    ObjAsociarRPUViewModel.cargarInformacion();
});

function asociarRPUViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloBusqueda = Busqueda_Modelo();
    self.ModeloDatosCuenta = Datos_Cuenta_Modelo();
    self.ModeloRPUAsociado = RPU_Asociado_Modelo();
    self.arregloUsuarios = [];
    self.bloquearControl = ko.observable(false);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);


    self.Abrir_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();
    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };


    self.limpiarDatos = function () {

        self.ModeloDatosCuenta.RPU('');
        self.ModeloDatosCuenta.Nombre_Usuario('');
        self.ModeloDatosCuenta.Predio_ID('');
        self.ModeloDatosCuenta.Colonia('');
        self.ModeloDatosCuenta.Calle('');
        self.ModeloDatosCuenta.Numero_Exterior('');
        self.ModeloDatosCuenta.Numero_Interior('');
        self.ModeloDatosCuenta.errors.showAllMessages(false);
    }


    self.asignarDatosUsuario = function (datos) {
        self.ModeloDatosCuenta.RPU(datos.RPU);
        self.ModeloDatosCuenta.Nombre_Usuario(datos.Nombre_Usuario);
        self.ModeloDatosCuenta.Predio_ID(datos.Predio_ID);
        self.ModeloDatosCuenta.Colonia(datos.Colonia);
        self.ModeloDatosCuenta.Calle(datos.Calle);
        self.ModeloDatosCuenta.Numero_Exterior(datos.Numero_Exterior);
        self.ModeloDatosCuenta.Numero_Interior(datos.Numero_Interior);

    }

    // Busquedas *************************************************
    self.cargarInformacion = function () { 
        self.ModeloDatosCuenta.Usuarios_ID(Datos_USUARIO_ID);
        createTableRPUAsociados();
    };

    self.regresar = function () {
        window.history.back();

    };

   
    self.buscarCuenta = function () {

        if (self.ModeloBusqueda.errors().length > 0) {
            self.ModeloBusqueda.errors.showAllMessages();
            $.toast({
                heading: 'Asociar RPU',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        self.Abrir_Ventana_Espera();
        self.limpiarDatos();

        $.ajax({
            type: "GET",
            url: 'Obtener_Datos_Cuenta?RPU=' + self.ModeloBusqueda.RPU(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {

                    self.arregloUsuarios = $.parseJSON(response.Dato1);
                    self.asignarDatosUsuario(self.arregloUsuarios[0]);
                    $.toast({
                        heading: 'Asociar RPU',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                }
                else {
                    $.toast({
                        heading: 'Asociar RPU',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Asociar RPU");

            }

        });



    }

    self.buscar = function () {
        TablaRPUAsociados.bootstrapTable('refresh');
    }

    self.asociarRPU = function () {


        if (self.ModeloDatosCuenta.errors().length > 0) {
            self.ModeloDatosCuenta.errors.showAllMessages();
            $.toast({
                heading: 'Asociar RPU',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        self.ModeloRPUAsociado.RPU_Asociados_ID(0);
        self.ModeloRPUAsociado.Usuarios_ID(self.ModeloDatosCuenta.Usuarios_ID());
        self.ModeloRPUAsociado.RPU(self.ModeloDatosCuenta.RPU());
        self.ModeloRPUAsociado.Predio_ID(self.ModeloDatosCuenta.Predio_ID());
        self.TipoOperacion('Registar_RPU_Asociado');
        self.Procesar_Operacion();
        self.ValidarDatos(false);

    }


    self.eliminarElemento = function (datos) {

        self.ModeloRPUAsociado.RPU_Asociados_ID(datos.RPU_Asociados_ID);
        self.ModeloRPUAsociado.Usuarios_ID(datos.Usuarios_ID);
        self.ModeloRPUAsociado.RPU(datos.RPU);
        self.ModeloRPUAsociado.Predio_ID(datos.Predio_ID);
        self.TipoOperacion('Eliminar_RPU_Asociado');
        self.Procesar_Operacion();
        self.ValidarDatos(false);
    }

    self.Procesar_Operacion = function () {

        var strDatos;
        strDatos = ko.toJSON(self.ModeloRPUAsociado);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.limpiarDatos();
                    self.buscar();
                    $.toast({
                        heading: 'Asociar RPU',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                }
                else {
                    $.toast({
                        heading: 'Asociar RPU',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Catálogo de Trámites");

            }

        });

    };


}