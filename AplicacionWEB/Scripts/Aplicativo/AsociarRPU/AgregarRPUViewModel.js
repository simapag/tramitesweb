﻿var ObjAgregarRPUViewModel;
var funcionTramiteSeleccionado;

$(function () {
    ObjAgregarRPUViewModel = new agregarRPUViewModel();
    ObjAgregarRPUViewModel.ModeloBusqueda.errors = ko.validation.group(ObjAgregarRPUViewModel.ModeloBusqueda);
    ObjAgregarRPUViewModel.ModeloDatosCuenta.errors = ko.validation.group(ObjAgregarRPUViewModel.ModeloDatosCuenta);
    ko.applyBindings(ObjAgregarRPUViewModel, $("#Interfaz_Agregar_RPU")[0]);
    ObjAgregarRPUViewModel.cargarInformacion();
});


function agregarRPUViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloBusqueda = Busqueda_Modelo();
    self.ModeloDatosCuenta = Datos_Cuenta_Modelo();
    self.ModeloRPUAsociado = RPU_Asociado_Modelo();
    self.arregloUsuarios = [];
    self.bloquearControl = ko.observable(false);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);


    self.Abrir_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera_RPU');
        ventantaEspera.modal();
    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera_RPU');
        ventantaEspera.modal('hide');
    };

    self.Cerrar_Ventana_Emergente = function () {

        var ventantaEspera = $('#Ventana_Agregar_RPU');
        ventantaEspera.modal('hide');
    }


    self.asignarFuncionTramite = function (ObjViewModel) {
        funcionTramiteSeleccionado = ObjViewModel;
    }

    self.limpiarDatos = function () {

        self.ModeloDatosCuenta.RPU('');
        self.ModeloDatosCuenta.Nombre_Usuario('');
        self.ModeloDatosCuenta.Predio_ID('');
        self.ModeloDatosCuenta.Colonia('');
        self.ModeloDatosCuenta.Calle('');
        self.ModeloDatosCuenta.Numero_Exterior('');
        self.ModeloDatosCuenta.Numero_Interior('');
        self.ModeloDatosCuenta.errors.showAllMessages(false);
    }


    self.asignarDatosUsuario = function (datos) {
        self.ModeloDatosCuenta.RPU(datos.RPU);
        self.ModeloDatosCuenta.Nombre_Usuario(datos.Nombre_Usuario);
        self.ModeloDatosCuenta.Predio_ID(datos.Predio_ID);
        self.ModeloDatosCuenta.Colonia(datos.Colonia);
        self.ModeloDatosCuenta.Calle(datos.Calle);
        self.ModeloDatosCuenta.Numero_Exterior(datos.Numero_Exterior);
        self.ModeloDatosCuenta.Numero_Interior(datos.Numero_Interior);

    }

    // Busquedas *************************************************
    self.cargarInformacion = function () {
        self.ModeloDatosCuenta.Usuarios_ID(Datos_USUARIO_ID);
   
    };

    self.buscarCuenta = function () {

        if (self.ModeloBusqueda.errors().length > 0) {
            self.ModeloBusqueda.errors.showAllMessages();
            $.toast({
                heading: 'Asociar RPU',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        self.Abrir_Ventana_Espera();
        self.limpiarDatos();

        $.ajax({
            type: "GET",
            url: '../AsociarRPU/Obtener_Datos_Cuenta?RPU=' + self.ModeloBusqueda.RPU(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {

                    self.arregloUsuarios = $.parseJSON(response.Dato1);
                    self.asignarDatosUsuario(self.arregloUsuarios[0]);
                    $.toast({
                        heading: 'Asociar RPU',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                }
                else {
                    $.toast({
                        heading: 'Asociar RPU',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Asociar RPU");

            }

        });



    }


    self.asociarRPU = function () {


        if (self.ModeloDatosCuenta.errors().length > 0) {
            self.ModeloDatosCuenta.errors.showAllMessages();
            $.toast({
                heading: 'Asociar RPU',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        self.ModeloRPUAsociado.RPU_Asociados_ID(0);
        self.ModeloRPUAsociado.Usuarios_ID(self.ModeloDatosCuenta.Usuarios_ID());
        self.ModeloRPUAsociado.RPU(self.ModeloDatosCuenta.RPU());
        self.ModeloRPUAsociado.Predio_ID(self.ModeloDatosCuenta.Predio_ID());
        self.TipoOperacion('../AsociarRPU/Registar_RPU_Asociado');
        self.Procesar_Operacion();
        self.ValidarDatos(false);
    }

      self.Procesar_Operacion = function () {

            var strDatos;
            strDatos = ko.toJSON(self.ModeloRPUAsociado);
            self.Abrir_Ventana_Espera();

            $.ajax({
                type: "POST",
                url: self.TipoOperacion(),
                data: strDatos,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    self.Cerrar_Ventana_Espera();
                    if (response.Mensaje === "bien") {
                        self.limpiarDatos();

                        if (funcionTramiteSeleccionado !== undefined && funcionTramiteSeleccionado !== null) {
                            funcionTramiteSeleccionado.ArregloRPU($.parseJSON(response.Dato1));
                            setTimeout(self.Cerrar_Ventana_Emergente(), 300);
                        }
                        else {

                            setTimeout(function () {
                                window.location.href = Url_Pago_En_Linea;
                            }, 10);

                            $.toast({
                                heading: 'Asociar RPU',
                                text: 'Proceso Correcto',
                                position: 'top-right',
                                stack: false,
                                icon: 'success'
                            }); 
                            
                        }
                               
                        $.toast({
                            heading: 'Asociar RPU',
                            text: 'Proceso Correcto',
                            position: 'top-right',
                            stack: false,
                            icon: 'success'
                        });
                    }
                    else {
                        $.toast({
                            heading: 'Asociar RPU',
                            text: response.Mensaje,
                            position: 'top-right',
                            stack: false,
                            icon: 'error'
                        });
                    }

                },
                error: function (result) {
                    self.Cerrar_Ventana_Espera();
                    $.alert(" " + result.status + ' ' + result.statusText, "Catálogo de Trámites");

                }

            });

        }

}