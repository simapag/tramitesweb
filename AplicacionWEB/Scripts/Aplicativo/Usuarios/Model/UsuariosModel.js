﻿
   /// <summary>
        ///    Modelo de datos para usuarios
     /// </summary>

var Modelo_Usuarios = function () {


    var _Usuarios_ID = ko.observable(''),
        _Nombre = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Apellido_Paterno = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Apellido_Materno = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Correo_Electronico = ko.observable('').extend({ required: true })
            .extend({ email: true })
            .extend({ maxLength: 80 }),
        _Direccion = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Telefono_Celular = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
            .extend({ maxLength: 30 }),
        _Telefono_Oficina = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
            .extend({ maxLength: 30 }),
        _Estatus = ko.observable('').extend({ required: true });


    return {
        Usuarios_ID: _Usuarios_ID,
        Nombre: _Nombre,
        Apellido_Paterno: _Apellido_Paterno,
        Apellido_Materno: _Apellido_Materno,
        Correo_Electronico: _Correo_Electronico,
        Direccion: _Direccion,
        Telefono_Celular: _Telefono_Celular,
        Telefono_Oficina: _Telefono_Oficina,
        Estatus: _Estatus
    }


}