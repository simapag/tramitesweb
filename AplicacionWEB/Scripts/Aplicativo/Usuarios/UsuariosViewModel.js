﻿
var ObjUsuariosViewModel;

$(function () {

    ObjUsuariosViewModel = new Usuarios_ViewModel();

    ObjUsuariosViewModel.ModeloUsuario.errors = ko.validation.group(ObjUsuariosViewModel.ModeloUsuario);
    ko.applyBindings(ObjUsuariosViewModel, $("#Interface_Datos")[0]);
    ko.applyBindings(ObjUsuariosViewModel, $("#Interface_Botones")[0]);


});



 /// <summary>
        ///    El view model para usuarios
   /// </summary>

function Usuarios_ViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloUsuario = Modelo_Usuarios();
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);

     //-----------------------------------------------

    /// <summary>
    ///     abre una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }

    /// <summary>
    ///     cierra una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }

    /// <summary>
    ///     abre una ventana para registrar información en la base de datos
    /// </summary>

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    }

    /// <summary>
    ///     cierra una ventana para registrar información en la base de datos
    /// </summary>
    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    }


    /// <summary>
    ///     abre una ventana para inactivar elemento
    /// </summary>
    self.Abrir_Ventana_Inactivar = function () {
        var ventanaInterfaz = $('#VentanaInactivar')
        ventanaInterfaz.modal();
    }

    /// <summary>
    ///     cierra una ventana para inactivar elemento
    /// </summary>
    self.Cerrar_Ventana_Inactivar = function () {
        var ventanaInterfaz = $('#VentanaInactivar')
        ventanaInterfaz.modal('hide');
    }


    //----------------------------------------------
    /// <summary>
    ///    hace una busqueda para traer información a la tabla
    /// </summary>
    self.Buscar_Datos = function () {
        TablaUsuarios.bootstrapTable('refresh');
    }

    /// <summary>
    ///    Limpia los datos del modelo
    /// </summary>
    self.Limpiar_Cajas = function () {

        self.ModeloUsuario.Usuarios_ID('0');
        self.ModeloUsuario.Nombre('');
        self.ModeloUsuario.Apellido_Paterno('');
        self.ModeloUsuario.Apellido_Materno('');
        self.ModeloUsuario.Correo_Electronico('');
        self.ModeloUsuario.Direccion('');
        self.ModeloUsuario.Telefono_Celular('');
        self.ModeloUsuario.Telefono_Oficina('');
        self.ModeloUsuario.Estatus('ACTIVO');
    }

    /// <summary>
    ///    Asigna datos al modelo
    /// </summary>
    self.Asignar_Valores = function (p_Row) {

        self.ModeloUsuario.Usuarios_ID(p_Row.id);
        self.ModeloUsuario.Nombre(p_Row.nombre);
        self.ModeloUsuario.Apellido_Paterno(p_Row.apellido_paterno);
        self.ModeloUsuario.Apellido_Materno(p_Row.apellido_materno);
        self.ModeloUsuario.Correo_Electronico(p_Row.correo);
        self.ModeloUsuario.Direccion(p_Row.direccion);
        self.ModeloUsuario.Telefono_Celular(p_Row.telefono_celular);
        self.ModeloUsuario.Telefono_Oficina(p_Row.telefono_oficina);
        self.ModeloUsuario.Estatus(p_Row.estatus);
    }

    /// <summary>
    ///    Operación para un Nuevo Elemento
    /// </summary>
    self.Nuevo_Elemento = function () {
        self.Abrir_Ventana_Interfaz();
        self.Limpiar_Cajas();
    }

    /// <summary>
    ///   Operación para editar un elemento
    /// </summary>
    self.Editar_Elemento = function (p_Row) {
        self.Limpiar_Cajas();
        self.Asignar_Valores(p_Row);
        self.Abrir_Ventana_Interfaz();
    }

    /// <summary>
    ///  Operación para eliminar un elemento
    /// </summary>
    self.Borrar_Elemento = function (p_Row) {
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar_Usuario");
        self.ValidarDatos(false);
        self.Procesar_Operacion();
    }

    /// <summary>
    ///  funcionalidad del botón de aceptar
    /// </summary>
    self.Aceptar_Operacion = function () {

        if (self.ModeloUsuario.Usuarios_ID() === "0") {
            self.TipoOperacion('Nuevo_Usuario')
        } else
            self.TipoOperacion('Editar_Usuario')

        self.ValidarDatos(true);
        self.Procesar_Operacion();
    }


    //-----------------------------------------------


    /// <summary>
    ///   Ejecuta las operaciones de nuevo y editar
    /// </summary>
    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (ObjUsuariosViewModel.ModeloUsuario.errors().length > 0) {
                ObjUsuariosViewModel.ModeloUsuario.errors.showAllMessages();
                $.alert("Se requiren unos valores", "Usuarios");
                return;
            }
        }


        strDatos = ko.toJSON(self.ModeloUsuario);
        self.Abrir_Ventana_Espera();


        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Interfaz();
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {

                    if (self.ValidarDatos() === false) {

                        if (response.Mensaje === 'El elemento  esta referenciado en otra tabla') {
                            self.Abrir_Ventana_Inactivar();
                            $('#SpanDatoInactivar').html(self.ModeloUsuario.Correo_Electronico());
                        }
                    }else {
                        $.alert(" " + response.Mensaje, "Usuarios");
                    }

                
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Interfaz();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");
            }

        });

    }


    /// <summary>
    ///   Inactiva el elemento si el usuario así lo desea
    /// </summary>
    self.Inactivar_Elemento = function () {

        var strDatos;
        strDatos = ko.toJSON(self.ModeloUsuario);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Inactivar_Usuario',
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Inactivar();
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {
                    $.alert(" " + response.Mensaje, "Menu");
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");
            }

        });

    }



}