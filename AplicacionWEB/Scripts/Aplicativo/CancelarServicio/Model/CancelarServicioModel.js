﻿var Cancelar_Servicio_Model = function () {

    var _Formulario_Cancelar_Servicio_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _NoCuenta = ko.observable(''),
        _Nombre_Usuario = ko.observable(''),
        _Domicilio = ko.observable('');

    return {
        Formulario_Cancelar_Servicio_ID: _Formulario_Cancelar_Servicio_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        NoCuenta : _NoCuenta,
        RPU: _RPU,
        Nombre_Usuario: _Nombre_Usuario,
        Domicilio: _Domicilio
    }

}