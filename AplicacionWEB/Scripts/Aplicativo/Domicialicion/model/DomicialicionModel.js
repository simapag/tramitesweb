﻿
var Domiciliacion_Model = function () {


    var _Formulario_Domiciliacion_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _radioPlazoSelection = ko.observable(''),
        _RPU = ko.observable(''),
        _RFC = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                                .extend({ maxLength: 14 })
                                .extend({ required: true }),
        _Nombre_Proveedor = ko.observable('').extend({ required: true }),
        _Periodicidad = ko.observable('').extend({ required: true }),
        _Nombre_Banco = ko.observable('').extend({ required: true }),
        _Numero_Tarjeta = ko.observable('').extend({ digit: true })
                                 .extend({ required: true })
                                .extend({ minLength: 16 })
                                .extend({ maxLength: 16 }),
        _CLABE = ko.observable('').extend({ digit: true })
                                .extend({ required: true })
                                .extend({ minLength: 18 })
                                .extend({ maxLength: 18 }),
        _Telefono = ko.observable('').extend({ required: true })
                     .extend({ digit: true })
                     .extend({ minLength: 10 })
                     .extend({ maxLength: 10 }),
        _Monto_Maximo = ko.observable('').extend({ required: true })
           .extend({ min: 10 })
           .extend({ number: true })
           .extend({ maxLength: 10 }),
        _Plazo_Indeterminado = ko.observable('').extend({ required: true }),
        _Fecha_Vencimiento = ko.observable('').extend({ esFecha: true })
                  .extend({
                      required: {
                          onlyIf: function () {
                              if (_radioPlazoSelection() == "fecha_vencimiento")
                                  return true;
                              else
                                  return false;
                          }
                      }
                  });


    return {
        Formulario_Domiciliacion_ID: _Formulario_Domiciliacion_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        radioPlazoSelection: _radioPlazoSelection,
        RPU: _RPU,
        RFC: _RFC,
        Nombre_Proveedor: _Nombre_Proveedor,
        Periodicidad: _Periodicidad,
        Nombre_Banco: _Nombre_Banco,
        Numero_Tarjeta: _Numero_Tarjeta,
        CLABE: _CLABE,
        Telefono: _Telefono,
        Monto_Maximo: _Monto_Maximo,
        Plazo_Indeterminado: _Plazo_Indeterminado,
        Fecha_Vencimiento: _Fecha_Vencimiento
    }
}