﻿var ObjDomiciliacionViewModel;

$(function () {

    crearFechas();
    ObjDomiciliacionViewModel = new domiciliacionViewModel();
    ObjDomiciliacionViewModel.SolicitudTramiteModel.errors = ko.validation.group(ObjDomiciliacionViewModel.SolicitudTramiteModel);
    ObjDomiciliacionViewModel.DomiciliacionModel.errors = ko.validation.group(ObjDomiciliacionViewModel.DomiciliacionModel);
    ko.applyBindings(ObjDomiciliacionViewModel, $("#interfaz_documentos")[0]);
    ko.applyBindings(ObjDomiciliacionViewModel, $("#interfaz_domiciliacion")[0]);

    ObjDomiciliacionViewModel.cargarInformacion();
});


function crearFechas() {
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        language: "es",
        autoclose: "true",
        orientation: "bottom left"
    });
}


function domiciliacionViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.SolicitudTramiteModel = Solicitud_Tramite_Model();
    self.DomiciliacionModel = Domiciliacion_Model();
    self.ArregloDocumentos = ko.observable([]);
    self.ArregloRPU = ko.observableArray([]);
    self.ArregloPeriodicidad = ko.observableArray(['SEMANAL', 'QUINCENAL', 'MENSUAL', 'BIMESTRAL','ANUAL']);
    self.bloquearControl = ko.observable(false);
    self.TipoOperacion = ko.observable('');
    self.habilitado = ko.observable(false);
    self.llevaFecha = ko.observable(false);

    //Ventanas *******************************************
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };


    //cargar informacion **************************************

    self.cargarInformacion = function () {
        self.SolicitudTramiteModel.RPU_Requerido(Model_Solicitud_Tramite.RPU_Requerido);
        self.SolicitudTramiteModel.Tramite_Solicitados_ID(Model_Solicitud_Tramite.Tramite_Solicitados_ID);
        self.SolicitudTramiteModel.Usuarios_ID(Model_Solicitud_Tramite.Usuarios_ID);
        self.SolicitudTramiteModel.Tramite_ID(Model_Solicitud_Tramite.Tramite_ID);
        self.SolicitudTramiteModel.Costo_Tramite(Model_Solicitud_Tramite.Costo_Tramite);
        self.SolicitudTramiteModel.Estatus(Model_Solicitud_Tramite.Estatus);
        self.SolicitudTramiteModel.Estatus_Revision(Model_Solicitud_Tramite.Estatus_Revision);
        self.SolicitudTramiteModel.RPU(Model_Solicitud_Tramite.RPU);
        self.SolicitudTramiteModel.Observaciones(Model_Solicitud_Tramite.Observaciones);
        self.bloquearControl(Model_Solicitud_Tramite.Seleccionado);

        createTablaDocumentos(self.SolicitudTramiteModel.Tramite_Solicitados_ID());

        if (self.SolicitudTramiteModel.Tramite_Solicitados_ID() == 0) {
            TablaDocumentosSolicitud.bootstrapTable('hideColumn', 'pdf');
        } else {
            TablaDocumentosSolicitud.bootstrapTable('showColumn', 'pdf');
        }

        self.ArregloRPU($.parseJSON(Arreglo_RPU_Asociados));
        self.ArregloDocumentos($.parseJSON(Arreglo_Lista_Documentos));
        self.SolicitudTramiteModel.RPU_Selected(self.ArregloRPU.find('name', { name: self.SolicitudTramiteModel.RPU() }));
        self.SolicitudTramiteModel.errors.showAllMessages(false);

        var objetoFormulario = $.parseJSON(Model_Formulario_Domiciliacion);


        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

        $('#txt_fecha_vencimiento').datepicker('setDate', today);

        self.DomiciliacionModel.Formulario_Domiciliacion_ID(objetoFormulario.Formulario_Domiciliacion_ID);
        self.DomiciliacionModel.Tramite_Solicitados_ID(objetoFormulario.Tramite_Solicitados_ID);
        self.DomiciliacionModel.RPU(objetoFormulario.RPU);
        self.DomiciliacionModel.RFC(objetoFormulario.RFC);
        self.DomiciliacionModel.Nombre_Proveedor(objetoFormulario.Nombre_Proveedor);
        self.DomiciliacionModel.Periodicidad('MENSUAL');
        self.DomiciliacionModel.Nombre_Banco(objetoFormulario.Nombre_Banco);
        self.DomiciliacionModel.Numero_Tarjeta(objetoFormulario.Numero_Tarjeta);
        self.DomiciliacionModel.Telefono(objetoFormulario.Telefono);
        self.DomiciliacionModel.CLABE(objetoFormulario.CLABE);
        self.DomiciliacionModel.Monto_Maximo(objetoFormulario.Monto_Maximo);
        self.DomiciliacionModel.Plazo_Indeterminado(objetoFormulario.Plazo_Indeterminado);

        if (objetoFormulario.Plazo_Indeterminado) {
            self.DomiciliacionModel.radioPlazoSelection('indeterminado');
            self.llevaFecha(false);
        } else {
            self.DomiciliacionModel.radioPlazoSelection('fecha_vencimiento');
            self.llevaFecha(true);
            if (objetoFormulario.Fecha_Vencimiento.length > 0) {
                $('#txt_fecha_vencimiento').datepicker('update', self.obtenerFecha(objetoFormulario.Fecha_Vencimiento));
            }
        }

        self.DomiciliacionModel.errors.showAllMessages(false);

    }

    self.obtenerFecha = function (strFecha) {
        let strDay = strFecha.substring(0, 2);
        let strMonth = strFecha.substring(3, 5);
        let strYear = strFecha.substring(6, 10)


        return new Date(parseInt(strYear), parseInt(strMonth) - 1, parseInt(strDay));
    }

    // validacion de documentos
    self.faltanDocumentosQueSubir = function (arreglo_documentos_agregados) {
        var documento_id;
        var nombre_documento;
        var respuestaDocumento;
        var respuesta = false;

        for (contador = 0; contador < self.ArregloDocumentos().length; contador++) {
            documento_id = self.ArregloDocumentos()[contador].id;
            nombre_documento = self.ArregloDocumentos()[contador].name;
            respuestaDocumento = self.seEncuentaAgregadoUnDocumento(documento_id, arreglo_documentos_agregados);

            if (respuestaDocumento == false) {
                respuesta = true;
                $.toast({
                    heading: 'Realizar Trámites',
                    text: 'Falta subir este documento: ' + nombre_documento,
                    position: 'top-right',
                    stack: false
                });
                break;
            }

        }

        return respuesta;
    }

    self.seEncuentaAgregadoUnDocumento = function (documento_id, arreglo_documentos_agregados) {

        var respuesta = false;

        for (index = 0; index < arreglo_documentos_agregados.length; index++) {

            if (documento_id == arreglo_documentos_agregados[index].Documento_ID) {
                respuesta = true
            }
        }

        return respuesta;
    }



    /// eventos --------------------------------------------

    self.eventoRPU = function (data, event) {


        if (self.SolicitudTramiteModel.RPU_Selected() !== undefined) {
            var valor = self.SolicitudTramiteModel.RPU_Selected().name;
            self.bucarInformacionRPU(valor);
        } else {
            self.DomiciliacionModel.Nombre_Proveedor('');
        
        }
    }

    self.eventoRadioButton = function () {

        if (self.DomiciliacionModel.radioPlazoSelection() == "indeterminado") {
            self.DomiciliacionModel.radioPlazoSelection('indeterminado');
            self.llevaFecha(false);
        }

        if (self.DomiciliacionModel.radioPlazoSelection() == "fecha_vencimiento") {
            self.DomiciliacionModel.radioPlazoSelection('fecha_vencimiento');
            self.llevaFecha(true);
            self.DomiciliacionModel.Fecha_Vencimiento('');
        }


        return true;

    }
    self.abrirVentanaAgregarRPU = function () {

        $.get(Ruta_Agregar_RPU, function (d) {

            $('.body-content').prepend(d);
            $('#Ventana_Agregar_RPU').modal('show');

            setTimeout(function () {
                ObjAgregarRPUViewModel.asignarFuncionTramite(ObjDomiciliacionViewModel);
            }, 300);


        });

    }
    self.regresar = function () {


        bootbox.confirm({
            message: "¿Realmente desear salir de trámite?",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result) {
                    window.history.back();
                }
            }
        });



    };

    self.aceptarOperacion = function () {

        if (self.SolicitudTramiteModel.Tramite_Solicitados_ID() == "0")
            self.TipoOperacion("Guardar");
        else
            self.TipoOperacion("Editar");

        self.guardar();
    }

    self.guardar = function () {

        if (self.SolicitudTramiteModel.errors().length > 0) {
            self.SolicitudTramiteModel.errors.showAllMessages();
            $.toast({
                heading: 'Realizar Trámites',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }


        if (self.DomiciliacionModel.errors().length > 0) {
            self.DomiciliacionModel.errors.showAllMessages();
            $.toast({
                heading: 'Realizar Trámites',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        var rows = TablaDocumentosSolicitud.bootstrapTable('getData');


        if (rows.length == 0) {
            $.toast({
                heading: 'Realizar Trámites',
                text: 'No ha subido ningún documento',
                position: 'top-right',
                stack: false
            });
            return;
        }

        if (self.faltanDocumentosQueSubir(rows)) {
            return;
        }

        self.SolicitudTramiteModel.RPU(self.SolicitudTramiteModel.RPU_Selected().name);
        self.DomiciliacionModel.RPU(self.SolicitudTramiteModel.RPU_Selected().name);

        if (self.DomiciliacionModel.radioPlazoSelection() == "indeterminado") {
            self.DomiciliacionModel.Plazo_Indeterminado(true);
        } else {
            self.DomiciliacionModel.Plazo_Indeterminado(false);
        }

        var strSolicitudTramite = ko.toJSON(self.SolicitudTramiteModel);
        var strListaTramites = ko.toJSON(rows);
        var strFormulario = ko.toJSON(self.DomiciliacionModel);
        self.Abrir_Ventana_Espera();
        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: "{'strSolicitudTramites': '" + strSolicitudTramite + "' , 'strListaDocumentos':'" + strListaTramites + "' , 'strFormDomiciliacion': '" + strFormulario + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    $.toast({
                        heading: 'Realizar Trámite',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                    window.location.href = regresarURLOperacion;
                }
                else {
                    $.toast({
                        heading: 'Realizar Trámite',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Catálogo de Trámites");

            }

        });

        // guardar

    }


    self.bucarInformacionRPU = function (rpu) {
        self.Abrir_Ventana_Espera();
        $.ajax({
            type: "GET",
            url: 'Obtener_Informacion_RPU?RPU=' + rpu,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();

                if (response.Mensaje = "bien") {
                
                    self.DomiciliacionModel.Nombre_Proveedor(response.Dato1);
                } else {
                    $.toast({
                        heading: 'Realizar Trámite',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Trámites");

            }
        });


    }


}