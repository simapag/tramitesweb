﻿var Modelo_Menu = function () {

    var _Menu_ID = ko.observable(''),
        _Parent_ID = ko.observable(''),
        _Menu_Descripcion = ko.observable('').extend({ required: true })
            .extend({ maxLength: 40 })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Es_Sub_Menu = ko.observable(false),
        _Accion = ko.observable('').extend({ required: { onlyIf: _Es_Sub_Menu } })
            .extend({ maxLength: 40 })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Controlador = ko.observable('').extend({ required: { onlyIf: _Es_Sub_Menu } })
            .extend({ maxLength: 40 })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Orden = ko.observable('').extend({ required: { onlyIf: _Es_Sub_Menu } })
            .extend({ digit: true })
            .extend({ maxLength: 2 }),
        _Estatus = ko.observable('').extend({ required: true }),
        _Tipo = ko.observable(''),
        _SeleccionMenu = ko.observable('').extend({ required: { onlyIf: _Es_Sub_Menu } });


    return {
        Menu_ID: _Menu_ID,
        Parent_ID: _Parent_ID,
        Menu_Descripcion: _Menu_Descripcion,
        Accion: _Accion,
        Controlador: _Controlador,
        Orden: _Orden,
        Estatus: _Estatus,
        Tipo: _Tipo,
        MenuSeleccion: _SeleccionMenu,
        Es_Sub_Menu: _Es_Sub_Menu
    }


}