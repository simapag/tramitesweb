﻿var ObjMenuViewModel;

$(function () {

    ObjMenuViewModel = new Menu_ViewModel();
    ObjMenuViewModel.ModeloMenu.errors = ko.validation.group(ObjMenuViewModel.ModeloMenu);

    ko.applyBindings(ObjMenuViewModel, $("#Interface_Datos")[0]);
    ko.applyBindings(ObjMenuViewModel, $("#Interface_Botones")[0]);

});


   /// <summary>
        ///    funcion principal del View Model 
   /// </summary>

function Menu_ViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloMenu = Modelo_Menu();
    self.ArregloTipo = ko.observableArray(['Menu', 'Sub Menu']);
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.ArregloMenuPaPa = ko.observableArray([]);
    self.RenglonSeleccionado = ko.observable();
    self.BloquearTipo = ko.observable(false);
    self.SolicitarDato = ko.observable(false);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);


    //-----------------------------------------------

       /// <summary>
        ///     abre una ventana indicando que se esta haciendo una petición ajax
     /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }

    /// <summary>
        ///     cierra una ventana indicando que se esta haciendo una petición ajax
     /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }

        /// <summary>
        ///     abre una ventana para registrar información en la base de datos
     /// </summary>

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    }


    /// <summary>
        ///     cierra una ventana para registrar información en la base de datos
     /// </summary>
    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    }
    

    /// <summary>
    ///     abre una ventana para inactivar elemento
    /// </summary>
    self.Abrir_Ventana_Inactivar = function () {
        var ventanaInterfaz = $('#VentanaInactivar')
        ventanaInterfaz.modal();
    }

    /// <summary>
    ///     cierra una ventana para inactivar elemento
    /// </summary>
    self.Cerrar_Ventana_Inactivar = function () {
        var ventanaInterfaz = $('#VentanaInactivar')
        ventanaInterfaz.modal('hide');
    }


    
    //-----------------------------------------------

        /// <summary>
        ///    hace una busqueda para traer información a la tabla
     /// </summary>
    self.Buscar_Datos = function () {
        TablaMenus.bootstrapTable('refresh');
    }


    /// <summary>
        ///    solicita que se haga un nuevo elemento
     /// </summary>

    self.PeticionNuevoElemento = function () {
        self.Obtener_Menus_Papa('Nuevo');

    }


    /// <summary>
        ///    solicita que se edita un elemento
     /// </summary>
    self.PeticionEditarElemento = function () {
         self.Obtener_Menus_Papa('Editar')
    }


    /// <summary>
        ///    Evento click del combo tipo de menu
     /// </summary>
    self.Evento_Cambio_Tipo_Menu = function () {

        if (self.ModeloMenu.Tipo() === 'Menu') {
            self.ModeloMenu.Es_Sub_Menu(false);
            self.SolicitarDato(false);
        }

        if (self.ModeloMenu.Tipo() === 'Sub Menu') {
            self.ModeloMenu.Es_Sub_Menu(true);
            self.SolicitarDato(true);
        }


        self.ModeloMenu.Orden('');
        self.ModeloMenu.Controlador('');
        self.ModeloMenu.Accion('');
        self.ModeloMenu.Menu_Descripcion('');
        self.ModeloMenu.MenuSeleccion(self.ArregloMenuPaPa.find("name", { name: 'Selecciona' }));

    }


        /// <summary>
        ///    Obtiene la lista de los nodos papa de la tabla menu
     /// </summary>
    self.Obtener_Menus_Papa = function (p_Tipo_Solicitud) {

        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Obtener_Menus_Papas',
            data: '',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {

                    self.ArregloMenuPaPa([]);
                    self.ArregloMenuPaPa(ko.utils.parseJson(response.Dato1));

                    if (p_Tipo_Solicitud === 'Nuevo') {
                        self.Nuevo_Elemento();
                    } else {
                        self.Editar_Elemento();
                    }
                  
                }   
                else {
                    $.alert(" " + response.Mensaje, "Menús");
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Interfaz();

            }

        });




    }

    //-----------------------------------------------
      /// <summary>
        ///     Limpia los datos del modelo
     /// </summary>
    self.Limpiar_Cajas = function () {

        self.ModeloMenu.Menu_ID('0');
        self.ModeloMenu.Parent_ID(null);
        self.ModeloMenu.Menu_Descripcion('');
        self.ModeloMenu.Accion('');
        self.ModeloMenu.Controlador('');
        self.ModeloMenu.Orden('');
        self.ModeloMenu.Estatus('ACTIVO');
        self.ModeloMenu.Tipo('Menu')
        self.ModeloMenu.MenuSeleccion(self.ArregloMenuPaPa.find("name", { name: 'Selecciona' }));
        self.BloquearTipo(true);
        self.ModeloMenu.Es_Sub_Menu(false);
    }


   /// <summary>
        ///    Asigna datos al modelo
     /// </summary>
    
    self.Asignar_Valores = function (p_Row) {

        self.ModeloMenu.Menu_ID(p_Row.id);
        self.ModeloMenu.Parent_ID(p_Row.id_parent);
       
        self.ModeloMenu.Accion(p_Row.accion);
        self.ModeloMenu.Controlador(p_Row.controlador);
        self.ModeloMenu.Orden(p_Row.orden);
        self.ModeloMenu.Estatus(p_Row.estatus);
        
        self.ModeloMenu.MenuSeleccion(self.ArregloMenuPaPa.find("id", { id: self.ModeloMenu.Parent_ID() }));

        if (self.ModeloMenu.MenuSeleccion() === undefined) {
            self.ModeloMenu.Tipo('Menu');
            self.ModeloMenu.Menu_Descripcion(p_Row.sub_menu);
            self.SolicitarDato(false);
        }
        else {
            self.ModeloMenu.Tipo('Sub Menu');
            self.ModeloMenu.Menu_Descripcion(p_Row.sub_menu);
            self.SolicitarDato(true);
        }

        self.BloquearTipo(false);
    }


      /// <summary>
        ///    Operación para editar un elemento
     /// </summary>
    self.Editar_Elemento = function () {
        self.Limpiar_Cajas();
        self.Asignar_Valores(self.RenglonSeleccionado());
        self.Abrir_Ventana_Interfaz();
    }

      /// <summary>
        ///  Operación para un Nuevo Elemento
     /// </summary>
    self.Nuevo_Elemento = function () {
        self.Abrir_Ventana_Interfaz();
        self.Limpiar_Cajas();
        self.SolicitarDato(false);
    }


    /// <summary>
        ///  Operación para eliminar un elemento
     /// </summary>
    self.Borrar_Elemento = function (p_Row) {
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar_Menu");
        self.ValidarDatos(false);
        self.Procesar_Operacion();
    }

    /// <summary>
        ///  Acepta las opciones de editar y Nuevo
     /// </summary>
    self.Aceptar_Operacion = function () {


        if (self.ModeloMenu.Tipo() === "Menu") {
            self.ModeloMenu.Parent_ID(null);
            self.ModeloMenu.Orden(0);
            self.ModeloMenu.Es_Sub_Menu(false);
        } else {
             self.ModeloMenu.Es_Sub_Menu(true);
            // significa que el tipo sub menú
            if (self.ModeloMenu.MenuSeleccion() !== undefined){
                self.ModeloMenu.Parent_ID(self.ModeloMenu.MenuSeleccion().id);    
            }
          
        }


        if (self.ModeloMenu.Menu_ID() === "0") {
            self.TipoOperacion('Nuevo_Menu') 

        } else {
            self.TipoOperacion('Editar_Menu')
        }
        self.ValidarDatos(true);
        self.Procesar_Operacion();
    }


    //------------------------------------------------------------

       /// <summary>
        ///   Ejecuta las operaciones de nuevo y editar
     /// </summary>
    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (ObjMenuViewModel.ModeloMenu.errors().length > 0) {
                ObjMenuViewModel.ModeloMenu.errors.showAllMessages();
                $.alert("Se requiren unos valores", "Menú");
                return;
            }
        }


        strDatos = ko.toJSON(self.ModeloMenu);
        self.Abrir_Ventana_Espera();


        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Interfaz();
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {

                    if (self.ValidarDatos() === false) {
                        if (response.Mensaje === 'El elemento  esta referenciado en otra tabla') {
                            self.Abrir_Ventana_Inactivar();
                            $('#SpanDatoInactivar').html(self.ModeloMenu.Menu_Descripcion());
                        }
                    } else {
                        $.alert(" " + response.Mensaje, "Menu");
                    }

                    
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");
            }

        });




    }



    /// <summary>
    ///   Inactiva el elemento si el usuario así lo desea
    /// </summary>
    self.Inactivar_Elemento = function () {

        var strDatos;
        strDatos = ko.toJSON(self.ModeloMenu);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Inactivar_Menu',
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Inactivar();
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {
                    $.alert(" " + response.Mensaje, "Menu");
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");

            }

        });

    }


}