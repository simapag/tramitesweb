﻿var TablaMenus = $('#tabla_menus');
var RenglonSeleccionado;

$(function () {

    Inicializar_Tabla();
});


/// <summary>
///   Inicializar parametros de la tabla
/// </summary>
function Inicializar_Tabla() {
    TablaMenus.bootstrapTable('destroy');
    TablaMenus.bootstrapTable({
        url: 'Obtener_Menus', pageList: [100, 150, 200], pageSize: 100,idField: 'id', treeShowField: 'menu_padre', parentIdField: 'id_parent',
        onLoadSuccess: function (data) {
                
            // jquery.treegrid.js
            TablaMenus.treegrid({
                // initialState: 'collapsed',
                treeColumn: 2,
                // expanderExpandedClass: 'glyphicon glyphicon-minus',
                // expanderCollapsedClass: 'glyphicon glyphicon-plus',
                onChange: function () {
                    tabla_menus.bootstrapTable('resetWidth');
                }
            });

        }
    });
}


/// <summary>
///   pone los iconos de operación Eliminar
/// </summary>
function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Eliminar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}


/// <summary>
///   pone los iconos de operación Editar
/// </summary>
function Botones_Operacion_Formato_Editar(value, row, index) {
    return [
        '<a class="editar" href="javascript:void(0)" title="Editar">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>'
    ].join('');
}


/// <summary>
///   escuchador de iconos de operación
/// </summary>
window.Botones_Operacion_Evento = {

    'click .eliminar': function (e, value, row, index) {

        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.sub_menu + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionado = row; 

        },
    'click .editar': function (e, value, row, index) {

        ObjMenuViewModel.RenglonSeleccionado(row);
        ObjMenuViewModel.PeticionEditarElemento();
    }


};


/// <summary>
///   Ejecuta Proceso de Eliminacion
/// </summary>
function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjMenuViewModel.Borrar_Elemento(RenglonSeleccionado);
}

/// <summary>
///   Ejecuta Proceso de Inactivación
/// </summary>
function Inactivar_Elemento() {
    ObjMenuViewModel.Inactivar_Elemento();

}