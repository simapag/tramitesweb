﻿
var Suspension_Model = function () {

    var _Tramite_Solicitados_ID = ko.observable(''),
        _Formulario_Suspension_ID = ko.observable(''),
        _Nombre = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Domicilio = ko.observable(''),
        _RPU = ko.observable(''),
        _NoCuenta = ko.observable(''),
        _Telefono_Notificacion = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 50 }),
        _Domicilio_Notificacion = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 100 }),
        _Motivo_Suspension = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 199 });

    return {
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Formulario_Suspension_ID: _Formulario_Suspension_ID,
        Nombre: _Nombre,
        Domicilio: _Domicilio,
        RPU:_RPU,
        NoCuenta: _NoCuenta,
        Telefono_Notificacion: _Telefono_Notificacion,
        Domicilio_Notificacion: _Domicilio_Notificacion,
        Motivo_Suspension: _Motivo_Suspension
    }
}