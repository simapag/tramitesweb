﻿
var DOCUMENTO_ID = 0;
var NOMBRE_DOCUMENTO = "";
var TablaDocumentosSolicitud = $('#Tabla_Solicitudes_Documento');
$(function () {


    // obtenemos el clik cuando seleccionan un documento
    $('.list-group-item').on('click', function () {
        var $this = $(this);
        var $identificador = $this.data('identificador');
        var $nombre = $this.data('nombre');

        $('.active').removeClass('active');
        $this.toggleClass('active')

        DOCUMENTO_ID = $identificador;
        NOMBRE_DOCUMENTO = $nombre;
        return false;
    });


    new AjaxUpload('#btn_subir_archivo', {
        action: '../BaseSolicitudTramite/Subir_Archivo_Temporal',
        responseType: 'json',
        onSubmit: function (file, ext) {
            if (!(ext && /^(pdf)$/i.test(ext))) {
                $.toast({
                    heading: 'Realizar Trámite',
                    text: 'Solo se permiten archivos con extensión PDF',
                    position: 'top-right',
                    stack: false
                });
                return false;
            }

            if (DOCUMENTO_ID == 0) {
                $.toast({
                    heading: 'Realizar Trámite',
                    text: 'Seleccione un documento',
                    position: 'top-right',
                    stack: false
                });
                return false;
            }

            abrirVentanEspera();

        },
        onComplete: function (file, response) {
            cerrarVentanaEspera();

            if (response.Mensaje == "bien") {

                var nombre_archvo = response.Dato1;
                agregarArchivo(nombre_archvo);
                //alert(response.Dato1);
                //Obj_Solicitud.Ruta_Imagen = response.Dato1;
                //self.Ruta_Imagen(response.Dato1);
            }
            else {

                $.toast({
                    heading: 'Realizar Trámite',
                    text: response.Mensaje,
                    position: 'top-right',
                    stack: false
                });
                
            }
        }
    });




});


/// botones de las tablas----------------------------------------------------



function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar_documento" href="javascript:void(0)" title="Eliminar Documento">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}

function Botones_Operacion_Bajar_PDF(value, row, index) {
    return [
        '<a class="bajar_archivo" href="javascript:void(0)" title="Bajar_Archivo">',
        '<i class="glyphicon glyphicon-cloud-download"></i>',
        '</a>  '
    ].join('');
}



window.Botones_Operacion_Evento = {

    'click .bajar_archivo': function (e, value, row, index) {


        if (row.Ruta_Archivo.length >0) {
            window.open(Ruta_Reporte_PDF + "?ruta=" + row.Ruta_Archivo, "SIMAPAG", "width=800px,height=600px,scrollbars=YES,resizable=YES");
        }


    },
    'click .eliminar_documento': function (e, value, row, index) {

        let rowid = $(this).closest('tr').data('index');

     
            TablaDocumentosSolicitud.bootstrapTable('removeRow', {
                index: rowid
            });
        
    }


};


//----------------funciones ----------------------------------------------

function abrirVentanEspera() {
    var ventantaEspera = $('#Ventana_Espera');
    ventantaEspera.modal();
}

function cerrarVentanaEspera() {
    var ventantaEspera = $('#Ventana_Espera');
    ventantaEspera.modal('hide');
}

function createTablaDocumentos(Tramite_Solicitados_ID) {

    TablaDocumentosSolicitud.bootstrapTable('destroy');
    TablaDocumentosSolicitud.bootstrapTable({
        url: '../BaseSolicitudTramite/Obtener_Documentos_En_Una_Solicitud',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                Tramite_Solicitados_ID: Tramite_Solicitados_ID
            };
        }
    });

}



function agregarArchivo(nombreArchivo) {
    var row = { Tramite_Solicitados_Documento_ID: 0, Tramite_Solicitados_ID: 0, Documento_ID: DOCUMENTO_ID, Nombre_Documento: NOMBRE_DOCUMENTO, Ruta_Archivo:'' , Nombre_Archivo: nombreArchivo, URL_Archivo:'' };
    TablaDocumentosSolicitud.bootstrapTable('append', row);

}