﻿
var Solicitud_Tramite_Model = function () {

    var _Tramite_Solicitados_ID = ko.observable(''),
        _Usuarios_ID = ko.observable(''),
        _Tramite_ID = ko.observable(''),
        _Costo_Tramite = ko.observable(''),
        _Estatus = ko.observable(''),
        _Estatus_Revision = ko.observable(''),
        _RPU = ko.observable(''),
        _RPU_Requerido = ko.observable(''),
        _Observaciones = ko.observable('')
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ maxLength: 500 }),
        _RPU_Selected = ko.observable('').extend({
            required: {
                onlyIf: function () {
                    if (_RPU_Requerido())
                        return true;
                    else
                        return false;
                }
            }
        });

    return {
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Usuarios_ID: _Usuarios_ID,
        Tramite_ID: _Tramite_ID,
        Costo_Tramite: _Costo_Tramite,
        Estatus: _Estatus,
        Estatus_Revision: _Estatus_Revision,
        RPU: _RPU,
        Observaciones: _Observaciones,
        RPU_Selected: _RPU_Selected,
        RPU_Requerido: _RPU_Requerido
    }
};