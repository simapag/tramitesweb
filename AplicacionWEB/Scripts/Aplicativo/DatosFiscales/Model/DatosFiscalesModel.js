﻿var Datos_Fiscales_Model = function () {

    var _Formulario_Datos_Fiscales_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _Comprobante_Cfdi_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _Razon_Social = ko.observable('').extend({ required: true })
               .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
               .extend({ maxLength: 100 }),
        _Colonia = ko.observable('').extend({ required: true })
              .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
              .extend({ maxLength: 100 }),
        _Calle = ko.observable('').extend({ required: true })
             .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
             .extend({ maxLength: 100 }),
        _Numero_Exterior = ko.observable('').extend({ required: true })
                  .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                  .extend({ maxLength: 40 }),
        _Numero_Interior = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                                      .extend({ maxLength: 40 }),
        _Codigo_Postal = ko.observable('').extend({ required: true })
        .extend({ digit: true })
        .extend({ maxLength: 5 }),
        _Correo_Electronico = ko.observable('').extend({ required: true })
           .extend({ email: true }),
        _RFC = ko.observable('').extend({ required: true })
        .extend({ maxLength: 14 }),
        _Ciudad = ko.observable('').extend({ required: true })
                 .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                 .extend({ maxLength: 100 }),
        _Estado = ko.observable('').extend({ required: true })
                 .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                 .extend({ maxLength: 100 });
       _Compobante_Cfdi_Selected = ko.observable('').extend({ required: true });


    return {

        Formulario_Datos_Fiscales_ID: _Formulario_Datos_Fiscales_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Comprobante_Cfdi_ID: _Comprobante_Cfdi_ID,
        RPU: _RPU,
        Razon_Social: _Razon_Social,
        Colonia: _Colonia,
        Calle: _Calle,
        Numero_Exterior: _Numero_Exterior,
        Numero_Interior: _Numero_Interior,
        Codigo_Postal: _Codigo_Postal,
        Correo_Electronico: _Correo_Electronico,
        RFC: _RFC,
        Ciudad: _Ciudad,
        Estado: _Estado,
        Compobante_Cfdi_Selected: _Compobante_Cfdi_Selected
    }
}