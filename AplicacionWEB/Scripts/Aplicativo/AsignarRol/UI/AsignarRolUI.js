﻿var TablaUsuario = $('#Tabla_Buscar_Usuario');


$(function () {

   
    // cuando se abra la ventana modal, se ejecuta las siguientes intrucciones
    $('#Ventana_Buscar').on('shown.bs.modal', function () {
        TablaUsuario.bootstrapTable('resetSearch');
        TablaUsuario.bootstrapTable('resetView');
        TablaUsuario.bootstrapTable('refresh');
    });

     // nos permite seleccionar un elemento
    TablaUsuario.on('click-row.bs.table', function (e, row, $element) {
        $('.success').removeClass('success');
        $($element).addClass('success');
    });


    // click del evento aceptar cuando se selecciona un elemento
    $('#Btn_Aceptar').click(function () {

        var renglonSeleccionado;

        renglonSeleccionado = Obtener_Elemento_Seleccionado();

        if (renglonSeleccionado === undefined || renglonSeleccionado === null) {
            $.alert("Se require seleccionar un elemento", "Asignación Roles");
            return;
        }

        ObjAsignarRolViewModel.Asignar_Informacion(renglonSeleccionado);

    });


});


 /// <summary>
    ///    Obtiene un elemento seleccionado de la tabla de bootstrap
  /// </summary>

function Obtener_Elemento_Seleccionado() {
    var index = TablaUsuario.find('tr.success').data('index');
    return TablaUsuario.bootstrapTable('getData')[index];

}



