﻿var ObjAsignarRolViewModel;

$(function () {

    ObjAsignarRolViewModel = new Asignar_Rol_ViewModel();
    ObjAsignarRolViewModel.ModeloAsignarRol.errors = ko.validation.group(ObjAsignarRolViewModel.ModeloAsignarRol);

    ko.applyBindings(ObjAsignarRolViewModel, $("#Interface_Datos")[0]);
    ObjAsignarRolViewModel.Cargar_Informacion();
  
});


function Asignar_Rol_ViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";


    self.ModeloAsignarRol = Modelo_Asignar_Rol();
    self.ArregloRoles = ko.observableArray([]);
    self.Usuario = ko.observable();
    self.BloquearDato = ko.observable(false);
    //-----------------------------------------------

    /// <summary>
    ///     abre una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }

    /// <summary>
    ///     cierra una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }


    /// <summary>
    ///     cierra la ventana de busqueda
    /// </summary>
    self.Cerrar_Ventana_Busqueda = function () {
        var ventanaBusqueda = $('#Ventana_Buscar');
        ventanaBusqueda.modal('hide');
    }



     //-----------------------------------------------

    /// <summary>
    ///  Llena el combo de los roles, y ponde datos de usuario
    /// </summary>

   
    self.Cargar_Informacion = function () {
        self.ArregloRoles($.parseJSON(DatosRoles));
        self.Usuario($.parseJSON(DatosUsuario));

        self.ModeloAsignarRol.SeleccionRol(self.ArregloRoles.find("id", { id: self.Usuario()[0].id_rol }));
        self.ModeloAsignarRol.Nombre(self.Usuario()[0].nombre + ' ' + self.Usuario()[0].apellido_paterno);
        self.ModeloAsignarRol.Correo(self.Usuario()[0].correo);
        self.ModeloAsignarRol.Password(self.Usuario()[0].password );
        self.ModeloAsignarRol.ConfirmarPassword(self.Usuario()[0].password);
        self.ModeloAsignarRol.Usuario_ID(self.Usuario()[0].id);
    }

     //-----------------------------------------------

    /// <summary>
       ///  Asignar información del elemento seleccionado
    /// </summary>
    self.Asignar_Informacion = function (p_Row) {

        self.ModeloAsignarRol.SeleccionRol(self.ArregloRoles.find("id", { id: p_Row.id_rol }));
        self.ModeloAsignarRol.Nombre(p_Row.nombre + ' ' + p_Row.apellido_paterno);
        self.ModeloAsignarRol.Correo(p_Row.correo);
        self.ModeloAsignarRol.Password(p_Row.password);
        self.ModeloAsignarRol.ConfirmarPassword(p_Row.password);
        self.ModeloAsignarRol.Usuario_ID(p_Row.id);
        
        self.Cerrar_Ventana_Busqueda();

    }


    /// <summary>
       ///  Guardar Configuracion
    /// </summary>

    self.Guardar_Configuracion = function () {

        if (ObjAsignarRolViewModel.ModeloAsignarRol.errors().length > 0) {
            ObjAsignarRolViewModel.ModeloAsignarRol.errors.showAllMessages();
            $.alert("Se requiren unos valores", "Asignación de Roles");
            return;
        }


        strDatos = ko.toJSON(self.ModeloAsignarRol);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Guardar_Asignacion',
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {
                    $.alert(" " + response.Mensaje, "Asignaciones Roles");
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");

            }

        });



    }

}

