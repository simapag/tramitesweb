﻿
 /// <summary>
        ///    Modelo de datos para asignar roles
     /// </summary>

var Modelo_Asignar_Rol = function () {


    var _Correo = ko.observable('').extend({ required: true })
             .extend({ email: true }),
        _Password = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ minLength: 4, maxLength: 8 }),
        _ConfirmarPassword = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ es_igual: _Password }),
        _Nombre = ko.observable('').extend({ required: true }),
        _SeleccionRol = ko.observable('').extend({ required: true }),
        _Usuario_ID = ko.observable('');


    return {
        Correo: _Correo,
        Password: _Password,
        ConfirmarPassword: _ConfirmarPassword,
        SeleccionRol: _SeleccionRol,
        Nombre: _Nombre,
        Usuario_ID: _Usuario_ID
    }
}