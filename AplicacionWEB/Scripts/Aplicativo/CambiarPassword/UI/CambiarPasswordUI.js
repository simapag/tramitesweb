﻿
$(function () {

    const togglePassword = document.querySelector('#togglePasswordNueva');
    const password = document.querySelector('#passwordNuevo');

    togglePassword.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });

    const togglePasswordConfirmar = document.querySelector('#togglePasswordNuevaConfirmar');
    const passwordConfirmar = document.querySelector('#passwordNuevoConfirmar');

    togglePasswordConfirmar.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = passwordConfirmar.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordConfirmar.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });

});

 /// <summary>
    ///    Abre la ventana para cambio de password
  /// </summary>

function Abrir_Ventana_Cambio_Password() {

   
    ObjCambiarPasswordViewModel.limpiarDatos();
    ObjCambiarPasswordViewModel.Abrir_Ventana_Interfaz();

}