﻿/// <summary>
///    Modelo de datos para cambio de passwors
/// </summary>

var Modelo_Cambio_Password = function () {


    var _Correo = ko.observable('').extend({ required: true })
            .extend({ email: true }),
        _Password = ko.observable('').extend({ required: true })
             .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
             .extend({ minLength: 4, maxLength: 12 }),
        _ConfirmarPassword = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ es_igual: _Password }),
        _Nombre = ko.observable('').extend({ required: true }),
        _PasswordActual = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
           .extend({ minLength: 4, maxLength: 12 });
       


    return {
        Correo: _Correo,
        Password: _Password,
        ConfirmarPassword: _ConfirmarPassword,
        PasswordActual: _PasswordActual
   
    }
}