﻿var ObjCambiarPasswordViewModel;

$(function () {

    ObjCambiarPasswordViewModel = new Cambiar_Password_ViewModel();
    ObjCambiarPasswordViewModel.ModelCambioPassword.errors = ko.validation.group(ObjCambiarPasswordViewModel.ModelCambioPassword);

    ko.applyBindings(ObjCambiarPasswordViewModel, $("#Interface_Datos")[0]);

});


   /// <summary>
        ///    funcion principal del View Model 
   /// </summary>

function Cambiar_Password_ViewModel() {
    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModelCambioPassword = Modelo_Cambio_Password();


      //-----------------------------------------------
    /// <summary>
    ///     abre una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }

    /// <summary>
    ///     cierra una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }

    /// <summary>
    ///     abre una ventana para registrar información en la base de datos
    /// </summary>

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    }

    /// <summary>
    ///     cierra una ventana para registrar información en la base de datos
    /// </summary>
    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    }

    //-----------------------------------------------

    self.limpiarDatos = function () {

        self.ModelCambioPassword.Correo('');
        self.ModelCambioPassword.Password('');
        self.ModelCambioPassword.ConfirmarPassword('');
        self.ModelCambioPassword.PasswordActual('');

    }

      //-----------------------------------------------

      /// <summary>
    ///     ejecuta el cambio del password
    /// </summary>

    self.Cambiar_Password = function () {
        var strDatos;

        if (ObjCambiarPasswordViewModel.ModelCambioPassword.errors().length > 0) {
            ObjCambiarPasswordViewModel.ModelCambioPassword.errors.showAllMessages();
            $.alert("Se requiren unos valores", "Cambio Contraseña");
            return;
        }


        strDatos = ko.toJSON(self.ModelCambioPassword);
        self.Abrir_Ventana_Espera();

        var rutaNavegador = window.location.href;
        var posicion;
        var operacion;

        posicion = rutaNavegador.indexOf('Verificar_Usuario');

        if (posicion === -1) {
           // signfica que el usuario puso el enlace por default
            operacion = 'Login/Cambiar_Password';
        } else {
            // significa que en la ruta puso controlador y accion en la ruta del navegador
            operacion ='Cambiar_Password'
        }

  

        $.ajax({
            type: "POST",
            url: operacion,
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Cerrar_Ventana_Interfaz();
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {
                    $.alert(" " + response.Mensaje, "Cambio Contraseña");
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Neumáticos");
            }

        });



    }

}