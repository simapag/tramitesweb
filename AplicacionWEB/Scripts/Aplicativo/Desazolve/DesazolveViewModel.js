﻿var ObjServicioDesazolveViewModel;
var TablaCosto = $("#Tabla_Costos");

$(function () {
    ObjServicioDesazolveViewModel = new servicioDesazolveModel();
    ObjServicioDesazolveViewModel.SolicitudTramiteModel.errors = ko.validation.group(ObjServicioDesazolveViewModel.SolicitudTramiteModel);
    ObjServicioDesazolveViewModel.ServicioDesazolveModel.errors = ko.validation.group(ObjServicioDesazolveViewModel.ServicioDesazolveModel);
    ko.applyBindings(ObjServicioDesazolveViewModel, $("#interfaz_documentos")[0]);
    ko.applyBindings(ObjServicioDesazolveViewModel, $("#interfaz_desazolve")[0]);

    ObjServicioDesazolveViewModel.cargarInformacion();

});

function servicioDesazolveModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.SolicitudTramiteModel = Solicitud_Tramite_Model();
    self.ServicioDesazolveModel = Servicio_Desazolve_Model();
    self.ArregloDocumentos = ko.observable([]);
    self.ArregloRPU = ko.observableArray([]);
    self.bloquearControl = ko.observable(false);
    self.TipoOperacion = ko.observable('');
    self.habilitado = ko.observable(false);
    self.llevaDatos = ko.observable(false);
    self.bloquear = ko.observable(false);
    //Ventanas *******************************************
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };

    //cargar informacion **************************************

    self.obtenerCostos = function () {
        TablaCosto.bootstrapTable('destroy');
        TablaCosto.bootstrapTable({ url: '../Costos/Obtener_Costo_Desazolve' });
    }

    self.cargarInformacion = function () {
        self.SolicitudTramiteModel.RPU_Requerido(Model_Solicitud_Tramite.RPU_Requerido);
        self.SolicitudTramiteModel.Tramite_Solicitados_ID(Model_Solicitud_Tramite.Tramite_Solicitados_ID);
        self.SolicitudTramiteModel.Usuarios_ID(Model_Solicitud_Tramite.Usuarios_ID);
        self.SolicitudTramiteModel.Tramite_ID(Model_Solicitud_Tramite.Tramite_ID);
        self.SolicitudTramiteModel.Costo_Tramite(Model_Solicitud_Tramite.Costo_Tramite);
        self.SolicitudTramiteModel.Estatus(Model_Solicitud_Tramite.Estatus);
        self.SolicitudTramiteModel.Estatus_Revision(Model_Solicitud_Tramite.Estatus_Revision);
        self.SolicitudTramiteModel.RPU(Model_Solicitud_Tramite.RPU);
        self.SolicitudTramiteModel.Observaciones(Model_Solicitud_Tramite.Observaciones);
        self.bloquearControl(Model_Solicitud_Tramite.Seleccionado);

        createTablaDocumentos(self.SolicitudTramiteModel.Tramite_Solicitados_ID());

        if (self.SolicitudTramiteModel.Tramite_Solicitados_ID() == 0) {
            TablaDocumentosSolicitud.bootstrapTable('hideColumn', 'pdf');
        } else {
            TablaDocumentosSolicitud.bootstrapTable('showColumn', 'pdf');
        }

        self.ArregloRPU($.parseJSON(Arreglo_RPU_Asociados));
        self.ArregloDocumentos($.parseJSON(Arreglo_Lista_Documentos));
        
        self.SolicitudTramiteModel.errors.showAllMessages(false);

        var objetoFormulario = $.parseJSON(Model_Formulario_Operacion);

        self.ServicioDesazolveModel.Formulario_Servicio_Operativo_ID(objetoFormulario.Formulario_Servicio_Operativo_ID);
        self.ServicioDesazolveModel.Tramite_Solicitados_ID(objetoFormulario.Tramite_Solicitados_ID);
        self.ServicioDesazolveModel.RPU(objetoFormulario.RPU);
        self.ServicioDesazolveModel.NoCuenta(objetoFormulario.NoCuenta);
        self.ServicioDesazolveModel.Nombre_Usuario(objetoFormulario.Nombre_Usuario);
        self.ServicioDesazolveModel.Domicilio(objetoFormulario.Domicilio);


        if (objetoFormulario.Formulario_Servicio_Operativo_ID > 0) {

            if (self.SolicitudTramiteModel.RPU().length > 0) {
                self.SolicitudTramiteModel.RPU_Selected(self.ArregloRPU.find('name', { name: self.SolicitudTramiteModel.RPU() }));
                self.ServicioDesazolveModel.radioTipoUsuario('usuario_simapag');
                self.llevaDatos(false);
                self.habilitado(true);
                self.SolicitudTramiteModel.RPU_Requerido(true);
            } else {
                self.ServicioDesazolveModel.radioTipoUsuario('usuario_libre');
                self.llevaDatos(true);
                self.habilitado(false);
                //self.SolicitudTramiteModel.RPU_Selected(self.ArregloRPU.find("name", { name: 'Selecciona' }));
                self.SolicitudTramiteModel.RPU_Requerido(false);
            }
        } else {
            self.ServicioDesazolveModel.radioTipoUsuario('usuario_simapag');
            self.llevaDatos(false);
            self.habilitado(true);
        }

        self.ServicioDesazolveModel.errors.showAllMessages(false);
        self.obtenerCostos();

    }
    // validacion de documentos
    self.faltanDocumentosQueSubir = function (arreglo_documentos_agregados) {
        var documento_id;
        var nombre_documento;
        var respuestaDocumento;
        var respuesta = false;

        for (contador = 0; contador < self.ArregloDocumentos().length; contador++) {
            documento_id = self.ArregloDocumentos()[contador].id;
            nombre_documento = self.ArregloDocumentos()[contador].name;
            respuestaDocumento = self.seEncuentaAgregadoUnDocumento(documento_id, arreglo_documentos_agregados);

            if (respuestaDocumento == false) {
                respuesta = true;
                $.toast({
                    heading: 'Realizar Trámites',
                    text: 'Falta subir este documento: ' + nombre_documento,
                    position: 'top-right',
                    stack: false
                });
                break;
            }

        }

        return respuesta;
    }

    self.seEncuentaAgregadoUnDocumento = function (documento_id, arreglo_documentos_agregados) {

        var respuesta = false;

        for (index = 0; index < arreglo_documentos_agregados.length; index++) {

            if (documento_id == arreglo_documentos_agregados[index].Documento_ID) {
                respuesta = true
            }
        }

        return respuesta;
    }

    /// eventos --------------------------------------------

    self.eventoRPU = function (data, event) {


        if (self.SolicitudTramiteModel.RPU_Selected() !== undefined) {
            var valor = self.SolicitudTramiteModel.RPU_Selected().name;
            self.bucarInformacionRPU(valor);
        } else {
            self.ServicioDesazolveModel.Nombre_Usuario('');
            self.ServicioDesazolveModel.NoCuenta('');
            self.ServicioDesazolveModel.Domicilio('');
        }
    }

    self.eventoRadioButton = function () {

        if (self.ServicioDesazolveModel.radioTipoUsuario() == "usuario_simapag") {
            self.ServicioDesazolveModel.radioTipoUsuario('usuario_simapag');
            self.SolicitudTramiteModel.RPU_Requerido(true);
            self.llevaDatos(false);
            self.habilitado(true);
        }

        if (self.ServicioDesazolveModel.radioTipoUsuario() == "usuario_libre") {
            self.ServicioDesazolveModel.radioTipoUsuario('usuario_libre');  
            self.SolicitudTramiteModel.RPU_Selected(self.ArregloRPU.find("name", { name: 'Selecciona' }));
            self.SolicitudTramiteModel.RPU_Requerido(false);
            self.llevaDatos(true);
            self.habilitado(false);
            self.ServicioDesazolveModel.Nombre_Usuario('');
            self.ServicioDesazolveModel.NoCuenta('');
            self.ServicioDesazolveModel.Domicilio('');
            self.ServicioDesazolveModel.RPU('');
            
        }


        return true;

    }

    self.abrirVentanaAgregarRPU = function () {

        $.get(Ruta_Agregar_RPU, function (d) {

            $('.body-content').prepend(d);
            $('#Ventana_Agregar_RPU').modal('show');

            setTimeout(function () {
                ObjAgregarRPUViewModel.asignarFuncionTramite(ObjServicioDesazolveViewModel);
            }, 300);


        });

    }

    self.regresar = function () {


        bootbox.confirm({
            message: "¿Realmente desear salir de trámite?",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {

                if (result) {
                    window.history.back();
                }
            }
        });



    };


    self.aceptarOperacion = function () {

        if (self.SolicitudTramiteModel.Tramite_Solicitados_ID() == "0")
            self.TipoOperacion("Guardar");
        else
            self.TipoOperacion("Editar");

        self.guardar();
    }

    self.guardar = function () {

        if (self.SolicitudTramiteModel.errors().length > 0) {
            self.SolicitudTramiteModel.errors.showAllMessages();
            $.toast({
                heading: 'Realizar Trámites',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }


        if (self.ServicioDesazolveModel.errors().length > 0) {
            self.ServicioDesazolveModel.errors.showAllMessages();
            $.toast({
                heading: 'Realizar Trámites',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        var rows = TablaDocumentosSolicitud.bootstrapTable('getData');


        if (rows.length == 0) {
            $.toast({
                heading: 'Realizar Trámites',
                text: 'No ha subido ningún documento',
                position: 'top-right',
                stack: false
            });
            return;
        }

        if (self.faltanDocumentosQueSubir(rows)) {
            return;
        }


        if (self.SolicitudTramiteModel.RPU_Selected() == "Selecciona" || self.SolicitudTramiteModel.RPU_Selected() == undefined) {
            self.SolicitudTramiteModel.RPU('');
            self.ServicioDesazolveModel.RPU('');
        } else {
            self.SolicitudTramiteModel.RPU(self.SolicitudTramiteModel.RPU_Selected().name);
            self.ServicioDesazolveModel.RPU(self.SolicitudTramiteModel.RPU_Selected().name);
        }

        

        var strSolicitudTramite = ko.toJSON(self.SolicitudTramiteModel);
        var strListaTramites = ko.toJSON(rows);
        var strFormulario = ko.toJSON(self.ServicioDesazolveModel);
        self.Abrir_Ventana_Espera();
        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: "{'strSolicitudTramites': '" + strSolicitudTramite + "' , 'strListaDocumentos':'" + strListaTramites + "' , 'strFormulario': '" + strFormulario + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    $.toast({
                        heading: 'Realizar Trámite',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                    window.location.href = regresarURLOperacion;
                }
                else {
                    $.toast({
                        heading: 'Realizar Trámite',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Catálogo de Trámites");

            }

        });

        // guardar

    }


    self.bucarInformacionRPU = function (rpu) {
        self.Abrir_Ventana_Espera();
        $.ajax({
            type: "GET",
            url: 'Obtener_Informacion_RPU?RPU=' + rpu,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();

                if (response.Mensaje = "bien") {

                    self.ServicioDesazolveModel.Nombre_Usuario(response.Dato1);
                    self.ServicioDesazolveModel.NoCuenta(response.Dato3);
                    self.ServicioDesazolveModel.Domicilio(response.Dato2);
                } else {
                    $.toast({
                        heading: 'Realizar Trámite',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Trámites");

            }
        });


    }


}