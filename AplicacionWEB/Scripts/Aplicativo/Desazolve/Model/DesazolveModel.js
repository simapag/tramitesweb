﻿var Servicio_Desazolve_Model = function () {

    var _Formulario_Servicio_Operativo_ID = ko.observable(''),
         _radioTipoUsuario = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _NoCuenta = ko.observable(''),
        _Nombre_Usuario = ko.observable('')
               .extend({
                   required: {
                       onlyIf: function () {
                           if (_radioTipoUsuario() == "usuario_libre")
                               return true;
                           else
                               return false;
                       }
                   }
               }),
        _Domicilio = ko.observable('')
              .extend({
                  required: {
                      onlyIf: function () {
                          if (_radioTipoUsuario() == "usuario_libre")
                              return true;
                          else
                              return false;
                      }
                  }
              });

    return {
        Formulario_Servicio_Operativo_ID: _Formulario_Servicio_Operativo_ID,
        radioTipoUsuario: _radioTipoUsuario,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        NoCuenta: _NoCuenta,
        RPU: _RPU,
        Nombre_Usuario: _Nombre_Usuario,
        Domicilio: _Domicilio
    }

}