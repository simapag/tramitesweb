﻿
var TablaSolicitudes = $("#Tabla_Solicitudes_Tramite");
var TablaBuscarCodigo = $("#Tabla_Busqueda_Codigo"); // para hacer busquedas por código de barras o rpu
var TablaBuscarCodigoDetalle = $("#Tabla_Busqueda_Codigo_Detalle");
var TablaPagosAgregados = $("#Tabla_Pagos_Agregados");
var TablaPagosAgregadosDetalle = $("#Tabla_Pagos_Agregados_Detalle");


var RenglonSeleccionadoBusquedaPago = null;
var RenglonSeleccionado = null;
var RengloSeleccionadoPago = null;

$(function () {

    TablaSolicitudes.on('click-row.bs.table', function (e, row, $element) {
        $('.success').removeClass('success');
        $($element).addClass('success');
        RenglonSeleccionado = row;

    });

});

//--------------------funciones


function crearTablaSolicitudes() {
    TablaSolicitudes.bootstrapTable('destroy');
    TablaSolicitudes.bootstrapTable({
        url: 'Obtener_Solicitudes_Pendientes',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                search: p.search,
                tramite_id: ObjAtenderTramiteViewModel.ModeloBusquedaSolicitud.Tramite_ID(),
                estatus: ObjAtenderTramiteViewModel.ModeloBusquedaSolicitud.Estatus()
            }
        }
    });

}


//  esta el tab de lista de pagos agregados
function crearTablaPagosAsignados() {
    TablaPagosAgregados.bootstrapTable('destroy');
    TablaPagosAgregados.bootstrapTable({
        url: 'Obtener_Pagos_Asignados',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                search: p.search,
                tramite_solicitados_id: ObjAtenderTramiteViewModel.ModeloBuscarCodigo.Tramite_Solicitados_ID()
                
            }
        }
    });

    crearEventoPagosAsignados();

}

//  esta el tab de lista de pagos agregados
function crearTablaPagosAsignadosDetalles() {
    TablaPagosAgregadosDetalle.bootstrapTable('destroy');
    TablaPagosAgregadosDetalle.bootstrapTable({
        url: 'Obtener_Pago_Asignado_Detalle',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                pago_solicitudes_id: ObjAtenderTramiteViewModel.Pago_Solicitudes_ID()

            }
        }
    });

}



function crearTableBuquedaCodigo() {


    TablaBuscarCodigo.bootstrapTable('destroy');
    TablaBuscarCodigo.bootstrapTable({
        url: 'Buscar_Codigo_Barras',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                codigo_barras: ObjAtenderTramiteViewModel.ModeloBuscarCodigo.Codigo_Barras()
            }
        }
    });


    crearEventoPagosBusquedas();
}

function crearTablaBusquedaDetalle() {

    TablaBuscarCodigoDetalle.bootstrapTable('destroy');
    TablaBuscarCodigoDetalle.bootstrapTable({
        url: 'Obtener_Pago_Busqueda_Detalle',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                no_diverso: ObjAtenderTramiteViewModel.ModeloBuscarCodigo.No_Diverso()
            }
        }
    });
}


// Eventos para obtener los detalles de la tabla asignada
// pagos asignados asignados asignados
function crearEventoPagosAsignados() {

    TablaPagosAgregados.on('click-row.bs.table', function (e, row, $element) {
        $('.success').removeClass('success');
        $($element).addClass('success');
        ObjAtenderTramiteViewModel.Pago_Solicitudes_ID(row.Pago_Solicitudes_ID);
        TablaPagosAgregadosDetalle.bootstrapTable('resetView');
        TablaPagosAgregadosDetalle.bootstrapTable('refresh');
        

    });

}

//tabla busquedas busquedas busquedas busquedas
// eventos para obtener el renglon seleccionado de la tabla de buscar código
function crearEventoPagosBusquedas() {

    TablaBuscarCodigo.on('click-row.bs.table', function (e, row, $element) {
        $('.success').removeClass('success');
        $($element).addClass('success');
        RenglonSeleccionadoBusquedaPago = row;
        ObjAtenderTramiteViewModel.ModeloBuscarCodigo.No_Diverso(row.No_Diverso);
        TablaBuscarCodigoDetalle.bootstrapTable('resetView');
        TablaBuscarCodigoDetalle.bootstrapTable('refresh');


    });

}


//********************* métodos ---------------------------------------------------------------------------------
function inspeccionar() {

    if (RenglonSeleccionado === undefined || RenglonSeleccionado === null) {
        $.toast({
            heading: 'Atender Trámite',
            text: 'Selecciona un trámite',
            position: 'top-right',
            stack: false
        });
        return;
    }

    ObjAtenderTramiteViewModel.inspeccionar(RenglonSeleccionado);
}

function mensajesFormatter(value, row) {

    return  '<span class="badge">' + row.NumeroMensajes + '</span>';
}

function pagosFormatter(value, row) {
    return  '<span class="badge">' + row.NumeroPago + '</span>';
}

function calificar() {

    if (RenglonSeleccionado === undefined || RenglonSeleccionado === null) {
        $.toast({
            heading: 'Atender Trámite',
            text: 'Selecciona un trámite',
            position: 'top-right',
            stack: false
        });
        return;
    }

    ObjAtenderTramiteViewModel.limpiarMensajes();
    ObjAtenderTramiteViewModel.asingarDatosMensaje(RenglonSeleccionado);
    ObjAtenderTramiteViewModel.Abrir_Ventana_Calificar();

}


function visualizarMensajes() {

    if (RenglonSeleccionado === undefined || RenglonSeleccionado === null) {
        $.toast({
            heading: 'Atender Trámite',
            text: 'Selecciona un trámite',
            position: 'top-right',
            stack: false
        });
        return;
    }

    ObjAtenderTramiteViewModel.visualizarMensajes(RenglonSeleccionado);

}

function agregarPagos() {

    if (RenglonSeleccionado === undefined || RenglonSeleccionado === null) {
        $.toast({
            heading: 'Atender Trámite',
            text: 'Selecciona un trámite',
            position: 'top-right',
            stack: false
        });
        return;
    }

    ObjAtenderTramiteViewModel.inicializarBusquedaCodigoBarras(RenglonSeleccionado);
    ObjAtenderTramiteViewModel.ModeloBuscarCodigo.Codigo_Barras(RenglonSeleccionado.RPU);
    ObjAtenderTramiteViewModel.Abrir_Ventana_Pago();

}


function Botones_Operacion_Formato_Cancelar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Cancelar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}

window.Botones_Operacion_Evento = {

    'click .eliminar': function (e, value, row, index) {

        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.Codigo_Barras + ' </b>');
        ventanaEliminar.modal();
        RengloSeleccionadoPago = row;
    }
}


function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjAtenderTramiteViewModel.cancelarPago(RengloSeleccionadoPago.Pago_Solicitudes_ID);
}