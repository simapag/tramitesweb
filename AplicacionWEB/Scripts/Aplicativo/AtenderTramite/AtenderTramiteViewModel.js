﻿
var ObjAtenderTramiteViewModel;


$(function () {

    ObjAtenderTramiteViewModel = new atenderTramiteViewModel();
    ObjAtenderTramiteViewModel.ModeloBusquedaSolicitud.errors = ko.validation.group(ObjAtenderTramiteViewModel.ModeloBusquedaSolicitud);
    ObjAtenderTramiteViewModel.ModeloMensajeSolicitud.errors = ko.validation.group(ObjAtenderTramiteViewModel.ModeloMensajeSolicitud);
    ObjAtenderTramiteViewModel.ModeloBuscarCodigo.errors = ko.validation.group(ObjAtenderTramiteViewModel.ModeloBuscarCodigo);
    ko.applyBindings(ObjAtenderTramiteViewModel, $("#Interfaz_Atender_Tramites")[0]);
    ko.applyBindings(ObjAtenderTramiteViewModel, $("#Interfaz_Calificar_Solicitud")[0]);
    ko.applyBindings(ObjAtenderTramiteViewModel, $("#Interfaz_Agregar_Pago")[0]);
    ObjAtenderTramiteViewModel.Cargar_Informacion();
});


function atenderTramiteViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloBuscarCodigo = Modelo_Busqueda_Codigo_Barras();
    self.ModeloBusquedaSolicitud = Modelo_Busqueda_Solicitud();
    self.ModeloMensajeSolicitud = Modelo_Mensaje_Solicitud();
    self.ArregloTramite = ko.observableArray([]);
    self.ArregloEstatus = ko.observableArray(['PENDIENTE', 'EN PROCESO', 'PENDIENTE PAGO','PAGADO' ,'CANCELADO', 'TERMINADO']);
    self.bloquearControl = ko.observable(false);
    self.Pago_Solicitudes_ID = ko.observable('0');
    //Ventanas *******************************************

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };


    self.Abrir_Ventana_Calificar = function () {

        var ventantaEspera = $('#Ventana_Interfaz_Calificar_Solicitud');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Calificar= function () {
        var ventantaEspera = $('#Ventana_Interfaz_Calificar_Solicitud');
        ventantaEspera.modal('hide');
    };


    self.Abrir_Ventana_Pago = function () {

        var ventantaEspera = $('#Ventana_Interfaz_Agregar_Pago');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Pago = function () {
        var ventantaEspera = $('#Ventana_Interfaz_Agregar_Pago');
        ventantaEspera.modal('hide');
    };





    // Busquedas *************************************************
    self.Cargar_Informacion = function () {

        self.ArregloTramite($.parseJSON(Datos_Lista_Tramites));
        crearTablaSolicitudes();
        crearTableBuquedaCodigo();
        crearTablaBusquedaDetalle();
        crearTablaPagosAsignados();
        crearTablaPagosAsignadosDetalles();
    }

    self.buscar = function () {
        TablaSolicitudes.bootstrapTable('refresh');
    }

    // eventos combo de busquedas ****************

    self.eventoTramite = function (data, event) {


        if (self.ModeloBusquedaSolicitud.Tramite_Selected() === undefined) {
            self.ModeloBusquedaSolicitud.Tramite_ID('0');
        } else {
            self.ModeloBusquedaSolicitud.Tramite_ID(self.ModeloBusquedaSolicitud.Tramite_Selected().id);
        }

        self.buscar();
    }

    self.eventoEstatus = function (data, event) {
        var e = self.ModeloBusquedaSolicitud.Estatus();
        console.log(e);
        self.buscar();

    }


    // eventos toolbar
    self.inspeccionar = function (row) {


        var rutaNavegador = window.location.href;
        var posicion;
        posicion = rutaNavegador.indexOf('AtenderTramite');
        var ruta_base = rutaNavegador.substring(0, posicion -1);
        var ruta_completa = ruta_base + "/" + row.Controlador + "/Editar?Tramite_Solicitados_ID=" + row.Tramite_Solicitados_ID;
        window.location.href = ruta_completa;

    }

    self.visualizarMensajes = function (row) {

        window.location.href = URl_Visualizar_Mensaje + "?Tramite_Solicitados_ID=" + row.Tramite_Solicitados_ID;

    }


    // eventos calificar solicitud
    self.limpiarMensajes = function () {
        self.ModeloMensajeSolicitud.Tramite_Solicitados_ID('');
        self.ModeloMensajeSolicitud.Usuarios_ID('');
        self.ModeloMensajeSolicitud.Mensaje('');
        self.ModeloMensajeSolicitud.Estatus_Solicitud('');
        self.ModeloMensajeSolicitud.Nombre_Tramite('');
        self.ModeloMensajeSolicitud.Visto(false);

    }

    self.asingarDatosMensaje = function (row) {
        self.ModeloMensajeSolicitud.Tramite_Solicitados_ID(row.Tramite_Solicitados_ID);
        self.ModeloMensajeSolicitud.Usuarios_ID(0);
        self.ModeloMensajeSolicitud.Mensaje('');
        self.ModeloMensajeSolicitud.Estatus_Solicitud(row.Estatus_Revision);
        self.ModeloMensajeSolicitud.Nombre_Tramite(row.Nombre_Tramite);
        self.ModeloMensajeSolicitud.Visto(false);
        self.ModeloMensajeSolicitud.errors.showAllMessages(false);
    }

    self.guardarMensaje = function () {

        var strDatos;

        if (self.ModeloMensajeSolicitud.errors().length > 0) {
            self.ModeloMensajeSolicitud.errors.showAllMessages();
            $.toast({
                heading: 'Atender Trámites',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        strDatos = ko.toJSON(self.ModeloMensajeSolicitud);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'GuardarMensaje',
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Cerrar_Ventana_Calificar();
                    self.buscar();
                    $.toast({
                        heading: 'Atender Trámites',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    })

                }
                else {
                    $.toast({
                        heading: 'Atender Trámites',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "");
            }

        });

    
    }

    self.inicializarBusquedaCodigoBarras = function (row) {

        self.ModeloBuscarCodigo.Tramite_Solicitados_ID(row.Tramite_Solicitados_ID);
        self.ModeloBuscarCodigo.Codigo_Barras('');
        self.ModeloBuscarCodigo.errors.showAllMessages(false);

        self.ModeloBuscarCodigo.Lista_Pago_Encabezado([]);
        self.ModeloBuscarCodigo.Lista_Pago_Encabezado_Detalle([]);

        TablaBuscarCodigo.bootstrapTable('removeAll');
        TablaBuscarCodigo.bootstrapTable('resetView');
        TablaBuscarCodigoDetalle.bootstrapTable('removeAll');
        TablaBuscarCodigoDetalle.bootstrapTable('resetView');

        self.buscarPagosAsignados();
    }

    self.limpiarBusquedaCodigoBarras = function () {
        self.ModeloBuscarCodigo.Codigo_Barras('');
        self.ModeloBuscarCodigo.errors.showAllMessages(false);

        self.ModeloBuscarCodigo.Lista_Pago_Encabezado([]);
        self.ModeloBuscarCodigo.Lista_Pago_Encabezado_Detalle([]);

        TablaBuscarCodigo.bootstrapTable('removeAll');
        TablaBuscarCodigo.bootstrapTable('resetView');
        TablaBuscarCodigoDetalle.bootstrapTable('removeAll');
        TablaBuscarCodigoDetalle.bootstrapTable('resetView');

    }

    self.buscarPagosAsignados = function () {
        TablaPagosAgregados.bootstrapTable('resetView');
        TablaPagosAgregados.bootstrapTable('refresh');
    }
 
    self.buscarCodigoBarras = function () {

        if (self.ModeloBuscarCodigo.errors().length > 0) {
            self.ModeloBuscarCodigo.errors.showAllMessages();
            $.toast({
                heading: 'Atender Trámites',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        TablaBuscarCodigo.bootstrapTable('refresh');

        //self.Abrir_Ventana_Espera();

        //$.ajax({
        //    type: "GET",
        //    url: 'Buscar_Codigo_Barras?Codigo_Barras=' + self.ModeloBuscarCodigo.Codigo_Barras(),
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (response) {
        //        self.Cerrar_Ventana_Espera();
        //        if (response.Mensaje === "bien") {
            
        //            self.ModeloBuscarCodigo.Lista_Pago_Encabezado( $.parseJSON(response.Dato1));
        //            //self.ModeloBuscarCodigo.Lista_Pago_Encabezado_Detalle($.parseJSON(response.Dato2));

        //            TablaBuscarCodigo.bootstrapTable('load', self.ModeloBuscarCodigo.Lista_Pago_Encabezado());
        //            //TablaBuscarCodigoDetalle.bootstrapTable('load', self.ModeloBuscarCodigo.Lista_Pago_Encabezado_Detalle());

        //            $.toast({
        //                heading: 'Atender Trámites',
        //                text: 'Proceso Correcto',
        //                position: 'top-right',
        //                stack: false,
        //                icon: 'success'
        //            })

        //        }
        //        else {
        //            $.toast({
        //                heading: 'Atender Trámites',
        //                text: response.Mensaje,
        //                position: 'top-right',
        //                stack: false,
        //                icon: 'error'
        //            });
        //        }
        //    },
        //    error: function (result) {
        //        self.Cerrar_Ventana_Espera();
        //        $.alert(" " + result.status + ' ' + result.statusText, "");
        //    }

        //});


    }

    self.guardarPago = function () {


        if (RenglonSeleccionadoBusquedaPago === null || RenglonSeleccionadoBusquedaPago === undefined) {

            $.toast({
                heading: 'Atender Trámites',
                text: 'A un no seleciona un pago',
                position: 'top-right',
                stack: false
            });

            return;
        }

        var strPagos = ko.toJSON(RenglonSeleccionadoBusquedaPago);
        var renglonesDetalles = TablaBuscarCodigoDetalle.bootstrapTable('getData', true);


        if (renglonesDetalles === null || renglonesDetalles === undefined) {

            $.toast({
                heading: 'Atender Trámites',
                text: 'A un no seleciona un pago',
                position: 'top-right',
                stack: false
            });

            return;
        }


        var strPagosDetalle = ko.toJSON(renglonesDetalles);

        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'GuadarPago',
            data: "{'Tramite_Solicitados_ID': '" + self.ModeloBuscarCodigo.Tramite_Solicitados_ID() + "' , 'strListaPagoEncabezado':'" + strPagos + "' , 'strListaPagoDetalle': '" + strPagosDetalle + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {

                    self.limpiarBusquedaCodigoBarras();
                    self.buscarPagosAsignados();
                    self.buscar();
                    RenglonSeleccionadoBusquedaPago = null;
                    $.toast({
                        heading: 'Atender Trámites',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    })

                }
                else {
                    $.toast({
                        heading: 'Atender Trámites',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "");
            }

        });

    }

    self.cancelarPago = function (Pago_Solicitudes_ID) {

        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Cancelar_Pagos?Pago_Solicitudes_ID='+ Pago_Solicitudes_ID,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {

                    self.buscarPagosAsignados();
                    self.buscar();
                    $.toast({
                        heading: 'Atender Trámites',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    })

                }
                else {
                    $.toast({
                        heading: 'Atender Trámites',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "");
            }

        });
    }


}
