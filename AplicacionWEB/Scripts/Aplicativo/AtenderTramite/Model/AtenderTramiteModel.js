﻿
var Modelo_Busqueda_Solicitud = function () {

    var _Tramite_Selected = ko.observable(''),
        _Tramite_ID = ko.observable('0'),
        _Estatus = ko.observable('');

    return {
        Tramite_Selected: _Tramite_Selected,
        Tramite_ID:_Tramite_ID,
        Estatus : _Estatus
    }

}

var Modelo_Mensaje_Solicitud = function () {

    var _Tramite_Solicitados_ID = ko.observable(''),
        _Usuarios_ID = ko.observable('0'),
        _Mensaje = ko.observable('')
            .extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ maxLength: 500 }),
        _Estatus_Solicitud = ko.observable('').extend({ required: true }),
        _Nombre_Tramite = ko.observable('').extend({ required: true }),
        _Visto = ko.observable(false);

    return {
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Usuarios_ID: _Usuarios_ID,
        Mensaje: _Mensaje,
        Estatus_Solicitud: _Estatus_Solicitud,
        Nombre_Tramite : _Nombre_Tramite,
        Visto:_Visto
    }

}


var Modelo_Busqueda_Codigo_Barras = function () {
    
    var _Tramite_Solicitados_ID = ko.observable('0'),
        _Lista_Pago_Encabezado = ko.observableArray([]),
        _Lista_Pago_Encabezado_Detalle = ko.observableArray([]),
        _No_Diverso = ko.observable('-1')
        _Codigo_Barras = ko.observable('-1').extend({ required: true })
        .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' });

    return {
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Codigo_Barras: _Codigo_Barras,
        No_Diverso : _No_Diverso,
        Lista_Pago_Encabezado: _Lista_Pago_Encabezado,
        Lista_Pago_Encabezado_Detalle: _Lista_Pago_Encabezado_Detalle
    }

}
