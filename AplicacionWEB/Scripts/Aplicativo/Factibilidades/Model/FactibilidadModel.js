﻿
var Factibilidad_Model = function () {

    var _Formulario_Factibilidades_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _Nombre = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 200 }),
        _Colonia = ko.observable('').extend({ required: true })
             .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
             .extend({ maxLength: 200 }),
        _Calle = ko.observable('').extend({ required: true })
             .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
             .extend({ maxLength: 200 }),
        _Numero = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 50 }),
        _Codigo_Postal = ko.observable('').extend({ digit: true })
                                         .extend({ maxLength: 5 }),
        _Correo_Electronico = ko.observable('').extend({ email: true })
        .extend({ maxLength: 80 }),
        _Telefono = ko.observable('').extend({ required: true }).extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                                     .extend({ maxLength: 40 }),
        _RFC = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()' })
                                .extend({ maxLength: 15 }),
        _Ciudad = ko.observable('').extend({ required: true })
             .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
             .extend({ maxLength: 100 }),
        _Estado = ko.observable('').extend({ required: true })
              .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
             .extend({ maxLength: 100 });

    return {
        Formulario_Factibilidades_ID: _Formulario_Factibilidades_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        Nombre: _Nombre,
        Colonia: _Colonia,
        Calle: _Calle,
        Numero: _Numero,
        Codigo_Postal: _Codigo_Postal,
        Correo_Electronico: _Correo_Electronico,
        Telefono: _Telefono,
        RFC: _RFC,
        Ciudad: _Ciudad,
        Estado: _Estado

    }
}