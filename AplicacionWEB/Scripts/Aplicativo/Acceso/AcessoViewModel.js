﻿var ObjAccesoViewModel;


$(function () {

    ObjAccesoViewModel = new Acceso_ViewModel();

    ko.applyBindings(ObjAccesoViewModel, $("#Interface_Datos")[0]);
    ObjAccesoViewModel.errors = ko.validation.group(ObjAccesoViewModel);
    ObjAccesoViewModel.Cargar_Informacion();

});

// funcion principal del View Model
function Acceso_ViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";


    self.ArregloPapa = ko.observableArray([]);
    self.SeleccionRol = ko.observable().extend({ required: true });

    //-----------------------------------------------
     /// <summary>
        /// abre una ventana indicando que se esta haciendo una petición ajax
      /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

      /// <summary>
        ///  cierra una ventana indicando que se esta haciendo una petición ajax
      /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };


       /// <summary>
        ///  Evento click del combo tipo de menu
      /// </summary>
    self.Evento_Clic_Rol = function () {
        TablaAcceso.bootstrapTable('uncheckAll');
        $('#tabla_acceso input[type="checkbox"]').prop('checked', false);

        if (self.SeleccionRol() !== undefined) {


            self.Abrir_Ventana_Espera();

            $.ajax({
                url: 'Obtener_Menu_A_Partir_Rol',
                type: 'post',
                dataType: 'json',
                data: "{'p_Rol_ID':'" + self.SeleccionRol().id + "'}",
                contentType: 'application/json',
                success: function (response) {
                    self.Cerrar_Ventana_Espera();
                    if (response.Mensaje === "bien") {
                        var Valores_Obtenidos = ko.utils.parseJson(response.Dato1)
                        var arreglo = [];

                        for (var index = 0; index < Valores_Obtenidos.length; index++) {
                            arreglo.push(Valores_Obtenidos[index].id);
                        }
   
                        TablaAcceso.bootstrapTable("checkBy", { field: "id", values: arreglo });

                    }
                    else {
                        $.alert(" " + response.Mensaje, "Accesos");
                    }


                },
                error: function (result) {
                    self.Cerrar_Ventana_Espera();
                    $.alert('Ocurrio un Error!' + result.status + ' ' + result.statusText);

                }

            });

        }

    }

    /// <summary>
        ///  Llena el combo de los roles
    /// </summary>

    //-----------------------------------------------
    self.Cargar_Informacion = function () {
        self.ArregloPapa($.parseJSON(Datos_Roles_Global));
    }

    //-----------------------------------------------

   /// <summary>
        ///  Obtienes los elementos que fueron seleccionados
    /// </summary>

    self.Obtener_Elementos_Seleccionados = function () {

        return TablaAcceso.bootstrapTable('getSelections');

    }

     //-----------------------------------------------

     /// <summary>
        ///  Guarda la configuracion de un rol, para un conjunto de menús
    /// </summary>
    self.Guardar_Configuracion = function () {

        var modeloAcceso = { Menu_ID: 0, Rol_ID: 0 };
        var arregloAcceso = new Array();
        var strDatos = '';

        if (ObjAccesoViewModel.errors().length > 0) {
            ObjAccesoViewModel.errors.showAllMessages();
            $.alert("Se requiren unos valores", "Menú");
            return;
        }


        var elementosSeleccinados = self.Obtener_Elementos_Seleccionados();

        if (elementosSeleccinados.length === 0) {
            modeloAcceso = new Object();
            modeloAcceso.Menu_ID = 0;
            modeloAcceso.Rol_ID = self.SeleccionRol().id
            arregloAcceso.push(modeloAcceso)
        } else {

            $.map(elementosSeleccinados, function (row) {
                modeloAcceso = new Object();
                modeloAcceso.Menu_ID = row.id;
                modeloAcceso.Rol_ID = self.SeleccionRol().id
                arregloAcceso.push(modeloAcceso);
            });
        }

        strDatos = ko.toJSON(arregloAcceso);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: 'Guardar_Configuracion',
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                                            
                    $.alert("Proceso Terminado", { type: 'success' });
                }
                else {
                    $.alert(" " + response.Mensaje, "Accesos");
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Interfaz();

            }

        });

    }







}