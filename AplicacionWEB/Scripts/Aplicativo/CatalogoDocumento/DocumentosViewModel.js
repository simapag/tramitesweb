﻿var ObjDocumentosViewModel;

$(function () {
    ObjDocumentosViewModel = new documentoViewModel();
    ObjDocumentosViewModel.ModeloDocumento.errors = ko.validation.group(ObjDocumentosViewModel.ModeloDocumento);
    ko.applyBindings(ObjDocumentosViewModel, $("#Interfaz_catalogo_documentos")[0]);
});


function documentoViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";


    self.ModeloDocumento = Modelo_Documento();
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);

    //Ventanas *******************************************

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    };

    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    };

    // Busquedas *************************************************
    self.Buscar_Datos = function () {
        TablaDocumento.bootstrapTable('refresh');
    };


    self.Limpiar_Cajas = function () {

        self.ModeloDocumento.Documento_ID('0');
        self.ModeloDocumento.Nombre_Documento('');
        self.ModeloDocumento.Estatus('');
        self.ModeloDocumento.errors.showAllMessages(false);

    };

    self.Asignar_Valores = function (p_Row) {
        self.ModeloDocumento.Documento_ID(p_Row.Documento_ID);
        self.ModeloDocumento.Nombre_Documento(p_Row.Nombre_Documento);
        self.ModeloDocumento.Estatus(p_Row.Estatus);
        self.ModeloDocumento.errors.showAllMessages(false);
    }

    // Operaciones ***********************************************

    self.Nuevo_Elemento = function () {
        self.Abrir_Ventana_Interfaz();
        self.Limpiar_Cajas();
    };

    self.Editar_Elemento = function (p_Row) {
        self.Limpiar_Cajas();
        self.Asignar_Valores(p_Row);
        self.Abrir_Ventana_Interfaz();
    };

    self.Borrar_Elemento = function (p_Row) {
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar_Documento");
        self.ValidarDatos(false);
        self.Procesar_Operacion();
    };

    self.Aceptar_Operacion = function () {
        self.TipoOperacion('Guardar_Documento');
        self.ValidarDatos(true);
        self.Procesar_Operacion();
    };

    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (self.ModeloDocumento.errors().length > 0) {
                self.ModeloDocumento.errors.showAllMessages();
                $.toast({
                    heading: 'Catálogo de Trámites',
                    text: 'Se requiere un valores',
                    position: 'top-right',
                    stack: false
                });
                return;
            }
        }



        strDatos = ko.toJSON(self.ModeloDocumento);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Interfaz();
                    $.toast({
                        heading: 'Catálogo de Documentos',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    })
                }
                else {
                    $.toast({
                        heading: 'Catálogo de Documentos',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "Catálogo de Documentos");

            }

        });

    };

}