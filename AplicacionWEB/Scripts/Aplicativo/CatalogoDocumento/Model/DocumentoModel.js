﻿var Modelo_Documento = function () {

    var _Documento_ID = ko.observable(''),
        _Nombre_Documento = ko.observable('')
            .extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.,' })
            .extend({ maxLength: 500 }),
        _Estatus = ko.observable('').extend({ required: true });

    return {
        Documento_ID: _Documento_ID,
        Nombre_Documento: _Nombre_Documento,
        Estatus: _Estatus
    }
}