﻿
var TablaDocumento = $("#Tabla_Documento");

$(function () {
    Inicializar_Tabla();
});


function Inicializar_Tabla() {
    TablaDocumento.bootstrapTable('destroy');
    TablaDocumento.bootstrapTable({ url: 'Obtener_Documentos' });

}


function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Eliminar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}

function Botones_Operacion_Formato_Editar(value, row, index) {
    return [
        '<a class="editar" href="javascript:void(0)" title="Editar">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>'
    ].join('');
}

window.Botones_Operacion_Evento = {

    'click .eliminar': function (e, value, row, index) {

        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.Nombre_Documento + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionado = row;


    },
    'click .editar': function (e, value, row, index) {
        ObjDocumentosViewModel.Editar_Elemento(row);
    }


};

function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjDocumentosViewModel.Borrar_Elemento(RenglonSeleccionado);
}