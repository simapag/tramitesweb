﻿var ObjRecuperarPasswordViewModel;

$(function () {

    ObjRecuperarPasswordViewModel = new recuperarPasswordViewModel();
    ObjRecuperarPasswordViewModel.ModelRecuperarPassword.errors = ko.validation.group(ObjRecuperarPasswordViewModel.ModelRecuperarPassword);
    ko.applyBindings(ObjRecuperarPasswordViewModel, $("#Interface_Recupera_Password")[0]);

});


function recuperarPasswordViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";


    self.ModelRecuperarPassword = Modelo_Recuperar_Password();

    //-----------------------------------------------
    /// <summary>
    ///     abre una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }

    /// <summary>
    ///     cierra una ventana indicando que se esta haciendo una petición ajax
    /// </summary>
    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }

    /// <summary>
    ///     abre una ventana para registrar información en la base de datos
    /// </summary>

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Recuperar_Password');
        ventanaInterfaz.modal();
    }

    /// <summary>
    ///     cierra una ventana para registrar información en la base de datos
    /// </summary>
    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Recuperar_Password')
        ventanaInterfaz.modal('hide');
    }


    //-----------------------------------------------

    self.limpiarDatos = function () {

        self.ModelRecuperarPassword.Correo('');
        self.ModelRecuperarPassword.errors.showAllMessages(false);
    }

    self.RecuperarPassword = function () {


        if (self.ModelRecuperarPassword.errors().length > 0) {
            self.ModelRecuperarPassword.errors.showAllMessages();
            $.toast({
                heading: 'Recuperar Contraseña',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

       
        self.Abrir_Ventana_Espera();

        var rutaNavegador = window.location.href;
        var posicion;
        var operacion;

        posicion = rutaNavegador.indexOf('Verificar_Usuario');

        if (posicion === -1) {
            // signfica que el usuario puso el enlace por default
            operacion = 'Login/Recuperar_Password?Email=' + self.ModelRecuperarPassword.Correo();
        } else {
            // significa que en la ruta puso controlador y accion en la ruta del navegador
            operacion = 'Recuperar_Password?Email=' + self.ModelRecuperarPassword.Correo();
        }

        $.ajax({
            type: "GET",
            url: operacion,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Cerrar_Ventana_Interfaz();
                    $.toast({
                        heading: 'Recuperación de Constraseña',
                        text: 'Se te ha enviado la constraseña a tu correo',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    })

                }
                else {
                    $.toast({
                        heading: 'Recuperación de Constraseña',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "");
            }

        });


    }
}