﻿var Servicio_Operativo_Model = function () {

    var _Formulario_Servicio_Operativo_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _NoCuenta = ko.observable(''),
        _Nombre_Usuario = ko.observable(''),
        _Domicilio = ko.observable('');

    return {
        Formulario_Servicio_Operativo_ID: _Formulario_Servicio_Operativo_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        NoCuenta: _NoCuenta,
        RPU: _RPU,
        Nombre_Usuario: _Nombre_Usuario,
        Domicilio: _Domicilio
    }

}