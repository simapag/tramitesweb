﻿
$(function () {

    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });

    const togglePasswordConfirmar = document.querySelector('#togglePasswordConfirmar');
    const passwordConfirmar = document.querySelector('#passwordConfirmar');

    togglePasswordConfirmar.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = passwordConfirmar.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordConfirmar.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });

});


function abrirVentanaRegistrarUsuario() {
    ObjRegistroUsuarioViewModel.limpiarDatos();
    ObjRegistroUsuarioViewModel.Abrir_Ventana_Interfaz();
}