﻿var Modelo_Usuarios_Registro = function () {


    var _Usuarios_ID = ko.observable(''),
        _Nombre = ko.observable('').extend({ required: true })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Apellido_Paterno = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ required: true }),
        _Apellido_Materno = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' }),
        _Correo_Electronico = ko.observable('').extend({ required: true })
            .extend({ email: true })
            .extend({ maxLength: 50 }),
        _Telefono_Celular = ko.observable('').extend({ digit: true })
            .extend({ minLength: 10 })
            .extend({ maxLength: 10 })
            .extend({ required: true }),
       _Password = ko.observable('').extend({ required: true })
         .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
         .extend({ minLength: 4, maxLength: 12 })
       _ConfirmarPassword = ko.observable('').extend({ required: true })
        .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
        .extend({ es_igual: _Password });


    return {
        Usuarios_ID: _Usuarios_ID,
        Nombre: _Nombre,
        Apellido_Paterno: _Apellido_Paterno,
        Apellido_Materno: _Apellido_Materno,
        Correo_Electronico: _Correo_Electronico,
        Telefono_Celular: _Telefono_Celular,
        Password: _Password,
        ConfirmarPassword: _ConfirmarPassword
     
    
    }


}