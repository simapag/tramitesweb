﻿
var ObjRegistroUsuarioViewModel;


$(function () {
    ObjRegistroUsuarioViewModel = new registroUsuarioViewModel();
    ObjRegistroUsuarioViewModel.ModeloRegistroUsuario.errors = ko.validation.group(ObjRegistroUsuarioViewModel.ModeloRegistroUsuario);
    ko.applyBindings(ObjRegistroUsuarioViewModel, $("#Interface_Registro_Usuario")[0]);

});



function registroUsuarioViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloRegistroUsuario = Modelo_Usuarios_Registro();


   

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    }


    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    }


    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Registro_Usuario');
        ventanaInterfaz.modal();
    }

    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Registro_Usuario')
        ventanaInterfaz.modal('hide');
    }

    self.limpiarDatos = function () {

        self.ModeloRegistroUsuario.Nombre('');
        self.ModeloRegistroUsuario.Apellido_Paterno('');
        self.ModeloRegistroUsuario.Apellido_Materno('');
        self.ModeloRegistroUsuario.Correo_Electronico('');
        self.ModeloRegistroUsuario.Telefono_Celular('');
        self.ModeloRegistroUsuario.Password('');
        self.ModeloRegistroUsuario.errors.showAllMessages(false);
    }


    self.guardar = function () {
        var strDatos;

        if (self.ModeloRegistroUsuario.errors().length > 0) {
            self.ModeloRegistroUsuario.errors.showAllMessages();
            $.toast({
                heading: 'Registro de Usuarios',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        }

        strDatos = ko.toJSON(self.ModeloRegistroUsuario);
        self.Abrir_Ventana_Espera();

        var rutaNavegador = window.location.href;
        var posicion;
        var operacion;

        posicion = rutaNavegador.indexOf('Verificar_Usuario');

        if (posicion === -1) {
            // signfica que el usuario puso el enlace por default
            operacion = 'Login/Registrar_Usuario';
        } else {
            // significa que en la ruta puso controlador y accion en la ruta del navegador
            operacion = 'Registrar_Usuario'
        }

        $.ajax({
            type: "POST",
            url: operacion,
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Cerrar_Ventana_Interfaz();
                    $.toast({
                        heading: 'Registro de Usuarios',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    })
                
                }
                else {
                    $.toast({
                        heading: 'Registro de Usuarios',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });  
                }
            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.alert(" " + result.status + ' ' + result.statusText, "");
            }

        });

    };

}