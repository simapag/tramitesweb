﻿
var Constancia_Model = function () {

    var _Formulario_Constancia_ID = ko.observable(''),
        _Tramite_Solicitados_ID = ko.observable(''),
        _RPU = ko.observable(''),
        _Nombre_Usuario = ko.observable(''),
        _Domicilio = ko.observable('');

    return {
        Formulario_Constancia_ID: _Formulario_Constancia_ID,
        Tramite_Solicitados_ID: _Tramite_Solicitados_ID,
        RPU: _RPU,
        Nombre_Usuario: _Nombre_Usuario,
        Domicilio: _Domicilio
    }

}