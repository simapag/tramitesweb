﻿using iText.Barcodes;
using iText.IO.Font;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Kernel.Pdf.Xobject;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AplicacionWEB.ReportePDF
{
    public class Ticket
    {

        public decimal Total_Pagar;
        DatosCuentaView CuentaView;
        public string ruta;
        public DatosPagoView Datos_Pagos;
        public string No_Factura_Recibo;


        public Ticket(string ruta, DatosCuentaView cuentaView, DatosPagoView datosPagoView, decimal totalPagar, string no_factura_recibo)
        {
        
            CuentaView = cuentaView;
            Datos_Pagos = datosPagoView;
            Total_Pagar = totalPagar;
            No_Factura_Recibo = no_factura_recibo;
            this.ruta = ruta;
        }


        public void IniciarReporte()
        {
            PdfWriter writer = new PdfWriter(ruta);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);


            document.Add(Crear_Tabla_Encabezado());

            //Paragraph titulo = new Paragraph("Datos de la cuenta")
            //    .SetFontSize(15);
            //document.Add(titulo);

            LineSeparator ls = new LineSeparator(new SolidLine());
            //document.Add(ls);

            // New line
            Paragraph newline = new Paragraph(new Text("\n"));

            //document.Add(newline);
            //document.Add(Crear_Tabla_Datos_Usuario());

           // document.Add(newline);
            Paragraph titulo2 = new Paragraph("RPU " + CuentaView.RPU + " Período de adeudo: " + Datos_Pagos.Mes_Pago )
            .SetFontSize(15);
            document.Add(titulo2);
            document.Add(ls);
            document.Add(Crear_Tabla_Desglose());
           


            //document.Add(newline);
            //Paragraph titulo1 = new Paragraph("Código de pago")
            // .SetFontSize(15);
            //document.Add(titulo1);
            document.Add(ls);
            document.Add(Crear_Tabla_Leyendas());
            document.Add(Crea_Tabla_Codigo(pdf));
            

            int n = pdf.GetNumberOfPages();

            for (int i = 1; i <= n; i++)
            {
                document.ShowTextAligned(new Paragraph("Trámites en línea " + DateTime.Now.ToString("dd/MM/yyyy HH:mm")),
                   559, 830, i, TextAlignment.RIGHT,
                   VerticalAlignment.TOP, 0);
            }

            document.Close();
        }


        public Table Crear_Tabla_Datos_Usuario()
        {
            Table table = new Table(4, false);

            table.AddCell(Obtener_Celda_Encabezado("RPU"));
            table.AddCell(Obtener_Celda(CuentaView.RPU));
            table.AddCell(Obtener_Celda_Encabezado("Colonia"));
            table.AddCell(Obtener_Celda(CuentaView.Colonia));


            table.AddCell(Obtener_Celda_Encabezado("No. Cuenta"));
            table.AddCell(Obtener_Celda(CuentaView.No_Cuenta));
            table.AddCell(Obtener_Celda_Encabezado("Calle"));
            table.AddCell(Obtener_Celda(CuentaView.Calle));


            table.AddCell(Obtener_Celda_Encabezado("Usuario"));
            table.AddCell(Obtener_Celda(CuentaView.Nombre_Usuario));
            table.AddCell(Obtener_Celda_Encabezado("Número Exterior"));
            table.AddCell(Obtener_Celda(CuentaView.Numero_Exterior));

            return table;
        }


        public Cell Obtener_Celda_Encabezado(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetBackgroundColor(ColorConstants.GRAY)
                  .SetTextAlignment(TextAlignment.CENTER)
                  .Add(new Paragraph(text));


            return cell;
        }

        public Cell Obtener_Celda_Encabezado1(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetFont(PdfFontFactory.CreateFont(iText.IO.Font.Constants.StandardFonts.HELVETICA_BOLD))
                  .SetTextAlignment(TextAlignment.CENTER)
                  .Add(new Paragraph(text));


            return cell;
        }

        public Cell Obtener_Celda(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetTextAlignment(TextAlignment.CENTER)
                  .Add(new Paragraph(text));


            return cell;
        }

        public Cell Obtener_Celda_Derecha(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetVerticalAlignment(VerticalAlignment.BOTTOM)
                  .SetTextAlignment(TextAlignment.RIGHT)
                  .Add(new Paragraph(text));


            return cell;
        }

        public Cell Obtener_Celda_Izquierda(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetVerticalAlignment(VerticalAlignment.BOTTOM)
                  .SetTextAlignment(TextAlignment.LEFT)
                  .Add(new Paragraph(text));


            return cell;
        }

        public Table Crear_Tabla_Encabezado()
        {
            Table table = new Table(2, false).UseAllAvailableWidth();

            Cell cell = new Cell(1, 1)
             .SetTextAlignment(TextAlignment.CENTER)
             .Add(Obtener_Imagen())
             .SetBorder(Border.NO_BORDER);

            Paragraph header = new Paragraph("Ticket Para Pago")
               .SetFontSize(20);

            Cell cell1 = new Cell(1, 1)
                .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                .SetHorizontalAlignment(HorizontalAlignment.CENTER)
                .Add(header)
                .SetBorder(Border.NO_BORDER);

            table.AddCell(cell);
            table.AddCell(cell1);

            return table;
        }

        public Table Crea_Tabla_Codigo(PdfDocument pdfDoc)
        {
            Table table = new Table(2, false).UseAllAvailableWidth();
            table.AddCell(Obtener_Celda_Codigo( No_Factura_Recibo + "F", pdfDoc));
            table.AddCell(Obtener_Celda_Codigo(Obtener_Codigo_Barras_Externo(),pdfDoc));

            return table;
        }


        public Table Crear_Tabla_Leyendas()
        {
            Table table = new Table(2, false).UseAllAvailableWidth();
            table.AddCell(Obtener_Celda_Izquierda("Pago en cajas").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda_Derecha("Pago en Oxxo y cajeros SIMAPAG").SetBorder(Border.NO_BORDER));
            return table;
        }


        public Table Crear_Tabla_Desglose()
        {
            Table table = new Table(2, false).UseAllAvailableWidth();

            table.AddCell(Obtener_Celda_Encabezado1("Meses de Adeudo").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Meses_Adeudo.ToString()).SetBorder(Border.NO_BORDER));

            table.AddCell(Obtener_Celda_Encabezado1("Presente Mes").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Total_Mes.ToString("c")).SetBorder(Border.NO_BORDER));

            table.AddCell(Obtener_Celda_Encabezado1("Adeudos Anteriores").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Adeudo.ToString("c")).SetBorder(Border.NO_BORDER));

            table.AddCell(Obtener_Celda_Encabezado1("Saldo a Favor").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Saldo_Favor.ToString("c")).SetBorder(Border.NO_BORDER));


            table.AddCell(Obtener_Celda_Encabezado1("Sanción").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Sancion_Adeudo.ToString("c")).SetBorder(Border.NO_BORDER));

            table.AddCell(Obtener_Celda_Encabezado1("Convenio Sin Interés").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Convenio_Adeudo.ToString("c")).SetBorder(Border.NO_BORDER));


            table.AddCell(Obtener_Celda_Encabezado1("Diversos").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Datos_Pagos.Diverso_Adeudo.ToString("c")).SetBorder(Border.NO_BORDER));

            table.AddCell(Obtener_Celda_Encabezado1("Total a Pagar").SetBorder(Border.NO_BORDER));
            table.AddCell(Obtener_Celda(Total_Pagar.ToString("c")).SetBorder(Border.NO_BORDER));


            return table;

        }


        public Cell Obtener_Celda_Qr(string codigo, PdfDocument pdfDoc)
        {
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode(codigo);
            PdfFormXObject barcodeObject = barcodeQRCode.CreateFormXObject(ColorConstants.BLACK,pdfDoc);
            Image barcodeImage = new Image(barcodeObject).SetWidth(100f).SetHeight(100f).SetHorizontalAlignment(HorizontalAlignment.CENTER);
            Cell cell = new Cell().Add(barcodeImage);
            cell.SetPaddingTop(10);
            cell.SetPaddingRight(10);
            cell.SetPaddingBottom(10);
            cell.SetPaddingLeft(10);
            cell.SetBorder(Border.NO_BORDER);
            cell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);

            return cell;

        }

        public Cell Obtener_Celda_Codigo(string codigo, PdfDocument pdfDoc)
        {
            Barcode128 code128 = new Barcode128(pdfDoc);
            code128.SetCode(codigo);
            code128.SetCodeType(Barcode128.CODE128);

            PdfFormXObject barcodeObject = code128.CreateFormXObject(null, null, pdfDoc);
            Cell cell = new Cell(1,1).Add(new Image(barcodeObject).SetHorizontalAlignment(HorizontalAlignment.CENTER));
            cell.SetPaddingTop(10);
            cell.SetPaddingRight(10);
            cell.SetPaddingBottom(10);
            cell.SetPaddingLeft(10);
            cell.SetBorder(Border.NO_BORDER);
            cell.SetVerticalAlignment(VerticalAlignment.TOP);
            cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
            return cell;

        }

        public Image Obtener_Imagen()
        {


            string ruta_imagen = System.AppDomain.CurrentDomain.BaseDirectory + "img\\logo_sim.png";

            // Add image
            Image img = new Image(ImageDataFactory
               .Create(ruta_imagen))
               .SetTextAlignment(TextAlignment.LEFT)
               .SetWidth(90f)
               .SetHeight(90f);


            return img;
        }

        public string Obtener_Codigo_Barras_Externo()
        {
            string codigo = "";
            int posicion;
            int Total_Pagar_Entera = (int)Total_Pagar;
            string Total_Pagar_Cadena = Total_Pagar.ToString();
            string Parte_Decimal;

            posicion = Total_Pagar_Cadena.IndexOf(".");

            Parte_Decimal = Total_Pagar_Cadena.Substring(posicion + 1,2);

            codigo = CuentaView.RPU + ""+ String.Format("{0:00000000}", Total_Pagar_Entera) + String.Format("{0:00}", Parte_Decimal);

            return codigo;
        }
    }
}