﻿using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AplicacionWEB.ReportePDF
{
    public class Reporte_Mis_Tramites
    {
        public List<TramitesSolicitudView> Lista_Solicitudes;
        public string ruta;

        public Reporte_Mis_Tramites(string ruta, List<TramitesSolicitudView> listaSolicitudes)
        {
            this.ruta = ruta;
            Lista_Solicitudes = listaSolicitudes;
        }

        public void IniciarReporte()
        {
            PdfWriter writer = new PdfWriter(ruta);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            document.Add(Crear_Tabla_Encabezado());

            // Line separator
            LineSeparator ls = new LineSeparator(new SolidLine());
            document.Add(ls);

            // New line
            Paragraph newline = new Paragraph(new Text("\n"));

            document.Add(newline);

            document.Add(Crear_Tabla());


            int n = pdf.GetNumberOfPages();

            for (int i = 1; i <= n; i++)
            {
                document.ShowTextAligned(new Paragraph("Trámites en línea " + DateTime.Now.ToString("dd/MM/yyyy HH:mm")),
                   559, 830, i, TextAlignment.RIGHT,
                   VerticalAlignment.TOP, 0);
            }

            document.Close();
        }

        
        public Table Crear_Tabla_Encabezado()
        {
            Table table = new Table(2, false);

            Cell cell = new Cell(1, 1)
             .SetTextAlignment(TextAlignment.CENTER)
             .Add(Obtener_Imagen())
             .SetBorder(Border.NO_BORDER);

            Paragraph header = new Paragraph("Reporte de Trámites Personales")
               .SetFontSize(20);

            Cell cell1 = new Cell(1, 1)
                .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                .Add(header)
                .SetBorder(Border.NO_BORDER);

            table.AddCell(cell);
            table.AddCell(cell1);

            return table;
        }

        public Table Crear_Tabla()
        {
            Table table = new Table(6, false);

            table.AddCell(Obtener_Celda_Encabezado("Fecha"));
            table.AddCell(Obtener_Celda_Encabezado("Folio Solicitud"));
            table.AddCell(Obtener_Celda_Encabezado("Nombre del Trámite"));
            table.AddCell(Obtener_Celda_Encabezado("RPU"));
            table.AddCell(Obtener_Celda_Encabezado("Usuario"));
            table.AddCell(Obtener_Celda_Encabezado("Observaciones"));

            foreach(TramitesSolicitudView item in Lista_Solicitudes)
            {
                table.AddCell(Obtener_Celda(item.Fecha_Solicitud.ToString("dd/MM/yyyy HH:mm")));
                table.AddCell(Obtener_Celda(item.Tramite_Solicitados_ID.ToString()));
                table.AddCell(Obtener_Celda(item.Nombre_Tramite));
                table.AddCell(Obtener_Celda(item.RPU));
                table.AddCell(Obtener_Celda(item.Usuario));
                table.AddCell(Obtener_Celda(item.Observaciones));
            }

            return table;
        }


        public Cell Obtener_Celda_Encabezado(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetBackgroundColor(ColorConstants.GRAY)
                  .SetTextAlignment(TextAlignment.CENTER)
                  .Add(new Paragraph(text));


            return cell;
        }

        public Cell Obtener_Celda(string text)
        {
            Cell cell = new Cell(1, 1)
                  .SetTextAlignment(TextAlignment.CENTER)
                  .Add(new Paragraph(text));


            return cell;
        }


        public Image Obtener_Imagen()
        {


            string ruta_imagen = System.AppDomain.CurrentDomain.BaseDirectory + "img\\logo_sim.png";

            // Add image
            Image img = new Image(ImageDataFactory
               .Create(ruta_imagen))
               .SetTextAlignment(TextAlignment.LEFT)
               .SetWidth(90f)
               .SetHeight(90f);


            return img;
        }
    }
}