﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AplicacionWEB.Infraestructura;
using System.Web.Optimization;
using ModelDomain.Contexto;
using System.Data.Entity;
using ModelDomain.ViewModel;
using AplicacionWEB.Models;

namespace AplicacionWEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer<EFDbContext>(null);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());

            ModelBinders.Binders.Add(typeof(Form_Pago_Multipagos), new FormMultipagoBinder());
        }

        //void Session_Start(object sender, EventArgs e)
        //{
        //    if (Session.IsNewSession)
        //    {
        //        //do things that need to happen
        //        //when a new session starts.
        //        Session.Timeout = 120;
        //    }
        //}
    }
}
