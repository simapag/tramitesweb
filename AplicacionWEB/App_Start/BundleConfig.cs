﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace AplicacionWEB
{
    public class BundleConfig
    {

        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
               "~/Scripts/jquery-2.1.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap-table.min.js"));

            //Create bundel for jQueryUI  
            //js  
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
          "~/Scripts/jquery-ui-{version}.js"));
            //css  
            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
                   "~/Content/jquery-ui.css"));


            bundles.Add(new ScriptBundle("~/bundles/ko").Include(
                "~/Scripts/knockout-3.3.0.js",
                 "~/Scripts/validacionesKO/knockout.validation.js", 
                "~/Scripts/validacionesKO/Reglas_Validacion.js",
                "~/Scripts/validacionesKO/es-ES.js",
                "~/Scripts/validacionesKO/Util_KO.js"));
 
            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/bootstrap.css",
                 "~/Content/bootstrap-table.min.css",
                 "~/Content/font-awesome.css",
                 "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/Roles").Include(
                "~/Scripts/Aplicativo/Roles/UI/RolesUI.js",
                "~/Scripts/Aplicativo/Roles/Model/RolesModel.js",
                "~/Scripts/Aplicativo/Roles/RolesViewModel.js"));

        }
    }
}