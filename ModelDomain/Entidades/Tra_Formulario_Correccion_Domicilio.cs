﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Formulario_Correccion_Domicilio")]
    public class Tra_Formulario_Correccion_Domicilio
    {
        [Key]
        public int Formulario_Correccion_Domicilio_ID { get; set; }
        public int Tramite_Solicitados_ID {get; set;}
        public string RPU { get; set; }
        public string Domicilio_Anterior { get; set; }
        public string Domicilio_Nuevo { get; set; }
    }
}
