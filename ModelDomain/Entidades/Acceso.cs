﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{

    /// <summary>
    /// Entidad Acceso
    /// </summary>
    [Table("Tra_Accesos")]
   public class Acceso
    {

        public Acceso() {

        }

        [Key]
        public int Acesso_ID { get; set; }
        public int Menu_ID { get; set; }
        public int Rol_ID { get; set; }

        public string Usuario_Creo { get; set; }
        public Nullable<DateTime> Fecha_Creo { get; set; }

        public virtual Menu Menu { get; set; }
        public virtual Roles Roles { get; set; }

    }
}
