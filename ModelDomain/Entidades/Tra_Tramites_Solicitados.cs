﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Tramites_Solicitados")]
    public class Tra_Tramites_Solicitados
    {
        [Key]
        public int Tramite_Solicitados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public int Tramite_ID { get; set; }
        public decimal Costo_Tramite { get; set; }
        public string Estatus { get; set; }
        public string Estatus_Revision { get; set; }
        public string Referencia_Pago { get; set; }
        public int? Usuarios_Asignado_ID { get; set; }
        public  string RPU { get; set; }
        public string Observaciones { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }

        public virtual ICollection<Tra_Tramites_Solicitados_Documentos> Tra_Tramites_Solicitados_Documentos { get; set; }
        public virtual ICollection<Tra_Mensajes_Solicitudes> Tra_Mensajes_Solicitudes { get; set; }
        public virtual ICollection<Tra_Pagos_Solicitudes> Tra_Pagos_Solicitudes { get; set; }
        public virtual Tra_Tramites Tra_Tramites { get; set; }
        [ForeignKey("Usuarios_ID")]
        public virtual Usuarios Usuarios { get; set; }
        [ForeignKey("Usuarios_Asignado_ID")]
        public virtual Usuarios UsuarioAsignado { get; set; }

        
    }
}
