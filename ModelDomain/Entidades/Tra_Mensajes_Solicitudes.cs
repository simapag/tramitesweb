﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Mensajes_Solicitudes")]
    public class Tra_Mensajes_Solicitudes
    {
        [Key]
        public int Mensajes_Solicitudes_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public string Mensaje { get; set; }
        public string Entidad { get; set; }
        public string Estatus_Solicitud { get; set; }
        public bool Visto { get; set; }
        public string Nombre_Tramite { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
         
        public virtual Usuarios Usuarios { get; set; }
        public virtual Tra_Tramites_Solicitados Tra_Tramites_Solicitados { get; set; }
    } 
}
