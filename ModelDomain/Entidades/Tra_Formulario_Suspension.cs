﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelDomain.Entidades
{
    [Table("Tra_Formulario_Suspension")]
    public class Tra_Formulario_Suspension
    {
        [Key]
        public int Formulario_Suspension_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public  string RPU { get; set; }
        public string Nombre { get; set; }
        public string Domicilio { get; set; }
        public string NoCuenta { get; set; }
        public string Telefono_Notificacion { get; set; }
        public string Domicilio_Notificacion { get; set; }
        public string Motivo_Suspension { get; set; }
    }
}
