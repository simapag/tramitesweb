﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Entidades
{
    /// <summary>
    /// Entidad Roles
    /// </summary>

    [Table("Tra_Roles")]
    public class Roles
    {
        [Key]
        public int Rol_ID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
        public Nullable<DateTime> Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public Nullable<DateTime> Fecha_Modifico { get; set; }


    }
}
