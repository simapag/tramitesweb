﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_RPU_Asociados")]
    public class Tra_RPU_Asociados
    {
        [Key]
        public int RPU_Asociados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public  string RPU { get; set; }
        public string Predio_ID { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
    }
}
