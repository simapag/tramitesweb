﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Pagos_Solicitudes_Detalle")]
   public class Tra_Pagos_Solicitudes_Detalle
    {
        [Key]
        public int Pagos_Solicitudes_Detalle_ID { get; set; }
        public int Pago_Solicitudes_ID { get; set; }
        public string Concepto_ID { get; set; }
        public string Concepto { get; set; }
        public  decimal Cantidad { get; set; }
        public decimal Precio_Unitario { get; set; }
        public decimal Importe { get; set; }
    }
}
