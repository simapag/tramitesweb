﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelDomain.Entidades
{
    [Table("Tra_Formulario_Constancia")]
    public class Tra_Formulario_Constancia
    {
        [Key]
        public int Formulario_Constancia_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string Nombre_Usuario { get; set; }
        public string Domicilio { get; set; }
    }
}
