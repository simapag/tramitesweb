﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    /// <summary>
    /// Entidad Menu
    /// </summary>
    [Table("Tra_Menu")]
    public class Menu
    {


        public  Menu(){

          }

        [Key]
        public int Menu_ID { get; set; }
        public Nullable<int> Parent_ID { get; set; }

        public Menu Menu_Parent { get; set; }
        public string Menu_Descripcion { get; set; }
        public string Accion { get; set; }
        public string Controlador { get; set; }
        public int Orden { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
        public Nullable<DateTime> Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public Nullable<DateTime> Fecha_Modifico { get; set; }

        public virtual ICollection<Menu> Child_Menu { get; set; }

    }
}
