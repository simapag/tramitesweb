﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Tramites_Documentos")]
    public  class Tra_Tramites_Documentos
    {
        [Key]
        public int Tramite_Documento_ID { get; set; }
        public int Tramite_ID { get; set; }
        public int Documento_ID { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }

        public virtual Tra_Tramites Tra_Tramites { get; set; }
        public virtual Tra_Documentos Tra_Documentos { get; set; }
    }
}
