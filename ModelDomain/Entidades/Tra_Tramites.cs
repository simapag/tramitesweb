﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelDomain.Entidades
{
    [Table("Tra_Tramites")]
    public  class Tra_Tramites
    {
        [Key]
        public int Tramite_ID { get; set; }
        public  string Nombre_Tramite { get; set; }
        public decimal Costo_Tramite { get; set; }
        public  string Estatus { get; set; }
        public string Controlador { get; set; }
        public string Nivel_Banco { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }

        public Tra_Tramites() {
            this.Tramites_Documentos = new HashSet<Tra_Tramites_Documentos>();
        }
        public virtual ICollection<Tra_Tramites_Documentos> Tramites_Documentos { get; set; }

      

    }
}
