﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Formulario_Actualizar_Contrato")]
    public class Tra_Formulario_Actualizar_Contrato
    {
        [Key]
        public int Formulario_Actualizar_Contrato_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string Nombre_Actual { get; set; }
        public string Domicilio_Actual { get; set; }
        public string Nombre_Nuevo { get; set; }
    }
}
