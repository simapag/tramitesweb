﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Pago_En_Linea")]
    public class Tra_Pago_En_Linea
    {
        [Key]
        public int Pago_En_Linea_ID { get; set; }
        public int Pago_Solicitudes_ID { get; set; }
        public string Referencia { get; set; }
        public string Correo_Electronico { get; set; }
        public string Telefono { get; set; }
        public string Folio_Pago { get; set; }
        public string Nombre_CuentaHabiente { get; set; }
        public string Numero_Aprobacion_Bancaria { get; set; }
        public string Tipo_Pago { get; set; }
        public decimal Importe { get; set; }
        public string No_Diverso { get; set; }
        public string Tipo_Tarjeta { get; set; }
        public string Banco_Emisor { get; set; }
        public DateTime Fecha { get; set; }
        public string Cadena_Hash_Generada { get; set; }
        public string Cadena_Hash_Regresada { get; set; }

        public Tra_Pagos_Solicitudes Tra_Pagos_Solicitudes { get; set; }
    }
}
