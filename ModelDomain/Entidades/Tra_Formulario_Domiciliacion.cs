﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Formulario_Domiciliacion")]
    public  class Tra_Formulario_Domiciliacion
    {
        [Key]
        public int Formulario_Domiciliacion_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string RFC { get; set; }
        public string Nombre_Proveedor { get; set; }
        public string Periodicidad { get; set; }
        public string Nombre_Banco { get; set; }
        public string Numero_Tarjeta { get; set; }
        public string CLABE { get; set; }
        public string Telefono { get; set; }
        public  decimal Monto_Maximo { get; set; }
        public bool Plazo_Indeterminado { get; set; }
        public  DateTime? Fecha_Vencimiento { get; set; }
    }
}
