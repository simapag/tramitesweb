﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{

    /// <summary>
    /// Entidad Usuario
    /// </summary>

    [Table("Tra_Usuarios")]
    public class Usuarios
    {
        [Key]
        public int Usuarios_ID { get; set; }
        public Nullable<int> Rol_ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido_Paterno { get; set; }
        public string Apellido_Materno { get; set; }
        public string Correo_Electronico { get; set; }
        public string Direccion { get; set; }
        public string Telefono_Celular { get; set; }
        public string Telefono_Oficina { get; set; }
        public string  Password { get; set; }
        public string Estatus { get; set; }
        public string Token { get; set; }
        public string Usuario_Creo { get; set; }
        public Nullable<DateTime> Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public Nullable<DateTime> Fecha_Modifico { get; set; }

        public virtual Roles Roles { get; set; }

    }

}
