﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Pago_En_Linea_Recibo")]
    public class Tra_Pago_En_Linea_Recibo
    {
        [Key]
        public int Pago_En_Linea_Recibo_ID { get; set; }
        public  string No_Factura_Recibo { get; set; }
        public string RPU { get; set; }
        public string Referencia_Bancaria { get; set; }
        public string Correo_Electronico { get; set; }
        public string Telefono { get; set; }
        public string Folio_Pago { get; set; }
        public string Nombre_Cuenta_Habiente { get; set; }
        public string Numero_Apobacion_Bancaria { get; set; }
        public string Tipo_Pago { get; set; }
        public decimal Importe { get; set; }
        public string Tipo_Tarjeta { get; set; }
        public string Banco_Emisor { get; set; }
        public DateTime Fecha { get; set; }
        public string Cadena_Hash_Generada { get; set; }
        public string Cadena_Hash_Regresada { get; set; }
        public string Node_Banco { get; set; }
    }
}
