﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Comprobante_Cfdi")]
    public class Tra_Comprobante_Cfdi
    {

        [Key]
        public int Comprobante_Cfdi_ID { get; set; }
        public string Clave { get; set; }
        public string Descripcion { get; set; }
    }
}
