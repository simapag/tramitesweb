﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Costos_Desazolve")]
    public class Tra_Costos_Desazolve
    {
        [Key]
        public int Desazolve_Costo_ID { get; set; }
        public string Concepto { get; set; }
        public decimal Importe { get; set; }
    }
}
