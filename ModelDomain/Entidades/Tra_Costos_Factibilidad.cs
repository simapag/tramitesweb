﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Costos_Factibilidad")]
    public class Tra_Costos_Factibilidad
    {
        [Key]
        public int Factbilidad_Costo_ID { get; set; }
        public string Concepto { get; set; }
        public decimal Importe { get; set; }
    }
}
