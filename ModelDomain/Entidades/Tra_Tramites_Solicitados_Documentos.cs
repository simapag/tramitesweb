﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Tramites_Solicitados_Documentos")]
    public class Tra_Tramites_Solicitados_Documentos
    {
         [Key]
         public int Tramite_Solicitados_Documento_ID { get; set; }
         public int Tramite_Solicitados_ID { get; set; }
         public int Documento_ID { get; set; }
         public string Nombre_Archivo { get; set; }
         public string Ruta_Archivo { get; set; }
         public string URL_Archivo { get; set; }

        public virtual Tra_Documentos Tra_Documentos { get; set; }
    }
}
