﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Formulario_Factibilidades")]
    public class Tra_Formulario_Factibilidades
    {
        [Key]
        public int Formulario_Factibilidades_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string Nombre { get; set; }
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Codigo_Postal { get; set; }
        public string Correo_Electronico { get; set; }
        public string Telefono { get; set; }
        public string RFC { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
    }
}
