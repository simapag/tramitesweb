﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelDomain.Entidades
{
    [Table("Tra_Pagos_Solicitudes")]
    public class Tra_Pagos_Solicitudes
    {
        [Key]
        public int Pago_Solicitudes_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public  string No_Diverso { get; set; }
        public string Codigo_Barras { get; set; }
        public string Estatus { get; set; }
        public decimal Importe { get; set; }
        public decimal IVA { get; set; }
        public decimal Total { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }

        public virtual Tra_Tramites_Solicitados Tra_Tramites_Solicitados { get; set; }
        public virtual ICollection<Tra_Pagos_Solicitudes_Detalle> Tra_Pagos_Solicitudes_Detalle { get; set; }
    }
}
