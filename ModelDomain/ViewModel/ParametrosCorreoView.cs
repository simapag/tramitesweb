﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class ParametrosCorreoView
    {
        public int Parametro_Correo_ID { get; set; }
        public string Email { get; set; }
        public string Host_Email { get; set; }
        public string Puerto { get; set; }
        public string Password { get; set; }
        public string Usuario_Creo { get; set; }


        public ParametrosCorreoView() {
            Parametro_Correo_ID = 0;
            Email = "";
            Host_Email = "";
            Puerto = "";
            Password = "";
            Usuario_Creo = "";
        }
    }
}
