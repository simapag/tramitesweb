﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class Form_Pago_Multipagos
    {

        public string mp_account { get; set; }
        public int mp_product { get; set; }
        public string mp_order { get; set; }
        public string mp_reference { get; set; }
        public string mp_node { get; set; }
        public string mp_concept { get; set; }
        public decimal mp_amount { get; set; }
        public string mp_customername { get; set; }
        public int mp_currency { get; set; }
        public string mp_signature { get; set; }
        public string mp_urlsuccess { get; set; }
        public string mp_urlfailure { get; set; }

        //Tipo de pago
        public string mp_paymentMethod { get; set; }
        //Fecha y hora
        public string mp_date { get; set; }
        //Número de cuenta/ referencia
        public string mp_pan { get; set; }
        // Autorización del cargo a cuenta
        public string mp_authorization { get; set; }
        //Descripción del código sobre el estado de la transacción
        public string mp_responsemsg { get; set; }
        //Código sobre el estado de la transacción
        public string mp_response { get; set; }
        //Folio de pago
        public string mp_trx_historyid { get; set; }
        //Nombre del cuentahabiente
        public string mp_cardholdername { get; set; }
        // Correo electrónico
        public string mp_email { get; set; }
        //Teléfono
        public string mp_phone { get; set; }

        public string mp_cardType { get; set; }
        public string mp_bankname { get; set; }

        public string origen { get; set; }
        public string mensaje { get; set; }
        public string nombre_tramite { get; set; }
        public string cantidad_cadena { get; set; }

        public Form_Pago_Multipagos() {
            mp_product = 1;
            mp_account = "";
        }
    }
}
