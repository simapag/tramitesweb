﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class SimpleComboView
    {
        public string name { get; set; }

        public SimpleComboView()
        {
            this.name = "";
        }
    }
}
