﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class ComboView
    {
        public int id { get; set; }
        public string name { get; set; }
        public string clave { get; set; }

        public ComboView()
        {
            id = 0;
            name = "";
            clave = "";
        }
    }
}
