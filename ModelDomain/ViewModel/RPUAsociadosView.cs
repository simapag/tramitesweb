﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class RPUAsociadosView
    {
        public int RPU_Asociados_ID { get; set; }
        public  int Usuarios_ID { get; set; }
        public  string RPU { get; set; }
        public  string Predio_ID { get; set; }
        public string Usuario_Creo { get; set; }
    }
}
