﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
  public  class FormularioDatosFiscalesView
    {
        public int Formulario_Datos_Fiscales_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public int Comprobante_Cfdi_ID { get; set; }
        public string RPU { get; set; }
        public string Razon_Social { get; set; }
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string Numero_Exterior { get; set; }
        public string Numero_Interior { get; set; }
        public string Codigo_Postal { get; set; }
        public string Correo_Electronico { get; set; }
        public string RFC { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }

        public FormularioDatosFiscalesView() {
            Formulario_Datos_Fiscales_ID = 0;
            Tramite_Solicitados_ID = 0;
            Comprobante_Cfdi_ID = 0;
        }

    }
}
