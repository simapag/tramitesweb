﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class FormActualizarContratoView
    {
        public int Formulario_Actualizar_Contrato_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string Nombre_Actual { get; set; }
        public string Domicilio_Actual { get; set; }
        public string Nombre_Nuevo { get; set; }
    }
}
