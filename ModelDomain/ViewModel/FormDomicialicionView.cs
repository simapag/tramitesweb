﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class FormDomicialicionView
    {
        public int Formulario_Domiciliacion_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string RFC { get; set; }
        public string Nombre_Proveedor { get; set; }
        public string Periodicidad { get; set; }
        public string Nombre_Banco { get; set; }
        public string Numero_Tarjeta { get; set; }
        public string CLABE { get; set; }
        public string Telefono { get; set; }
        public decimal Monto_Maximo { get; set; }
        public bool Plazo_Indeterminado { get; set; }
        public string Fecha_Vencimiento { get; set; }

    }
}
