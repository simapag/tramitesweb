﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class FormCalificarView
    {
        public string returnUrl { get; set; }
        public int Tramite_Solicitados_ID { get; set; }

        public string Resultado { get; set; }
        [Required(ErrorMessage = "Se requiere una especialidad")]
        [StringLength(200, ErrorMessage = "La longitud del mensaje es incorrecto", MinimumLength = 2)]
        public string Mensaje { get; set; }
        [Required(ErrorMessage = "Se requiere un estatus")]
        public string Estatus { get; set; }

        public FormCalificarView()
        {
            this.returnUrl = "";
            this.Tramite_Solicitados_ID = 0;
            this.Mensaje = "";
            this.Estatus = "";
        }
    }
}
