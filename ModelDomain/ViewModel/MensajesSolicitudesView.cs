﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class MensajesSolicitudesView
    {
        public int Mensajes_Solicitudes_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public string Mensaje { get; set; }
        public string Entidad { get; set; }
        public string Estatus_Solicitud { get; set; }
        public string Nombre_Tramite { get; set; }
        public bool Visto { get; set; }
        public string Usuario_Creo { get; set; }
        [System.ComponentModel.DataAnnotations.DisplayFormat(DataFormatString = "{0: MM/dd/yyyy hh:mm}")]
        public DateTime Fecha { get; set; }


        public MensajesSolicitudesView() {
            this.Mensajes_Solicitudes_ID = 0;
            this.Tramite_Solicitados_ID = 0;
            this.Usuarios_ID = 0;
            this.Mensaje = "";
            this.Entidad = "";
            this.Estatus_Solicitud = "";
            this.Nombre_Tramite = "";
            this.Visto = false;
            this.Usuario_Creo = "";
            this.Fecha = DateTime.Now;
        }
    }
}
