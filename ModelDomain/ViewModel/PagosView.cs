﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class PagosView
    {
      public  List<Pagos_Solicitudes_View> Lista_Pagos { get; set; }
      public  List<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalle { get; set; }
      public Form_Pago_Multipagos Form_Pago_Multipagos { get; set; } 

        public PagosView() {
            Lista_Pagos = new List<Pagos_Solicitudes_View>();
            Lista_Pagos_Detalle = new List<Pagos_Solicitudes_Detalle_View>();
            Form_Pago_Multipagos = new Form_Pago_Multipagos();
        }

    }
}
