﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
  public  class FormularioConstanciaView
    {

        public int Formulario_Constancia_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string Nombre_Usuario { get; set; }
        public string Domicilio { get; set; }
    }
}
