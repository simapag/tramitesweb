﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
    public class TramitesView
    {
        public int Tramite_ID { get; set; }
        public string Nombre_Tramite { get; set; }
        public decimal Costo_Tramite { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
        public string Controlador { get; set; }
        public string Nivel_Banco { get; set; }
        public List<ComboView> Lista_Documentos { get; set; }

        public TramitesView() {
            Lista_Documentos = new List<ComboView>();
        }

       
    }
}
