﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class DocumentosView
    {
        public int Documento_ID { get; set; }
        public string Nombre_Documento { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
    }
}
