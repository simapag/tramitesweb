﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
  public  class App_ConfiguracionView
    {
        public string Servicio_PE_Moviles { get; set; }
        public int Servicio_PE_Meses_Adeudo { get; set; }

        public App_ConfiguracionView()
        {
            Servicio_PE_Moviles = "";
            Servicio_PE_Meses_Adeudo = 0;
        }
    }
}
