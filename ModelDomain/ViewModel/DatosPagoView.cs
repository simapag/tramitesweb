﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class DatosPagoView
    {

         public decimal Convenio_Adeudo { get; set; }
         public decimal Sancion_Adeudo { get; set; }
         public decimal Diverso_Adeudo { get; set; }
         public int Meses_Adeudo { get; set; }
         public decimal Adeudo { get; set; }
         public decimal Saldo_Favor { get; set; }
         public string Fecha_Limite_Pago { get; set; }
         public string Mes_Pago { get; set; }
         public decimal Total { get; set; }
         public decimal Total_Mes { get; set; }
         public decimal TotalConvenio { get; set; }
         public decimal TotalDescuentoEmpleado { get; set; }


        public DatosPagoView()
        {
            Convenio_Adeudo = 0;
            Sancion_Adeudo = 0;
            Diverso_Adeudo = 0;
            Meses_Adeudo = 0;
            Adeudo = 0;
            Fecha_Limite_Pago = "";
            Mes_Pago = "";
            Total = 0;
            Saldo_Favor = 0;
            TotalDescuentoEmpleado = 0;
        }

    }
}
