﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
    public class TramitesSolicitudView
    {
        public int Tramite_Solicitados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public int Usuarios_Asignado_ID { get; set; }
        public int Tramite_ID { get; set; }
        public decimal Costo_Tramite { get; set; }
        public string Estatus { get; set; }
        public string Estatus_Revision { get; set; }
        public string RPU { get; set; }
        public string Observaciones { get; set; }
        public bool RPU_Requerido { get; set; }
        public string Usuario_Creo { get; set; } 
        public string Nombre_Tramite { get; set; }
        public string Referencia_Pago { get; set; }
        public string Controlador { get; set; }
        public string Nivel_Banco { get; set; }
        public string Str_Fecha_Solicitud { get; set; }
        [System.ComponentModel.DataAnnotations.DisplayFormat(DataFormatString = "{0: dd/MM/yyyy}")]
        public DateTime Fecha_Solicitud { get; set; }
        public bool Seleccionado { get; set; }
        public string Usuario { get; set; }
        public string Telefono { get; set; }
        public int NumeroMensajes { get; set; }
        public int NumeroPago { get; set; }
        public string Usuario_Asignado { get; set; }
        public string Correo_Electronico { get; set; }

        public TramitesSolicitudView()
        {
            this.Tramite_Solicitados_ID = 0;
            this.Usuarios_ID = 0;
            this.Usuarios_Asignado_ID = 0;
            this.Tramite_ID = 0;
            this.Costo_Tramite = 0;
            this.Estatus = "";
            this.Estatus_Revision = "";
            this.RPU = "";
            this.Observaciones = "";
            this.RPU_Requerido = true;
            this.Nombre_Tramite = "";
            this.Controlador = "";
            this.Fecha_Solicitud = DateTime.Now;
            this.Seleccionado = false;
            this.Usuario_Creo = "";
            this.Usuario = "";
            this.Telefono = "";
            this.NumeroMensajes = 0;
            this.NumeroPago = 0;
            this.Nivel_Banco = "";
            this.Usuario_Asignado = "";
            this.Correo_Electronico = "";
        }
    }
}
