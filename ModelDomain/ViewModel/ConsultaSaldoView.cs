﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class ConsultaSaldoView
    {

        public int no_movimento { get; set; }
        public string concepto_id { get; set; }
        public string concepto { get; set; }
        public decimal importe { get; set; }
        public decimal impuesto { get; set; }
        public decimal total { get; set; }
        public decimal importe_abonado { get; set; }
        public decimal impuesto_abonado { get; set; }
        public decimal total_abonado { get; set; }
        public decimal  importe_saldo { get; set; }
        public decimal impuesto_saldo { get; set; }
        public decimal total_saldo { get; set; }
        public int anio { get; set; }
        public int periodo { get; set; }
        public string estatus { get; set; }
        public decimal importe_total { get; set; }
    }
}
