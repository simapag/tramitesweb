﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class PagosViewAsignacion
    {
        public  int Tramite_Solicitados_ID { get; set; }
        public int Pago_Solicitudes_ID { get; set; }
        public int  Tramite_ID { get; set; }
        public string returnUrl { get; set; }
        public string No_Diverso { get; set; }
        public string queryString { get; set; }
        public string mensaje { get; set; }
        public List<Pagos_Solicitudes_View> Lista_Pagos { get; set; }
        public List<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalle { get; set; }

        public PagosViewAsignacion()
        {
            Lista_Pagos = new List<Pagos_Solicitudes_View>();
            Lista_Pagos_Detalle = new List<Pagos_Solicitudes_Detalle_View>();
            Tramite_ID = 0;
            Tramite_Solicitados_ID = 0;
            queryString = "";
            mensaje = "";
            Pago_Solicitudes_ID = 0;
        }
    }
}
