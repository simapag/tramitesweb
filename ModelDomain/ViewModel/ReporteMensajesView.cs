﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
    public class ReporteMensajesView
    {
        [System.ComponentModel.DataAnnotations.DisplayFormat(DataFormatString = "{0: dd/MM/yyyy HH:mm}")]
        public DateTime Fecha_Creo { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string Nombre_Tramite { get; set; }
        public string RPU { get; set; }
        public string Estatus_Solicitud { get; set; }
        public string Usuario_Creo { get; set; }
        public string Mensaje { get; set; }
    }
}
