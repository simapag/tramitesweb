﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class Tramites_Documento_View
    {
        public int Tramite_Documento_ID { get; set; }
        public int Tramite_ID { get; set; }
        public int Documento_ID { get; set; }
        public string Nombre_Tramite { get; set; }
        public string Nombre_Documento { get; set; }
        public string Usuario_Creo { get; set; }
    }
}
