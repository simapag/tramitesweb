﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
    public class PagosLineaView
    {


        public decimal Total { get; set; }
        public List<ConsultaSaldoView> Lista_Saldo { get; set; }
        public DatosCuentaView Datos_Cuenta { get; set; }
        public App_ConfiguracionView ConfiguracionView { get; set; }
        public Form_Pago_Multipagos Form_Multipagos { get; set; }
        public DatosPagoView Datos_Pago { get; set; }
        

        public PagosLineaView()
        {
            
            Total = 0;
            Lista_Saldo = new List<ConsultaSaldoView>();
            Datos_Pago = new DatosPagoView();
            Datos_Cuenta = new DatosCuentaView();
            ConfiguracionView = new App_ConfiguracionView();
            Form_Multipagos = new Form_Pago_Multipagos();
           
        }

    }
}
