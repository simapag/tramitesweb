﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class Pagos_Solicitudes_View
    {

        public int Pago_Solicitudes_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public string No_Diverso { get; set; }
        public string Codigo_Barras { get; set; }
        public string Estatus { get; set; }
        public decimal Importe { get; set; }
        public decimal IVA { get; set; }
        public decimal Total { get; set; }
        public string Usuario_Creo { get; set; }
        public string Fecha_Creo { get; set; }
        public DateTime DatFecha_Creo { get; set; }
        public string RPU { get; set; }

      
        
        public Pagos_Solicitudes_View()
        {
            this.Pago_Solicitudes_ID = 0;
            this.Tramite_Solicitados_ID = 0;
            this.Usuarios_ID = 0;
            this.No_Diverso = "";
            this.Codigo_Barras = "";
            this.Estatus = "";
            this.Importe = 0;
            this.IVA = 0;
            this.Total = 0;
            this.Usuario_Creo = "";
            this.RPU = "";
            this.Fecha_Creo = "";
            this.DatFecha_Creo = DateTime.Now;
    
        }
    }
}
