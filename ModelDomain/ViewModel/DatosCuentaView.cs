﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class DatosCuentaView
    {

        public string RPU { get; set; }
        public string Nombre_Usuario { get; set; }
        public string Predio_ID { get; set; }
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string Numero_Exterior { get; set; }
        public string Numero_Interior { get; set; }
        public string No_Cuenta { get; set; }
        public string Descuento_Empleado { get; set; }
        public string Tiene_Beneficio_JPA { get; set; }

        public DatosCuentaView()
        {
            RPU = "";
            Nombre_Usuario = "";
            Predio_ID = "";
            Colonia = "";
            Calle = "";
            Numero_Exterior = "";
            Numero_Interior = "";
            No_Cuenta = "";
        }
    }
}
