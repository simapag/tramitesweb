﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class Pago_En_Linea_No_Aplicado_View
    {

        public int Pagos_No_Aplicados_ID { get; set; }
        public string Folio_Pago { get; set; }
        public string RPU { get; set; }
        public string No_Factura { get; set; }
        public string Referencia_Bancaria { get; set; }
        public string Tipo_Pago { get; set; }
        public string Tipo_Tarjeta { get; set; }
        public decimal Importe { get; set; }
        public string Estatus { get; set; }
        public DateTime Fecha_Creo { get; set; }
    }
}
