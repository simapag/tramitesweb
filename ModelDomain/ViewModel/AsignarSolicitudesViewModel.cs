﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class AsignarSolicitudesViewModel
    {
        public List<TramitesSolicitudView> Lista_Solicitudes { get; set; }
        public List<UsuarioView> Lista_Ejecutivos { get; set; }
    }
}
