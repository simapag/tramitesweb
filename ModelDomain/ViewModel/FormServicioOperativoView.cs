﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class FormServicioOperativoView
    {

        public int Formulario_Servicio_Operativo_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string NoCuenta { get; set; }
        public string Nombre_Usuario { get; set; }
        public string Domicilio { get; set; }
    }
}
