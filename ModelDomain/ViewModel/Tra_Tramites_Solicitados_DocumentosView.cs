﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
  public  class Tra_Tramites_Solicitados_DocumentosView
    {

        public int Tramite_Solicitados_Documento_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public int Documento_ID { get; set; }
        public string Nombre_Archivo { get; set; }
        public string Ruta_Archivo { get; set; }
        public string Nombre_Documento { get; set; }
        public string URL_Archivo { get; set; }

        public Tra_Tramites_Solicitados_DocumentosView() {
            this.Tramite_Solicitados_Documento_ID = 0;
            this.Tramite_Solicitados_ID = 0;
            this.Documento_ID = 0;
            this.Nombre_Archivo = "";
            this.Ruta_Archivo = "";
            this.Nombre_Documento = "";
            this.URL_Archivo = "";
        }
    }
}
