﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class FormularioCorreccionDomicilioView
    {
        public int Formulario_Correccion_Domicilio_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string RPU { get; set; }
        public string Domicilio_Anterior { get; set; }
        public string Domicilio_Nuevo { get; set; }


        public FormularioCorreccionDomicilioView() {
            Tramite_Solicitados_ID = 0;
            Formulario_Correccion_Domicilio_ID = 0;
            RPU = "";
            Domicilio_Anterior = "";
            Domicilio_Nuevo = "";
        }
    }
}
