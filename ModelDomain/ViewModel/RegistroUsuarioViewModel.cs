﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class RegistroUsuarioViewModel
    {
        public string Nombre { get; set; }
        public string Apellido_Paterno { get; set; }
        public string Apellido_Materno { get; set; }
        public string Correo_Electronico { get; set; }
        public string Telefono_Celular { get; set; }
        public string Password { get; set; }
    
    }
}
