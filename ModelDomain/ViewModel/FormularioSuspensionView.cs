﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class FormularioSuspensionView
    {
        public int Formulario_Suspension_ID { get; set; }
        public int Tramite_Solicitados_ID { get; set; }
        public string Nombre { get; set; }
        public string Domicilio { get; set; }
        public string RPU { get; set; }
        public string NoCuenta { get; set; }
        public string Telefono_Notificacion { get; set; }
        public string Domicilio_Notificacion { get; set; }
        public string Motivo_Suspension { get; set; }

        public FormularioSuspensionView()
        {
            Formulario_Suspension_ID = 0;
            Tramite_Solicitados_ID = 0;
            Nombre = "";
            Domicilio = "";
            RPU = "";
            NoCuenta = "";
            Telefono_Notificacion = "";
            Domicilio_Notificacion = "";
            Motivo_Suspension = "";
        }
    }
}
