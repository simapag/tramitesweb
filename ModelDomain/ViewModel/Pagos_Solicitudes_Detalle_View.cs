﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class Pagos_Solicitudes_Detalle_View
    {

        public int Pagos_Solicitudes_Detalle_ID { get; set; }
        public int Pago_Solicitudes_ID { get; set; }
        public string Concepto_ID { get; set; }
        public string Concepto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio_Unitario { get; set; }
        public decimal Importe { get; set; }

        public Pagos_Solicitudes_Detalle_View()
        {
            this.Pagos_Solicitudes_Detalle_ID = 0;
            this.Pago_Solicitudes_ID = 0;
            this.Concepto_ID = "";
            this.Concepto = "";
            this.Cantidad = 0;
            this.Precio_Unitario = 0;
            this.Importe = 0;
        }
    }
}
