﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.ViewModel
{
   public class UsuarioView
    {
        public int Usuarios_ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido_Paterno { get; set; }
        public string Apellido_Materno { get; set; }
        public string Correo_Electronico { get; set; }
        public string Password { get; set; }


        public UsuarioView() {
            this.Usuarios_ID = 0;
            this.Nombre = "";
            this.Apellido_Paterno = "";
            this.Apellido_Materno = "";
            this.Correo_Electronico = "";
        }
    }
}
