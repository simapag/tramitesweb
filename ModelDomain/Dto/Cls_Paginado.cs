﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Dto
{
   public class Cls_Paginado<T>
    {
        public int total { get; set; }
        public List<T> rows { get; set; }

        public Cls_Paginado(List<T> rows, int total)
        {
            this.total = total;
            this.rows = rows;
        }
    }
}
