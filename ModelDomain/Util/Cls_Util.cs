﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Util
{
   public class Cls_Util
    {
        public static DateTime ConvertirCadenaFecha(string strFecha)
        {
            DateTime date;

            date = DateTime.ParseExact(strFecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            return date;
        }


        public static decimal redondearTotalPagar(decimal docantidad_pagar, Boolean Se_Tiene_que_Redondear)
        {
            decimal respuesta = 0;
            decimal resta = 0;
            int incantidad_pagar = (int)docantidad_pagar;


            if (Se_Tiene_que_Redondear)
            {

                resta = 1 - (docantidad_pagar - incantidad_pagar);
                if (resta <= Convert.ToDecimal(0.49))
                {
                    respuesta = docantidad_pagar + resta;
                }
                else
                {
                    if (resta == 1)
                    {
                        respuesta = docantidad_pagar;
                    }
                    else
                    {
                        respuesta = incantidad_pagar;
                    }
                }
            }
            else
            {
                respuesta = docantidad_pagar;
            }

            //cantida_pagar_redondeada = respuesta;
           // ajuste_para_redondeo = resta;

            return respuesta;
        }


    }
}
