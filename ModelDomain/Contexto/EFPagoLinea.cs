﻿using ModelDomain.Interfaces;
using ModelDomain.Util;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Contexto
{
    public class EFPagoLinea : IPagoLinea
    {

        private EFDbContextSIMAPAG ContextoSimapag = new EFDbContextSIMAPAG();
        private string Str_Sql = "";



     

        public List<App_ConfiguracionView> Obtener_Configuracion()
        {
            Str_Sql = " SELECT Servicio_PE_Moviles, Servicio_PE_Meses_Adeudo FROM APP_Configuracion ";

            return ContextoSimapag.Database
                .SqlQuery<App_ConfiguracionView>(Str_Sql)
                .ToList();
        }

        public DatosPagoView Obtener_Datos_Pagos(string No_Factura_Recibo)
        {
            DataTable dt = null;
            SqlDataReader dr;
            DatosPagoView datosPagosView = new DatosPagoView();


            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();
                using (SqlCommand comando = conexion.CreateCommand())
                {
                    comando.CommandText = "sp_recibito_consulta_usuario_nuevo";
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    SqlParameter paraNofacturaRecibo = new SqlParameter("@No_Factura_Recibo", SqlDbType.Char, 10);
                    paraNofacturaRecibo.Value = No_Factura_Recibo;
                    comando.Parameters.Add(paraNofacturaRecibo);
                    comando.CommandTimeout = 200;

                    dr = comando.ExecuteReader();
                    dt = new DataTable();
                    dt.Load(dr);

                }

            }

            if(dt != null)
            {
                if(dt.Rows.Count > 0)
                {
                    datosPagosView.Adeudo = Cls_Util.redondearTotalPagar( Convert.ToDecimal(dt.Rows[0]["Adeudo"]),true);
                    datosPagosView.Convenio_Adeudo = Convert.ToDecimal(dt.Rows[0]["Convenio_Adeudo"]);
                    datosPagosView.Diverso_Adeudo = Convert.ToDecimal(dt.Rows[0]["Diverso_Adeudo"]);
                    datosPagosView.Meses_Adeudo = Convert.ToInt16(dt.Rows[0]["Meses_Adeudo"]);
                    datosPagosView.Mes_Pago = Convert.ToString(dt.Rows[0]["Mes_Facturado"]);
                    datosPagosView.Fecha_Limite_Pago = Convert.ToString(dt.Rows[0]["Fecha_Limite_Pago_Arriba"]);
                    datosPagosView.Total = Cls_Util.redondearTotalPagar( Convert.ToDecimal(dt.Rows[0]["Total_Pagar_Abajo"]),true);
                    datosPagosView.Total_Mes =Cls_Util.redondearTotalPagar( Convert.ToDecimal(dt.Rows[0]["Total_Mes"]),true);
                    datosPagosView.Saldo_Favor = Cls_Util.redondearTotalPagar( Convert.ToDecimal(dt.Rows[0]["Saldo_Favor"]),true);
                }
            }


                return datosPagosView;
        }

        public string Obtener_NoFactura_Actual(string RPU)
        {
            string factura = "";


            using (SqlConnection conexion = new  SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {

                conexion.Open();
                using (SqlCommand comando = conexion.CreateCommand())
                {
                    comando.CommandText = "SELECT top 1 No_Factura_Recibo FROM Ope_Cor_Facturacion_Recibos WHERE RPU=@rpu ORDER BY Anio DESC, Bimestre DESC";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                    factura = comando.ExecuteScalar().ToString();
                }


            }


            return factura;
        }

        public Form_Pago_Multipagos Obtener_Parametros_Pago(decimal Total_Pagar, string No_Factura_Recibo, string RPU, string Nombre_Usuario)
        {
            Form_Pago_Multipagos form_Pago_Multipagos = new Form_Pago_Multipagos();

            var encoding = new System.Text.ASCIIEncoding();

            string llave = "0Bd59d959a54Ae1d503e2d051e04c9dB26bc3659aed013827132ea796fb35$D5f";
            string cadena_proteger = "";
            string cadena_hash;
            string referencia = RPU;
            cadena_proteger = No_Factura_Recibo + referencia + Total_Pagar.ToString();

            byte[] keyByte = encoding.GetBytes(llave);
            byte[] messageBytes = encoding.GetBytes(cadena_proteger);

            HMACSHA256 hMACSHA256 = new HMACSHA256(keyByte);
         
            byte[] hashmessage1 = hMACSHA256.ComputeHash(messageBytes);
            cadena_hash = ByteToString(hashmessage1);

            form_Pago_Multipagos.mp_order = No_Factura_Recibo;
            form_Pago_Multipagos.mp_reference = referencia;

            form_Pago_Multipagos.mp_node = "1";
            form_Pago_Multipagos.mp_concept = "1";
            form_Pago_Multipagos.mp_amount = Total_Pagar;
            form_Pago_Multipagos.mp_customername = Nombre_Usuario;
            form_Pago_Multipagos.mp_currency = 1;
            form_Pago_Multipagos.mp_signature = cadena_hash.ToLower();
            form_Pago_Multipagos.mp_account = Cls_Conexion.Mp_Account;
            form_Pago_Multipagos.mp_urlfailure = Cls_Conexion.Url_Error_Recibo;
            form_Pago_Multipagos.mp_urlsuccess = Cls_Conexion.Url_Exitoso_Recibo;


            return form_Pago_Multipagos;
        }


        private string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }

        public string Obtener_Predio_ID(string RPU)
        {
            string predio = "";

            

                using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
                {
                conexion.Open();
                    using (var comando = conexion.CreateCommand())
                    {
                        comando.CommandText = "SELECT Predio_ID FROM Cat_Cor_Predios  WHERE RPU=@rpu";
                        comando.CommandType = System.Data.CommandType.Text;
                        comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                        predio = comando.ExecuteScalar().ToString();
                    }
                }
            

    

         

            return predio;
        }

        public List<ConsultaSaldoView> Obtener_Saldo_Consulta(string RPU)
        {
            return ContextoSimapag.Database
                 .SqlQuery<ConsultaSaldoView>("obtenerFacturasNormalesDetallesAgrupado2021 @rpu", new SqlParameter("@rpu", RPU))
                 .ToList();
        }

        public decimal Obtener_Total_Pagar(string Predio_ID)
        {
            decimal respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    comando.CommandText = "SELECT dbo.fn_obtenerFacturaTotalPagar(@predio_id)";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@predio_id", Predio_ID));
                    respuesta = Convert.ToDecimal(comando.ExecuteScalar());

                }
            }

                return respuesta;
        }

        public decimal Obtener_Saldo_Convenio_Total(string RPU)
        {
            decimal respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    comando.CommandText = "SELECT dbo.fn_obtenerConvenioTodo(@rpu)";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                    respuesta = Convert.ToDecimal(comando.ExecuteScalar());

                }
            }

            return respuesta;
        }

        public decimal Obtener_Total_Pagar_Empleado(string RPU)
        {
            decimal respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    comando.CommandText = "SELECT dbo.fn_obtener_saldo_pagar_empleado(@rpu )";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                    respuesta = Convert.ToDecimal(comando.ExecuteScalar());

                }
            }

            return respuesta;
        }

        public decimal Obtener_Total_Descuento_Empleado(string RPU)
        {
            decimal respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    comando.CommandText = "SELECT dbo.fn_obtener_suma_descuento_empleado(@rpu )";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                    respuesta = Convert.ToDecimal(comando.ExecuteScalar());

                }
            }

            return respuesta;
        }

        public decimal Obtener_total_Pagar_Tiene_Beneficio(string RPU)
        {
            decimal respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    comando.CommandText = "SELECT dbo.fn_obtener_saldo_pagar_jpa(@rpu )";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                    respuesta = Convert.ToDecimal(comando.ExecuteScalar());

                }
            }

            return respuesta;
        }

        public decimal Obtener_Total_Descuento_Beneficio(string RPU)
        {
            decimal respuesta = 0;

            using (SqlConnection conexion = new SqlConnection(Cls_Conexion.Cadena_Conexion_Siac))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {

                    comando.CommandText = "SELECT dbo.fn_obtener_suma_descuento_jpa(@rpu )";
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.Parameters.Add(new SqlParameter("@rpu", RPU));
                    respuesta = Convert.ToDecimal(comando.ExecuteScalar());

                }
            }

            return respuesta;
        }
    }
}
