﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Contexto
{
    public class EFConnectionAccessor : IDisposable
    {
        private readonly SqlConnection sqlConnection;
        private readonly EFDbContextSIMAPAG entities;

        public EFConnectionAccessor()
        {
            entities = new EFDbContextSIMAPAG();

            var entityConnection = entities.Database.Connection;

            if (entityConnection != null)
            {
                sqlConnection = entityConnection as SqlConnection;
            }
        }


        public SqlConnection connection
        {
            get
            {
                sqlConnection.Open();
                return sqlConnection;
            }
        }

        public void Dispose()
        {
            sqlConnection.Close();
            sqlConnection.Dispose();
            entities.Dispose();
        }
    }
}
