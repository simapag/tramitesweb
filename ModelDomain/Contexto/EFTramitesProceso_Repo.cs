﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.Util;
using ModelDomain.ViewModel;


namespace ModelDomain.Contexto

{
   public class EFTramitesProceso_Repo : ITramitesProceso_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public void Editar_Actualizacion_Titular(EFDbContext mContexto, FormActualizarContratoView formActualizar)
        {
            Tra_Formulario_Actualizar_Contrato tra_Formulario_Actualizar_Contrato = mContexto.Tra_Formulario_Actualizar_Contrato.Find(formActualizar.Formulario_Actualizar_Contrato_ID);
            tra_Formulario_Actualizar_Contrato.Domicilio_Actual = formActualizar.Domicilio_Actual;
            tra_Formulario_Actualizar_Contrato.Nombre_Actual = formActualizar.Nombre_Actual;
            tra_Formulario_Actualizar_Contrato.RPU = formActualizar.RPU;
            tra_Formulario_Actualizar_Contrato.Nombre_Nuevo = formActualizar.Nombre_Nuevo;
            mContexto.SaveChanges();
        }

        public void Editar_Correccion_Domicilio(EFDbContext mContenxto, FormularioCorreccionDomicilioView formularioCorreccion)
        {
            Tra_Formulario_Correccion_Domicilio formulario_Correccion_Domicilio = mContenxto.Tra_Formulario_Correccion_Domicilio.Find(formularioCorreccion.Formulario_Correccion_Domicilio_ID);
            formulario_Correccion_Domicilio.Domicilio_Anterior = formularioCorreccion.Domicilio_Anterior;
            formulario_Correccion_Domicilio.Domicilio_Nuevo = formularioCorreccion.Domicilio_Nuevo;
            formulario_Correccion_Domicilio.RPU = formularioCorreccion.RPU;
            mContenxto.SaveChanges();


        }

        public void Editar_Datos_Cancelar_Servicio(EFDbContext mContexto, FormCancelarServicioView formCancelarServicioView)
        {
            Tra_Formulario_Cancelar_Servicio tra_Formulario_Cancelar_Servicio = mContexto.Tra_Formulario_Cancelar_Servicio.Find(formCancelarServicioView.Formulario_Cancelar_Servicio_ID);
            tra_Formulario_Cancelar_Servicio.RPU = formCancelarServicioView.RPU;
            tra_Formulario_Cancelar_Servicio.NoCuenta = formCancelarServicioView.NoCuenta;
            tra_Formulario_Cancelar_Servicio.Nombre_Usuario = formCancelarServicioView.Nombre_Usuario;
            tra_Formulario_Cancelar_Servicio.Domicilio = formCancelarServicioView.Domicilio;
            mContexto.SaveChanges();
        }

        public void Editar_Datos_Constancia(EFDbContext mContexto, FormularioConstanciaView formularioConstanciaView)
        {
            Tra_Formulario_Constancia tra_Formulario_Constancia = mContexto.Tra_Formulario_Constancia.Find(formularioConstanciaView.Formulario_Constancia_ID);
            tra_Formulario_Constancia.RPU = formularioConstanciaView.RPU;
            tra_Formulario_Constancia.Nombre_Usuario = formularioConstanciaView.Nombre_Usuario;
            tra_Formulario_Constancia.Domicilio = formularioConstanciaView.Domicilio;
            mContexto.SaveChanges();
        }

        public void Editar_Datos_Domiciliacion(EFDbContext mContexto, FormDomicialicionView formDomicialicionView)
        {
            Tra_Formulario_Domiciliacion tra_Formulario_Domiciliacion = mContexto.Tra_Formulario_Domiciliacion.Find(formDomicialicionView.Formulario_Domiciliacion_ID);
            tra_Formulario_Domiciliacion.CLABE = formDomicialicionView.CLABE;
            tra_Formulario_Domiciliacion.Numero_Tarjeta = formDomicialicionView.Numero_Tarjeta;
            tra_Formulario_Domiciliacion.Nombre_Banco = formDomicialicionView.Nombre_Banco;
            tra_Formulario_Domiciliacion.Monto_Maximo = formDomicialicionView.Monto_Maximo;
            tra_Formulario_Domiciliacion.Nombre_Proveedor = formDomicialicionView.Nombre_Proveedor;
            tra_Formulario_Domiciliacion.Periodicidad = formDomicialicionView.Periodicidad;
            tra_Formulario_Domiciliacion.RFC = formDomicialicionView.RFC;
            tra_Formulario_Domiciliacion.RPU = formDomicialicionView.RPU;
            tra_Formulario_Domiciliacion.Telefono = formDomicialicionView.Telefono;
            tra_Formulario_Domiciliacion.Plazo_Indeterminado = formDomicialicionView.Plazo_Indeterminado;

            if (formDomicialicionView.Plazo_Indeterminado)
                tra_Formulario_Domiciliacion.Fecha_Vencimiento = null;
            else
                tra_Formulario_Domiciliacion.Fecha_Vencimiento = Cls_Util.ConvertirCadenaFecha(formDomicialicionView.Fecha_Vencimiento);
            mContexto.SaveChanges();
        }

        public void Editar_Datos_Factibilidad(EFDbContext mContexto, Formulario_Factibilidades_View formulario_Factibilidades_View)
        {
            Tra_Formulario_Factibilidades tra_Formulario_Factibilidades = mContexto.Tra_Formulario_Factibilidades.Find(formulario_Factibilidades_View.Formulario_Factibilidades_ID);
            tra_Formulario_Factibilidades.Calle = formulario_Factibilidades_View.Calle;
            tra_Formulario_Factibilidades.Colonia = formulario_Factibilidades_View.Colonia;
            tra_Formulario_Factibilidades.Ciudad = formulario_Factibilidades_View.Ciudad;
            tra_Formulario_Factibilidades.Codigo_Postal = formulario_Factibilidades_View.Codigo_Postal;
            tra_Formulario_Factibilidades.Correo_Electronico = formulario_Factibilidades_View.Correo_Electronico;
            tra_Formulario_Factibilidades.Nombre = formulario_Factibilidades_View.Nombre;
            tra_Formulario_Factibilidades.Numero = formulario_Factibilidades_View.Numero;
            tra_Formulario_Factibilidades.Telefono = formulario_Factibilidades_View.Telefono;
            tra_Formulario_Factibilidades.Ciudad = formulario_Factibilidades_View.Ciudad;
            tra_Formulario_Factibilidades.Estado = formulario_Factibilidades_View.Estado;
            tra_Formulario_Factibilidades.RFC = formulario_Factibilidades_View.RFC;
            mContexto.SaveChanges();
        }

        public void Editar_Datos_Fiscales(EFDbContext mContexto, FormularioDatosFiscalesView formularioDatosFiscalesView)
        {
            Tra_Formulario_Datos_Fiscales tra_Formulario_Datos_Fiscales = mContexto.Tra_Formulario_Datos_Fiscales.Find(formularioDatosFiscalesView.Formulario_Datos_Fiscales_ID);
            tra_Formulario_Datos_Fiscales.Numero_Exterior = formularioDatosFiscalesView.Numero_Exterior;
            tra_Formulario_Datos_Fiscales.Numero_Interior = formularioDatosFiscalesView.Numero_Interior;
            tra_Formulario_Datos_Fiscales.Razon_Social = formularioDatosFiscalesView.Razon_Social.ToUpper();
            tra_Formulario_Datos_Fiscales.RFC = formularioDatosFiscalesView.RFC.ToUpper();
            tra_Formulario_Datos_Fiscales.RPU = formularioDatosFiscalesView.RPU;
            tra_Formulario_Datos_Fiscales.Tramite_Solicitados_ID = formularioDatosFiscalesView.Tramite_Solicitados_ID;
            tra_Formulario_Datos_Fiscales.Calle = formularioDatosFiscalesView.Calle.ToUpper();
            tra_Formulario_Datos_Fiscales.Colonia = formularioDatosFiscalesView.Colonia.ToUpper();
            tra_Formulario_Datos_Fiscales.Comprobante_Cfdi_ID = formularioDatosFiscalesView.Comprobante_Cfdi_ID;
            tra_Formulario_Datos_Fiscales.Codigo_Postal = formularioDatosFiscalesView.Codigo_Postal;
            tra_Formulario_Datos_Fiscales.Correo_Electronico = formularioDatosFiscalesView.Correo_Electronico;
            tra_Formulario_Datos_Fiscales.Estado = formularioDatosFiscalesView.Estado.ToUpper();
            tra_Formulario_Datos_Fiscales.Ciudad = formularioDatosFiscalesView.Ciudad.ToUpper();

            mContexto.SaveChanges();
        }

        public void Editar_Datos_Servicio_Operativo(EFDbContext mContexto, FormServicioOperativoView formServicioOperativoView)
        {
            Tra_Formulario_Servicio_Operativos tra_Formulario_Servicio_Operativos = mContexto.Tra_Formulario_Servicio_Operativos.Find(formServicioOperativoView.Formulario_Servicio_Operativo_ID);
            tra_Formulario_Servicio_Operativos.Nombre_Usuario = formServicioOperativoView.Nombre_Usuario;
            tra_Formulario_Servicio_Operativos.RPU = formServicioOperativoView.RPU;
            tra_Formulario_Servicio_Operativos.NoCuenta = formServicioOperativoView.NoCuenta;
            tra_Formulario_Servicio_Operativos.Domicilio = formServicioOperativoView.Domicilio;
            mContexto.SaveChanges();
        }

        public void Editar_Suspension(EFDbContext mContexto, FormularioSuspensionView formularioSuspension)
        {
            Tra_Formulario_Suspension tra_Formulario_Suspension = mContexto.Tra_Formulario_Suspension.Find(formularioSuspension.Formulario_Suspension_ID);
            tra_Formulario_Suspension.Domicilio = formularioSuspension.Domicilio;
            tra_Formulario_Suspension.Domicilio_Notificacion = formularioSuspension.Domicilio_Notificacion;
            tra_Formulario_Suspension.Motivo_Suspension = formularioSuspension.Motivo_Suspension;
            tra_Formulario_Suspension.NoCuenta = formularioSuspension.NoCuenta;
            tra_Formulario_Suspension.RPU = formularioSuspension.RPU;
            tra_Formulario_Suspension.Nombre = formularioSuspension.Nombre;
            tra_Formulario_Suspension.Telefono_Notificacion = formularioSuspension.Telefono_Notificacion;
            mContexto.SaveChanges();
        }

        public void Guarar_Datos_Fiscales(EFDbContext mContexto, FormularioDatosFiscalesView formularioDatosFiscalesView)
        {
            Tra_Formulario_Datos_Fiscales tra_Formulario_Datos_Fiscales = new Tra_Formulario_Datos_Fiscales();

            tra_Formulario_Datos_Fiscales.Numero_Exterior = formularioDatosFiscalesView.Numero_Exterior;
            tra_Formulario_Datos_Fiscales.Numero_Interior = formularioDatosFiscalesView.Numero_Interior;
            tra_Formulario_Datos_Fiscales.Razon_Social = formularioDatosFiscalesView.Razon_Social.ToUpper();
            tra_Formulario_Datos_Fiscales.RFC = formularioDatosFiscalesView.RFC.ToUpper();
            tra_Formulario_Datos_Fiscales.RPU = formularioDatosFiscalesView.RPU;
            tra_Formulario_Datos_Fiscales.Tramite_Solicitados_ID = formularioDatosFiscalesView.Tramite_Solicitados_ID;
            tra_Formulario_Datos_Fiscales.Calle = formularioDatosFiscalesView.Calle.ToUpper();
            tra_Formulario_Datos_Fiscales.Colonia = formularioDatosFiscalesView.Colonia.ToUpper();
            tra_Formulario_Datos_Fiscales.Comprobante_Cfdi_ID = formularioDatosFiscalesView.Comprobante_Cfdi_ID;
            tra_Formulario_Datos_Fiscales.Codigo_Postal = formularioDatosFiscalesView.Codigo_Postal;
            tra_Formulario_Datos_Fiscales.Correo_Electronico = formularioDatosFiscalesView.Correo_Electronico;
            tra_Formulario_Datos_Fiscales.Estado = formularioDatosFiscalesView.Estado.ToUpper();
            tra_Formulario_Datos_Fiscales.Ciudad = formularioDatosFiscalesView.Ciudad.ToUpper();

            mContexto.Tra_Formulario_Datos_Fiscales.Add(tra_Formulario_Datos_Fiscales);
            mContexto.SaveChanges();
        }

        public void Guardar_Actualizacion_Titular(EFDbContext mContexto, FormActualizarContratoView formActualizar)
        {
            Tra_Formulario_Actualizar_Contrato tra_Formulario_Actualizar_Contrato = new Tra_Formulario_Actualizar_Contrato();
            tra_Formulario_Actualizar_Contrato.Domicilio_Actual = formActualizar.Domicilio_Actual;
            tra_Formulario_Actualizar_Contrato.Nombre_Actual = formActualizar.Nombre_Actual;
            tra_Formulario_Actualizar_Contrato.RPU = formActualizar.RPU;
            tra_Formulario_Actualizar_Contrato.Nombre_Nuevo = formActualizar.Nombre_Nuevo;
            tra_Formulario_Actualizar_Contrato.Tramite_Solicitados_ID = formActualizar.Tramite_Solicitados_ID;

            mContexto.Tra_Formulario_Actualizar_Contrato.Add(tra_Formulario_Actualizar_Contrato);
            mContexto.SaveChanges();
        }

        public void Guardar_Correccion_Domicilio(EFDbContext mContenxto, FormularioCorreccionDomicilioView formularioCorreccion)
        {
            Tra_Formulario_Correccion_Domicilio formulario_Correccion_Domicilio = new Tra_Formulario_Correccion_Domicilio();
            formulario_Correccion_Domicilio.Domicilio_Anterior = formularioCorreccion.Domicilio_Anterior;
            formulario_Correccion_Domicilio.Domicilio_Nuevo = formularioCorreccion.Domicilio_Nuevo;
            formulario_Correccion_Domicilio.RPU = formularioCorreccion.RPU;
            formulario_Correccion_Domicilio.Tramite_Solicitados_ID = formularioCorreccion.Tramite_Solicitados_ID;

            mContenxto.Tra_Formulario_Correccion_Domicilio.Add(formulario_Correccion_Domicilio);
            mContenxto.SaveChanges();
        }

        public void Guardar_Datos_Cancelar_Servicio(EFDbContext mContexto, FormCancelarServicioView formCancelarServicioView)
        {
            Tra_Formulario_Cancelar_Servicio tra_Formulario_Cancelar_Servicio = new Tra_Formulario_Cancelar_Servicio();
            tra_Formulario_Cancelar_Servicio.Tramite_Solicitados_ID = formCancelarServicioView.Tramite_Solicitados_ID;
            tra_Formulario_Cancelar_Servicio.RPU = formCancelarServicioView.RPU;
            tra_Formulario_Cancelar_Servicio.NoCuenta = formCancelarServicioView.NoCuenta;
            tra_Formulario_Cancelar_Servicio.Nombre_Usuario = formCancelarServicioView.Nombre_Usuario;
            tra_Formulario_Cancelar_Servicio.Domicilio = formCancelarServicioView.Domicilio;
            mContexto.Tra_Formulario_Cancelar_Servicio.Add(tra_Formulario_Cancelar_Servicio);
            mContexto.SaveChanges();

        }

        public void Guardar_Datos_Constancia(EFDbContext mContexto, FormularioConstanciaView formularioConstanciaView)
        {
            Tra_Formulario_Constancia tra_Formulario_Constancia = new Tra_Formulario_Constancia();
            tra_Formulario_Constancia.RPU = formularioConstanciaView.RPU;
            tra_Formulario_Constancia.Nombre_Usuario = formularioConstanciaView.Nombre_Usuario;
            tra_Formulario_Constancia.Domicilio = formularioConstanciaView.Domicilio;
            tra_Formulario_Constancia.Tramite_Solicitados_ID = formularioConstanciaView.Tramite_Solicitados_ID;
            mContexto.Tra_Formulario_Constancia.Add(tra_Formulario_Constancia);
            mContexto.SaveChanges();

        }

        public void Guardar_Datos_Domiciliacion(EFDbContext mContexto, FormDomicialicionView formDomicialicionView)
        {
            Tra_Formulario_Domiciliacion tra_Formulario_Domiciliacion = new Tra_Formulario_Domiciliacion();
            tra_Formulario_Domiciliacion.Tramite_Solicitados_ID = formDomicialicionView.Tramite_Solicitados_ID;
            tra_Formulario_Domiciliacion.CLABE = formDomicialicionView.CLABE;
            tra_Formulario_Domiciliacion.Numero_Tarjeta = formDomicialicionView.Numero_Tarjeta;
            tra_Formulario_Domiciliacion.Nombre_Banco = formDomicialicionView.Nombre_Banco;
            tra_Formulario_Domiciliacion.Monto_Maximo = formDomicialicionView.Monto_Maximo;
            tra_Formulario_Domiciliacion.Nombre_Proveedor = formDomicialicionView.Nombre_Proveedor;
            tra_Formulario_Domiciliacion.Periodicidad = formDomicialicionView.Periodicidad;
            tra_Formulario_Domiciliacion.RFC = formDomicialicionView.RFC;
            tra_Formulario_Domiciliacion.RPU = formDomicialicionView.RPU;
            tra_Formulario_Domiciliacion.Telefono = formDomicialicionView.Telefono;
            tra_Formulario_Domiciliacion.Plazo_Indeterminado = formDomicialicionView.Plazo_Indeterminado;

            if (formDomicialicionView.Plazo_Indeterminado)
                tra_Formulario_Domiciliacion.Fecha_Vencimiento = null;
            else
                tra_Formulario_Domiciliacion.Fecha_Vencimiento = Cls_Util.ConvertirCadenaFecha(formDomicialicionView.Fecha_Vencimiento);

            mContexto.Tra_Formulario_Domiciliacion.Add(tra_Formulario_Domiciliacion);
            mContexto.SaveChanges();

        }

        public void Guardar_Datos_Factibilidad(EFDbContext mContexto, Formulario_Factibilidades_View formulario_Factibilidades_View)
        {
            Tra_Formulario_Factibilidades tra_Formulario_Factibilidades = new Tra_Formulario_Factibilidades();
            tra_Formulario_Factibilidades.Tramite_Solicitados_ID = formulario_Factibilidades_View.Tramite_Solicitados_ID;
            tra_Formulario_Factibilidades.Calle = formulario_Factibilidades_View.Calle;
            tra_Formulario_Factibilidades.Colonia = formulario_Factibilidades_View.Colonia;
            tra_Formulario_Factibilidades.Ciudad = formulario_Factibilidades_View.Ciudad;
            tra_Formulario_Factibilidades.Codigo_Postal = formulario_Factibilidades_View.Codigo_Postal;
            tra_Formulario_Factibilidades.Correo_Electronico = formulario_Factibilidades_View.Correo_Electronico;
            tra_Formulario_Factibilidades.Nombre = formulario_Factibilidades_View.Nombre;
            tra_Formulario_Factibilidades.Numero = formulario_Factibilidades_View.Numero;
            tra_Formulario_Factibilidades.Telefono = formulario_Factibilidades_View.Telefono;
            tra_Formulario_Factibilidades.Ciudad = formulario_Factibilidades_View.Ciudad;
            tra_Formulario_Factibilidades.Estado = formulario_Factibilidades_View.Estado;
            tra_Formulario_Factibilidades.RFC = formulario_Factibilidades_View.RFC;

            mContexto.Tra_Formulario_Factibilidades.Add(tra_Formulario_Factibilidades);
            mContexto.SaveChanges();
        }

        public void Guardar_Datos_Servicio_Operativo(EFDbContext mContexto, FormServicioOperativoView formServicioOperativoView)
        {
            Tra_Formulario_Servicio_Operativos tra_Formulario_Servicio_Operativos = new Tra_Formulario_Servicio_Operativos();
            tra_Formulario_Servicio_Operativos.Tramite_Solicitados_ID = formServicioOperativoView.Tramite_Solicitados_ID;
            tra_Formulario_Servicio_Operativos.Nombre_Usuario = formServicioOperativoView.Nombre_Usuario;
            tra_Formulario_Servicio_Operativos.RPU = formServicioOperativoView.RPU;
            tra_Formulario_Servicio_Operativos.NoCuenta = formServicioOperativoView.NoCuenta;
            tra_Formulario_Servicio_Operativos.Domicilio = formServicioOperativoView.Domicilio;

            mContexto.Tra_Formulario_Servicio_Operativos.Add(tra_Formulario_Servicio_Operativos);
            mContexto.SaveChanges();
        }

        public void Guardar_Suspension(EFDbContext mContexto, FormularioSuspensionView formularioSuspension)
        {

            Tra_Formulario_Suspension tra_Formulario_Suspension = new Tra_Formulario_Suspension();
            tra_Formulario_Suspension.Domicilio = formularioSuspension.Domicilio;
            tra_Formulario_Suspension.Domicilio_Notificacion = formularioSuspension.Domicilio_Notificacion;
            tra_Formulario_Suspension.Motivo_Suspension = formularioSuspension.Motivo_Suspension;
            tra_Formulario_Suspension.NoCuenta = formularioSuspension.NoCuenta;
            tra_Formulario_Suspension.RPU = formularioSuspension.RPU;
            tra_Formulario_Suspension.Nombre = formularioSuspension.Nombre;
            tra_Formulario_Suspension.Telefono_Notificacion = formularioSuspension.Telefono_Notificacion;
            tra_Formulario_Suspension.Tramite_Solicitados_ID = formularioSuspension.Tramite_Solicitados_ID;

            mContexto.Tra_Formulario_Suspension.Add(tra_Formulario_Suspension);
            mContexto.SaveChanges();

        }

        public FormActualizarContratoView Obtener_Formulario_Actualizar_Titular(int Tramite_Solicitados_ID)
        {
            FormActualizarContratoView formActualizar = new FormActualizarContratoView();

            formActualizar = Contexto.Tra_Formulario_Actualizar_Contrato.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new FormActualizarContratoView
                {
                    Formulario_Actualizar_Contrato_ID = x.Formulario_Actualizar_Contrato_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Domicilio_Actual = x.Domicilio_Actual,
                    Nombre_Actual = x.Nombre_Actual,
                    Nombre_Nuevo = x.Nombre_Nuevo,
                    RPU = x.RPU

                }).FirstOrDefault();


            return formActualizar;
        }

        public FormCancelarServicioView Obtener_Formulario_Cancelar(int Tramite_Solicitados_ID)
        {
            FormCancelarServicioView formCancelarServicioView = new FormCancelarServicioView();

            formCancelarServicioView = Contexto.Tra_Formulario_Cancelar_Servicio.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
           .Select(x => new FormCancelarServicioView
           {
               Formulario_Cancelar_Servicio_ID = x.Formulario_Cancelar_Servicio_ID,
               NoCuenta = x.NoCuenta,
               Nombre_Usuario = x.Nombre_Usuario,
               RPU = x.RPU,
               Tramite_Solicitados_ID = x.Tramite_Solicitados_ID

           }).FirstOrDefault();

            return formCancelarServicioView;
        }

        public FormularioConstanciaView Obtener_Formulario_Constancia(int Tramite_Solicitados_ID)
        {
            FormularioConstanciaView formularioConstanciaView = new FormularioConstanciaView();

            formularioConstanciaView = Contexto.Tra_Formulario_Constancia
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new FormularioConstanciaView
                {
                    Formulario_Constancia_ID = x.Formulario_Constancia_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Domicilio = x.Domicilio,
                    Nombre_Usuario = x.Nombre_Usuario,
                    RPU = x.RPU
                }).FirstOrDefault();

            return formularioConstanciaView;
        }

        public FormularioCorreccionDomicilioView Obtener_Formulario_Correccion(int Tramite_Solicitados_ID)
        {
            FormularioCorreccionDomicilioView formularioCorreccionDomicilioView = new FormularioCorreccionDomicilioView();

            formularioCorreccionDomicilioView = Contexto.Tra_Formulario_Correccion_Domicilio
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new FormularioCorreccionDomicilioView
                {
                    Formulario_Correccion_Domicilio_ID = x.Formulario_Correccion_Domicilio_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Domicilio_Anterior = x.Domicilio_Anterior,
                    Domicilio_Nuevo = x.Domicilio_Nuevo,
                    RPU = x.RPU
                }).FirstOrDefault();
            return formularioCorreccionDomicilioView;
        }

        public FormularioDatosFiscalesView Obtener_Formulario_Datos_Fiscales(int Tramite_Solicitados_ID)
        {
            FormularioDatosFiscalesView formularioDatosFiscalesView = new FormularioDatosFiscalesView();

            formularioDatosFiscalesView = Contexto.Tra_Formulario_Datos_Fiscales
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new FormularioDatosFiscalesView
                {
                    Correo_Electronico = x.Correo_Electronico,
                    Comprobante_Cfdi_ID = x.Comprobante_Cfdi_ID,
                    Calle = x.Calle,
                    Codigo_Postal = x.Codigo_Postal,
                    Colonia = x.Colonia,
                    Ciudad = x.Ciudad,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Estado = x.Estado,
                    Formulario_Datos_Fiscales_ID = x.Formulario_Datos_Fiscales_ID,
                    Numero_Exterior = x.Numero_Exterior,
                    Numero_Interior = x.Numero_Interior,
                    Razon_Social = x.Razon_Social,
                    RFC = x.RFC,
                    RPU = x.RPU
                }).FirstOrDefault();


            return formularioDatosFiscalesView;
        }

        public FormDomicialicionView Obtener_Formulario_Domiciliacion(int Tramite_Solicitados_ID)
        {
            FormDomicialicionView formDomicialicionView = new FormDomicialicionView();

            formDomicialicionView = Contexto.Tra_Formulario_Domiciliacion
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .AsEnumerable()
                .Select(x => new FormDomicialicionView
                {
                    Monto_Maximo = x.Monto_Maximo,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    CLABE = x.CLABE,
                    Formulario_Domiciliacion_ID = x.Formulario_Domiciliacion_ID,
                    Nombre_Banco = x.Nombre_Banco,
                    Nombre_Proveedor = x.Nombre_Proveedor,
                    Numero_Tarjeta = x.Numero_Tarjeta,
                    Periodicidad = x.Periodicidad,
                    Plazo_Indeterminado = x.Plazo_Indeterminado,
                    RFC = x.RFC,
                    RPU = x.RPU,
                    Telefono = x.Telefono,
                    Fecha_Vencimiento = x.Fecha_Vencimiento.HasValue ?  x.Fecha_Vencimiento.Value.ToString("dd/MM/yyyy") : ""
                    
                }).FirstOrDefault();


            return formDomicialicionView;
        }

        public Formulario_Factibilidades_View Obtener_Formulario_Factibilidades(int Tramite_Solicitados_ID)
        {
            Formulario_Factibilidades_View formulario_Factibilidades_View = new Formulario_Factibilidades_View();

            formulario_Factibilidades_View = Contexto.Tra_Formulario_Factibilidades
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new Formulario_Factibilidades_View
                {
                    Calle = x.Calle,
                    Colonia = x.Colonia,
                    Codigo_Postal = x.Codigo_Postal,
                    Ciudad = x.Ciudad,
                    Correo_Electronico = x.Correo_Electronico,
                    Estado = x.Estado,
                    Formulario_Factibilidades_ID = x.Formulario_Factibilidades_ID,
                    Nombre = x.Nombre,
                    Numero = x.Numero,
                    RFC = x.RFC,
                    Telefono = x.Telefono,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID

                }).FirstOrDefault();


            return formulario_Factibilidades_View;
        }

        public FormServicioOperativoView Obtener_Formulario_Servicio_Operativo(int Tramite_Solicitados_ID)
        {
            FormServicioOperativoView formServicioOperativoView = new FormServicioOperativoView();

            formServicioOperativoView = Contexto.Tra_Formulario_Servicio_Operativos
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new FormServicioOperativoView
                {
                  Formulario_Servicio_Operativo_ID = x.Formulario_Servicio_Operativo_ID,
                  Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                  Domicilio = x.Domicilio,
                  NoCuenta = x.NoCuenta,
                  Nombre_Usuario = x.Nombre_Usuario,
                  RPU = x.RPU

                }).FirstOrDefault();

            return formServicioOperativoView;
        }

        public FormularioSuspensionView Obtener_Formulario_Suspension(int Tramite_Solicitados_ID)
        {
            FormularioSuspensionView formularioSuspensionView = new FormularioSuspensionView();


            formularioSuspensionView = Contexto.Tra_Formulario_Suspension
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new FormularioSuspensionView
                {
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Formulario_Suspension_ID = x.Formulario_Suspension_ID,
                    Domicilio = x.Domicilio,
                    Domicilio_Notificacion = x.Domicilio_Notificacion,
                    Motivo_Suspension = x.Motivo_Suspension,
                    NoCuenta = x.NoCuenta,
                    Nombre = x.Nombre,
                    RPU = x.RPU,
                    Telefono_Notificacion = x.Telefono_Notificacion

                }).FirstOrDefault();

            return formularioSuspensionView;
        }

        public List<TramitesView> Obtener_Tramites()
        {
            List<TramitesView> Lista_Tramite = new List<TramitesView>();


            Lista_Tramite = Contexto.Tra_Tramites.Where(x => x.Estatus == "ACTIVO")
                .OrderBy(x=> x.Nombre_Tramite)
                .Select(x => new TramitesView
                {
                    Costo_Tramite = x.Costo_Tramite,
                    Estatus = x.Estatus,
                    Nombre_Tramite = x.Nombre_Tramite,
                    Tramite_ID = x.Tramite_ID,
                    Controlador = x.Controlador == null ? "": x.Controlador,
                    Lista_Documentos = x.Tramites_Documentos.Select(t=> new ComboView {
                        id = t.Documento_ID,
                        name = t.Tra_Documentos.Nombre_Documento
                    }).ToList()
                }).ToList();

            return Lista_Tramite;
        }

        public TramitesView Obtener_Un_Tramite(int Tramite_ID)
        {
            TramitesView tramitesView = new TramitesView();

            tramitesView = Contexto.Tra_Tramites.Where(x => x.Estatus == "ACTIVO" && x.Tramite_ID == Tramite_ID)
               .OrderBy(x => x.Nombre_Tramite)
               .Select(x => new TramitesView
               {
                   Costo_Tramite = x.Costo_Tramite,
                   Estatus = x.Estatus,
                   Nombre_Tramite = x.Nombre_Tramite,
                   Tramite_ID = x.Tramite_ID,
                   Controlador = x.Controlador == null ? "" : x.Controlador,
                   Lista_Documentos = x.Tramites_Documentos.Select(t => new ComboView
                   {
                       id = t.Documento_ID,
                       name = t.Tra_Documentos.Nombre_Documento
                   }).ToList()
               }).FirstOrDefault();

            return tramitesView;
        }
    }
}
