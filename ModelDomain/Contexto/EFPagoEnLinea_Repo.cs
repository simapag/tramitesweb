﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.Util;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
    public class EFPagoEnLinea_Repo : IPagosEnLinea_Repo
    {

        private EFDbContext Contexto = new EFDbContext();
        public IQueryable<Tra_Pago_En_Linea> Tra_Pago_En_Linea => Contexto.Tra_Pago_En_Linea;

        public bool Existe_EL_Pago(int Pago_Solicitudes_ID)
        {
            bool respuesta = false;


            var buscarPagos = Contexto.Tra_Pago_En_Linea.Where(x => x.Pago_Solicitudes_ID == Pago_Solicitudes_ID);

            if (buscarPagos.Any())
            {
                respuesta = true;
            }

            return respuesta;
        }

        public bool Existe_El_Pago_Recibo(string No_Factura_Recibo)
        {
            bool respuesta = false;

            var buscarPagosRecibos = Contexto.Tra_Pago_En_Linea_Recibo.Where(x => x.No_Factura_Recibo == No_Factura_Recibo);

            if (buscarPagosRecibos.Any()) {
                respuesta = true;
            }

            return respuesta;
        }

        public IQueryable<PagosEnLineaReciboView> Obtener_Pagos_En_Linea_Recibo(string sortOrder, string FechaInicio, string FechaFin, string RPU, string Folio_Pago_Bancomer)
        {
            var dbPagos = Contexto.Tra_Pago_En_Linea_Recibo.Where(x=> x.Pago_En_Linea_Recibo_ID > 0);


            if (!String.IsNullOrEmpty(FechaInicio))
            {
                DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                dbPagos = dbPagos.Where(x => x.Fecha >= dateTimeIncio);
            }

            if (!String.IsNullOrEmpty(FechaFin))
            {
                DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                dbPagos = dbPagos.Where(x => x.Fecha <= dateTimeFinMas);
            }

            if (!String.IsNullOrEmpty(RPU))
            {
                dbPagos = dbPagos.Where(x => x.RPU.Contains(RPU));
            }


            if (!String.IsNullOrEmpty(Folio_Pago_Bancomer))
            {
                dbPagos = dbPagos.Where(x => x.Folio_Pago.Contains(Folio_Pago_Bancomer));
            }


            switch (sortOrder)
            {
                case "fecha_asc":
                    dbPagos = dbPagos.OrderBy(x => x.Fecha);
                    break;
                default:
                    dbPagos = dbPagos.OrderByDescending(x => x.Fecha);
                    break;
            }


            return dbPagos.Select(x => new PagosEnLineaReciboView
            {
                Cadena_Hash_Generada = x.Cadena_Hash_Generada,
                Banco_Emisor = x.Banco_Emisor,
                Cadena_Hash_Regresada = x.Cadena_Hash_Regresada,
                Correo_Electronico = x.Correo_Electronico,
                Fecha = x.Fecha,
                Folio_Pago = x.Folio_Pago,
                Importe = x.Importe,
                Node_Banco = x.Node_Banco,
                Nombre_Cuenta_Habiente = x.Nombre_Cuenta_Habiente,
                No_Factura_Recibo = x.No_Factura_Recibo,
                Numero_Apobacion_Bancaria = x.Numero_Apobacion_Bancaria,
                Pago_En_Linea_Recibo_ID = x.Pago_En_Linea_Recibo_ID,
                Referencia_Bancaria = x.Referencia_Bancaria,
                RPU = x.RPU,
                Telefono = x.Telefono,
                Tipo_Pago = x.Tipo_Pago,
                Tipo_Tarjeta = x.Tipo_Tarjeta

            });

        }

        public void Registrar_Pago(EFDbContext ContextoTransaccion, PagosEnLineaView pagosEnLineaView)
        {
            Tra_Pago_En_Linea pago_En_Linea = new Tra_Pago_En_Linea();

            pago_En_Linea.Banco_Emisor = pagosEnLineaView.Banco_Emisor;
            pago_En_Linea.Correo_Electronico = pagosEnLineaView.Correo_Electronico;
            pago_En_Linea.Fecha = DateTime.Now;
            pago_En_Linea.Folio_Pago = pagosEnLineaView.Folio_Pago;
            pago_En_Linea.Importe = pagosEnLineaView.Importe;
            pago_En_Linea.Nombre_CuentaHabiente = pagosEnLineaView.Nombre_CuentaHabiente;
            pago_En_Linea.No_Diverso = pagosEnLineaView.No_Diverso;
            pago_En_Linea.Numero_Aprobacion_Bancaria = pagosEnLineaView.Numero_Aprobacion_Bancaria;
            pago_En_Linea.Pago_Solicitudes_ID = pagosEnLineaView.Pago_Solicitudes_ID;
            pago_En_Linea.Referencia = pagosEnLineaView.Referencia;
            pago_En_Linea.Telefono = pagosEnLineaView.Telefono;
            pago_En_Linea.Tipo_Pago = pagosEnLineaView.Tipo_Pago;
            pago_En_Linea.Tipo_Tarjeta = pagosEnLineaView.Tipo_Tarjeta;
            pago_En_Linea.Cadena_Hash_Generada = pagosEnLineaView.Cadena_Hash_Generada;
            pago_En_Linea.Cadena_Hash_Regresada = pagosEnLineaView.Cadena_Hash_Regresada;

            ContextoTransaccion.Tra_Pago_En_Linea.Add(pago_En_Linea);
            ContextoTransaccion.SaveChanges();


        }

        public void Registrar_Pago_Recibo(PagosEnLineaReciboView pagosEnLineaReciboView)
        {
            Tra_Pago_En_Linea_Recibo tra_Pago_En_Linea_Recibo = new Tra_Pago_En_Linea_Recibo();
            tra_Pago_En_Linea_Recibo.Banco_Emisor = pagosEnLineaReciboView.Banco_Emisor;
            tra_Pago_En_Linea_Recibo.Cadena_Hash_Generada = pagosEnLineaReciboView.Cadena_Hash_Generada;
            tra_Pago_En_Linea_Recibo.Cadena_Hash_Regresada = pagosEnLineaReciboView.Cadena_Hash_Regresada;
            tra_Pago_En_Linea_Recibo.Correo_Electronico = pagosEnLineaReciboView.Correo_Electronico;
            tra_Pago_En_Linea_Recibo.Fecha = DateTime.Now;
            tra_Pago_En_Linea_Recibo.Folio_Pago = pagosEnLineaReciboView.Folio_Pago;
            tra_Pago_En_Linea_Recibo.Importe = pagosEnLineaReciboView.Importe;
            tra_Pago_En_Linea_Recibo.Nombre_Cuenta_Habiente = pagosEnLineaReciboView.Nombre_Cuenta_Habiente;
            tra_Pago_En_Linea_Recibo.No_Factura_Recibo = pagosEnLineaReciboView.No_Factura_Recibo;
            tra_Pago_En_Linea_Recibo.Numero_Apobacion_Bancaria = pagosEnLineaReciboView.Numero_Apobacion_Bancaria;
            tra_Pago_En_Linea_Recibo.Referencia_Bancaria = pagosEnLineaReciboView.Referencia_Bancaria;
            tra_Pago_En_Linea_Recibo.RPU = pagosEnLineaReciboView.RPU;
            tra_Pago_En_Linea_Recibo.Telefono = pagosEnLineaReciboView.Telefono;
            tra_Pago_En_Linea_Recibo.Tipo_Pago = pagosEnLineaReciboView.Tipo_Pago;
            tra_Pago_En_Linea_Recibo.Tipo_Tarjeta = pagosEnLineaReciboView.Tipo_Tarjeta;
            tra_Pago_En_Linea_Recibo.Node_Banco = pagosEnLineaReciboView.Node_Banco;
            Contexto.Tra_Pago_En_Linea_Recibo.Add(tra_Pago_En_Linea_Recibo);
            Contexto.SaveChanges();

        }
    }
}
