﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System.Data.SqlClient;

namespace ModelDomain.Contexto
{
   public class EFAtenderTramite_Repo : IAtenderTramite_Repo
    {
        private EFDbContext Contexto = new EFDbContext();
        private EFDbContextSIMAPAG ContextoSimapag = new EFDbContextSIMAPAG();
        string Str_Sql;

        public void Agregar_Pagos(EFDbContext mContexto, int Tramite_Solicitados_ID, Pagos_Solicitudes_View pagos_Solicitudes_View, List<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalle)
        {
            Tra_Pagos_Solicitudes tra_Pagos_Solicitudes = new Tra_Pagos_Solicitudes();
            List<Tra_Pagos_Solicitudes_Detalle> Lista_Detalle = new List<Tra_Pagos_Solicitudes_Detalle>();

            tra_Pagos_Solicitudes.Tramite_Solicitados_ID = Tramite_Solicitados_ID;
            tra_Pagos_Solicitudes.Usuarios_ID = pagos_Solicitudes_View.Usuarios_ID;
            tra_Pagos_Solicitudes.IVA = pagos_Solicitudes_View.IVA;
            tra_Pagos_Solicitudes.Importe = pagos_Solicitudes_View.Importe;
            tra_Pagos_Solicitudes.Total = pagos_Solicitudes_View.Total;
            tra_Pagos_Solicitudes.Usuario_Creo = pagos_Solicitudes_View.Usuario_Creo;
            tra_Pagos_Solicitudes.Fecha_Creo = DateTime.Now;
            tra_Pagos_Solicitudes.Estatus = "PENDIENTE";
            tra_Pagos_Solicitudes.No_Diverso = pagos_Solicitudes_View.No_Diverso;
            tra_Pagos_Solicitudes.Codigo_Barras = pagos_Solicitudes_View.Codigo_Barras;

            foreach (Pagos_Solicitudes_Detalle_View item in Lista_Pagos_Detalle)
            {
                Tra_Pagos_Solicitudes_Detalle pagos_Solicitudes_Detalle = new Tra_Pagos_Solicitudes_Detalle();
                pagos_Solicitudes_Detalle.Cantidad = item.Cantidad;
                pagos_Solicitudes_Detalle.Concepto_ID = item.Concepto_ID;
                pagos_Solicitudes_Detalle.Importe = item.Importe;
                pagos_Solicitudes_Detalle.Concepto = item.Concepto;
                pagos_Solicitudes_Detalle.Precio_Unitario = item.Precio_Unitario;
                Lista_Detalle.Add(pagos_Solicitudes_Detalle);
            }

            tra_Pagos_Solicitudes.Tra_Pagos_Solicitudes_Detalle = Lista_Detalle;

            mContexto.Tra_Pagos_Solicitudes.Add(tra_Pagos_Solicitudes);
            mContexto.SaveChanges();


        }

        public void Cambiar_Estatus_Pago(string Nuevo_Estatus, string Usuario_Modifico, int Pago_Solicitudes_ID)
        {
            Tra_Pagos_Solicitudes tra_Pagos_Solicitudes = Contexto.Tra_Pagos_Solicitudes.Find(Pago_Solicitudes_ID);
            tra_Pagos_Solicitudes.Estatus = Nuevo_Estatus;
            tra_Pagos_Solicitudes.Fecha_Modifico = DateTime.Now;
            tra_Pagos_Solicitudes.Usuario_Modifico = Usuario_Modifico;

            Contexto.SaveChanges();
        }

        public void Cambiar_Estatus_Pago(EFDbContext mContexto, string Nuevo_Estatus, string Usuario_Modifico, int Pago_Solicitudes_ID)
        {
            Tra_Pagos_Solicitudes tra_Pagos_Solicitudes = mContexto.Tra_Pagos_Solicitudes.Find(Pago_Solicitudes_ID);
            tra_Pagos_Solicitudes.Estatus = Nuevo_Estatus;
            tra_Pagos_Solicitudes.Fecha_Modifico = DateTime.Now;
            tra_Pagos_Solicitudes.Usuario_Modifico = Usuario_Modifico;
            mContexto.SaveChanges();
        }

        public string Esta_Repetido_Codigo_Barras(string Codigo_Barras)
        {

            string respuesta = "NO";
            int contador = 0;

            var buscar = Contexto.Tra_Pagos_Solicitudes.Where(x => x.Codigo_Barras == Codigo_Barras);

            if (buscar.Any()) {

                foreach (Tra_Pagos_Solicitudes item in buscar) {
                    if(item.Estatus == "CANCELADO")
                    {
                        contador++;
                    }
                }
                
                if(contador != buscar.Count())
                      respuesta = "SI";
            }

            return respuesta;
        }

        public List<Pagos_Solicitudes_View> Obtener_Datos_Codigo_Barras(string Codigo_Barras)
        {
            List<Pagos_Solicitudes_View> Lista_Solicitudes = new List<Pagos_Solicitudes_View>();

            Str_Sql = "SELECT No_Diverso " +
                    ",Total " +
                    ",Importe " +
                    ",IVA " +
                    ",Codigo_Barras " +
                    ",Estatus " +
                    ",0 AS Pago_Solicitudes_ID " +
                    ",0 AS Tramite_Solicitados_ID " +
                     ",0 AS Usuarios_ID " +
                    ", ISNULL(Usuario_Creo, '') AS Usuario_Creo " +
                    ", isnull(RPU,'') as RPU " +
                "FROM Ope_Cor_Diversos " +
                "WHERE Codigo_Barras = @codigo_barras or RPU = @rpu " +
                 "   AND Estatus = 'PENDIENTE' ";

            Lista_Solicitudes = ContextoSimapag.Database.SqlQuery<Pagos_Solicitudes_View>(Str_Sql, new SqlParameter("@codigo_barras", Codigo_Barras),new SqlParameter("@rpu", Codigo_Barras)).ToList();

            return Lista_Solicitudes;
            
        }

        public List<Pagos_Solicitudes_Detalle_View> Obtener_Datos_Codigo_Barras_Detalle(string No_Diverso)
        {
            List<Pagos_Solicitudes_Detalle_View> Lista_Detalle = new List<Pagos_Solicitudes_Detalle_View>();

            Str_Sql = "SELECT No_Diverso " +
	                ",Concepto_ID " + 
	                ",Concepto " + 
	                ",Cantidad " + 
	                ",Precio_Unitario " +
	                ",Importe " +
	                ",Estatus "  +
	                ",0 AS Pagos_Solicitudes_Detalle_ID " +
                    ",0 AS Pago_Solicitudes_ID " +
                "FROM Ope_Cor_Diversos_Detalles " +
                "WHERE No_Diverso = @no_diverso"  ;

            Lista_Detalle = ContextoSimapag.Database.SqlQuery<Pagos_Solicitudes_Detalle_View>(Str_Sql, new SqlParameter("@no_diverso", No_Diverso)).ToList();

            return Lista_Detalle;
        }

        public Cls_Paginado<Pagos_Solicitudes_Detalle_View> Obtener_Detalla_Pago(int offset, int limit,int pago_solicitudes_id)
        {
            Cls_Paginado<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalle;

            var busquedaFiltro = Contexto.Tra_Pagos_Solicitudes_Detalle.Where(x => x.Pago_Solicitudes_ID == pago_solicitudes_id)
                 .OrderBy(x=> x.Concepto)
                 .Select(x => new Pagos_Solicitudes_Detalle_View
                 {
                     Cantidad = x.Cantidad,
                     Concepto_ID = x.Concepto_ID,
                     Concepto = x.Concepto,
                     Importe = x.Importe,
                     Precio_Unitario = x.Precio_Unitario,
                     Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                     Pagos_Solicitudes_Detalle_ID = x.Pagos_Solicitudes_Detalle_ID

                 });
            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();
            Lista_Pagos_Detalle = new Cls_Paginado<Pagos_Solicitudes_Detalle_View>(datos_visualizar, busquedaFiltro.Count());
            return Lista_Pagos_Detalle;
        }

        public Cls_Paginado<Pagos_Solicitudes_View> Obtener_Pagos_Registrados(int offset, int limit, int tramite_solicitados_id, string search)
        {
            Cls_Paginado<Pagos_Solicitudes_View> Paginado;

            var busquedaFiltro = Contexto.Tra_Pagos_Solicitudes.Where(
                x => (string.IsNullOrEmpty(search) || x.Codigo_Barras.Contains(search)))
                .Where(x=> x.Tramite_Solicitados_ID == tramite_solicitados_id)
                .OrderByDescending(x => x.Fecha_Creo)
                .AsEnumerable()
                .Select(x => new Pagos_Solicitudes_View
                {
                    Codigo_Barras = x.Codigo_Barras,
                    Estatus = x.Estatus,
                    Fecha_Creo = x.Fecha_Creo.ToString("dd/MM/yyyy"),
                    Importe = x.Importe,
                    IVA = x.IVA,
                    No_Diverso = x.No_Diverso,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Total = x.Total,
                    Usuarios_ID = x.Usuarios_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<Pagos_Solicitudes_View>(datos_visualizar, busquedaFiltro.Count());

            return Paginado;
        }

        public List<Pagos_Solicitudes_View> Obtener_Pagos_Registrados(int Tramite_Solicitados_ID)
        {
            List<Pagos_Solicitudes_View> Lista_Pagos = new List<Pagos_Solicitudes_View>();

            Lista_Pagos = Contexto.Tra_Pagos_Solicitudes.Where(
                x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .OrderByDescending(x => x.Fecha_Creo)
                .Select(x => new Pagos_Solicitudes_View
                {
                    Codigo_Barras = x.Codigo_Barras,
                    Estatus = x.Estatus,
                    DatFecha_Creo = x.Fecha_Creo,
                    Importe = x.Importe,
                    IVA = x.IVA,
                    No_Diverso = x.No_Diverso,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Total = x.Total,
                    Usuarios_ID = x.Usuarios_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID
                }).ToList();

            return Lista_Pagos;
        }

        public List<Pagos_Solicitudes_Detalle_View> Obtener_Pagos_Registrados_Detalle(int Pago_Solicitudes_ID)
        {
            List<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalles = new List<Pagos_Solicitudes_Detalle_View>();

            Lista_Pagos_Detalles = Contexto.Tra_Pagos_Solicitudes_Detalle.Where(x => x.Pago_Solicitudes_ID == Pago_Solicitudes_ID)
                .OrderBy(x => x.Concepto)
                .Select(x => new Pagos_Solicitudes_Detalle_View
                {
                    Concepto_ID = x.Concepto_ID,
                    Concepto = x.Concepto,
                    Importe = x.Importe,
                    Precio_Unitario = x.Precio_Unitario,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Pagos_Solicitudes_Detalle_ID = x.Pagos_Solicitudes_Detalle_ID

                }).ToList();

            return Lista_Pagos_Detalles;
        }

        public Cls_Paginado<TramitesSolicitudView> Obtener_Solicitudes(int offset, int limit, int tramite_id, string search, string estatus, int Usuarios_Asignado_ID)
        {
            Cls_Paginado<TramitesSolicitudView> Paginado;

            var busquedaFiltro = Contexto.Tra_Tramites_Solicitados.Where(
                  x => (string.IsNullOrEmpty(search) ||  x.Tramite_Solicitados_ID.ToString().Contains(search) || x.Observaciones.Contains(search) || x.RPU.Contains(search)) 
                  && (string.IsNullOrEmpty(estatus) || x.Estatus_Revision == estatus)
                  && x.Usuarios_Asignado_ID == Usuarios_Asignado_ID)
                .OrderBy(x => x.Fecha_Creo)
                .Where(x=> tramite_id == 0 || x.Tramite_ID == tramite_id)
                .AsEnumerable()
                .Select(x => new TramitesSolicitudView
                {

                    Controlador = x.Tra_Tramites.Controlador,
                    Costo_Tramite = x.Costo_Tramite,
                    Estatus = x.Estatus,
                    Estatus_Revision = x.Estatus_Revision,
                    Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                    Observaciones = x.Observaciones,
                    RPU = x.RPU == null ? "" : x.RPU,
                    Tramite_ID = x.Tramite_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Usuarios_ID = x.Usuarios_ID,
                    Fecha_Solicitud = x.Fecha_Creo,
                    Str_Fecha_Solicitud = x.Fecha_Creo.ToString("dd/MM/yyyy"),
                    Referencia_Pago = x.Referencia_Pago == null ? "" : x.Referencia_Pago.Trim(),
                    Usuario = x.Usuarios.Nombre + " " + x.Usuarios.Apellido_Paterno + " " + (x.Usuarios.Apellido_Materno == null ? "": x.Usuarios.Apellido_Materno),
                    Telefono = x.Usuarios.Telefono_Celular == null ? "": x.Usuarios.Telefono_Celular,
                    NumeroMensajes = x.Tra_Mensajes_Solicitudes.Count,
                    NumeroPago = x.Tra_Pagos_Solicitudes.Where(y=> y.Estatus != "CANCELADO").Count()
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<TramitesSolicitudView>(datos_visualizar, busquedaFiltro.Count());

            return Paginado;
        }

        public List<ComboView> Obtener_Tramites_Asignados(int Usuarios_Asignado_ID)
        {
            List<ComboView> Lista = new List<ComboView>();

            Lista = Contexto.Tra_Tramites_Solicitados.Where(x => x.Usuarios_Asignado_ID == Usuarios_Asignado_ID && (x.Estatus_Revision != "TERMINADO" || x.Estatus_Revision != "CANCELADO"))
                    .OrderBy(x => x.Tra_Tramites.Nombre_Tramite)
                    .Select(x => new ComboView
                    {
                        id = x.Tra_Tramites.Tramite_ID,
                        name = x.Tra_Tramites.Nombre_Tramite
                    }).Distinct().ToList();


            return Lista;
        }

        public List<Pagos_Solicitudes_View> Obtener_Un_Diverso(string No_Diverso)
        {
            List<Pagos_Solicitudes_View> Lista_Solicitudes = new List<Pagos_Solicitudes_View>();

            Str_Sql = "SELECT No_Diverso " +
                    ",Total " +
                    ",Importe " +
                    ",IVA " +
                    ",Codigo_Barras " +
                    ",Estatus " +
                    ",0 AS Pago_Solicitudes_ID " +
                    ",0 AS Tramite_Solicitados_ID " +
                     ",0 AS Usuarios_ID " +
                    ", ISNULL(Usuario_Creo, '') AS Usuario_Creo " +
                    ", isnull(RPU,'') as RPU " +
                "FROM Ope_Cor_Diversos " +
                "WHERE No_Diverso = @no_diverso "; 
             

            Lista_Solicitudes = ContextoSimapag.Database.SqlQuery<Pagos_Solicitudes_View>(Str_Sql, new SqlParameter("@no_diverso", No_Diverso)).ToList();

            return Lista_Solicitudes;
        }

        public Pagos_Solicitudes_View Obtener_Un_Pago(int Pago_Solicitudes_ID)
        {
            Pagos_Solicitudes_View pagos_Solicitudes_View = new Pagos_Solicitudes_View();

            pagos_Solicitudes_View = Contexto.Tra_Pagos_Solicitudes.Where(x => x.Pago_Solicitudes_ID == Pago_Solicitudes_ID)
                .Select(x => new Pagos_Solicitudes_View
                {
                    No_Diverso = x.No_Diverso,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Codigo_Barras = x.Codigo_Barras,
                    Estatus = x.Estatus,
                    Importe = x.Importe,
                    IVA = x.IVA,
                    Total = x.Total,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Usuarios_ID = x.Usuarios_ID
                    

                }).FirstOrDefault();

            return pagos_Solicitudes_View;
        }
    }
}
