﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;

namespace ModelDomain.Contexto
{
    public class EFMenus_Repositorio : IMenus_Repositorio
    {

        private EFDbContext Contexto = new EFDbContext();

        public IQueryable<Menu> Menus =>  Contexto.Menus ;

        public IQueryable<Acceso> Acceso => Contexto.Accesos;

        public string Eliminar_Menu(int p_Menu_ID)
        {
            string respuesta="";
            try
            {

                Menu dbMenusEntrada = Contexto.Menus.Find(p_Menu_ID);
                Contexto.Menus.Remove(dbMenusEntrada);
                Contexto.SaveChanges();
                respuesta = "bien";
            }
            catch (DbUpdateException e) {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                 return respuesta = "El elemento  esta referenciado en otra tabla";
                                
                            default:
                              return  respuesta = "No se puede eliminar el elemento";

                        }

                        
                    }
                }
                   
              
            }
            
            return respuesta;
        }

        public void Guarda_Menu(Menu p_Menus)
        {

            if (p_Menus.Menu_ID == 0)
            {
                Contexto.Menus.Add(p_Menus);
            }
            else {
                Menu dbMenuEntrada = Contexto.Menus.Find(p_Menus.Menu_ID);
                dbMenuEntrada.Menu_Descripcion = p_Menus.Menu_Descripcion;
                dbMenuEntrada.Parent_ID = p_Menus.Parent_ID;
                dbMenuEntrada.Accion = p_Menus.Accion;
                dbMenuEntrada.Controlador = p_Menus.Controlador;
                dbMenuEntrada.Estatus = p_Menus.Estatus;
                dbMenuEntrada.Orden = p_Menus.Orden;
                dbMenuEntrada.Usuario_Modifico = p_Menus.Usuario_Modifico;
                dbMenuEntrada.Fecha_Modifico = p_Menus.Fecha_Creo;


            }
            Contexto.SaveChanges();
        }
    }
}
