﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
   public class EFCosto_Repo : ICostos_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public Cls_Paginado<CostosView> Obtener_Costo_Desazolve(int offset, int limit)
        {
            Cls_Paginado<CostosView> Paginado;

            var busquedaFiltro = Contexto.Tra_Costos_Desazolve
                .OrderBy(x=>x.Desazolve_Costo_ID)
                .AsEnumerable()
                  .Select(x => new CostosView
                  {
                      Concepto = x.Concepto,
                      Importe = x.Importe.ToString("c")
                  });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<CostosView>(datos_visualizar, busquedaFiltro.Count());

            return Paginado;
        }

        public Cls_Paginado<CostosView> Obtener_Costo_Factibilidad(int offset, int limit)
        {
            Cls_Paginado<CostosView> Paginado;

            var busquedaFiltro = Contexto.Tra_Costos_Factibilidad
                .OrderBy(x=> x.Factbilidad_Costo_ID)
                .AsEnumerable()
                .Select(x => new CostosView
                {
                    Concepto = x.Concepto,
                    Importe = x.Importe.ToString("c")
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<CostosView>(datos_visualizar, busquedaFiltro.Count());

            return Paginado;
        }
    }
}
