﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ModelDomain.Entidades;
using System.Data.Common;

namespace ModelDomain.Contexto
{

    /// <summary>
    /// Contexto del Entity Framework
    /// </summary>
   public class EFDbContext: DbContext
    {

        public EFDbContext() {


        }

        public EFDbContext(DbConnection existingConnection, bool contextOwnsConnection)
        : base(existingConnection, contextOwnsConnection)
        {

        }

        /// <summary>
        /// Entidad Roles
        /// </summary>
        public DbSet<Roles> Roles { get; set; } 
        /// <summary>
        /// Entidad Menu
        /// </summary>
        public DbSet<Menu> Menus { get; set; }
        /// <summary>
        /// Entidad Acceso
        /// </summary>
        public DbSet<Acceso> Accesos { get; set; }

        /// <summary>
        /// Entidad Usuario
        /// </summary>
        public DbSet<Usuarios> Usuario { get; set; }

        public DbSet<Tra_Tramites> Tra_Tramites { get; set; }
        public DbSet<Tra_Documentos> Tra_Documentos { get; set; }
        public DbSet<Tra_Tramites_Documentos> Tra_Tramites_Documentos { get; set; }
        public DbSet<Tra_RPU_Asociados> Tra_RPU_Asociados { get; set; }
        public DbSet<Tra_Tramites_Solicitados> Tra_Tramites_Solicitados { get; set; }
        public DbSet<Tra_Tramites_Solicitados_Documentos> Tra_Tramites_Solicitados_Documentos { get; set; }
        public DbSet<Tra_Formulario_Correccion_Domicilio> Tra_Formulario_Correccion_Domicilio { get; set; }
        public DbSet<Tra_Mensajes_Solicitudes> Tra_Mensajes_Solicitudes { get; set; }
        public DbSet<Tra_Formulario_Suspension> Tra_Formulario_Suspension { get; set; }

        public DbSet<Tra_Pagos_Solicitudes> Tra_Pagos_Solicitudes { get; set; }
        public DbSet<Tra_Pagos_Solicitudes_Detalle> Tra_Pagos_Solicitudes_Detalle { get; set; }
        public DbSet<Tra_Formulario_Actualizar_Contrato> Tra_Formulario_Actualizar_Contrato { get; set; }
        public DbSet<Tra_Formulario_Factibilidades> Tra_Formulario_Factibilidades { get; set; }
        public DbSet<Tra_Formulario_Domiciliacion> Tra_Formulario_Domiciliacion { get; set; }
        public DbSet<Tra_Formulario_Constancia> Tra_Formulario_Constancia { get; set; }
        public DbSet<Tra_Formulario_Cancelar_Servicio> Tra_Formulario_Cancelar_Servicio { get; set; }
        public DbSet<Tra_Formulario_Servicio_Operativos> Tra_Formulario_Servicio_Operativos { get; set; }
        public DbSet<Tra_Costos_Desazolve> Tra_Costos_Desazolve { get; set; }
        public DbSet<Tra_Costos_Factibilidad> Tra_Costos_Factibilidad { get; set; }
        public DbSet<Tra_Parametros_Correo> Tra_Parametros_Correo { get; set; }
        public DbSet<Tra_Comprobante_Cfdi> Tra_Comprobante_Cfdi { get; set; }
        public DbSet<Tra_Formulario_Datos_Fiscales> Tra_Formulario_Datos_Fiscales { get; set; }
        public DbSet<Tra_Pago_En_Linea> Tra_Pago_En_Linea { get; set; }
        public DbSet<Tra_Pago_En_Linea_Recibo> Tra_Pago_En_Linea_Recibo { get; set; }
        public DbSet<Tra_Pagos_En_Linea_No_Aplicados> Tra_Pagos_En_Linea_No_Aplicados { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu>().HasOptional(i => i.Menu_Parent).WithMany(i => i.Child_Menu).HasForeignKey(l => l.Parent_ID);


            //modelBuilder.Entity<Tra_Tramites>()
            //      .HasMany<Tra_Documentos>(d => d.Tra_Documentos)
            //      .WithMany(t => t.Tra_Tramites)
            //      .Map(cs =>
            //      {
            //          cs.MapLeftKey("Tramite_ID");
            //          cs.MapRightKey("Documento_ID");
            //          cs.ToTable("Tra_Tramites_Documentos");
            //      });
        }
    }
}
