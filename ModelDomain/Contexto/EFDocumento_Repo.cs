﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;


namespace ModelDomain.Contexto
{
  public  class EFDocumento_Repo : IDocuemento_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public string Eliminar_Documento(DocumentosView documentosView)
        {
            string respuesta = "";

            try
            {
                Tra_Documentos tra_Documentos = Contexto.Tra_Documentos.Find(documentosView.Documento_ID);
                Contexto.Tra_Documentos.Remove(tra_Documentos);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch (DbUpdateException e) {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public string Esta_Repetido_Documento(string Nombre_Documento, int Documento_ID)
        {
            string respuesta = "NO";

            var buscarDocumento = Contexto.Tra_Documentos.Where(x => x.Nombre_Documento == Nombre_Documento && x.Documento_ID != Documento_ID);

            if (buscarDocumento.Any()) {
                respuesta = "SI";
            }

            return respuesta;
        }

        public Cls_Paginado<DocumentosView> Obtener_Documentos(int offset, int limit, string search)
        {
            Cls_Paginado<DocumentosView> Paginado;

            var busquedaFiltro = Contexto.Tra_Documentos.Where(x => string.IsNullOrEmpty(search) || x.Nombre_Documento.Contains(search))
                .OrderBy(x => x.Nombre_Documento)
                .Select(x => new DocumentosView
                {
                    Documento_ID = x.Documento_ID,
                    Estatus = x.Estatus,
                    Nombre_Documento = x.Nombre_Documento
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<DocumentosView>(datos_visualizar, busquedaFiltro.Count());

            return Paginado;
        }

        public List<ComboView> Obtener_Documentos_Combo()
        {
            List<ComboView> ListaCombo = new List<ComboView>();

            ListaCombo = Contexto.Tra_Documentos.Where(x => x.Estatus == "ACTIVO")
                .OrderBy(x => x.Nombre_Documento)
                .Select(x => new ComboView
                {
                    id = x.Documento_ID,
                    name = x.Nombre_Documento
                }).ToList();


            return ListaCombo;
        }

        public void Registrar_Documento(DocumentosView documentosView)
        {

            if (documentosView.Documento_ID == 0)
            {
                Tra_Documentos tra_Documentos = new Tra_Documentos();
                tra_Documentos.Estatus = documentosView.Estatus;
                tra_Documentos.Nombre_Documento = documentosView.Nombre_Documento;
                tra_Documentos.Usuario_Creo = documentosView.Usuario_Creo;
                tra_Documentos.Fecha_Creo = DateTime.Now;
                Contexto.Tra_Documentos.Add(tra_Documentos);
            }
            else {
                Tra_Documentos tra_Documentos = Contexto.Tra_Documentos.Find(documentosView.Documento_ID);
                tra_Documentos.Estatus = documentosView.Estatus;
                tra_Documentos.Nombre_Documento = documentosView.Nombre_Documento;
                tra_Documentos.Usuario_Modifico = documentosView.Usuario_Creo;
                tra_Documentos.Fecha_Modifico = DateTime.Now;
            }

            Contexto.SaveChanges();
        }
    }
}
