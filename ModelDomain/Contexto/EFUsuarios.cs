﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
    public class EFUsuarios : IUsuarios_Repositorio
    {
        private EFDbContext Contexto = new EFDbContext();

        public IQueryable<Usuarios> Usuarios => Contexto.Usuario;

        public UsuarioView Buscar_Usuario(int Usuarios_ID)
        {
            return Contexto.Usuario.Where(x => x.Usuarios_ID == Usuarios_ID)
                 .Select(x => new UsuarioView
                 {
                     Usuarios_ID = x.Usuarios_ID,
                     Password = x.Password,
                     Correo_Electronico = x.Correo_Electronico,
                     Nombre = x.Nombre,
                     Apellido_Paterno = x.Apellido_Paterno,
                     Apellido_Materno = x.Apellido_Materno

                 }).FirstOrDefault();
        }

        public UsuarioView Buscar_Usuario_Por_Correo(string Correo_Electronico)
        {
            return Contexto.Usuario.Where(x => x.Correo_Electronico == Correo_Electronico)
                .Select(x => new UsuarioView
                {
                    Usuarios_ID = x.Usuarios_ID,
                    Password = x.Password,
                    Correo_Electronico = x.Correo_Electronico,
                    Nombre = x.Nombre,
                    Apellido_Paterno = x.Apellido_Paterno,
                    Apellido_Materno = x.Apellido_Materno

                }).FirstOrDefault();
        }

        public string Eliminar_Usuario(int p_Usuario_ID)
        {
            string respuesta = "";

            try
            {
                Usuarios dbUsuariosEntrada = Contexto.Usuario.Find(p_Usuario_ID);
                Contexto.Usuario.Remove(dbUsuariosEntrada);
                respuesta = "bien";
                Contexto.SaveChanges();


            }
            catch (Exception e) {
                var sqlException = e.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }


                    }
                }



            }

            return respuesta;

        }

        public string Esta_Registrado_Rol_Usuario_Externo()
        {
            string respuesta = "NO";

            var buscarRol = Contexto.Roles.Where(x => x.Nombre == "Usuario_Externo");

            if (buscarRol.Any()) {
                respuesta = "SI";
            }

            return respuesta;
        }

        public string Esta_Repetido_Correo(string Correo_Electronico)
        {
            string respuesta = "NO";

            var buscarCorreo = Contexto.Usuario.Where(x => x.Correo_Electronico.ToLower() == Correo_Electronico.Trim().ToLower());

            if (buscarCorreo.Any()) {
                respuesta = "SI";
            }

            return respuesta;
        }

        public void Guardar_Usuarios(Usuarios p_Usuarios)
        {
            if (p_Usuarios.Usuarios_ID == 0)
            {
                Contexto.Usuario.Add(p_Usuarios);
            }
            else {

                Usuarios dbUsuariosEntrada = Contexto.Usuario.Find(p_Usuarios.Usuarios_ID);
                dbUsuariosEntrada.Nombre = p_Usuarios.Nombre;
                dbUsuariosEntrada.Apellido_Materno = p_Usuarios.Apellido_Materno;
                dbUsuariosEntrada.Apellido_Paterno = p_Usuarios.Apellido_Paterno;
                dbUsuariosEntrada.Correo_Electronico = p_Usuarios.Correo_Electronico;
                dbUsuariosEntrada.Estatus = p_Usuarios.Estatus;

                if (p_Usuarios.Rol_ID.HasValue)
                    dbUsuariosEntrada.Rol_ID = p_Usuarios.Rol_ID;
                        
                dbUsuariosEntrada.Password = dbUsuariosEntrada.Password;
                dbUsuariosEntrada.Direccion = p_Usuarios.Direccion;
                dbUsuariosEntrada.Telefono_Celular = p_Usuarios.Telefono_Celular;
                dbUsuariosEntrada.Telefono_Oficina = p_Usuarios.Telefono_Oficina;
                dbUsuariosEntrada.Usuario_Modifico = p_Usuarios.Usuario_Modifico;
                dbUsuariosEntrada.Fecha_Modifico = p_Usuarios.Fecha_Modifico;

            }

            Contexto.SaveChanges();
        }

        public List<UsuarioView> Obtener_Usuarios_Ejecutivos()
        {
            List<UsuarioView> Lista_Usuario = new List<UsuarioView>();

            Lista_Usuario = Contexto.Usuario.Where(x => x.Estatus == "ACTIVO" &&
              x.Roles.Nombre == "Ejecutivo")
              .OrderBy(x => x.Nombre)
              .Select(x => new UsuarioView
              {
                  Usuarios_ID = x.Usuarios_ID,
                  Apellido_Paterno = x.Apellido_Paterno,
                  Apellido_Materno = x.Apellido_Materno,
                  Correo_Electronico = x.Correo_Electronico,
                  Nombre = x.Nombre

              }).ToList();


            return Lista_Usuario;

        }

        public void Registrar_Usuario_Externo(RegistroUsuarioViewModel registroUsuarioViewModel)
        {
            Usuarios usuarios = new Usuarios();

            if (usuarios.Apellido_Materno == null)
                usuarios.Apellido_Materno = "";
             else
               usuarios.Apellido_Materno = registroUsuarioViewModel.Apellido_Materno;
            usuarios.Apellido_Paterno = registroUsuarioViewModel.Apellido_Paterno;
            usuarios.Correo_Electronico = registroUsuarioViewModel.Correo_Electronico;
            usuarios.Direccion = "";
            usuarios.Estatus = "ACTIVO";
            usuarios.Fecha_Creo = DateTime.Now;
            usuarios.Usuario_Creo = "usuario_externo";
            usuarios.Nombre = registroUsuarioViewModel.Nombre;
            usuarios.Telefono_Celular = registroUsuarioViewModel.Telefono_Celular;
            usuarios.Telefono_Oficina = "";
            usuarios.Password = registroUsuarioViewModel.Password;
            usuarios.Roles = Contexto.Roles.Where(x => x.Nombre == "Usuario_Externo").First();
            Contexto.Usuario.Add(usuarios);
            Contexto.SaveChanges();
        }
    }
}
