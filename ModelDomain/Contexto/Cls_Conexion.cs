﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ModelDomain.Contexto
{
  public  class Cls_Conexion
    {

        public static string Cadena_Conexion_Siac = ConfigurationManager.ConnectionStrings["EFDbContextSIMAPAG"].ConnectionString;
        public static string Mp_Account = ConfigurationManager.AppSettings["mp_count"];
        public static string Url_Exitoso_Recibo = ConfigurationManager.AppSettings["url_exitoso_factura"];
        public static string Url_Error_Recibo = ConfigurationManager.AppSettings["url_error_factura"];

    }
}
