﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using ModelDomain.Dto;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
   public class EFTramites_Documento_Repo :ITramites_Documento_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public string Eliminar_Tramite_Documento(int Tramite_Documento_ID)
        {
            string respuesta = "";

            try
            {
                Tra_Tramites_Documentos tra_Tramites_Documentos = Contexto.Tra_Tramites_Documentos.Find(Tramite_Documento_ID);
                Contexto.Tra_Tramites_Documentos.Remove(tra_Tramites_Documentos);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch (DbUpdateException e) {

                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public string Esta_Repetido(int Tramite_ID, int Documento_ID)
        {
            string respuesta = "NO";

            var buscar = Contexto.Tra_Tramites_Documentos.Where(x => x.Tramite_ID == Tramite_ID && x.Documento_ID == Documento_ID);

            if (buscar.Any()) {
                respuesta = "SI";
            }
            

            return respuesta;
        }

        public void Guardar_Tramites_Documento(Tramites_Documento_View tramites_Documento_View)
        {

            Tra_Tramites_Documentos tra_Tramites_Documentos = new Tra_Tramites_Documentos();
            tra_Tramites_Documentos.Tramite_ID = tramites_Documento_View.Tramite_ID;
            tra_Tramites_Documentos.Documento_ID = tramites_Documento_View.Documento_ID;
            tra_Tramites_Documentos.Fecha_Creo = DateTime.Now;
            tra_Tramites_Documentos.Usuario_Creo = tramites_Documento_View.Usuario_Creo;

            Contexto.Tra_Tramites_Documentos.Add(tra_Tramites_Documentos);
            Contexto.SaveChanges();
        }

        public List<ComboView> Obtener_Documentos_Por_Tramite(int Tramite_ID)
        {
            List<ComboView> Listado_Documentos = new List<ComboView>();

            Listado_Documentos = Contexto.Tra_Tramites_Documentos.Where(x => x.Tramite_ID == Tramite_ID)
                .OrderBy(x => x.Tra_Documentos.Nombre_Documento)
                .Select(x => new ComboView
                {
                    id = x.Documento_ID,
                    name = x.Tra_Documentos.Nombre_Documento

                }).ToList();

            return Listado_Documentos;
        }

        public Cls_Paginado<Tramites_Documento_View> Obtener_Tramites_Documentos(int offset, int limit, int Tramite_ID)
        {
            Cls_Paginado<Tramites_Documento_View> Paginado;

            var busquedaFiltro = Contexto.Tra_Tramites_Documentos.Where(x => x.Tramite_ID == Tramite_ID)
                .OrderBy(x => x.Tra_Documentos.Nombre_Documento)
                .Select(x => new Tramites_Documento_View
                {
                    Tramite_Documento_ID = x.Tramite_Documento_ID,
                    Documento_ID = x.Documento_ID,
                    Nombre_Documento = x.Tra_Documentos.Nombre_Documento,
                    Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                    Tramite_ID = x.Tramite_ID

                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();
            Paginado = new Cls_Paginado<Tramites_Documento_View>(datos_visualizar, busquedaFiltro.Count());
            return Paginado;
        }
    }
}
