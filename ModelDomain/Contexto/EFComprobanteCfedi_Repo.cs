﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
 public   class EFComprobanteCfedi_Repo : IComprobanteCfedi_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public List<ComboView> Obtener_Comprobante_Cdfi_Combo()
        {
            return Contexto.Tra_Comprobante_Cfdi.OrderBy(x => x.Descripcion)
                .Select(x => new ComboView
                {
                    id = x.Comprobante_Cfdi_ID,
                    name = x.Descripcion,
                    clave = x.Clave

                }).ToList();
        }
    }
}
