﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.Util;
using ModelDomain.ViewModel;


namespace ModelDomain.Contexto
{
    public class EFMensajeSolictudes_Repo : IMensajesSolicitudes_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        string str_sql = "";

        public void Actualizar_Mensaje_Ejecutivo(int Usuario_ID)
        {
            str_sql = "UPDATE Tra_Mensajes_Solicitudes " +
            "SET Visto = 1 " +
            "WHERE Mensajes_Solicitudes_ID IN ( " +
            "        SELECT m.Mensajes_Solicitudes_ID " +
            "        FROM Tra_Tramites_Solicitados s " +
            "        JOIN Tra_Mensajes_Solicitudes m ON s.Tramite_Solicitados_ID = m.Tramite_Solicitados_ID " +
            "        WHERE m.Usuarios_ID <> @usuario_id " +
            "            AND m.Visto = 0 " +
            "            AND s.Usuarios_Asignado_ID = @usuario_id_asignado " +
            "        ) ";


            Contexto.Database.ExecuteSqlCommand(str_sql, new SqlParameter("@usuario_id", Usuario_ID), new SqlParameter("@usuario_id_asignado", Usuario_ID));

        }

        public void Actualizar_Mensaje_Usuario(int Usuario_ID)
        {
            str_sql = "UPDATE Tra_Mensajes_Solicitudes " +
           "SET Visto = 1 " +
           "WHERE Mensajes_Solicitudes_ID IN ( " +
           "        SELECT m.Mensajes_Solicitudes_ID " +
           "        FROM Tra_Tramites_Solicitados s " +
           "        JOIN Tra_Mensajes_Solicitudes m ON s.Tramite_Solicitados_ID = m.Tramite_Solicitados_ID " +
           "        WHERE m.Usuarios_ID <> @usuario_id " +
           "            AND m.Visto = 0 " +
           "            AND s.Usuarios_ID = @usuario_id_asignado " +
           "        ) ";


            Contexto.Database.ExecuteSqlCommand(str_sql, new SqlParameter("@usuario_id", Usuario_ID), new SqlParameter("@usuario_id_asignado", Usuario_ID));

        }

        public void Guardar_Mensaje(MensajesSolicitudesView mensajesSolicitudesView)
        {
            Tra_Mensajes_Solicitudes tra_Mensajes_ = new Tra_Mensajes_Solicitudes();
            tra_Mensajes_.Estatus_Solicitud = mensajesSolicitudesView.Estatus_Solicitud;
            tra_Mensajes_.Mensaje = mensajesSolicitudesView.Mensaje;
            tra_Mensajes_.Tramite_Solicitados_ID = mensajesSolicitudesView.Tramite_Solicitados_ID;
            tra_Mensajes_.Usuarios_ID = mensajesSolicitudesView.Usuarios_ID;
            tra_Mensajes_.Visto = false;
            tra_Mensajes_.Entidad = mensajesSolicitudesView.Entidad;
            tra_Mensajes_.Nombre_Tramite = mensajesSolicitudesView.Nombre_Tramite;
            tra_Mensajes_.Fecha_Creo = DateTime.Now;
            tra_Mensajes_.Usuario_Creo = mensajesSolicitudesView.Usuario_Creo;
            Contexto.Tra_Mensajes_Solicitudes.Add(tra_Mensajes_);
            Contexto.SaveChanges();

        }

        public void Guardar_Mensaje(EFDbContext mContexto, MensajesSolicitudesView mensajesSolicitudesView)
        {
            Tra_Mensajes_Solicitudes tra_Mensajes_ = new Tra_Mensajes_Solicitudes();
            tra_Mensajes_.Estatus_Solicitud = mensajesSolicitudesView.Estatus_Solicitud;
            tra_Mensajes_.Mensaje = mensajesSolicitudesView.Mensaje;
            tra_Mensajes_.Tramite_Solicitados_ID = mensajesSolicitudesView.Tramite_Solicitados_ID;
            tra_Mensajes_.Usuarios_ID = mensajesSolicitudesView.Usuarios_ID;
            tra_Mensajes_.Visto = false;
            tra_Mensajes_.Entidad = mensajesSolicitudesView.Entidad;
            tra_Mensajes_.Nombre_Tramite = mensajesSolicitudesView.Nombre_Tramite;
            tra_Mensajes_.Fecha_Creo = DateTime.Now;
            tra_Mensajes_.Usuario_Creo = mensajesSolicitudesView.Usuario_Creo;
            mContexto.Tra_Mensajes_Solicitudes.Add(tra_Mensajes_);
            mContexto.SaveChanges();
        }

        public List<MensajesSolicitudesView> Obtener_Mensajes(int Tramite_Solicitados_ID)
        {
            List<MensajesSolicitudesView> Lista_Mensaje = new List<MensajesSolicitudesView>();

            Lista_Mensaje = Contexto.Tra_Mensajes_Solicitudes.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .OrderByDescending(x => x.Fecha_Creo)
                .Select(x => new MensajesSolicitudesView
                {
                    Entidad = x.Entidad,
                    Estatus_Solicitud = x.Estatus_Solicitud,
                    Mensaje = x.Mensaje,
                    Mensajes_Solicitudes_ID = x.Mensajes_Solicitudes_ID,
                    Nombre_Tramite = x.Nombre_Tramite,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Usuario_Creo = x.Usuario_Creo,
                    Usuarios_ID = x.Usuarios_ID,
                    Visto = x.Visto,
                    Fecha = x.Fecha_Creo
                }).ToList();

            return Lista_Mensaje;
        }

        public List<MensajesSolicitudesView> Obtener_Mensajes_Usuario(int Usuarios_ID)
        {
            List<MensajesSolicitudesView> ListaMensaje = new List<MensajesSolicitudesView>();

            ListaMensaje = Contexto.Tra_Mensajes_Solicitudes.Where(x => x.Usuarios_ID != Usuarios_ID
         && x.Tra_Tramites_Solicitados.Usuarios_ID == Usuarios_ID && x.Visto == false)
          .OrderByDescending(x => x.Fecha_Creo)
          .Select(x => new MensajesSolicitudesView
          {
              Entidad = x.Entidad,
              Estatus_Solicitud = x.Estatus_Solicitud,
              Mensaje = x.Mensaje,
              Mensajes_Solicitudes_ID = x.Mensajes_Solicitudes_ID,
              Nombre_Tramite = x.Nombre_Tramite,
              Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
              Usuario_Creo = x.Usuario_Creo,
              Usuarios_ID = x.Usuarios_ID,
              Visto = x.Visto,
              Fecha = x.Fecha_Creo
          }).ToList();

            return ListaMensaje;

        }

        public List<MensajesSolicitudesView> Obtener_Mensaje_Ejecutivo(int Usuarios_ID)
        {
            List<MensajesSolicitudesView> ListaMensaje = new List<MensajesSolicitudesView>();


            ListaMensaje = Contexto.Tra_Mensajes_Solicitudes.Where(x => x.Usuarios_ID != Usuarios_ID
            && x.Tra_Tramites_Solicitados.Usuarios_Asignado_ID == Usuarios_ID && x.Visto == false)
            .OrderByDescending(x=> x.Fecha_Creo)
            .Select(x => new MensajesSolicitudesView
            {
                Entidad = x.Entidad,
                Estatus_Solicitud = x.Estatus_Solicitud,
                Mensaje = x.Mensaje,
                Mensajes_Solicitudes_ID = x.Mensajes_Solicitudes_ID,
                Nombre_Tramite = x.Nombre_Tramite,
                Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                Usuario_Creo = x.Usuario_Creo,
                Usuarios_ID = x.Usuarios_ID,
                Visto = x.Visto,
                Fecha = x.Fecha_Creo
            }).ToList();


            return ListaMensaje;
        }

        public int Obtener_Numero_Mensajes(int Usuarios_ID)
        {
            int respuesta = 0;


            var Busqueda = Contexto.Tra_Mensajes_Solicitudes.Where(x => x.Usuarios_ID != Usuarios_ID
            && x.Tra_Tramites_Solicitados.Usuarios_ID == Usuarios_ID && x.Visto == false);

            if (Busqueda.Any()) {
                respuesta = Busqueda.Count();
            }

            return respuesta;
        }

        public int Obtener_Numero_Mensaje_Ejecutivo(int Usuarios_ID)
        {
            int respuesta = 0;


            var Busqueda = Contexto.Tra_Mensajes_Solicitudes.Where(x => x.Usuarios_ID != Usuarios_ID
            && x.Tra_Tramites_Solicitados.Usuarios_Asignado_ID == Usuarios_ID && x.Visto == false);

            if (Busqueda.Any())
            {
                respuesta = Busqueda.Count();
            }

            return respuesta;
        }

        public IQueryable<ReporteMensajesView> Obtener_Reporte_Mensaje(string sortOrder, string FechaInicio, string FechaFin, int Usuario_Asignado_ID)
        {
            var dbMensajes = Contexto.Tra_Mensajes_Solicitudes.Where(x => x.Tra_Tramites_Solicitados.Usuarios_Asignado_ID == Usuario_Asignado_ID
                && x.Entidad == "USUARIO");

            if (!String.IsNullOrEmpty(FechaInicio))
            {
                DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                dbMensajes = dbMensajes.Where(x => x.Fecha_Creo >= dateTimeIncio);
            }


            if (!String.IsNullOrEmpty(FechaFin))
            {
                DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                dbMensajes = dbMensajes.Where(x => x.Fecha_Creo <= dateTimeFinMas);
            }


            switch (sortOrder)
            {

                case "fecha_asc":
                    dbMensajes = dbMensajes.OrderBy(x => x.Fecha_Creo);
                    break;
                default:
                    dbMensajes = dbMensajes.OrderByDescending(x => x.Fecha_Creo);
                    break;

            }

            return dbMensajes.Select(x => new ReporteMensajesView
            {
                Mensaje = x.Mensaje,
                Estatus_Solicitud = x.Estatus_Solicitud,
                Fecha_Creo = x.Fecha_Creo,
                Nombre_Tramite = x.Nombre_Tramite,
                RPU = x.Tra_Tramites_Solicitados.RPU,
                Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                Usuario_Creo = x.Usuario_Creo
                
            });

        }
    }
}
