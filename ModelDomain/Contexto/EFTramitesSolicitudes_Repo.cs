﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System.Data.Entity;
using ModelDomain.Util;

namespace ModelDomain.Contexto
{
    public class EFTramitesSolicitudes_Repo : ITramitesSolicitudes_Repo
    {
        private EFDbContext Contexto = new EFDbContext();


        public string existeDocumentoEnSolicictudNueva(int Tramite_Solicitados_Documento_ID, List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentosNuevos)
        {
            string respuesta = "NO";

            foreach (Tra_Tramites_Solicitados_DocumentosView item in listaDocumentosNuevos) {

                if (item.Tramite_Solicitados_Documento_ID == Tramite_Solicitados_Documento_ID) {
                    respuesta = "SI";
                    break;
                }
            }

            return respuesta;
        }

        public IQueryable<Tra_Tramites_Solicitados> Tra_Tramites_Solicitados => Contexto.Tra_Tramites_Solicitados;

        public void Editar_Solicitud_Tramite(EFDbContext mContenxto, TramitesSolicitudView tramitesSolicitudView, List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos)
        {
            bool banEntro = false;
            bool banEntroBorrar = false;
            Tra_Tramites_Solicitados tra_Tramites_Solicitados = mContenxto.Tra_Tramites_Solicitados.Find(tramitesSolicitudView.Tramite_Solicitados_ID);
            tra_Tramites_Solicitados.RPU = tramitesSolicitudView.RPU;
            tra_Tramites_Solicitados.Observaciones = tramitesSolicitudView.Observaciones;
            tra_Tramites_Solicitados.Usuario_Modifico = tramitesSolicitudView.Usuario_Creo;
            tra_Tramites_Solicitados.Fecha_Modifico = DateTime.Now;

            mContenxto.SaveChanges();


            List<Tra_Tramites_Solicitados_DocumentosView> Lista_Documentos_Anteriores = mContenxto.Tra_Tramites_Solicitados_Documentos
                .Where(x => x.Tramite_Solicitados_ID == tramitesSolicitudView.Tramite_Solicitados_ID)
                .Select(x => new Tra_Tramites_Solicitados_DocumentosView
                {
                    Documento_ID = x.Documento_ID,
                    Nombre_Archivo = x.Nombre_Archivo,
                    Tramite_Solicitados_Documento_ID = x.Tramite_Solicitados_Documento_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Ruta_Archivo = x.Ruta_Archivo,
                    URL_Archivo = x.URL_Archivo
                }).ToList();

            foreach (Tra_Tramites_Solicitados_DocumentosView item in Lista_Documentos_Anteriores) {

                if(existeDocumentoEnSolicictudNueva(item.Tramite_Solicitados_Documento_ID,listaDocumentos) == "NO")
                {
                    // significa que se puede borrar
                    banEntroBorrar = true;
                    Tra_Tramites_Solicitados_Documentos Tra_Tramites_Solicitados_Documentos = mContenxto.Tra_Tramites_Solicitados_Documentos.Find(item.Tramite_Solicitados_Documento_ID);
                    mContenxto.Tra_Tramites_Solicitados_Documentos.Remove(Tra_Tramites_Solicitados_Documentos);
                }

            }

            if (banEntroBorrar) {
                mContenxto.SaveChanges();
            }


            foreach (Tra_Tramites_Solicitados_DocumentosView item in listaDocumentos)
            {
                if (item.Tramite_Solicitados_Documento_ID == 0) {
                    banEntro = true;
                    Tra_Tramites_Solicitados_Documentos tra_Tramites_Solicitados_Documentos = new Tra_Tramites_Solicitados_Documentos();
                    tra_Tramites_Solicitados_Documentos.Documento_ID = item.Documento_ID;
                    tra_Tramites_Solicitados_Documentos.Tramite_Solicitados_ID = tra_Tramites_Solicitados.Tramite_Solicitados_ID;
                    tra_Tramites_Solicitados_Documentos.Nombre_Archivo = item.Nombre_Archivo;
                    tra_Tramites_Solicitados_Documentos.Ruta_Archivo = item.Ruta_Archivo;
                    tra_Tramites_Solicitados_Documentos.URL_Archivo = item.URL_Archivo;

                    mContenxto.Tra_Tramites_Solicitados_Documentos.Add(tra_Tramites_Solicitados_Documentos);
                }
            }

            if (banEntro) {
                mContenxto.SaveChanges();
            }


          }

        public void Guardar_Archivos(List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos ,  string rutaPrimeraParte , string rutaURL)
        {
            string ArchivoTemporal="";
            string rutaFinal;
    

            foreach (Tra_Tramites_Solicitados_DocumentosView item in listaDocumentos) {
                 ArchivoTemporal = Path.Combine(Path.GetTempPath(), item.Nombre_Archivo);                   
                 rutaFinal = Path.Combine(rutaPrimeraParte,item.Nombre_Archivo);
                 item.Ruta_Archivo = rutaFinal;
                 item.URL_Archivo = rutaURL + item.Nombre_Archivo;

                if (item.Tramite_Solicitados_Documento_ID == 0)
                {
                    if (File.Exists(ArchivoTemporal))
                        File.Move(ArchivoTemporal, rutaFinal);
                     
                }
            }

                
        }

        public int Guardar_Solicitud_Tramite(EFDbContext mContenxto, TramitesSolicitudView tramitesSolicitudView, List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos)
        {
            Tra_Tramites_Solicitados tra_Tramites_Solicitados = new Tra_Tramites_Solicitados();

            tra_Tramites_Solicitados.Costo_Tramite = tramitesSolicitudView.Costo_Tramite;
            tra_Tramites_Solicitados.Estatus = tramitesSolicitudView.Estatus;
            tra_Tramites_Solicitados.Estatus_Revision = tramitesSolicitudView.Estatus_Revision;
            tra_Tramites_Solicitados.Observaciones = tramitesSolicitudView.Observaciones;
            tra_Tramites_Solicitados.RPU = tramitesSolicitudView.RPU;
            tra_Tramites_Solicitados.Tramite_ID = tramitesSolicitudView.Tramite_ID;
            tra_Tramites_Solicitados.Usuarios_ID = tramitesSolicitudView.Usuarios_ID;
            tra_Tramites_Solicitados.Usuario_Creo = tramitesSolicitudView.Usuario_Creo;
            tra_Tramites_Solicitados.Fecha_Creo = DateTime.Now;

            mContenxto.Tra_Tramites_Solicitados.Add(tra_Tramites_Solicitados);
            mContenxto.SaveChanges();


            foreach (Tra_Tramites_Solicitados_DocumentosView item in listaDocumentos) {
                Tra_Tramites_Solicitados_Documentos tra_Tramites_Solicitados_Documentos = new Tra_Tramites_Solicitados_Documentos();
                tra_Tramites_Solicitados_Documentos.Documento_ID = item.Documento_ID;
                tra_Tramites_Solicitados_Documentos.Tramite_Solicitados_ID = tra_Tramites_Solicitados.Tramite_Solicitados_ID;
                tra_Tramites_Solicitados_Documentos.Nombre_Archivo = item.Nombre_Archivo;
                tra_Tramites_Solicitados_Documentos.Ruta_Archivo = item.Ruta_Archivo;
                tra_Tramites_Solicitados_Documentos.URL_Archivo = item.URL_Archivo;

                mContenxto.Tra_Tramites_Solicitados_Documentos.Add(tra_Tramites_Solicitados_Documentos);
            }
            mContenxto.SaveChanges();
            return tra_Tramites_Solicitados.Tramite_Solicitados_ID;

        }

        public Cls_Paginado<Tra_Tramites_Solicitados_DocumentosView> Obtener_Documento_Solicitudes(int offset, int limit, int Tramite_Solicitados_ID)
        {
            Cls_Paginado<Tra_Tramites_Solicitados_DocumentosView> Paginado;

            var busquedaFiltro = Contexto.Tra_Tramites_Solicitados_Documentos.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .OrderBy(x => x.Tra_Documentos.Nombre_Documento)
                .Select(x => new Tra_Tramites_Solicitados_DocumentosView
                {
                    Documento_ID = x.Documento_ID,
                    Nombre_Archivo = x.Nombre_Archivo,
                    Nombre_Documento = x.Tra_Documentos.Nombre_Documento,
                    Ruta_Archivo = x.Ruta_Archivo,
                    Tramite_Solicitados_Documento_ID = x.Tramite_Solicitados_Documento_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    URL_Archivo = x.URL_Archivo
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<Tra_Tramites_Solicitados_DocumentosView>(datos_visualizar, busquedaFiltro.Count());
            return Paginado;
        }

        public TramitesSolicitudView Obtener_Una_Solicitud(int Tramite_Solicitados_ID)
        {
            TramitesSolicitudView tramitesSolicitudView = new TramitesSolicitudView();

            tramitesSolicitudView = Contexto.Tra_Tramites_Solicitados.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => new TramitesSolicitudView
                {
                    Estatus_Revision = x.Estatus_Revision,
                    Estatus = x.Estatus,
                    Costo_Tramite = x.Costo_Tramite,
                    Controlador = x.Tra_Tramites.Controlador,
                    Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                    Observaciones = x.Observaciones,
                    RPU = x.RPU == null ? "": x.RPU,
                    Tramite_ID = x.Tramite_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Usuarios_ID = x.Usuarios_ID,
                    Usuarios_Asignado_ID = x.Usuarios_Asignado_ID.HasValue ? x.Usuarios_Asignado_ID.Value : 0,
                    Nivel_Banco = x.Tra_Tramites.Nivel_Banco == null ? "" : x.Tra_Tramites.Nivel_Banco,
                    Usuario = x.Usuarios.Nombre + " " + x.Usuarios.Apellido_Paterno + " " + (x.Usuarios.Apellido_Materno == null ? " ": x.Usuarios.Apellido_Materno)

                }).FirstOrDefault();

            return tramitesSolicitudView;
        }

        public List<TramitesSolicitudView> Obtener_Solicitudes_Seleccionadas(int[] SolicitudesTramiteLista)
        {
            List<TramitesSolicitudView> Lista_Tramites_Solicitud = new List<TramitesSolicitudView>();

            Lista_Tramites_Solicitud = Contexto.Tra_Tramites_Solicitados.Where(x => SolicitudesTramiteLista.Contains(x.Tramite_Solicitados_ID))
                .OrderBy(x => x.Tramite_Solicitados_ID)
                .Select(x => new TramitesSolicitudView
                {
                    Controlador = x.Tra_Tramites.Controlador,
                    Costo_Tramite = x.Costo_Tramite,
                    Estatus = x.Estatus,
                    Estatus_Revision = x.Estatus_Revision,
                    Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                    Observaciones = x.Observaciones,
                    RPU = x.RPU == null ? "" : x.RPU,
                    Tramite_ID = x.Tramite_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                    Usuarios_ID = x.Usuarios_ID,
                    Fecha_Solicitud = x.Fecha_Creo,
                    Referencia_Pago = x.Referencia_Pago == null ? "" : x.Referencia_Pago.Trim(),
                    Seleccionado = true,
                    Nivel_Banco = x.Tra_Tramites.Nivel_Banco == null ? "" : x.Tra_Tramites.Nivel_Banco

                }).ToList();


            return Lista_Tramites_Solicitud;
        }

        public string Guardar_Asignacion(int[] SolicitudesTramiteLista, int UsuarioSelected, string Nombre_Usuario)
        {
            string respuesta = "bien";

            using (DbContextTransaction transaction = Contexto.Database.BeginTransaction()) {

                try
                {

                    foreach (int item in SolicitudesTramiteLista) {

                        Tra_Tramites_Solicitados tra_Tramites_Solicitados = Contexto.Tra_Tramites_Solicitados.Find(item);
                        tra_Tramites_Solicitados.Estatus = "ASIGNADA";
                        tra_Tramites_Solicitados.Usuarios_Asignado_ID = UsuarioSelected;
                        tra_Tramites_Solicitados.Fecha_Modifico = DateTime.Now;
                        tra_Tramites_Solicitados.Usuario_Modifico = Nombre_Usuario;
                    }
                    Contexto.SaveChanges();

                    transaction.Commit();
                    respuesta = "bien";
                }
                catch (Exception ex) {
                    transaction.Rollback();
                    respuesta = ex.Message;
                }

            }

                return respuesta;
        }

        public void Cambiar_Estatus(EFDbContext mContexto, string Estatus, int Tramite_Solicitados_ID)
        {
            Tra_Tramites_Solicitados tramites_Solicitados = mContexto.Tra_Tramites_Solicitados.Find(Tramite_Solicitados_ID);
            tramites_Solicitados.Estatus_Revision = Estatus;
            mContexto.SaveChanges();
        }

        public List<SimpleComboView> Obtener_Estatus_Solicitud()
        {
            List<SimpleComboView> Lista_Combo = new List<SimpleComboView>();

            Lista_Combo.Add(new SimpleComboView { name = "PENDIENTE" });
            Lista_Combo.Add(new SimpleComboView { name = "EN PROCESO" });
            Lista_Combo.Add(new SimpleComboView { name = "PENDIENTE PAGO" });
            Lista_Combo.Add(new SimpleComboView { name = "PAGADO" });
            Lista_Combo.Add(new SimpleComboView { name = "CANCELADO" });
            Lista_Combo.Add(new SimpleComboView { name = "TERMINADO" });

            return Lista_Combo;
        }

        public string Obtener_Nombre_Tramite(EFDbContext mContenxto, int Tramite_Solicitados_ID)
        {
            return mContenxto.Tra_Tramites_Solicitados.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID)
                .Select(x => x.Tra_Tramites.Nombre_Tramite).FirstOrDefault();
        }

        public IQueryable<TramitesSolicitudView> Obtener_Reporte_Solicitudes(string sortOrder, string FechaInicio, string FechaFin, int? Tramite_ID)
        {

            var dbSolicitudes = Contexto.Tra_Tramites_Solicitados.Where(x=> x.Tramite_Solicitados_ID > 0);

            if (!String.IsNullOrEmpty(FechaInicio))
            {
                DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                dbSolicitudes = dbSolicitudes.Where(x => x.Fecha_Creo >= dateTimeIncio);
            }

            if (!String.IsNullOrEmpty(FechaFin))
            {
                DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                dbSolicitudes = dbSolicitudes.Where(x => x.Fecha_Creo <= dateTimeFinMas);
            }

            if (Tramite_ID.HasValue)
            {
                dbSolicitudes = dbSolicitudes.Where(x => x.Tramite_ID == Tramite_ID.Value);
            }


            switch (sortOrder)
            {
                case "Nombre_Tramite":
                    dbSolicitudes = dbSolicitudes.OrderBy(x => x.Tra_Tramites.Nombre_Tramite);
                    break;
                case "fecha_asc":
                    dbSolicitudes = dbSolicitudes.OrderBy(x => x.Fecha_Creo);
                    break;
                case "nombre_tramite_desc":
                    dbSolicitudes = dbSolicitudes.OrderByDescending(x => x.Tra_Tramites.Nombre_Tramite);
                    break;
                default:
                    dbSolicitudes = dbSolicitudes.OrderByDescending(x => x.Fecha_Creo);
                    break;
            }


            return dbSolicitudes.Select(x => new TramitesSolicitudView
            {
                Controlador = x.Tra_Tramites.Controlador,
                Costo_Tramite = x.Costo_Tramite,
                Estatus = x.Estatus,
                Estatus_Revision = x.Estatus_Revision,
                Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                Observaciones = x.Observaciones,
                RPU = x.RPU == null ? "" : x.RPU,
                Tramite_ID = x.Tramite_ID,
                Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                Usuarios_ID = x.Usuarios_ID,
                Fecha_Solicitud = x.Fecha_Creo,
                Referencia_Pago = x.Referencia_Pago == null ? "" : x.Referencia_Pago.Trim(),
                Usuario = x.Usuarios.Nombre + " " + x.Usuarios.Apellido_Paterno + " " + (x.Usuarios.Apellido_Materno == null ? "" : x.Usuarios.Apellido_Materno),
                Telefono = x.Usuarios.Telefono_Celular == null ? "" : x.Usuarios.Telefono_Celular,
                NumeroMensajes = x.Tra_Mensajes_Solicitudes.Count,
                NumeroPago = x.Tra_Pagos_Solicitudes.Where(y => y.Estatus != "CANCELADO").Count(),
                Usuario_Asignado = x.UsuarioAsignado == null ? "" : x.UsuarioAsignado.Nombre + " " + x.UsuarioAsignado.Apellido_Paterno + " " + (x.UsuarioAsignado.Apellido_Materno == null ? "" : x.UsuarioAsignado.Apellido_Materno),
                Correo_Electronico = x.Usuarios.Correo_Electronico
                
            });
        }

        public IQueryable<TramitesSolicitudView> Obtener_Solicitudes_Por_Usuario(string sortOrder, string searchString, int Usuario_ID)
        {
            var dbSolicitudTramite = Contexto.Tra_Tramites_Solicitados.Where(x => x.Usuarios_ID == Usuario_ID && x.Estatus != "CANCELADO");

            if (!String.IsNullOrEmpty(searchString))
            {
                dbSolicitudTramite = dbSolicitudTramite.Where(s => s.Tra_Tramites.Nombre_Tramite.Contains(searchString)
                || s.Observaciones.Contains(searchString) || s.Tramite_Solicitados_ID.ToString().Contains(searchString));
            }

            switch (sortOrder)
            {
                case "nombre_tramite_desc":
                    dbSolicitudTramite = dbSolicitudTramite.OrderBy(x => x.Tra_Tramites.Nombre_Tramite);
                    break;
                default:
                    dbSolicitudTramite = dbSolicitudTramite.OrderByDescending(x => x.Tramite_Solicitados_ID);
                    break;
            }


            return dbSolicitudTramite.Select(x => new TramitesSolicitudView
            {
                Controlador = x.Tra_Tramites.Controlador,
                Costo_Tramite = x.Costo_Tramite,
                Estatus = x.Estatus,
                Estatus_Revision = x.Estatus_Revision,
                Nombre_Tramite = x.Tra_Tramites.Nombre_Tramite,
                Observaciones = x.Observaciones,
                RPU = x.RPU == null ? "" : x.RPU,
                Tramite_ID = x.Tramite_ID,
                Tramite_Solicitados_ID = x.Tramite_Solicitados_ID,
                Usuarios_ID = x.Usuarios_ID,
                Fecha_Solicitud = x.Fecha_Creo,
                Referencia_Pago = x.Referencia_Pago == null ? "" : x.Referencia_Pago.Trim(),
                Usuario = x.Usuarios.Nombre + " " + x.Usuarios.Apellido_Paterno + " " + (x.Usuarios.Apellido_Materno == null ? "" : x.Usuarios.Apellido_Materno),
                Telefono = x.Usuarios.Telefono_Celular == null ? "" : x.Usuarios.Telefono_Celular,
                NumeroMensajes = x.Tra_Mensajes_Solicitudes.Count,
                NumeroPago = x.Tra_Pagos_Solicitudes.Where(y => y.Estatus != "CANCELADO").Count(),
                Usuario_Asignado = x.UsuarioAsignado == null ? "" : x.UsuarioAsignado.Nombre + " " + x.UsuarioAsignado.Apellido_Paterno + " " + (x.UsuarioAsignado.Apellido_Materno == null ? "" : x.UsuarioAsignado.Apellido_Materno),
                Correo_Electronico = x.Usuarios.Correo_Electronico

            });


        }
    }
}
