﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using System.Data.SqlClient;
using System.Security.Cryptography;


namespace ModelDomain.Contexto
{
    public class EFPagos_Repo : IPagos_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public List<Pagos_Solicitudes_Detalle_View> Obtener_Pagos_Detalle(int Pago_Solicitudes_ID)
        {
            return Contexto.Tra_Pagos_Solicitudes_Detalle.Where(x => x.Pago_Solicitudes_ID == Pago_Solicitudes_ID)
                .OrderBy(x => x.Concepto)
                .Select(x => new Pagos_Solicitudes_Detalle_View
                {
                    Cantidad = x.Cantidad,
                    Concepto = x.Concepto,
                    Concepto_ID = x.Concepto_ID,
                    Importe = x.Importe,
                    Precio_Unitario = x.Precio_Unitario
                }).ToList();
        }

        public Form_Pago_Multipagos Obtener_Parametro_Pago(TramitesSolicitudView tramitesSolicitudView, Pagos_Solicitudes_View pagos_Solicitudes_View)
        {
            Form_Pago_Multipagos form_Pago_Multipagos = new Form_Pago_Multipagos();

            var encoding = new System.Text.ASCIIEncoding();

            string llave = "0Bd59d959a54Ae1d503e2d051e04c9dB26bc3659aed013827132ea796fb35$D5f";
            string cadena_proteger = "";
            string cadena_hash;
            string referencia = pagos_Solicitudes_View.Codigo_Barras;
            cadena_proteger = pagos_Solicitudes_View.Pago_Solicitudes_ID.ToString() + referencia + pagos_Solicitudes_View.Total.ToString();

            byte[] keyByte = encoding.GetBytes(llave);
            byte[] messageBytes = encoding.GetBytes(cadena_proteger);

            HMACSHA256 hMACSHA256 = new HMACSHA256(keyByte);
           // HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
            byte[] hashmessage1 = hMACSHA256.ComputeHash(messageBytes);
            cadena_hash = ByteToString(hashmessage1);


            form_Pago_Multipagos.mp_order = pagos_Solicitudes_View.Pago_Solicitudes_ID.ToString();
            form_Pago_Multipagos.mp_reference = referencia;

            //SIMAPAG en ambiente preproductivo
            //form_Pago_Multipagos.mp_account = "11090";

            form_Pago_Multipagos.mp_node = "2";
            form_Pago_Multipagos.mp_concept = tramitesSolicitudView.Nivel_Banco;
            form_Pago_Multipagos.mp_amount = pagos_Solicitudes_View.Total;
            form_Pago_Multipagos.mp_customername = tramitesSolicitudView.Usuario;
            form_Pago_Multipagos.mp_currency = 1;
            form_Pago_Multipagos.mp_signature = cadena_hash.ToLower();

            

            return form_Pago_Multipagos;

        }

        public  string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }

        List<Pagos_Solicitudes_View> IPagos_Repo.Obtener_Pagos(int Tramite_Solicitados_ID)
        {
            return Contexto.Tra_Pagos_Solicitudes.Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID
                && x.Estatus != "CANCELADO")
                .OrderByDescending(x=> x.Fecha_Creo)
                .AsEnumerable()
                .Select(x => new Pagos_Solicitudes_View
                {
                    Codigo_Barras = x.Codigo_Barras,
                    Estatus = x.Estatus,
                    Importe = x.Importe,
                    Fecha_Creo = x.Fecha_Creo.ToString("dd/MM/yyyy"),
                    IVA = x.IVA,
                    No_Diverso = x.No_Diverso,
                    Total = x.Total,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID
                    

                }).ToList();
        }

        public Pagos_Solicitudes_View Obtener_Un_Pago(int Pago_Solicitudes_ID)
        {
            Pagos_Solicitudes_View pagos_Solicitudes_View = new Pagos_Solicitudes_View();

            pagos_Solicitudes_View = Contexto.Tra_Pagos_Solicitudes.Where(x => x.Pago_Solicitudes_ID == Pago_Solicitudes_ID)
                .Select(x => new Pagos_Solicitudes_View
                {
                    Codigo_Barras = x.Codigo_Barras,
                    Importe = x.Importe,
                    IVA = x.IVA,
                    Pago_Solicitudes_ID = x.Pago_Solicitudes_ID,
                    Estatus = x.Estatus,
                    No_Diverso = x.No_Diverso,
                    Total = x.Total,
                    Usuarios_ID = x.Usuarios_ID,
                    Usuario_Creo = x.Usuario_Creo,
                    Tramite_Solicitados_ID = x.Tramite_Solicitados_ID
                    
                }).FirstOrDefault();

            return pagos_Solicitudes_View;
        }

        public void Actualizar_Estatus_Pagos(EFDbContext ContextoTransaccion, int Pago_Solicitudes_ID)
        {
            Tra_Pagos_Solicitudes tra_Pagos_Solicitudes = ContextoTransaccion.Tra_Pagos_Solicitudes.Find(Pago_Solicitudes_ID);
            tra_Pagos_Solicitudes.Estatus = "PAGADO";
            ContextoTransaccion.SaveChanges();
        }

        public int Obtener_Pagos_Pendientes(EFDbContext ContextoTransaccion, int Tramite_Solicitados_ID)
        {
            return ContextoTransaccion.Tra_Pagos_Solicitudes
                .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID && x.Estatus == "PENDIENTE").Count();
        }

        public string Obtener_Hash(Form_Pago_Multipagos form_Pago_Multipagos)
        {
            string respuesta = "";

            var encoding = new System.Text.ASCIIEncoding();

            string llave = "0Bd59d959a54Ae1d503e2d051e04c9dB26bc3659aed013827132ea796fb35$D5f";
            string cadena_proteger = "";
            string cadena_hash;
            string referencia = form_Pago_Multipagos.mp_reference;
            cadena_proteger = form_Pago_Multipagos.mp_order + referencia + form_Pago_Multipagos.cantidad_cadena.Trim() + form_Pago_Multipagos.mp_authorization;

            byte[] keyByte = encoding.GetBytes(llave);
            byte[] messageBytes = encoding.GetBytes(cadena_proteger);

            HMACSHA256 hMACSHA256 = new HMACSHA256(keyByte);
            byte[] hashmessage1 = hMACSHA256.ComputeHash(messageBytes);
            cadena_hash = ByteToString(hashmessage1);

            respuesta = cadena_hash.ToLower() ;

            return respuesta;
        }

        public int Obtner_Pagos_Pagados(EFDbContext ContextoTransaccion, int Tramite_Solicitados_ID)
        {
            return ContextoTransaccion.Tra_Pagos_Solicitudes
               .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID && x.Estatus == "PAGADO").Count();
        }

        public int Obtener_Pagos_Cantidad(string Estatus, int Tramite_Solicitados_ID)
        {
            return Contexto.Tra_Pagos_Solicitudes
               .Where(x => x.Tramite_Solicitados_ID == Tramite_Solicitados_ID && x.Estatus == Estatus).Count();
        }
    }
}
