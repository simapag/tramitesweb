﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;


namespace ModelDomain.Contexto
{
    public class EFParametrosCorreo_Repo : IParametroCorreo_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public ParametrosCorreoView ObtenerParametros()
        {
           return   Contexto.Tra_Parametros_Correo
                .Select(x => new ParametrosCorreoView
                {
                    Email = x.Email,
                    Host_Email = x.Host_Email,
                    Parametro_Correo_ID = x.Parametro_Correo_ID,
                    Password = x.Password,
                    Puerto = x.Puerto,
                    Usuario_Creo = x.Usuario_Creo
                }).FirstOrDefault();
        }
    }
}
