﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;

namespace ModelDomain.Contexto
{
    
  public  class EFAcceso_Repositorio : IAcceso_Respositorio
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Acceso> Accesos => context.Accesos;

        public void Guardar_Accesos(List<Acceso> p_Lista_Accesos_Nuevos, List<Acceso> p_Lista_Acceso_Anterior, string p_Usuario_Creo)
        {
            using (System.Transactions.TransactionScope sp = new System.Transactions.TransactionScope())
            {


                    
                try
                {

                    foreach (Acceso Lista_Anteior in p_Lista_Acceso_Anterior)
                    {

                        context.Accesos.Remove(Lista_Anteior);
                    }

                    foreach (Acceso Lista_Nueva in p_Lista_Accesos_Nuevos)
                    {
                        if (Lista_Nueva.Menu_ID != 0)
                        {
                            Lista_Nueva.Fecha_Creo = DateTime.Now;
                            Lista_Nueva.Usuario_Creo = p_Usuario_Creo;
                            context.Accesos.Add(Lista_Nueva);
                        }


                    }
                    context.SaveChanges();

                    sp.Complete();
                   

                }
                catch (Exception ex)
                {

                }

            }
        }
    }
}
