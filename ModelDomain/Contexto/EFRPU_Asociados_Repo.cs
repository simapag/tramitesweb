﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using ModelDomain.Dto;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
   public class EFRPU_Asociados_Repo : IAsociarRPU_Repo
    {
        private EFDbContext Contexto = new EFDbContext();
        private EFDbContextSIMAPAG ContextoSimapag = new EFDbContextSIMAPAG();
        private string Str_Sql = "";

        public string Eliminar_RPU_Asociados(int RPU_Asociados_ID)
        {
            string respuesta = "";

            try
            {
                Tra_RPU_Asociados tra_RPU_Asociados = Contexto.Tra_RPU_Asociados.Find(RPU_Asociados_ID);
                Contexto.Tra_RPU_Asociados.Remove(tra_RPU_Asociados);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }
           return respuesta;
        }

        public string Esta_Repetido_RPU(string RPU, int Usuarios_ID)
        {
            string Respuesta = "NO";

            var buscar = Contexto.Tra_RPU_Asociados.Where(x => x.RPU == RPU && x.Usuarios_ID == Usuarios_ID);

            if (buscar.Any()) {
                Respuesta = "SI";
            }

            return Respuesta;
        }

        public void Guardar_RPU_Asociados(RPUAsociadosView rPUAsociadosView)
        {

            Tra_RPU_Asociados tra_RPU_Asociados = new Tra_RPU_Asociados();

            tra_RPU_Asociados.RPU = rPUAsociadosView.RPU;
            tra_RPU_Asociados.Fecha_Creo = DateTime.Now;
            tra_RPU_Asociados.Usuarios_ID = rPUAsociadosView.Usuarios_ID;
            tra_RPU_Asociados.Predio_ID = rPUAsociadosView.Predio_ID;
            tra_RPU_Asociados.Usuario_Creo = rPUAsociadosView.Usuario_Creo;

            Contexto.Tra_RPU_Asociados.Add(tra_RPU_Asociados);
            Contexto.SaveChanges();

        }

        public List< DatosCuentaView> Obtener_Datos_Cuenta(string RPU)
        {

            List<DatosCuentaView> Lista_Datos_Cuenta = new List<DatosCuentaView>();

            Str_Sql = "SELECT p.RPU " +
	        ",isnull(u.NOMBRE, '') + ' ' + ISNULL(u.APELLIDO_PATERNO, '') + ' ' + ISNULL(u.APELLIDO_MATERNO, '') AS[Nombre_Usuario] " +
	        ",p.Predio_ID " +
	        ",c.NOMBRE AS[Colonia] " +
            ", v.NOMBRE + ' ' + l.NOMBRE AS [Calle] " +
            " , ISNULL(p.Numero_Exterior, '') AS[Numero_Exterior] " +
	        ",ISNULL(p.Numero_Interior, '') AS[Numero_Interior] " +
            ",isnull(p.No_Cuenta,'') as [No_Cuenta] " +
        "FROM Cat_Cor_Predios p " +
        "JOIN Cat_Cor_Usuarios u ON p.Usuario_ID = u.USUARIO_ID " +
        "JOIN Cat_Cor_Colonias c ON c.COLONIA_ID = p.Colonia_ID " +
        "JOIN Cat_Cor_Calles l ON l.CALLE_ID = p.Calle_ID " +
        "JOIN  CAT_COR_VIALIDADES v on v.VIALIDAD_ID = l.VIALIDAD_ID  " +
        "WHERE p.RPU =@rpu and p.Estatus='ACTIVO'";


            //Lista_Datos_Cuenta = ContextoSimapag.Database.SqlQuery<DatosCuentaView>(Str_Sql, new SqlParameter("@rpu", RPU)).ToList();
            Lista_Datos_Cuenta = ContextoSimapag.Database.SqlQuery<DatosCuentaView>("sp_obtener_datos_cuenta_pago_tramite @rpu", new SqlParameter("@rpu", RPU)).ToList();

            return Lista_Datos_Cuenta;
        }

        public Cls_Paginado<RPUAsociadosView> Obtener_RPU_Asociados(int offset, int limit, int Usuarios_ID)
        {
            Cls_Paginado<RPUAsociadosView> Paginado;


            var busquedaFiltro = Contexto.Tra_RPU_Asociados.Where(x => x.Usuarios_ID == Usuarios_ID)
                .OrderBy(x => x.RPU)
                .Select(x => new RPUAsociadosView
                {
                    Predio_ID = x.Predio_ID,
                    RPU_Asociados_ID = x.RPU_Asociados_ID,
                    RPU = x.RPU,
                    Usuarios_ID = x.Usuarios_ID
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();
            Paginado = new Cls_Paginado<RPUAsociadosView>(datos_visualizar, busquedaFiltro.Count());
            return Paginado;
        }

        public List<RPUAsociadosView> Obtener_RPU_Asociados(int Usuario_ID)
        {
            List<RPUAsociadosView> listaRPU = new List<RPUAsociadosView>();

            listaRPU = Contexto.Tra_RPU_Asociados.Where(x => x.Usuarios_ID == Usuario_ID)
                        .Select(x => new RPUAsociadosView
                        {
                            Predio_ID = x.Predio_ID,
                            RPU_Asociados_ID = x.RPU_Asociados_ID,
                            RPU = x.RPU,
                            Usuarios_ID = x.Usuarios_ID
                        }).ToList();


            return listaRPU;
        }

        public List<ComboView> Obtener_RPU_Asociados_Combo(int Usuarios_ID)
        {
            List<ComboView> Lista_RPU_Asociados = new List<ComboView>();

            Lista_RPU_Asociados = Contexto.Tra_RPU_Asociados
                .Where(x=> x.Usuarios_ID == Usuarios_ID)
                .OrderBy(x => x.RPU)
                .Select(x => new ComboView
                {
                    id= x.RPU_Asociados_ID,
                    name = x.RPU
                }).ToList();

            return Lista_RPU_Asociados;
        }
    }
}
