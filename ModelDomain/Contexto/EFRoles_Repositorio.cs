﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Interfaces;
using ModelDomain.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace ModelDomain.Contexto
{

    /// <summary>
    /// Implementación de la intefarce de roles
    /// </summary>

    public class EFRoles_Repositorio : IRoles_Repositorio
    {
        /// <summary>
        /// El contexto de la base de datos
        /// </summary>
        private EFDbContext Contexto = new EFDbContext();

        public IQueryable<Roles> Roles {
             get { return Contexto.Roles; }
        }

        public IQueryable<Menu> Menu => Contexto.Menus;

        public IQueryable<Acceso> Accesos => Contexto.Accesos;

        public string Eliminar_Rol(int p_Rol_ID)
        {
            string respuesta = "";
            try
            {
                Roles dbRolesEntrada = Contexto.Roles.Find(p_Rol_ID);
                Contexto.Roles.Remove(dbRolesEntrada);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch (DbUpdateException e) {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                            

                    }
                }

            }

            return respuesta;
        }

        public string Es_Rol_Ejecutivo(int Rol_ID)
        {
            string respuesta = "NO";


            var buscar = Contexto.Roles.Where(x => x.Rol_ID == Rol_ID).FirstOrDefault();

            if (buscar != null) {

                if (buscar.Nombre.ToLower().Equals("ejecutivo")) {

                    respuesta = "SI";
                }
            }

            return respuesta;
        }

        public void Guardar_Roles(Roles p_Roles)
        {

            if (p_Roles.Rol_ID == 0)
            {
                Contexto.Roles.Add(p_Roles);
            }
            else {

                Roles dBRolesEntrada = Contexto.Roles.Find(p_Roles.Rol_ID);
                dBRolesEntrada.Nombre = p_Roles.Nombre;
                dBRolesEntrada.Descripcion = p_Roles.Descripcion;
                dBRolesEntrada.Estatus = p_Roles.Estatus;
                dBRolesEntrada.Fecha_Modifico = p_Roles.Fecha_Modifico;
                dBRolesEntrada.Usuario_Modifico = p_Roles.Usuario_Modifico;

            }
            Contexto.SaveChanges();

        }
    }
}
