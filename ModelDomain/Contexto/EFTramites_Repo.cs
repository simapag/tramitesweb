﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Dto;
using ModelDomain.Entidades;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;

namespace ModelDomain.Contexto
{
   public class EFTramites_Repo : ITramites_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public string Eliminar_Tramite(TramitesView tramitesView)
        {
            string respuesta = "";

            try {
                Tra_Tramites tra_Tramites = Contexto.Tra_Tramites.Find(tramitesView.Tramite_ID);
                Contexto.Tra_Tramites.Remove(tra_Tramites);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch(DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }

            }

            return respuesta;
        }

        public string Esta_Repetido_Tramite(string Nombre_Tramite, int Tramite_ID)
        {
            string respuesta = "NO";

            var buscarTramite = Contexto.Tra_Tramites.Where(x => x.Nombre_Tramite == Nombre_Tramite && x.Tramite_ID != Tramite_ID);

            if (buscarTramite.Any())
            {
                respuesta = "SI";
            }

            return respuesta;
        }

        public void Guardar_Tramite(TramitesView tramitesView)
        {
            if (tramitesView.Tramite_ID == 0)
            {
                Tra_Tramites tra_Tramites = new Tra_Tramites();
                tra_Tramites.Nombre_Tramite = tramitesView.Nombre_Tramite;
                tra_Tramites.Estatus = tramitesView.Estatus;
                tra_Tramites.Costo_Tramite = tramitesView.Costo_Tramite;
                tra_Tramites.Usuario_Creo = tramitesView.Usuario_Creo;
                tra_Tramites.Nivel_Banco = tramitesView.Nivel_Banco;
                if (tramitesView.Controlador != null)
                    tra_Tramites.Controlador = tramitesView.Controlador;
                else
                    tra_Tramites.Controlador = "";
                tra_Tramites.Fecha_Creo = DateTime.Now;
                Contexto.Tra_Tramites.Add(tra_Tramites);
            }
            else {

                Tra_Tramites tra_Tramites = Contexto.Tra_Tramites.Find(tramitesView.Tramite_ID);
                tra_Tramites.Nombre_Tramite = tramitesView.Nombre_Tramite;
                tra_Tramites.Estatus = tramitesView.Estatus;
                tra_Tramites.Nivel_Banco = tramitesView.Nivel_Banco;
                if (tramitesView.Controlador != null)
                    tra_Tramites.Controlador = tramitesView.Controlador;
                else
                    tra_Tramites.Controlador = "";

                tra_Tramites.Costo_Tramite = tramitesView.Costo_Tramite;
                tra_Tramites.Usuario_Modifico = tramitesView.Usuario_Creo;
                tra_Tramites.Fecha_Modifico = DateTime.Now;

            }

            Contexto.SaveChanges();
        }

        public decimal Obtener_Costos_Tramite(int Tramite_ID)
        {
            return Contexto.Tra_Tramites.Where(x => x.Tramite_ID == Tramite_ID).Select(x => x.Costo_Tramite).FirstOrDefault();
        }

        public Cls_Paginado<TramitesView> Obtener_Tramites(int offset, int limit, string search)
        {
            Cls_Paginado<TramitesView> Paginado;

            var busquedaFiltro = Contexto.Tra_Tramites.Where(x => string.IsNullOrEmpty(search) || x.Nombre_Tramite.Contains(search))
                 .OrderBy(x => x.Nombre_Tramite)
                 .Select(x => new TramitesView
                 {
                     Costo_Tramite = x.Costo_Tramite,
                     Estatus = x.Estatus,
                     Nombre_Tramite = x.Nombre_Tramite,
                     Tramite_ID = x.Tramite_ID,
                     Controlador = x.Controlador,
                     Usuario_Creo = x.Usuario_Creo,
                     Nivel_Banco = x.Nivel_Banco

                 });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();

            Paginado = new Cls_Paginado<TramitesView>(datos_visualizar, busquedaFiltro.Count());
           

            return Paginado;
        }

        public List<ComboView> Obtener_Tramites_Combo()
        {
            List<ComboView> Lista_Tramites = new List<ComboView>();

            var Lista = Contexto.Tra_Tramites.Where(x => x.Estatus == "ACTIVO")
                .OrderBy(x => x.Nombre_Tramite)
                .Select(x => new ComboView
                {
                    id = x.Tramite_ID,
                    name = x.Nombre_Tramite

                }).ToList();

            Lista_Tramites.Add(new ComboView { id = -1, name = "Seleccione" });

            foreach (ComboView item in Lista)
            {
                Lista_Tramites.Add(new ComboView { id = item.id, name =item.name });
            }

            

            return Lista_Tramites;
        }

        public List<ComboView> Obtener_Tramites_Combo_Especial()
        {
            List<ComboView> Lista_Tramites = new List<ComboView>();
            return Lista_Tramites = Contexto.Tra_Tramites.Where(x => x.Estatus == "ACTIVO")
              .OrderBy(x => x.Nombre_Tramite)
              .Select(x => new ComboView
              {
                  id = x.Tramite_ID,
                  name = x.Nombre_Tramite

              }).ToList();
        }
    }
}
