﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Interfaces;
using ModelDomain.ViewModel;
using ModelDomain.Entidades;
using ModelDomain.Util;

namespace ModelDomain.Contexto
{
    public class EFPagos_Linea_No_Registrados_Repo : IPagos_Linea_No_Registrados_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public void Registrar(Pago_En_Linea_No_Aplicado_View no_Aplicado_View)
        {
            Tra_Pagos_En_Linea_No_Aplicados tra_Pagos_En_Linea_No_Aplicados = new Tra_Pagos_En_Linea_No_Aplicados();
            tra_Pagos_En_Linea_No_Aplicados.Fecha_Creo = DateTime.Now;
            tra_Pagos_En_Linea_No_Aplicados.Folio_Pago = no_Aplicado_View.Folio_Pago;
            tra_Pagos_En_Linea_No_Aplicados.Importe = no_Aplicado_View.Importe;
            tra_Pagos_En_Linea_No_Aplicados.No_Factura = no_Aplicado_View.No_Factura;
            tra_Pagos_En_Linea_No_Aplicados.Referencia_Bancaria = no_Aplicado_View.Referencia_Bancaria;
            tra_Pagos_En_Linea_No_Aplicados.RPU = no_Aplicado_View.RPU;
            tra_Pagos_En_Linea_No_Aplicados.Tipo_Pago = no_Aplicado_View.Tipo_Pago;
            tra_Pagos_En_Linea_No_Aplicados.Tipo_Tarjeta = no_Aplicado_View.Tipo_Tarjeta;
            tra_Pagos_En_Linea_No_Aplicados.Estatus = "PENDIENTE";
            Contexto.Tra_Pagos_En_Linea_No_Aplicados.Add(tra_Pagos_En_Linea_No_Aplicados);
            Contexto.SaveChanges();
        }

        public void Registrar_Pagos_No_Aplicados(PagosEnLineaReciboView pagosEnLineaReciboView)
        {

            try
            {
                Tra_Pagos_En_Linea_No_Aplicados tra_Pagos_En_Linea_No_Aplicados = new Tra_Pagos_En_Linea_No_Aplicados();
                tra_Pagos_En_Linea_No_Aplicados.Fecha_Creo = DateTime.Now;
                tra_Pagos_En_Linea_No_Aplicados.Folio_Pago = pagosEnLineaReciboView.Folio_Pago;
                tra_Pagos_En_Linea_No_Aplicados.Importe = pagosEnLineaReciboView.Importe;
                tra_Pagos_En_Linea_No_Aplicados.No_Factura = pagosEnLineaReciboView.No_Factura_Recibo;
                tra_Pagos_En_Linea_No_Aplicados.Referencia_Bancaria = pagosEnLineaReciboView.Referencia_Bancaria;
                tra_Pagos_En_Linea_No_Aplicados.RPU = pagosEnLineaReciboView.RPU;
                tra_Pagos_En_Linea_No_Aplicados.Tipo_Pago = pagosEnLineaReciboView.Tipo_Pago;
                tra_Pagos_En_Linea_No_Aplicados.Tipo_Tarjeta = pagosEnLineaReciboView.Tipo_Tarjeta;
                tra_Pagos_En_Linea_No_Aplicados.Estatus = "PENDIENTE";
                Contexto.Tra_Pagos_En_Linea_No_Aplicados.Add(tra_Pagos_En_Linea_No_Aplicados);
                Contexto.SaveChanges();
            }
            catch (Exception ex) {
              
            } 

        }
    }
}
