﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;

namespace ModelDomain.Interfaces
{
   public interface ITramites_Documento_Repo
    {
        Cls_Paginado<Tramites_Documento_View> Obtener_Tramites_Documentos(int offset, int limit, int Tramite_ID);
        void Guardar_Tramites_Documento(Tramites_Documento_View tramites_Documento_View);
        string Esta_Repetido(int Tramite_ID, int Documento_ID);
        string Eliminar_Tramite_Documento(int Tramite_Documento_ID);
        List<ComboView> Obtener_Documentos_Por_Tramite(int Tramite_ID);

    }
}
