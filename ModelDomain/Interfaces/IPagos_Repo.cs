﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;
using ModelDomain.Contexto;

namespace ModelDomain.Interfaces
{
   public interface IPagos_Repo
    {
        List<Pagos_Solicitudes_View> Obtener_Pagos(int Tramite_Solicitados_ID);
        List<Pagos_Solicitudes_Detalle_View> Obtener_Pagos_Detalle(int Pago_Solicitudes_ID);

        Pagos_Solicitudes_View Obtener_Un_Pago(int Pago_Solicitudes_ID);

        Form_Pago_Multipagos Obtener_Parametro_Pago(TramitesSolicitudView tramitesSolicitudView, Pagos_Solicitudes_View pagos_Solicitudes_View);

        void Actualizar_Estatus_Pagos(EFDbContext ContextoTransaccion, int Pago_Solicitudes_ID);

        int Obtener_Pagos_Pendientes(EFDbContext ContextoTransaccion, int Tramite_Solicitados_ID);
        int Obtner_Pagos_Pagados(EFDbContext ContextoTransaccion, int Tramite_Solicitados_ID);
        int Obtener_Pagos_Cantidad(string Estatus, int Tramite_Solicitados_ID);

        string Obtener_Hash(Form_Pago_Multipagos form_Pago_Multipagos);
    }
}
