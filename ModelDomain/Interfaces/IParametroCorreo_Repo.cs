﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;


namespace ModelDomain.Interfaces
{
   public interface IParametroCorreo_Repo
    {

        ParametrosCorreoView ObtenerParametros();
    }
}
