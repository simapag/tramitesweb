﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;
using ModelDomain.Contexto;

namespace ModelDomain.Interfaces
{
   public interface ITramitesProceso_Repo
    {

        List<TramitesView> Obtener_Tramites();
        TramitesView Obtener_Un_Tramite(int Tramite_ID);

        // se van agregar los diverson campos que tiene cada trámite en particular
        void Guardar_Correccion_Domicilio(EFDbContext mContenxto, FormularioCorreccionDomicilioView formularioCorreccion);
        void Editar_Correccion_Domicilio(EFDbContext mContenxto, FormularioCorreccionDomicilioView formularioCorreccion);
        FormularioCorreccionDomicilioView Obtener_Formulario_Correccion(int Tramite_Solicitados_ID);


        void Guardar_Suspension(EFDbContext mContexto, FormularioSuspensionView formularioSuspension);
        void Editar_Suspension(EFDbContext mContexto, FormularioSuspensionView formularioSuspension);
        FormularioSuspensionView Obtener_Formulario_Suspension(int Tramite_Solicitados_ID);


        void Guardar_Actualizacion_Titular(EFDbContext mContexto, FormActualizarContratoView formActualizar);
        void Editar_Actualizacion_Titular(EFDbContext mContexto, FormActualizarContratoView formActualizar);
        FormActualizarContratoView Obtener_Formulario_Actualizar_Titular(int Tramite_Solicitados_ID);

        void Guardar_Datos_Factibilidad(EFDbContext mContexto, Formulario_Factibilidades_View formulario_Factibilidades_View);
        void Editar_Datos_Factibilidad(EFDbContext mContexto, Formulario_Factibilidades_View formulario_Factibilidades_View);
        Formulario_Factibilidades_View Obtener_Formulario_Factibilidades(int Tramite_Solicitados_ID);


        void Guardar_Datos_Domiciliacion(EFDbContext mContexto, FormDomicialicionView formDomicialicionView);
        void Editar_Datos_Domiciliacion(EFDbContext mContexto, FormDomicialicionView formDomicialicionView);
        FormDomicialicionView Obtener_Formulario_Domiciliacion(int Tramite_Solicitados_ID);


        void Guardar_Datos_Constancia(EFDbContext mContexto, FormularioConstanciaView formularioConstanciaView);
        void Editar_Datos_Constancia(EFDbContext mContexto, FormularioConstanciaView formularioConstanciaView);
        FormularioConstanciaView Obtener_Formulario_Constancia(int Tramite_Solicitados_ID);

        void Guardar_Datos_Cancelar_Servicio(EFDbContext mContexto, FormCancelarServicioView formCancelarServicioView);
        void Editar_Datos_Cancelar_Servicio(EFDbContext mContexto, FormCancelarServicioView formCancelarServicioView);
        FormCancelarServicioView Obtener_Formulario_Cancelar(int Tramite_Solicitados_ID);

        void Guardar_Datos_Servicio_Operativo(EFDbContext mContexto, FormServicioOperativoView formServicioOperativoView);
        void Editar_Datos_Servicio_Operativo(EFDbContext mContexto, FormServicioOperativoView formServicioOperativoView);
        FormServicioOperativoView Obtener_Formulario_Servicio_Operativo(int Tramite_Solicitados_ID);


        void Guarar_Datos_Fiscales(EFDbContext mContexto, FormularioDatosFiscalesView formularioDatosFiscalesView);
        void Editar_Datos_Fiscales(EFDbContext mContexto, FormularioDatosFiscalesView formularioDatosFiscalesView);
        FormularioDatosFiscalesView Obtener_Formulario_Datos_Fiscales(int Tramite_Solicitados_ID);

    }
}
