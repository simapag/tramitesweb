﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;

namespace ModelDomain.Interfaces
{
   public interface IDocuemento_Repo
    {

        Cls_Paginado<DocumentosView> Obtener_Documentos(int offset, int limit, string search);
        string Esta_Repetido_Documento(string Nombre_Documento, int Documento_ID);
        string Eliminar_Documento(DocumentosView documentosView);
        void Registrar_Documento(DocumentosView documentosView);
        List<ComboView> Obtener_Documentos_Combo();
    }
}
