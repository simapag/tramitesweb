﻿using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Interfaces
{
  public  interface IComprobanteCfedi_Repo
    {
        List<ComboView> Obtener_Comprobante_Cdfi_Combo();

    }
}
