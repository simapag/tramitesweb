﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;


namespace ModelDomain.Interfaces
{
   public interface ICostos_Repo
    {
        Cls_Paginado<CostosView> Obtener_Costo_Factibilidad(int offset, int limit);
        Cls_Paginado<CostosView> Obtener_Costo_Desazolve(int offset, int limit);

    }
}
