﻿using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Interfaces
{
   public interface IPagos_Linea_No_Registrados_Repo
    {

        void Registrar(Pago_En_Linea_No_Aplicado_View no_Aplicado_View);
        void Registrar_Pagos_No_Aplicados(PagosEnLineaReciboView pagosEnLineaReciboView);
    }
}
