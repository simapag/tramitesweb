﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Entidades;
using ModelDomain.Dto;
using ModelDomain.Contexto;

namespace ModelDomain.Interfaces
{
   public interface ITramitesSolicitudes_Repo
    {
        IQueryable<Tra_Tramites_Solicitados> Tra_Tramites_Solicitados { get; }

        Cls_Paginado<Tra_Tramites_Solicitados_DocumentosView>Obtener_Documento_Solicitudes(int offset, int limit, int Tramite_Solicitados_ID);
        int Guardar_Solicitud_Tramite(EFDbContext mContenxto, TramitesSolicitudView tramitesSolicitudView, List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos);
        void Editar_Solicitud_Tramite(EFDbContext mContenxto, TramitesSolicitudView tramitesSolicitudView, List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos);
        void Guardar_Archivos(List<Tra_Tramites_Solicitados_DocumentosView> listaDocumentos , string rutaPrimeraParte, string rutaURL);
        TramitesSolicitudView Obtener_Una_Solicitud(int Tramite_Solicitados_ID);

        List<TramitesSolicitudView> Obtener_Solicitudes_Seleccionadas(int[] SolicitudesTramiteLista);

        string Guardar_Asignacion(int[] SolicitudesTramiteLista, int UsuarioSelected, string Nombre_Usuario);

        void Cambiar_Estatus(EFDbContext mContexto, string Estatus, int Tramite_Solicitados_ID);

        List<SimpleComboView> Obtener_Estatus_Solicitud();
        string Obtener_Nombre_Tramite(EFDbContext mContenxto, int Tramite_Solicitados_ID);

        IQueryable<TramitesSolicitudView> Obtener_Reporte_Solicitudes(string sortOrder, string FechaInicio, string FechaFin, int? Tramite_ID);
        IQueryable<TramitesSolicitudView> Obtener_Solicitudes_Por_Usuario(string sortOrder, string searchString, int Usuario_ID);
    }
}
