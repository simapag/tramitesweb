﻿using ModelDomain.Contexto;
using ModelDomain.Entidades;
using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Interfaces
{
   public interface IPagosEnLinea_Repo
    {
        IQueryable<Tra_Pago_En_Linea> Tra_Pago_En_Linea { get; }
        void Registrar_Pago(EFDbContext ContextoTransaccion, PagosEnLineaView pagosEnLineaView);
        bool Existe_EL_Pago(int Pago_Solicitudes_ID);

        void Registrar_Pago_Recibo(PagosEnLineaReciboView pagosEnLineaReciboView);
        bool Existe_El_Pago_Recibo(string No_Factura_Recibo);

        IQueryable<PagosEnLineaReciboView> Obtener_Pagos_En_Linea_Recibo(string sortOrder, string FechaInicio, string FechaFin, string RPU, string Folio_Pago_Bancomer);
    }
}
