﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;

namespace ModelDomain.Interfaces
{
   public interface IAcceso_Respositorio
    {
        /// <summary>
        /// Obtiene la colección de los accesos
        /// </summary>
        IQueryable<Acceso> Accesos { get; }
        /// <summary>
        /// Guarda los accesos en la base de datos
        /// </summary>
        /// <param name="p_Lista_Accesos_Nuevos"></param>
        /// <param name="p_Lista_Acceso_Anterior"></param>
        /// <param name="p_Usuario_Creo"></param>
        void Guardar_Accesos(List<Acceso> p_Lista_Accesos_Nuevos, List<Acceso> p_Lista_Acceso_Anterior, string p_Usuario_Creo);


    }
}
