﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;

namespace ModelDomain.Interfaces
{ 
    /// <summary>
    /// Interfaz Roles
    /// </summary>
   public interface IRoles_Repositorio
    {

        /// <summary>
        /// Coleccion  de datos menu
        /// </summary>
        IQueryable<Menu> Menu { get; }
        /// <summary>
        /// Coloccion de datos acceso
        /// </summary>
        IQueryable<Acceso> Accesos { get; }

        /// <summary>
        /// Obtiene La coolección de datos de roles
        /// </summary>
        IQueryable<Roles> Roles { get; }

        /// <summary>
        /// Registra y Edita una Rol en la base de datos
        /// </summary>
        /// <param name="p_Roles">Entidad roles de Roles</param>
        void Guardar_Roles(Roles p_Roles);

        /// <summary>
        ///  Eliminar un Rol de la base datos
        /// </summary>
        /// <param name="p_Rol_ID"> llave primaria de roles</param>
        /// <returns>
        ///  mensaje que indica si se realizo con exito o no el proceso
        /// </returns>
        string Eliminar_Rol(int p_Rol_ID);

        string Es_Rol_Ejecutivo(int Rol_ID);

    }
}
