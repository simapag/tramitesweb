﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;

namespace ModelDomain.Interfaces
{
   public interface IAsociarRPU_Repo
    {

        List< DatosCuentaView> Obtener_Datos_Cuenta(string RPU);
        Cls_Paginado<RPUAsociadosView> Obtener_RPU_Asociados(int offset, int limit, int Usuarios_ID);
        List<RPUAsociadosView> Obtener_RPU_Asociados(int Usuario_ID);
        string Eliminar_RPU_Asociados(int RPU_Asociados_ID);
        string Esta_Repetido_RPU(string RPU, int Usuarios_ID);
        void Guardar_RPU_Asociados(RPUAsociadosView rPUAsociadosView);
        List<ComboView> Obtener_RPU_Asociados_Combo(int Usuarios_ID);
    }
}
