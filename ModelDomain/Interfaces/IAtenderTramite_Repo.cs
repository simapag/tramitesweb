﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;
using ModelDomain.Contexto;

namespace ModelDomain.Interfaces
{
   public interface IAtenderTramite_Repo
    {
        Cls_Paginado<TramitesSolicitudView> Obtener_Solicitudes(int offset, int limit, int tramite_id, string search , string estatus, int Usuarios_Asignado_ID);
        List<ComboView> Obtener_Tramites_Asignados(int Usuarios_Asignado_ID);

        List<Pagos_Solicitudes_View> Obtener_Datos_Codigo_Barras(string Codigo_Barras);
        List<Pagos_Solicitudes_Detalle_View> Obtener_Datos_Codigo_Barras_Detalle(string No_Diverso);
        List<Pagos_Solicitudes_View> Obtener_Un_Diverso(string No_Diverso);


        string Esta_Repetido_Codigo_Barras(string Codigo_Barras);
        void Agregar_Pagos(EFDbContext mContexto, int Tramite_Solicitados_ID, Pagos_Solicitudes_View pagos_Solicitudes_View, List<Pagos_Solicitudes_Detalle_View> Lista_Pagos_Detalle);


        Cls_Paginado<Pagos_Solicitudes_View> Obtener_Pagos_Registrados(int offset, int limit, int tramite_solicitados_id, string search);
        Cls_Paginado<Pagos_Solicitudes_Detalle_View> Obtener_Detalla_Pago(int offset, int limit, int pago_solicitudes_id);


       List<Pagos_Solicitudes_View> Obtener_Pagos_Registrados(int Tramite_Solicitados_ID);
       List<Pagos_Solicitudes_Detalle_View> Obtener_Pagos_Registrados_Detalle(int Pago_Solicitudes_ID);

        Pagos_Solicitudes_View Obtener_Un_Pago(int Pago_Solicitudes_ID);
        void Cambiar_Estatus_Pago(string Nuevo_Estatus, string Usuario_Modifico , int Pago_Solicitudes_ID);
        void Cambiar_Estatus_Pago(EFDbContext mContexto, string Nuevo_Estatus, string Usuario_Modifico, int Pago_Solicitudes_ID);

    }
}
