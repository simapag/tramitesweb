﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;

namespace ModelDomain.Interfaces
{
   public interface ITramites_Repo
    {
        Cls_Paginado<TramitesView> Obtener_Tramites(int offset, int limit, string search);
        string Esta_Repetido_Tramite(string Nombre_Tramite, int Tramite_ID);
        string Eliminar_Tramite(TramitesView tramitesView);
        void Guardar_Tramite(TramitesView tramitesView);
        List<ComboView> Obtener_Tramites_Combo();
        List<ComboView> Obtener_Tramites_Combo_Especial();
        decimal Obtener_Costos_Tramite(int Tramite_ID);
    }
}
