﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;


namespace ModelDomain.Interfaces
{
   public interface IMenus_Repositorio
    {

        /// <summary>
        ///  Obtiene la colección de acceso
        /// </summary>
        IQueryable<Acceso> Acceso { get; }

        /// <summary>
        /// Obtiene la colección de menú
        /// </summary>
        IQueryable<Menu> Menus { get; }

        /// <summary>
        /// Guarda un menu en la base datos
        /// </summary>
        /// <param name="p_Menus">Entidad Menu</param>
        void Guarda_Menu(Menu p_Menus);

        /// <summary>
        /// Elimina un Menú de la base de datos
        /// </summary>
        /// <param name="p_Menu_ID"Id del menu</param>
        /// <returns> regresa mensaje si el proceso fue exitoso</returns>
        string Eliminar_Menu(int p_Menu_ID);
    }
}
