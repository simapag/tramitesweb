﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.ViewModel;
using ModelDomain.Dto;
using ModelDomain.Contexto;


namespace ModelDomain.Interfaces
{
   public interface IMensajesSolicitudes_Repo
    {

        void Guardar_Mensaje(MensajesSolicitudesView mensajesSolicitudesView);
        void Guardar_Mensaje(EFDbContext mContexto, MensajesSolicitudesView mensajesSolicitudesView);
        List<MensajesSolicitudesView> Obtener_Mensajes(int Tramite_Solicitados_ID);

        List<MensajesSolicitudesView> Obtener_Mensajes_Usuario(int Usuarios_ID);
        List<MensajesSolicitudesView> Obtener_Mensaje_Ejecutivo(int Usuarios_ID);

        void Actualizar_Mensaje_Usuario(int Usuario_ID);
        void Actualizar_Mensaje_Ejecutivo(int Usuario_ID);

        int Obtener_Numero_Mensajes(int Usuarios_ID);
        int Obtener_Numero_Mensaje_Ejecutivo(int Usuarios_ID);

        IQueryable<ReporteMensajesView> Obtener_Reporte_Mensaje(string sortOrder, string FechaInicio, string FechaFin, int Usuario_Asignado_ID);
    }
}
