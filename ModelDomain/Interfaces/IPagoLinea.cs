﻿using ModelDomain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelDomain.Interfaces
{
   public interface IPagoLinea
    {

        List<ConsultaSaldoView> Obtener_Saldo_Consulta(string RPU);
        List<App_ConfiguracionView> Obtener_Configuracion();

        string Obtener_NoFactura_Actual(string RPU);
        string Obtener_Predio_ID(string RPU);
        decimal Obtener_Total_Pagar(string Predio_ID);
        decimal Obtener_Total_Pagar_Empleado(string RPU);
        decimal Obtener_total_Pagar_Tiene_Beneficio(string RPU);
        decimal Obtener_Total_Descuento_Empleado(string RPU);
        decimal Obtener_Total_Descuento_Beneficio(string RPU);
        decimal Obtener_Saldo_Convenio_Total(string RPU);
        DatosPagoView Obtener_Datos_Pagos(string No_Factura_Recibo);
        Form_Pago_Multipagos Obtener_Parametros_Pago(decimal Total_Pagar, string No_Factura_Recibo, string RPU ,string Nombre_Usuario);

    }
}
