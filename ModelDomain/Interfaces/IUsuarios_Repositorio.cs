﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelDomain.Entidades;
using ModelDomain.ViewModel;

namespace ModelDomain.Interfaces
{
   public interface IUsuarios_Repositorio
    {
        /// <summary>
        /// Obtiene la colecccion de los datos de usuario
        /// </summary>
        IQueryable<Usuarios> Usuarios { get; }

        /// <summary>
        /// guarda un elemento la base de datos
        /// </summary>
        /// <param name="p_Usuarios"></param>
        void Guardar_Usuarios(Usuarios p_Usuarios);
        /// <summary>
        /// Elemina un usuario del base datos
        /// </summary>
        /// <param name="p_Usuario_ID">llave primaria de la tabla usuario</param>
        /// <returns>
        ///  mensaje que indica si se realizo con exito o no el proceso
        /// </returns>
        string Eliminar_Usuario(int p_Usuario_ID);


        void Registrar_Usuario_Externo(RegistroUsuarioViewModel registroUsuarioViewModel);
        string Esta_Repetido_Correo(string Correo_Electronico);
        string Esta_Registrado_Rol_Usuario_Externo();

        List<UsuarioView> Obtener_Usuarios_Ejecutivos();
        UsuarioView Buscar_Usuario_Por_Correo(string Correo_Electronico);
        UsuarioView Buscar_Usuario(int Usuarios_ID);

    }
}
